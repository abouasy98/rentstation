import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/models/get/adCommentModel.dart';

class ReplyCard extends StatelessWidget {
  final String name;
  final String img;
  final String comment;
  final int index;
  final int idUser;
  final int idComment;
  final int idAd;
  final List<Replay> replays;
  final String data;

  ReplyCard(
      {this.name,
      this.img,
      this.comment,
      this.index,

      this.replays,
      this.idAd,
      this.idComment,
      this.data,
      this.idUser});

  @override
  Widget build(BuildContext context) {
    print('CommentId=$idComment');
    return Container(
      
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(5),
        child: ListView(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width / 1.5),
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(data),
                            Text(
                              name,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        Wrap(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 18.0),
                              child: Text(
                                comment,
                                textAlign: TextAlign.right,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 10),
                Container(
                  height: 30,
                  width: 30,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: CachedNetworkImage(
                      imageUrl: img == null ? "" : img,
                      errorWidget: (context, url, error) => ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child:
                              Image.asset('images/16.jpg', fit: BoxFit.fill)),
                      fadeInDuration: Duration(seconds: 2),
                      placeholder: (context, url) => ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child:
                              Image.asset('images/16.jpg', fit: BoxFit.fill)),
                      imageBuilder: (context, provider) {
                        return ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: Image(
                              image: provider,
                              fit: BoxFit.cover,
                            ));
                      },
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Divider(
                height: 1,
              ),
            )
          ],
        ),
      ),
    );
  }
}
