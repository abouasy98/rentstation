import 'package:flutter/material.dart';
import 'package:rentstation/models/get/conversationMessage.dart';

import 'package:shared_preferences/shared_preferences.dart';
import './message_Bubble.dart';
class Message extends StatelessWidget {
  SharedPreferences prefs;
  String chatId;
  final String imageUrl;
  Stream stream;
  ScrollController _listScrollController = ScrollController();
  String idTo;
  ConversationsMessages chatModel;
  Message({this.chatId, this.chatModel, this.stream, this.idTo,this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      controller: _listScrollController,
      reverse: true,
      itemCount: chatModel.data.length,
      itemBuilder: (ctx, i) {
        return MessageBubble(
          imageUrl:imageUrl,
          chatId: chatId,
        
          idTo: idTo,
        );
      },
    );
  }
}
