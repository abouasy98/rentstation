import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class MessageBubble extends StatefulWidget {
  final String message;
  final bool isMe;

  final String imageUrl;

  final Key key;

  String idTo;

  String chatId;

  MessageBubble({
    this.message,
    this.idTo,
    this.imageUrl,
    this.isMe,
    this.chatId,
    this.key,
  });

  @override
  _MessageBubbleState createState() => _MessageBubbleState();
}

class _MessageBubbleState extends State<MessageBubble> {
  var isHeart;

  double playerSeeker = 0;
  Duration playerDuration;
  bool isPlaying = false;
  int maxDuration = 0;

  @override
  Widget build(BuildContext context) {
    return Row(
        crossAxisAlignment:
            (!widget.isMe) ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment:
                  widget.isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
              children: <Widget>[
                if (!widget.isMe)
                  SafeArea(
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: 10,
                      ),
                    ),
                  ),
                Flexible(
                  child: Container(
                    decoration: widget.message == '👍'
                        ? BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(35),
                            ),
                            border: Border.all(color: Colors.grey[300]),
                          )
                        : BoxDecoration(
                            color: widget.isMe ? Colors.blue : Colors.grey[300],
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(35),
                                topRight: Radius.circular(35),
                                bottomLeft: !widget.isMe
                                    ? Radius.circular(0)
                                    : Radius.circular(35),
                                bottomRight: widget.isMe
                                    ? Radius.circular(0)
                                    : Radius.circular(35)),
                          ),
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
                    margin: EdgeInsets.symmetric(vertical: 16, horizontal: 8),
                  ),
                ),
                if (!widget.isMe)
                  Container(
                    margin: EdgeInsets.all(8),
                    child: CircleAvatar(
                      radius: 10,
                      backgroundImage:
                          CachedNetworkImageProvider(widget.imageUrl),
                    ),
                  ),
              ],
            ),
          ),
        ]);
  }
}
