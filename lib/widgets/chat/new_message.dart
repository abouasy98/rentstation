import 'package:flutter/material.dart';
import 'package:rentstation/provider/post/sendMessageProvider.dart';

import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:emoji_picker/emoji_picker.dart';
import 'package:path_provider/path_provider.dart' as sysPath;

class new_message extends StatefulWidget {
  int idTo;
  SharedPreferences prefs;
  new_message({this.prefs, this.idTo});
  @override
  _new_messageState createState() => _new_messageState();
}

class _new_messageState extends State<new_message> {
  String timeofNow = DateTime.now().millisecondsSinceEpoch.toString();
  ImageSource camera = ImageSource.camera;
  ImageSource gallery = ImageSource.gallery;
  dynamic photo;
  FocusNode textFieldFoucs = FocusNode();

  bool showEmojiPicker = false;
  bool isWriting = false;

  final _controller = TextEditingController();

  showKeyboard() => textFieldFoucs.requestFocus();
  hideKeyboard() => textFieldFoucs.unfocus();
  hideEmojiContainer() {
    setState(() {
      showEmojiPicker = false;
    });
  }

  showEmojiContainer() {
    setState(() {
      showEmojiPicker = true;
    });
  }

  // emojiContainer() {
  //   return EmojiPicker(
  //     bgColor: Colors.white54,
  //     indicatorColor: UniversalVariables.blackColor,
  //     rows: 3,
  //     columns: 7,
  //     onEmojiSelected: (emoji, category) {
  //       setState(() {
  //         isWriting = true;
  //       });
  //       _controller.text = _controller.text + emoji.emoji;
  //       enterdMessage = _controller.text;
  //     },
  //     recommendKeywords: ["face", "happy", "party", "sad"],
  //     numRecommended: 50,
  //   );
  // }

  // int numLines = 0;
  var enterdMessage = '';
  @override
  void initState() {
    Future.delayed(Duration(seconds: 2), () async {
      var path = await sysPath.getApplicationDocumentsDirectory();
      var file = File('${path.path}.m4a');
      print(file.path);

      var isExists = await file.exists();
      if (isExists) {
        print('exists');
        file.delete();
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    setWritingTo(bool val) {
      setState(() {
        isWriting = val;
      });
    }

    return Column(children: [
      Card(
        elevation: 1,
        margin: EdgeInsets.only(top: 1),
        child: Container(
          width: double.infinity,
          height: null,
          margin: EdgeInsets.only(top: 9.0),
          padding: const EdgeInsets.only(top: 6, bottom: 5),
          child: Row(children: <Widget>[
            if (!isWriting)
              IconButton(
                  icon: Icon(
                    Icons.my_location,
                    color: Colors.blue,
                  ),
                  onPressed: () {}),
            Flexible(
              child: Container(
                constraints: _controller.text.length > 50
                    ? BoxConstraints.expand(height: 90)
                    : BoxConstraints.expand(height: 35),
                // height: 35,
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    border: Border.all(
                      color: Colors.grey,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(32)),
                child: Stack(alignment: Alignment.centerRight, children: [
                  TextFormField(
                    onTap: () => hideEmojiContainer(),
                    minLines: 1,
                    focusNode: textFieldFoucs,
                    maxLines: 6,
                    keyboardType: TextInputType.multiline,
                    cursorColor: Colors.blue,
                    autocorrect: false,
                    textCapitalization: TextCapitalization.sentences,
                    enableSuggestions: true,
                    controller: _controller,
                    autofocus: false,
                    decoration: InputDecoration(
                      hintStyle: TextStyle(color: Colors.grey),
                      hintText: 'Aa',
                      contentPadding: EdgeInsets.zero,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(32.0),
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                        gapPadding: 2,
                        borderSide:
                            BorderSide(color: Colors.grey[300], width: 1.0),
                        borderRadius: BorderRadius.all(
                          Radius.circular(32.0),
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        gapPadding: 1,
                        borderSide:
                            BorderSide(color: Colors.grey[300], width: 2.0),
                        borderRadius: BorderRadius.all(
                          Radius.circular(32.0),
                        ),
                      ),
                    ),
                    onChanged: (value) {
                      (value.length > 0 && value.trim() != "")
                          ? setWritingTo(true)
                          : setWritingTo(false);
                      setState(() {
                        enterdMessage = value;
                      });
                    },
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.face,
                      color: Colors.blueAccent,
                    ),
                    onPressed: () {
                      if (!showEmojiPicker) {
                        showEmojiContainer();
                        hideKeyboard();
                      } else {
                        showKeyboard();
                        hideEmojiContainer();
                      }
                    },
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                  ),
                ]),
              ),
            ),
            IconButton(
                icon: Icon(
                  Icons.send,
                  color: Colors.blue,
                ),
                onPressed: () async {
                  await Provider.of<ChatsendProvider>(context, listen: false)
                      .sendMessage(widget.idTo, enterdMessage,
                          widget.prefs.getString('api_token'),context);
                          _controller.clear();
                })
          ]),
        ),
      ),
      showEmojiPicker
          ? Container(
            //  child: emojiContainer(),
            )
          : Container(),
    ]);
  }
}
