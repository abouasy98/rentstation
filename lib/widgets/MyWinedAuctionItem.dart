import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/models/get/myWinedAuction.dart';
import 'package:rentstation/screens/AuctionDetailsScreen.dart';
import 'package:intl/intl.dart';

import 'package:shared_preferences/shared_preferences.dart';

class MyWinedAuctionItem extends StatelessWidget {
  final String title;
  final String photo;
  final int id;
  final SharedPreferences prefs;
  final List<Price> prices;
  final int period;
  final String currency;
  final DateTime createdAt;
  final String winnerNamePhoto;
  final int heighPrice;
  final String auctionWinner;
  final String auctionOwner;
  MyWinedAuctionItem({
  this.title,
   this.createdAt,
  this.id,
  this.photo,
 this.winnerNamePhoto,
   this.heighPrice,
  this.prefs,
  this.auctionOwner,
 this.period,
  this.currency,
  this.auctionWinner,
 this.prices,
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => AuctionsDetails(
                  id: id,
                )));
      },
      child: Container(
          height: MediaQuery.of(context).size.height * 0.17,
          width: MediaQuery.of(context).size.width,
          child: Card(
            elevation: 10,
            clipBehavior: Clip.antiAlias,
            child: Container(
              height: 120,
              padding: const EdgeInsets.all(0),
              child: Row(children: [
                Expanded(
                  flex: 6,
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: CachedNetworkImageProvider(
                            photo,
                          ),
                          fit: BoxFit.fill),
                    ),
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
                Expanded(
                  flex: 14,
                  child: Container(
                    padding: const EdgeInsets.only(top: 5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(title,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.bold)),
                        if (auctionWinner == null)
                          Row(
                            children: <Widget>[
                              Text(
                                '${localization.text('Current_Price')} : ',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15),
                              ),
                              Text(
                                '${NumberFormat.currency(locale:localization.currentLanguage.toString() == "en"?null:'ar_EG',name: currency, decimalDigits: 0).format(heighPrice)}',
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                        if (auctionWinner != null)
                          Row(
                            children: [
                              winnerNamePhoto != null
                                  ? CachedNetworkImage(
                                      imageUrl: winnerNamePhoto,
                                      fadeInDuration: Duration(seconds: 2),
                                      placeholder: (context, url) =>
                                          CircleAvatar(
                                              radius: 12,
                                              backgroundImage:
                                                  AssetImage('images/16.jpg')),
                                      imageBuilder: (context, provider) {
                                        return CircleAvatar(
                                            radius: 12,
                                            backgroundImage: provider);
                                      },
                                    )
                                  : CircleAvatar(
                                      radius: 12,
                                      backgroundImage:
                                          AssetImage('images/16.jpg')),
                              SizedBox(
                                width: 5,
                              ),
                              Container(
                                alignment:
                                    localization.currentLanguage.toString() ==
                                            "en"
                                        ? Alignment.bottomLeft
                                        : Alignment.bottomRight,
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Text(auctionOwner,
                                    overflow: TextOverflow.ellipsis,
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                              ),
                            ],
                          ),
                        if (auctionWinner != null)
                          Row(
                            children: <Widget>[
                              Text(
                                '${localization.text('Current_Price')} : ',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15),
                              ),
                              Text(
                                '${NumberFormat.currency(locale:localization.currentLanguage.toString() == "en"?null:'ar_EG',name: currency, decimalDigits: 0).format(heighPrice)}',
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                      ],
                    ),
                  ),
                ),
              ]),
            ),
          )),
    );
  }
}
