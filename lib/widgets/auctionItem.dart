import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/provider/post/addAuctiontoArchieveProvider.dart';
import 'package:rentstation/screens/AuctionDetailsScreen.dart';
import 'package:rentstation/screens/userProfile.dart';
import 'package:rentstation/widgets/weekcountdawn.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';

class AuctionItem extends StatelessWidget {
  final String netWorkImage;
  final String city;
  final String username;
  final String title;
  final String userPhoto;
  final int pricesIncreases;
  final DateTime createdAt;
  final String currency;
  final int period;
  final int id;
  final int heighPrice;

  final int userId;
  final int initialPrice;
  AuctionItem(
      {this.netWorkImage,
      this.pricesIncreases,
      this.title,
      this.userPhoto,
      this.username,
      this.initialPrice,
      this.heighPrice,
      this.id,
      this.userId,
      this.currency,
      this.city,
      this.period,
      this.createdAt});
 // CustomDialog _dialog = CustomDialog();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        pushNewScreen(
          context,
          screen: AuctionsDetails(
            id: id,
          ),
          
          withNavBar: false, // OPTIONAL VALUE. True by default.
          pageTransitionAnimation: PageTransitionAnimation.cupertino,
        );
        // Navigator.of(context).push(MaterialPageRoute(
        //     builder: (context) => AuctionsDetails(
        //           id: id,
        //         )));
      },
      child: Container(
          height: MediaQuery.of(context).size.height * 0.2,
          width: MediaQuery.of(context).size.width,
          child: Card(
            elevation: 10,
            clipBehavior: Clip.antiAlias,
            child: Container(
              padding: const EdgeInsets.all(0),
              child: Row(children: [
                Expanded(
                  flex: 6,
                  child: Stack(children: [
                    netWorkImage != null
                        ? CachedNetworkImage(
                            imageUrl: netWorkImage,
                            fadeInDuration: Duration(seconds: 2),
                            placeholder: (context, url) => Container(
                                decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('images/clock-loading.gif'),
                                  fit: BoxFit.fill),
                            )),
                            imageBuilder: (context, provider) {
                              return Container(
                                  decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: provider, fit: BoxFit.fill),
                              ));
                            },
                          )
                        : Container(
                            decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('images/clock-loading.gif'),
                                fit: BoxFit.fill),
                          )),
                    if (userId ==
                        Provider.of<SharedPref>(context, listen: false).id)
                      Positioned(
                          child: IconButton(
                        icon: Icon(Icons.archive),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (ctx) => AlertDialog(
                              title: Text(
                                localization.text('Are_you_sure'),
                                textDirection: TextDirection.rtl,
                              ),
                              content: Text(
                                localization.text(
                                    'Are_you_sure_to_archive_this_auction?'),
                                textDirection: TextDirection.rtl,
                              ),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text(localization.text('no')),
                                  onPressed: () {
                                    Navigator.of(ctx).pop(false);
                                  },
                                ),
                                FlatButton(
                                  child: Text(localization.text('ok')),
                                  onPressed: () {
                                    Provider.of<AddAuctionToArchProvider>(
                                            context,
                                            listen: false)
                                        .addAuctionToarch(
                                            Provider.of<SharedPref>(context,
                                                    listen: false)
                                                .token,
                                            id,
                                            context);
                                    Navigator.of(ctx).pop(true);
                                  },
                                ),
                              ],
                            ),
                          );
                        },
                        color: Colors.lightBlue[400],
                      ))
                  ]),
                ),
                Spacer(
                  flex: 1,
                ),
                Expanded(
                  flex: 18,
                  child: Container(
                    padding: const EdgeInsets.only(top: 5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.05,
                          child: Text(title,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 20.0, fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.01,
                        ),
                        Expanded(
                          child: Container(
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    alignment: localization.currentLanguage
                                                .toString() ==
                                            "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                    width: MediaQuery.of(context).size.width *
                                        0.42,
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.18,
                                                child: FittedBox(
                                                  child: Text(
                                                    '${localization.text('Current_Price')}:',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 13),
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.2,
                                                alignment: localization
                                                            .currentLanguage
                                                            .toString() ==
                                                        "en"
                                                    ? Alignment.centerLeft
                                                    : Alignment.centerRight,
                                                child: FittedBox(
                                                  child: Text(
                                                    heighPrice == null
                                                        ? '${intl.NumberFormat.currency(locale: localization.currentLanguage.toString() == "en"?null:'ar_EG',name: currency, decimalDigits: 0).format(initialPrice)}'
                                                        : '${intl.NumberFormat.currency(locale: localization.currentLanguage.toString() == "en"?null:'ar_EG',name: currency, decimalDigits: 0).format(heighPrice)}',
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style:
                                                        TextStyle(fontSize: 15),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Expanded(child: Text('')),
                                          Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.22,
                                                  child: FittedBox(
                                                    child: Text(
                                                      '${localization.text('Min_Increment')}:',
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 13),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.19,
                                                  alignment: localization
                                                              .currentLanguage
                                                              .toString() ==
                                                          "en"
                                                      ? Alignment.centerLeft
                                                      : Alignment.centerRight,
                                                  child: FittedBox(
                                                    child: Text(
                                                      '${intl.NumberFormat.currency(locale:localization.currentLanguage.toString() == "en"?null:'ar_EG',name: currency, decimalDigits: 0).format(pricesIncreases)}',
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                          fontSize: 15),
                                                    ),
                                                  ),
                                                ),
                                              ]),
                                        ]),
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          if (Provider.of<SharedPref>(context,
                                                      listen: false)
                                                  .token ==
                                              null) {
                                            // _dialog.showOptionDialog(
                                            //     context: context,
                                            //     msg: localization.text('auth'),
                                            //     okFun: () {
                                            //       pushNewScreen(
                                            //         context,
                                            //         screen: SignInScreen(),
                                            //         withNavBar:
                                            //             false, // OPTIONAL VALUE. True by default.
                                            //         pageTransitionAnimation:
                                            //             PageTransitionAnimation
                                            //                 .cupertino,
                                            //       );
                                            //       // Navigator.push(
                                            //       //     context,
                                            //       //     MaterialPageRoute(
                                            //       //         builder: (c) =>
                                            //       //             SignInScreen()));
                                            //     },
                                            //     okMsg: localization.text('yes'),
                                            //     cancelMsg:
                                            //         localization.text('no'),
                                            //     cancelFun: () {
                                            //       return;
                                            //     });
                                          } else {
                                            pushNewScreen(
                                              context,
                                              screen: UserProfile(
                                                userid: userId,
                                              ),
                                              withNavBar:
                                                  false, // OPTIONAL VALUE. True by default.
                                              pageTransitionAnimation:
                                                  PageTransitionAnimation
                                                      .cupertino,
                                            );
                                            // Navigator.of(context).push(
                                            //     MaterialPageRoute(
                                            //         builder: (context) =>
                                            //             UserProfile(
                                            //               userid: userId,
                                            //             )));
                                          }
                                        },
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.21,
                                              alignment: localization
                                                          .currentLanguage
                                                          .toString() ==
                                                      "en"
                                                  ? Alignment.centerRight
                                                  : Alignment.centerLeft,
                                              child: Text(
                                                username,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(fontSize: 13),
                                              ),
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.01,
                                            ),
                                            userPhoto != null
                                                ? CachedNetworkImage(
                                                    imageUrl: userPhoto,
                                                    fadeInDuration:
                                                        Duration(seconds: 2),
                                                    placeholder: (context,
                                                            url) =>
                                                        CircleAvatar(
                                                            radius: 10,
                                                            backgroundImage:
                                                                AssetImage(
                                                                    'images/16.jpg')),
                                                    imageBuilder:
                                                        (context, provider) {
                                                      return CircleAvatar(
                                                          radius: 10,
                                                          backgroundImage:
                                                              provider);
                                                    },
                                                  )
                                                : CircleAvatar(
                                                    radius: 10,
                                                    backgroundImage: AssetImage(
                                                        'images/16.jpg')),
                                          ],
                                        ),
                                      ),
                                      Expanded(child: Text('')),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.19,
                                            alignment: localization
                                                        .currentLanguage
                                                        .toString() ==
                                                    "en"
                                                ? Alignment.bottomRight
                                                : Alignment.bottomLeft,
                                            child: Text(
                                              city,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          Icon(
                                            Icons.location_on,
                                            size: 17,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ]),
                          ),
                        ),
                        Container(
                            child: WeekCountdown(
                                createdAt: createdAt, period: period),
                            decoration: BoxDecoration(color: Colors.red)),
                      ],
                    ),
                  ),
                ),
              ]),
            ),
          )),
    );
  }
}
