import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/post/getAdsFilterByCity.dart';

import 'package:rentstation/widgets/aditem.dart';
import 'package:rentstation/models/post/getAdsFilterByCity.dart' as a7a;
import 'adItemGridView.dart';

class SearchAdsIteams extends StatelessWidget {
  List<a7a.Datum> ads;
  final Key key;
  GetAddsByCityFilter getAddsByFilterModel;
  final String query;
  bool gridView;
  SearchAdsIteams(
      {this.ads,
      this.query,
      this.key,
      this.gridView,
      this.getAddsByFilterModel});

  @override
  Widget build(BuildContext context) {
    ads = getAddsByFilterModel.data
        .where((element) =>
            element.countryId ==
            Provider.of<SharedPref>(context, listen: false).countryId)
        .toList();
    final List<a7a.Datum> suggteionList = query.isEmpty || query == ''
        ? []
        : ads != null
            ? ads.where((a7a.Datum user) {
                String getUserName = user.user.toLowerCase();
                String _query = query.toLowerCase();
                String getTitle = user.title.toLowerCase();
                bool matchUserName = getUserName.contains(_query);
                bool matchtitle = getTitle.contains(_query);
                return (matchUserName || matchtitle);
              }).toList()
            : [];

    return gridView
        ? suggteionList.length<=0? Container(
                      height: MediaQuery.of(context).size.height * 0.6,
                      child: Center(
                        child: Text(
                          localization.text('no_results'),
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    )
                  :GridView.builder(
            primary: false,
            shrinkWrap: true,
            itemCount: suggteionList.length,
            itemBuilder: ((context, i) {
              a7a.Datum searchedUser = a7a.Datum(
                email: suggteionList[i].email,
                title: suggteionList[i].title,
                photos: suggteionList[i].photos,
                userPhoto: suggteionList[i].userPhoto,
                userId: suggteionList[i].userId,
                price: suggteionList[i].price,
                id: suggteionList[i].id,
                currency: suggteionList[i].currency,
                description: suggteionList[i].description,
                user: suggteionList[i].user,
                priceStatus: suggteionList[i].priceStatus,
                phoneNumber: suggteionList[i].phoneNumber,
                createdAt: suggteionList[i].createdAt,
                city: suggteionList[i].city,
                categoryId: suggteionList[i].categoryId,
              );
              return  AdItem(
                      photo: searchedUser.photos.length == 0
                          ? ""
                          : searchedUser.photos[0].photo,
                      image: searchedUser.photos,
                      price: searchedUser.price,
                      sale: searchedUser.priceStatus,
                      country: searchedUser.country,
                      createdAt: searchedUser.createdAt,
                      title: searchedUser.title,
                      desc: searchedUser.description,
                      id: searchedUser.id,
                      favouritecheck: searchedUser.favorite,
                      pinned: searchedUser.pinned,
                      key: key,
                      countryId: searchedUser.countryId,
                      product: searchedUser,
                      userId: searchedUser.userId,
                      city: searchedUser.city,
                      userPhoto: searchedUser.userPhoto,
                      mobileNumber: searchedUser.phoneNumber,
                      currency: searchedUser.currency,
                      userName: searchedUser.user,
                      date: searchedUser.createdAt,
                    );
            }),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 0.7,
                crossAxisCount: 2,
                crossAxisSpacing: 5,
                mainAxisSpacing: 5))
        :suggteionList.length<=0? Container(
                      height: MediaQuery.of(context).size.height * 0.6,
                      child: Center(
                        child: Text(
                          localization.text('no_results'),
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    )
                  : ListView.builder(
            primary: false,
            shrinkWrap: true,
            itemCount: suggteionList.length,
            itemBuilder: ((context, i) {
              a7a.Datum searchedUser = a7a.Datum(
                email: suggteionList[i].email,
                title: suggteionList[i].title,
                photos: suggteionList[i].photos,
                userPhoto: suggteionList[i].userPhoto,
                userId: suggteionList[i].userId,
                price: suggteionList[i].price,
                id: suggteionList[i].id,
                currency: suggteionList[i].currency,
                description: suggteionList[i].description,
                user: suggteionList[i].user,
                priceStatus: suggteionList[i].priceStatus,
                phoneNumber: suggteionList[i].phoneNumber,
                createdAt: suggteionList[i].createdAt,
                city: suggteionList[i].city,
                categoryId: suggteionList[i].categoryId,
              );
              return ((searchedUser == null) ||
                      (suggteionList.length <= 0) ||
                      (suggteionList == null))
                  ? Container(
                      height: MediaQuery.of(context).size.height * 0.6,
                      child: Center(
                        child: Text(
                          localization.text('no_results'),
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    )
                  : AdItemGridView(
                      photo: searchedUser.photos.length == 0
                          ? ""
                          : searchedUser.photos[0].photo,
                      image: searchedUser.photos,
                      price: searchedUser.price,
                      sale: searchedUser.priceStatus,
                      country: searchedUser.country,
                      createdAt: searchedUser.createdAt,
                      title: searchedUser.title,
                      desc: searchedUser.description,
                      id: searchedUser.id,
                      favouritecheck: searchedUser.favorite,
                      pinned: searchedUser.pinned,
                      key: key,
                      category: searchedUser.category,
                      countryId: searchedUser.countryId,
                      product: searchedUser,
                      categoryId: searchedUser.categoryId,
                      userId: searchedUser.userId,
                      city: searchedUser.city,
                      userPhoto: searchedUser.userPhoto,
                      mobileNumber: searchedUser.phoneNumber,
                      currency: searchedUser.currency,
                      userName: searchedUser.user,
                      date: searchedUser.createdAt,
                    );
            }),
          );
  }
}
