
import 'package:flutter/material.dart';

import 'package:rentstation/Repository/appLocalization.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class MultiImagePickerBtn extends StatefulWidget {
  final ValueChanged<List<Asset>> onGetImages;

  const MultiImagePickerBtn({Key key, this.onGetImages}) : super(key: key);
  @override
  _MultiImagePickerBtnState createState() => _MultiImagePickerBtnState();
}

class _MultiImagePickerBtnState extends State<MultiImagePickerBtn> {
  List<Asset> files = [];
 
  _getImg(ValueChanged<List<Asset>> onGet) async {
    List<Asset> resultList =
        await MultiImagePicker.pickImages(maxImages: 5, enableCamera: false);
    setState(() {
      for (int i = 0; i < resultList.length; i++) {
        files.add(resultList[i]);
      }
      onGet(files);
    });
  }

  Widget _oneImgFile(Asset img, int index) {
    return Padding(
      padding: const EdgeInsets.only(right: 10, left: 10),
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 10, right: 10),
            child: Align(
              alignment: Alignment.centerRight,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: AssetThumb(
                    asset: img,
                    width: 300,
                    height: 300,
                  )),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            child: InkWell(
              onTap: () {
                setState(() {
                  files.removeAt(index);
                });
              },
              child: Material(
                color: Colors.white,
                elevation: 2,
                shape: CircleBorder(),
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: Icon(Icons.close, size: 18),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 10, left: 10),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          InkWell(
            onTap: () => _getImg(widget.onGetImages),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Color.fromRGBO(220, 220, 220, 1))),
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.camera_alt),
                    Text(
                      localization.text("photos"),
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 10),
          (files.length < 1) == true
              ? Container()
              : Directionality(
                  textDirection: TextDirection.rtl,
                  child: Container(
                    height: 120,
                    child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: files.length,
                      itemBuilder: (context, index) {
                        return _oneImgFile(files[index], index);
                      },
                    ),
                  ),
                )
        ],
      ),
    );
  }
}