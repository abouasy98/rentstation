import 'package:flutter/material.dart';

import 'package:fluttericon/font_awesome_icons.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';

import 'package:rentstation/models/post/getAuctionsByFilter.dart' as a8a;

import 'package:rentstation/widgets/auctionItem.dart';
import 'package:flutter/rendering.dart';
import 'package:rentstation/widgets/searchauction.dart';
import 'package:provider/provider.dart';
import 'package:rentstation/models/get/getCitiesModel.dart' as ads;

class AuctionsBody extends StatefulWidget {
  String query = '';
  String subCategory;
  int categoryId;
  String category;
  int subCategoryId;
  final Key key;
//  final GetAuctionsModel getAuctionssModel;
  final a8a.GetAuctionsByCityFilter getAuctionsByCityFilter;
  final ads.GetCities getCitiesByCountryIdModel;
  AuctionsBody(
      {this.getAuctionsByCityFilter,
      this.getCitiesByCountryIdModel,
      this.categoryId,
      this.key,
      this.subCategory,
      this.subCategoryId,
      this.query,
      this.category});
  @override
  _AuctionsBodyState createState() => _AuctionsBodyState();
}

class _AuctionsBodyState extends State<AuctionsBody> {
  bool a7a = true;

  bool myAuctionsonly = false;
  String _selected = localization.text('modern_to_old');
  List<a8a.Datum> productsAuctions;

  List _items = [
    localization.text('end_time'),
    localization.text('price_low_to_high'),
    localization.text('price_high_to_low'),
    localization.text('old_to_modern'),
    localization.text('modern_to_old'),
  ];
  void sort() {
    if (_selected == localization.text('old_to_modern')) {
      productsAuctions.sort(
        (family1, family2) => family1.createdAt.compareTo(
          family2.createdAt,
        ),
      );
    }
    if (_selected == localization.text('modern_to_old')) {
      productsAuctions.sort(
        (family1, family2) => family2.createdAt.compareTo(
          family1.createdAt,
        ),
      );
    } else if (_selected == localization.text('end_time')) {
      productsAuctions.sort(
        (family1, family2) => family1.period.compareTo(
          family2.period,
        ),
      );
    } else if (_selected == localization.text('price_low_to_high')) {
      productsAuctions.sort(
        (family1, family2) => family1.initialPrice.compareTo(
          family2.initialPrice,
        ),
      );
    } else if (_selected == localization.text('price_high_to_low')) {
      productsAuctions.sort(
        (family1, family2) => family2.initialPrice.compareTo(
          family1.initialPrice,
        ),
      );
    }
  }

  mainBottomSheet(BuildContext context) {
    return showModalBottomSheet<dynamic>(
        isScrollControlled: false,
        backgroundColor: Colors.white,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        builder: (BuildContext bc) {
          return Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: ListView(children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      if (widget.getAuctionsByCityFilter.data != null) {
                        setState(() {
                          productsAuctions = widget.getAuctionsByCityFilter.data
                              .where((element) =>
                                  element.countryId ==
                                  Provider.of<SharedPref>(context,
                                          listen: false)
                                      .countryId)
                              .toList();
                        });
                      }
                      Navigator.of(context).pop(widget.category);
                    },
                    child: Text(
                      localization.text('all'),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Divider(
                    height: 1,
                    color: Colors.grey,
                  ),
                  Container(
                    color: Colors.white,
                    child: ListView.builder(
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      itemCount: widget.getCitiesByCountryIdModel.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            ListTile(
                                onTap: () {
                                  print(
                                      'cityid=${widget.getCitiesByCountryIdModel.data[index].id}');
                                  if (widget.getAuctionsByCityFilter.data !=
                                      null) {
                                    setState(() {
                                      productsAuctions = widget
                                          .getAuctionsByCityFilter.data
                                          .where((element) =>
                                              element.countryId ==
                                              Provider.of<SharedPref>(context,
                                                      listen: false)
                                                  .countryId)
                                          .toList();

                                      productsAuctions = productsAuctions
                                          .where((element) =>
                                              element.cityId ==
                                              widget.getCitiesByCountryIdModel
                                                  .data[index].id)
                                          .toList();

                                      myAuctionsonly = true;
                                    });
                                  }
                                  Navigator.of(context).pop(widget
                                      .getCitiesByCountryIdModel
                                      .data[index]
                                      .name);
                                },
                                title: Text(
                                  widget.getCitiesByCountryIdModel.data[index]
                                      .name,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                            Divider(
                              height: 1,
                              color: Colors.grey,
                            )
                          ],
                        );
                      },
                    ),
                  ),
                ]),
              ),
            ],
          );
        });
  }

  sortBottomSheet(BuildContext context) {
    return showModalBottomSheet<dynamic>(
        isScrollControlled: false,
        backgroundColor: Colors.white,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        builder: (BuildContext bc) {
          return Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: ListView(children: <Widget>[
                  Container(
                    color: Colors.white,
                    child: ListView.builder(
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      itemCount: _items.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            ListTile(
                                onTap: () {
                                  setState(() {
                                    _selected = _items[index];
                                  });
                                  sort();
                                  Navigator.of(context).pop(_items[index]);
                                },
                                title: Text(
                                  _items[index],
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                            Divider(
                              height: 1,
                              color: Colors.grey,
                            )
                          ],
                        );
                      },
                    ),
                  ),
                ]),
              ),
            ],
          );
        });
  }

  Widget headerBuild() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, bottom: 10, left: 18, right: 18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.05,
                child: IconButton(
                    iconSize: 25,
                    icon: Icon(
                      FontAwesome.filter,
                      color: Colors.blueAccent,
                    ),
                    onPressed: () {
                      mainBottomSheet(context);
                    }),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.06,
              ),
              // Visibility(
              //   visible:
              //       Provider.of<SharedPref>(context, listen: false).token !=
              //           null,
              //   child: Container(
              //     width: MediaQuery.of(context).size.width * 0.26,
              //     height: MediaQuery.of(context).size.height * 0.05,
              //     decoration: BoxDecoration(
              //       color: Colors.blueAccent,
              //       borderRadius: BorderRadius.circular(20),
              //     ),
              //     child: FlatButton(
              //       child: FittedBox(
              //         child: Text(
              //           Provider.of<SharedPref>(context, listen: false).token ==
              //                   null
              //               ? localization.text('all_ads')
              //               : myAuctionsonly
              //                   ? localization.text('all')
              //                   : localization.text('my_auctions'),
              //           style: TextStyle(
              //               color: Colors.white,
              //               fontSize: 12,
              //               fontWeight: FontWeight.bold),
              //         ),
              //       ),
              //       onPressed: () {
              //         setState(() {
              //           if (Provider.of<SharedPref>(context, listen: false)
              //                   .token ==
              //               null) {
              //             productsAuctions = widget.getAuctionsByCityFilter.data
              //                 .where((element) =>
              //                     element.countryId ==
              //                     Provider.of<SharedPref>(context,
              //                             listen: false)
              //                         .countryId)
              //                 .toList();
              //           } else if (myAuctionsonly == false) {
              //             productsAuctions = productsAuctions
              //                 .where((element) =>
              //                     element.userId ==
              //                     Provider.of<SharedPref>(context,
              //                             listen: false)
              //                         .id)
              //                 .toList();
              //             myAuctionsonly = true;
              //           } else {
              //             productsAuctions = widget.getAuctionsByCityFilter.data
              //                 .where((element) =>
              //                     element.countryId ==
              //                     Provider.of<SharedPref>(context,
              //                             listen: false)
              //                         .countryId)
              //                 .toList();
              //             myAuctionsonly = false;
              //           }
              //         });
              //       },
              //     ),
              //   ),
              // ),
            ],
          ),
          Expanded(child: Text('')),
          GestureDetector(
            onTap: () {
              sortBottomSheet(context);
            },
            child: Container(
              height: MediaQuery.of(context).size.height * 0.045,
              width: MediaQuery.of(context).size.width * 0.48,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey,
                  width: 1,
                ),
                shape: BoxShape.rectangle,
              ),
              child: Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      _selected,
                      style: TextStyle(fontSize: 13),
                    ),
                    Icon(Icons.arrow_drop_down),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // print('sgggggggggggggggg${widget.getAuctionssModel.data}');
    if (widget.getAuctionsByCityFilter.data != null) {
      productsAuctions = widget.category == 'الكل'
          ? widget.getAuctionsByCityFilter.data
              .where((element) =>
                  element.countryId ==
                  Provider.of<SharedPref>(context, listen: false).countryId)
              .toList()
          : widget.subCategory.isNotEmpty
              ? widget.getAuctionsByCityFilter.data
                  .where((element) =>
                      element.subCategoryId == widget.subCategoryId)
                  .toList()
              : widget.getAuctionsByCityFilter.data
                  .where((element) => element.categoryId == widget.categoryId)
                  .toList();

      sort();
      productsAuctions.sort(
        (family1, family2) => family2.pinned.compareTo(
          family1.pinned,
        ),
      );
    } else {
      productsAuctions = [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      headerBuild(),
      ((productsAuctions == null) || (productsAuctions.length <= 0))
          ? Container(
              height: MediaQuery.of(context).size.height * 0.3,
              child: Center(
                child: Text(
                  localization.text('no_results'),
                  style: TextStyle(color: Colors.black),
                ),
              ),
            )
          : widget.query == ''
              ? Expanded(
                  child: ListView.builder(
                    primary: false,
                    shrinkWrap: true,
                    itemCount: productsAuctions.length,
                    itemBuilder: (BuildContext context, int i) => AuctionItem(
                      initialPrice: productsAuctions[i].initialPrice,
                      netWorkImage: productsAuctions[i].photos[0].photo,
                      pricesIncreases: productsAuctions[i].priceIncrease,
                      title: productsAuctions[i].name,
                      userPhoto: productsAuctions[i].userPhoto,
                      heighPrice: productsAuctions[i].heighPrice,
                      city: productsAuctions[i].city,
                      username: productsAuctions[i].user,
                      userId: productsAuctions[i].userId,
                      period: productsAuctions[i].period,
                      id: productsAuctions[i].id,
                      currency: productsAuctions[i].currency,
                      createdAt: productsAuctions[i].createdAt,
                    ),
                  ),
                )
              : SearchAuctionsIteams(
                  productsAuctions: productsAuctions,
                  query: widget.query,
                  getAuctionsByCityFilter: widget.getAuctionsByCityFilter,
                  key: UniqueKey(),
                ),
    ]);
  }
}
