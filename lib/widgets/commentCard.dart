import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentstation/Components/customBtn.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/get/adCommentModel.dart';
import 'package:rentstation/provider/get/getCommentProvider.dart';
import 'package:rentstation/provider/post/addReplyProvider.dart';
import 'package:rentstation/provider/post/reportCommentProvider.dart';
import 'package:rentstation/screens/addReply.dart';
import 'package:rentstation/widgets/register_text_field.dart';

import 'custom_bottom_sheet.dart';

class CommentCard extends StatelessWidget {
  final String name;
  final String img;
  final String comment;
  final int index;
  final int idUser;
  final int idComment;
  final int idAd;
  final List<Replay> replays;
  final String data;

  CommentCard(
      {Key key,
      this.name,
      this.img,
      this.comment,
      this.index,
      this.replays,
      this.idAd,
      this.idComment,
      this.data,
      this.idUser})
      : super(key: key);

  CustomDialog _dialog = CustomDialog();
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(5),
        child: ListView(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: CircleAvatar(
                    radius: 13,
                    backgroundColor: Colors.red[400],
                    child: IconButton(
                      iconSize: 12,
                      onPressed: () async {
                        CustomBottomSheet().show(
                            context: context,
                            child: Padding(
                              padding: const EdgeInsets.all(20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  BlockTextField(
                                    onChange: (v) {
                                      Provider.of<ReportCommentProvider>(
                                              context,
                                              listen: false)
                                          .blockReason = v;
                                    },
                                    label: localization.text('report_Reason'),
                                    icon: Icons.label,
                                    type: TextInputType.text,
                                  ),
                                  SizedBox(height: 20),
                                  CustomBtn(
                                    text: localization.text('confirm_report'),
                                    color: Colors.red,
                                    onTap: () async {
                                      Navigator.pop(context);
                                      await Provider.of<ReportCommentProvider>(
                                              context,
                                              listen: false)
                                          .reportComment(
                                              idComment,
                                              Provider.of<SharedPref>(context,
                                                      listen: false)
                                                  .token,
                                              context);
                                    },
                                    txtColor: Colors.white,
                                  )
                                ],
                              ),
                            ));
                      },
                      icon: Icon(
                        Icons.priority_high,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),

                if (Provider.of<SharedPref>(context, listen: false).id ==
                    idUser)
                  IconButton(
                    icon: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                    onPressed: () async {
                      _dialog.showOptionDialog(
                          context: context,
                          msg: localization.text(
                              'Are_you_sure_from__delete_Comment_this_ad'),
                          okFun: () async {
                            await Provider.of<GetCommentProvider>(context,
                                    listen: false)
                                .deleteCommentAd(
                                    context,
                                    idComment,
                                    idAd,
                                    Provider.of<SharedPref>(context,
                                            listen: false)
                                        .token);
                          },
                          okMsg: localization.text('yes'),
                          cancelMsg: localization.text('no'),
                          cancelFun: () {
                            return;
                          });
                    },
                  ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      constraints: BoxConstraints(
                          maxWidth: MediaQuery.of(context).size.width / 1.5),
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(data),
                                Text(
                                  name,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                            Wrap(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 18.0),
                                  child: Text(
                                    comment,
                                    textAlign: TextAlign.right,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    TextButton(
                              child: Text(localization.text('replays')),
                      onPressed: () {
                        if (Provider.of<AddReplyProvider>(context,
                                        listen: false)
                                    .reply
                                    .length >
                                0 ||
                            Provider.of<AddReplyProvider>(context,
                                        listen: false)
                                    .reply !=
                                null) {
                          Provider.of<AddReplyProvider>(context, listen: false)
                              .clear();
                        }
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => AddReply(
                                  replays: replays,
                                  id: idComment,
                                )));
                      },
                    ),
                  ],
                ),
                SizedBox(width: 10),
                Container(
                  height: 30,
                  width: 30,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: CachedNetworkImage(
                      imageUrl: img == null ? "" : img,
                      errorWidget: (context, url, error) => ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child:
                              Image.asset('images/16.jpg', fit: BoxFit.fill)),
                      fadeInDuration: Duration(seconds: 2),
                      placeholder: (context, url) => ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child:
                              Image.asset('images/16.jpg', fit: BoxFit.fill)),
                      imageBuilder: (context, provider) {
                        return ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: Image(
                              image: provider,
                              fit: BoxFit.cover,
                            ));
                      },
                    ),
                  ),
                ),

                // CircleAvatar(
                //     backgroundImage: widget.img == null
                //         ? AssetImage('assets/avatar.jpg')
                //         : FadeInImage(
                //             fit: BoxFit.cover,
                //             placeholder: AssetImage('assets/placeholder.png'),
                //             image: CacheImage(widget.img)),
                //     radius: 15),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Divider(
                height: 1,
              ),
            )
          ],
        ),
      ),
    );
  }
}
