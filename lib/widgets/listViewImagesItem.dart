import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/get/getAuctionsPhotoProvider.dart';
import 'package:rentstation/screens/fullPhoto.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ListViewImagesItem extends StatelessWidget {
  final String photo;
  final int length;
  final int photoId;
  final SharedPreferences prefs;
  ListViewImagesItem({this.length, this.photo, this.photoId, this.prefs});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => FullPhoto(
                    imageUrl: photo,
                  )));
        },
        child: Dismissible(
          direction: DismissDirection.startToEnd,
          key: UniqueKey(),
          onDismissed: (direction) {
            if (direction == DismissDirection.startToEnd) {}
          },
          confirmDismiss: (direction) {
            return showDialog(
                context: context,
                builder: (ctx) => AlertDialog(
                      title: Text(
                        localization.text('Are_you_sure'),
                        textDirection:
                            localization.currentLanguage.toString() == "en"
                                ? TextDirection.ltr
                                : TextDirection.rtl,
                      ),
                      content: Text(
                        localization.text('do_you_want_to_delete_this_photo'),
                        textDirection:
                            localization.currentLanguage.toString() == "en"
                                ? TextDirection.ltr
                                : TextDirection.rtl,
                      ),
                      actions: <Widget>[
                        FlatButton(
                          child: Text(localization.text('no')),
                          onPressed: () {
                            Navigator.of(ctx).pop(false);
                          },
                        ),
                        FlatButton(
                          child: Text(localization.text('ok')),
                          onPressed: () async {
                            if (length <= 1) {
                              Fluttertoast.showToast(
                                  msg: localization.text(
                                      'The_advertisement_cannot_be_without_at_least_one_image'));
                              Navigator.of(ctx).pop(false);
                            } else {
                              await Provider.of<GetphotosAuctionsbyIdProvider>(
                                      context,
                                      listen: false)
                                  .deletePhotobyId(
                                context,
                                photoId,
                                prefs.getString('api_token'),
                              );
                              Navigator.of(ctx).pop(true);
                            }
                          },
                        ),
                      ],
                    ));
          },
          background: Container(
            color: Theme.of(context).errorColor,
            child: Icon(
              Icons.delete,
              color: Colors.white,
              size: 40,
            ),
            alignment: Alignment.centerRight,
            padding: EdgeInsets.only(right: 20),
            margin: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
          ),
          child: Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            height: 200,
            child: GridTile(
              child: photo != null
                  ? CachedNetworkImage(
                      imageUrl: photo,
                      fadeInDuration: Duration(seconds: 2),
                      placeholder: (context, url) => Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('images/loading.gif'),
                                  fit: BoxFit.fill),
                            ),
                          ),
                      imageBuilder: (context, provider) {
                        return Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: provider, fit: BoxFit.fill),
                          ),
                        );
                      })
                  : Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('images/loading.gif'),
                            fit: BoxFit.fill),
                      ),
                    ),
            ),
          ),
        ));
  }
}
