import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/get/getMyAdds.dart';
import 'package:rentstation/screens/adDetails.dart';
import 'package:rentstation/screens/editAd.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyAdItem extends StatefulWidget {
  final int id;
  final String title;
  final String desc;
  final String labelCity;
  final int price;
  final List photos;
  final List images;
  final String image;
  final String categorylable;
  final String subCategorylable;
  final int subCategoryId;
  final int categoryId;
  final bool checked;

  final SharedPreferences prefs;
  MyAdItem(
      {this.id,
      this.title,
      this.labelCity,
      this.image,
      this.price,
      this.desc,
      this.photos,
      this.images,
      this.categoryId,
      this.categorylable,
      this.checked,
      this.subCategoryId,
      this.subCategorylable,
      this.prefs});

  @override
  _MyAdItemState createState() => _MyAdItemState();
}

class _MyAdItemState extends State<MyAdItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => AdDetails(
                  id: widget.id,
                )));
      },
      child: ListTile(
        leading: widget.image != null
            ? CachedNetworkImage(
                imageUrl: widget.image,
                fadeInDuration: Duration(seconds: 2),
                placeholder: (context, url) => CircleAvatar(
                  radius: 50,
                  backgroundImage: AssetImage(
                    'images/loading.gif',
                  ),
                ),
                imageBuilder: (context, provider) {
                  return CircleAvatar(radius: 50, backgroundImage: provider);
                },
              )
            : CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage(
                  'images/loading.gif',
                ),
              ),
        title: Text(
          widget.title,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        ),
        trailing: Container(
          width: 100,
          child: Row(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.edit),
                color: Theme.of(context).primaryColor,
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => EditAd(
                            images: widget.images,
                            photos: widget.photos,
                            categoryId: widget.categoryId,
                            categorylable: widget.categorylable,
                            checked: widget.checked,
                            desc: widget.desc,
                            id: widget.id,
                            title: widget.title,
                            key: UniqueKey(),
                            price: widget.price,
                            lableCity: widget.labelCity,
                            subCategoryId: widget.subCategoryId,
                            subCategorylable: widget.subCategorylable,
                          )));
                },
              ),
              IconButton(
                icon: Icon(Icons.delete),
                color: Theme.of(context).errorColor,
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (ctx) => AlertDialog(
                      title: Text(
                        localization.text('Are_you_sure'),
                        textDirection:
                            localization.currentLanguage.toString() == "en"
                                ? TextDirection.ltr
                                : TextDirection.rtl,
                      ),
                      content: Text(
                        localization.text('ad_ask_delete'),
                        textDirection:
                            localization.currentLanguage.toString() == "en"
                                ? TextDirection.ltr
                                : TextDirection.rtl,
                      ),
                      actions: [
                        FlatButton(
                          child: Text(localization.text('no')),
                          onPressed: () {
                            Navigator.of(ctx).pop();
                          },
                        ),
                        FlatButton(
                          child: Text(localization.text('ok')),
                          onPressed: () async {
                            try {
                              await Provider.of<GetMyAdsProvider>(context,
                                      listen: false)
                                  .deleteAd(widget.prefs.getString('api_token'),
                                      widget.id, context);

                              Navigator.of(ctx).pop(true);
                            } catch (error) {
                              print(error);
                            }
                          },
                        ),
                      ],
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
