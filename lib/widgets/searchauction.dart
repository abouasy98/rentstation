import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/post/getAuctionsByFilter.dart';
import 'auctionItem.dart';
import 'package:rentstation/models/post/getAuctionsByFilter.dart' as a8a;

class SearchAuctionsIteams extends StatelessWidget {
  final Key key;
  List<a8a.Datum> productsAuctions;
  GetAuctionsByCityFilter getAuctionsByCityFilter;
  final String query;
  SearchAuctionsIteams(
      {this.productsAuctions,
      this.query,
      this.key,
      this.getAuctionsByCityFilter});

  @override
  Widget build(BuildContext context) {
    productsAuctions =
        Provider.of<SharedPref>(context, listen: false).countryId != null
            ? getAuctionsByCityFilter.data
                .where((element) =>
                    element.countryId ==
                    Provider.of<SharedPref>(context, listen: false).countryId)
                .toList()
            : getAuctionsByCityFilter.data;
    final List<a8a.Datum> suggteionList = query.isEmpty
        ? []
        : productsAuctions != null
            ? productsAuctions.where((a8a.Datum user) {
                String getUserName = user.user.toLowerCase();
                String _query = query.toLowerCase();
                String getTitle = user.name.toLowerCase();
                bool matchUserName = getUserName.contains(_query);
                bool matchtitle = getTitle.contains(_query);
                return (matchUserName || matchtitle);
              }).toList()
            : [];
    return Directionality(
      textDirection: localization.currentLanguage.toString() == "en"
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: suggteionList.length <= 0
          ? Container(
              height: MediaQuery.of(context).size.height * 0.6,
              child: Center(
                child: Text(
                  localization.text('no_results'),
                  style: TextStyle(color: Colors.black),
                ),
              ),
            )
          : ListView.builder(
              primary: false,
              shrinkWrap: true,
              itemCount: suggteionList.length,
              itemBuilder: ((context, i) {
                a8a.Datum searchedUser = a8a.Datum(
                    email: suggteionList[i].email,
                    auctionWinner: suggteionList[i].auctionWinner,
                    photos: suggteionList[i].photos,
                    userPhoto: suggteionList[i].userPhoto,
                    currency: suggteionList[i].currency,
                    userId: suggteionList[i].userId,
                    auctionWinnerId: suggteionList[i].auctionWinnerId,
                    id: suggteionList[i].id,
                    cityId: suggteionList[i].cityId,
                    description: suggteionList[i].description,
                    user: suggteionList[i].user,
                    country: suggteionList[i].country,
                    phoneNumber: suggteionList[i].phoneNumber,
                    createdAt: suggteionList[i].createdAt,
                    city: suggteionList[i].city,
                    countryId: suggteionList[i].countryId,
                    details: suggteionList[i].details,
                    initialPrice: suggteionList[i].initialPrice,
                    memberDate: suggteionList[i].memberDate,
                    name: suggteionList[i].name,
                    period: suggteionList[i].period,
                    priceIncrease: suggteionList[i].priceIncrease,
                    prices: suggteionList[i].prices);
                return AuctionItem(
                  initialPrice: searchedUser.initialPrice,
                  netWorkImage: searchedUser.photos[0].photo,
                  pricesIncreases: searchedUser.priceIncrease,
                  title: searchedUser.name,
                  userPhoto: searchedUser.userPhoto,
                  heighPrice: searchedUser.heighPrice,
                  city: searchedUser.city,
                  username: searchedUser.user,
                  userId: searchedUser.userId,
                  period: searchedUser.period,
                  id: searchedUser.id,
                  currency: searchedUser.currency,
                  createdAt: searchedUser.createdAt,
                );
              })),
    );
  }
}
