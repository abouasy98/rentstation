import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';

import 'package:rentstation/provider/get/addAdToArchiveProvider.dart';
import 'package:rentstation/provider/get/addAdToFavProvider.dart';
import 'package:rentstation/provider/get/getMyFavAdsProvider.dart';
import 'package:rentstation/screens/adDetails.dart';
import 'package:rentstation/screens/userProfile.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';
import 'package:rentstation/models/post/getAdsFilterByCity.dart'as a7a;
class AdItemGridView extends StatefulWidget {
  final List<a7a.Photo> image;
  final int id;
  final String title;
  final String desc;
  final int price;
  final String photo;
  final int sale;

  final int countryId;
  final String country;
  final String category;
  final int categoryId;
  final int pinned;
  final String mobileNumber;
  final DateTime date;
  final String currency;
  final int userId;
  final String city;
  final DateTime createdAt;

  final Key key;
  final product;
  final String favouritecheck;
  final String userName;
  final String userPhoto;
  AdItemGridView(
      {this.title,
      this.userPhoto,
      this.id,
      this.userId,
      this.currency,
      this.city,
      this.createdAt,
      this.userName,
      this.sale,
      this.product,
      this.price,
      this.pinned,
      this.photo,
      this.mobileNumber,
      this.key,
      this.image,
      this.favouritecheck,
      this.desc,
      this.date,
      this.countryId,
      this.country,
      this.categoryId,
      this.category});

  @override
  _AdItemGridViewState createState() => _AdItemGridViewState();
}

class _AdItemGridViewState extends State<AdItemGridView> {
  bool isFavourite = true;
//  CustomDialog _dialog = CustomDialog();
  @override
  void didChangeDependencies() {
    print(widget.countryId);
    if (widget.favouritecheck == '1') {
      setState(() {
        isFavourite = true;
      });
    } else {
      setState(() {
        isFavourite = false;
      });
    }

    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        pushNewScreen(
          context,
          screen: AdDetails(
            id: widget.id,
          ),
          withNavBar: false, // OPTIONAL VALUE. True by default.
          pageTransitionAnimation: PageTransitionAnimation.cupertino,
        );
        // Navigator.of(context).push(MaterialPageRoute(
        //     builder: (context) => AdDetails(
        //           id: widget.id,
        //         )));
      },
      child: Container(
          height: MediaQuery.of(context).size.height * 0.17,
          width: MediaQuery.of(context).size.width,
          child: Card(
            elevation: 10,
            clipBehavior: Clip.antiAlias,
            child: Container(
              padding: const EdgeInsets.all(0),
              child: Row(children: [
                Expanded(
                  flex: 6,
                  child: Stack(children: [
                    widget.photo != null
                        ? CachedNetworkImage(
                            imageUrl: widget.photo,
                            fadeInDuration: Duration(seconds: 2),
                            placeholder: (context, url) => Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: Container(
                                  decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('images/clock-loading.gif'),
                                    fit: BoxFit.fill),
                              )),
                            ),
                            imageBuilder: (context, provider) {
                              return ClipRRect(
                                borderRadius: BorderRadius.circular(20.0),
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                  child: Container(
                                      decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: provider, fit: BoxFit.fill),
                                  )),
                                ),
                              );
                            },
                          )
                        : Container(
                            decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('images/clock-loading.gif'),
                                fit: BoxFit.fill),
                          )),
                    if (widget.userId ==
                        Provider.of<SharedPref>(context, listen: false).id)
                      Positioned(
                          left: -MediaQuery.of(context).size.width * 0.02,
                          child: IconButton(
                            icon: Icon(Icons.archive),
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (ctx) => AlertDialog(
                                  title: Text(
                                    localization.text('Are_you_sure'),
                                    textDirection: TextDirection.rtl,
                                  ),
                                  content: Text(
                                    localization.text(
                                        'Are_you_sure_from_Archieve_this_ad'),
                                    textDirection: TextDirection.rtl,
                                  ),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text(localization.text('no')),
                                      onPressed: () {
                                        Navigator.of(ctx).pop(false);
                                      },
                                    ),
                                    FlatButton(
                                      child: Text(localization.text('ok')),
                                      onPressed: () {
                                        Provider.of<AddAdToArchProvider>(
                                                context,
                                                listen: false)
                                            .addAdToarch(
                                                Provider.of<SharedPref>(context,
                                                        listen: false)
                                                    .token,
                                                widget.id,
                                                context);
                                        Navigator.of(ctx).pop(true);
                                      },
                                    ),
                                  ],
                                ),
                              );
                            },
                            color: Colors.lightBlue[400],
                          )),
                    Positioned(
                      child: IconButton(
                        onPressed: () async {
                          if (Provider.of<SharedPref>(context, listen: false)
                                  .token ==
                              null) {
                            // _dialog.showOptionDialog(
                            //     context: context,
                            //     msg: localization.text('auth'),
                            //     okFun: () {
                            //       pushNewScreen(
                            //         context,
                            //         screen: SignInScreen(),
                            //         withNavBar:
                            //             false, // OPTIONAL VALUE. True by default.
                            //         pageTransitionAnimation:
                            //             PageTransitionAnimation.cupertino,
                            //       );
                            //       // Navigator.push(
                            //       //     context,
                            //       //     MaterialPageRoute(
                            //       //         builder: (c) => SignInScreen()));
                            //     },
                            //     okMsg: localization.text('yes'),
                            //     cancelMsg: localization.text('no'),
                            //     cancelFun: () {
                            //       return;
                            //     });
                          } else if (isFavourite) {
                            await Provider.of<GetMyFavAdsProvider>(context,
                                    listen: false)
                                .deleteFavAd(
                              context,
                              false,
                              widget.id,
                              Provider.of<SharedPref>(context, listen: false)
                                  .token,
                            );
                            setState(() {
                              isFavourite = false;
                            });
                          } else {
                            await Provider.of<AddAdToFavProvider>(context,
                                    listen: false)
                                .addAdToFav(
                                    Provider.of<SharedPref>(context,
                                            listen: false)
                                        .token,
                                    widget.id,
                                    context);
                            setState(() {
                              isFavourite = true;
                            });
                          }
                        },
                        icon: Icon(isFavourite
                            ? Icons.favorite
                            : Icons.favorite_border),
                        color: Colors.red,
                      ),
                      height: 40,
                      // right:                -MediaQuery.of(context).size.width*0.1,
                    ),
                  ]),
                ),
                Spacer(
                  flex: 1,
                ),
                Expanded(
                  flex: 18,
                  child: Container(
                    padding: const EdgeInsets.only(top: 5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.05,
                          child: Text(widget.title,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 20.0, fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.01,
                        ),
                        Expanded(
                          child: Container(
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    alignment: localization.currentLanguage
                                                .toString() ==
                                            "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                    width: MediaQuery.of(context).size.width *
                                        0.42,
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          // Row(
                                          //   children: <Widget>[
                                          //     Container(
                                          //       width: MediaQuery.of(context)
                                          //               .size
                                          //               .width *
                                          //           0.18,
                                          //       child: FittedBox(
                                          //         child: Text(
                                          //           '${localization.text('Current_Price')}:',
                                          //           style: TextStyle(
                                          //               fontWeight:
                                          //                   FontWeight.bold,
                                          //               fontSize: 13),
                                          //         ),
                                          //       ),
                                          //     ),
                                          //     Container(
                                          //       width: MediaQuery.of(context)
                                          //               .size
                                          //               .width *
                                          //           0.2,
                                          //       alignment: localization
                                          //                   .currentLanguage
                                          //                   .toString() ==
                                          //               "en"
                                          //           ? Alignment.centerLeft
                                          //           : Alignment.centerRight,
                                          //       child: FittedBox(
                                          //         child: Text(
                                          //           heighPrice == null
                                          //               ? '${intl.NumberFormat.currency(name: currency, decimalDigits: 0).format(initialPrice)}'
                                          //               : '${intl.NumberFormat.currency(name: currency, decimalDigits: 0).format(heighPrice)}',
                                          //           overflow:
                                          //               TextOverflow.ellipsis,
                                          //           style:
                                          //               TextStyle(fontSize: 15),
                                          //         ),
                                          //       ),
                                          //     ),
                                          //   ],
                                          // ),
                                          // Expanded(child: Text('')),
                                          Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                // Container(
                                                //   width: MediaQuery.of(context)
                                                //           .size
                                                //           .width *
                                                //       0.1,
                                                //   child: AutoSizeText(
                                                //     '${localization.text('price')}:',
                                                //     wrapWords: true,
                                                //     style: TextStyle(
                                                //       fontWeight:
                                                //           FontWeight.bold,
                                                //     ),
                                                //   ),
                                                // ),
                                                Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.3,
                                                  alignment: localization
                                                              .currentLanguage
                                                              .toString() ==
                                                          "en"
                                                      ? Alignment.centerLeft
                                                      : Alignment.centerRight,
                                                  child: AutoSizeText(
                                                    '${intl.NumberFormat.currency(locale:localization.currentLanguage.toString() == "en"?null:'ar_EG',name: widget.currency, decimalDigits: 0).format(widget.price)}',
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    wrapWords: true,
                                                    style: TextStyle(
                                                      fontSize: 15,
                                                    ),
                                                  ),
                                                )
                                              ]),
                                          Expanded(child: Text('')),
                                          InkWell(
                                            onTap: () {
                                              if (Provider.of<SharedPref>(
                                                          context,
                                                          listen: false)
                                                      .token ==
                                                  null) {
                                                // _dialog.showOptionDialog(
                                                //     context: context,
                                                //     msg: localization
                                                //         .text('auth'),
                                                //     okFun: () {
                                                //       pushNewScreen(
                                                //         context,
                                                //         screen: SignInScreen(),
                                                //         withNavBar:
                                                //             false, // OPTIONAL VALUE. True by default.
                                                //         pageTransitionAnimation:
                                                //             PageTransitionAnimation
                                                //                 .cupertino,
                                                //       );
                                                //       // Navigator.push(
                                                //       //     context,
                                                //       //     MaterialPageRoute(
                                                //       //         builder: (c) =>
                                                //       //             SignInScreen()));
                                                //     },
                                                //     okMsg: localization
                                                //         .text('yes'),
                                                //     cancelMsg:
                                                //         localization.text('no'),
                                                //     cancelFun: () {
                                                //       return;
                                                //     });
                                              } else {
                                                pushNewScreen(
                                                  context,
                                                  screen: UserProfile(
                                                    userid: widget.userId,
                                                  ),
                                                  withNavBar:
                                                      false, // OPTIONAL VALUE. True by default.
                                                  pageTransitionAnimation:
                                                      PageTransitionAnimation
                                                          .cupertino,
                                                );
                                                // Navigator.of(context).push(
                                                //     MaterialPageRoute(
                                                //         builder: (context) =>
                                                //             UserProfile(
                                                //               userid: widget.userId,
                                                //             )));
                                              }
                                            },
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                widget.userPhoto != null
                                                    ? CachedNetworkImage(
                                                        imageUrl:
                                                            widget.userPhoto,
                                                        fadeInDuration:
                                                            Duration(
                                                                seconds: 2),
                                                        placeholder: (context,
                                                                url) =>
                                                            CircleAvatar(
                                                                radius: 10,
                                                                backgroundImage:
                                                                    AssetImage(
                                                                        'images/16.jpg')),
                                                        imageBuilder: (context,
                                                            provider) {
                                                          return CircleAvatar(
                                                              radius: 10,
                                                              backgroundImage:
                                                                  provider);
                                                        },
                                                      )
                                                    : CircleAvatar(
                                                        radius: 10,
                                                        backgroundImage:
                                                            AssetImage(
                                                                'images/16.jpg')),
                                                SizedBox(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                ),
                                                Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.21,
                                                  alignment: localization
                                                              .currentLanguage
                                                              .toString() ==
                                                          "en"
                                                      ? Alignment.centerLeft
                                                      : Alignment.centerRight,
                                                  child: Text(
                                                    widget.userName,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style:
                                                        TextStyle(fontSize: 13),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ]),
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.17,
                                        child:  Text(
                                              '${localization.text('ago')} ${DateTime.now().difference(DateTime(widget.createdAt.year, widget.createdAt.month, widget.createdAt.day)).inDays} ${localization.text('day')}',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 13),
                                              textAlign: localization
                                                          .currentLanguage
                                                          .toString() ==
                                                      "en"
                                                  ? TextAlign.right
                                                  : TextAlign.left),
                                       
                                      ),
                                      Expanded(child: Text('')),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.19,
                                            alignment: localization
                                                        .currentLanguage
                                                        .toString() ==
                                                    "en"
                                                ? Alignment.bottomRight
                                                : Alignment.bottomLeft,
                                            child: Text(
                                              widget.city,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          Icon(
                                            Icons.location_on,
                                            size: 17,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ]),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ]),
            ),
          )),
    );
  }
}
