import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/screens/Intro/SplashScreen.dart';

class WeekCountdown extends StatefulWidget {
  final DateTime createdAt;
  final int period;
  WeekCountdown({this.createdAt, this.period});
  @override
  State<StatefulWidget> createState() => _WeekCountdownState();
}

class _WeekCountdownState extends State<WeekCountdown> {
  Timer _timer;
  DateTime _currentTime;
  DateTime calculatefinishOfAuction(DateTime time) {
    return DateTime(time.year, time.month, time.day + widget.period, time.hour,
        time.minute, time.second);
  }

  @override
  void initState() {
    super.initState();
    _currentTime = DateTime.now();
    _timer = Timer.periodic(Duration(seconds: 1), _onTimeChange);
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void _onTimeChange(Timer timer) {
    setState(() {
      _currentTime = DateTime.now();
    });
  }

  void whentimeFinsih() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      Navigator.of(context).pushAndRemoveUntil(
        PageRouteBuilder(pageBuilder: (_, __, ___) => Splash()),
        (route) => false,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    final finishofAuction = calculatefinishOfAuction(widget.createdAt);
    final remaining = finishofAuction.difference(_currentTime);

    final days = remaining.inDays;
    final hours = remaining.inHours - remaining.inDays * 24;
    final minutes = remaining.inMinutes - remaining.inHours * 60;
    final seconds = remaining.inSeconds - remaining.inMinutes * 60;

    final formattedRemaining =
        '$days ${localization.text('day')} $hours ${localization.text('hour')} $minutes ${localization.text('minute')} $seconds ${localization.text('second')}';
    if (days <= 0 && hours <= 0 && minutes <= 0 && seconds < 0) {
      whentimeFinsih();
    }
    return FittedBox(
      child: Text(
        " ${localization.text('end_time')} : $formattedRemaining",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
    );
  }
}
