import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/get/getmyArchieveAds.dart';
import 'package:rentstation/screens/adDetails.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart' as intl;
class ArchievedItem extends StatelessWidget {
  final String title;
  final String photo;
  final int id;
  final SharedPreferences prefs;

  final int price;
  final String city;
  final String userPhoto;
  final String userName;
  final DateTime createdAt;
  ArchievedItem(
      {this.title,
      this.city,
      this.createdAt,
      this.id,
      this.photo,
      this.userPhoto,
      this.prefs,
      this.price,
      this.userName});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => AdDetails(
                  id: id,
                )));
      },
      child: Dismissible(
        direction:  DismissDirection.startToEnd,
         
        key: UniqueKey(),
        onDismissed: (direction) {
          if (direction == DismissDirection.startToEnd) {}
      
        },
        confirmDismiss: (direction) {
          return showDialog(
              context: context,
              builder: (ctx) => AlertDialog(
                    title: Text(
                      localization.text('Are_you_sure'),
                      textDirection:
                          localization.currentLanguage.toString() == "en"
                              ? TextDirection.ltr
                              : TextDirection.rtl,
                    ),
                    content: Text(
                      localization.text('Are_you_sure_from__delete_Archieve_this_ad'),
                      textDirection:
                          localization.currentLanguage.toString() == "en"
                              ? TextDirection.ltr
                              : TextDirection.rtl,
                    ),
                    actions: <Widget>[
                      FlatButton(
                        child: Text(
                          localization.text('no'),
                        ),
                        onPressed: () {
                          Navigator.of(ctx).pop(false);
                        },
                      ),
                      FlatButton(
                        child: Text(
                          localization.text('ok'),
                        ),
                        onPressed: () {
                          Provider.of<GetmyArchieveAdsProvider>(context,
                                  listen: false)
                              .deleteArchieveAd(
                                  context, id, prefs.getString('api_token'));
                          Navigator.of(ctx).pop(true);
                        },
                      ),
                    ],
                  ));
        },
        background: Container(
          color: Theme.of(context).errorColor,
          child: Icon(
            Icons.delete,
            color: Colors.white,
            size: 40,
          ),
          alignment: Alignment.centerRight,
          padding: EdgeInsets.only(right: 20),
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
        ),
        child: Container(
            height: MediaQuery.of(context).size.height * 0.15,
            width: MediaQuery.of(context).size.width,
            child: Card(
              elevation: 10,
              clipBehavior: Clip.antiAlias,
              child: Container(
                height: 120,
                padding: const EdgeInsets.all(0),
                child: Row(children: [
                  Expanded(
                    flex: 6,
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: CachedNetworkImageProvider(
                              photo,
                            ),
                            fit: BoxFit.fill),
                      ),
                    ),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  Expanded(
                    flex: 14,
                    child: Container(
                      padding: const EdgeInsets.only(top: 5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(title,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 20.0, fontWeight: FontWeight.bold)),
                          Row(
                            children: <Widget>[
                              Text(
                                '${localization.text('price')} : ',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                              Text(
                             '${intl.NumberFormat.currency(locale: localization.currentLanguage.toString() == "en"?null:'ar_EG',name: prefs.get("currency"), decimalDigits: 0).format(price)}',
                                style: TextStyle(fontSize: 20),
                              )
                            ],
                          ),
                          Align(
                            alignment:
                                localization.currentLanguage.toString() == "en"
                                    ? Alignment.bottomLeft
                                    : Alignment.bottomRight,
                            child: Text(
                             '${localization.text('ago')} ${DateTime.now().difference(DateTime(createdAt.year, createdAt.month, createdAt.day)).inDays} ${localization.text('day')}',
                          ))
                        ],
                      ),
                    ),
                  ),
                ]),
              ),
            )),
      ),
    );
  }
}
