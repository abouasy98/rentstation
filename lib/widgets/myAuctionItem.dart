import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/get/getmyAuctionsProvider.dart';
import 'package:rentstation/screens/AuctionDetailsScreen.dart';
import 'package:rentstation/screens/editAuctions.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyAuctionItem extends StatefulWidget {
  final int id;
  final String title;
  final String desc;
  final String labelCity;
  final int period;
  final Key key;
  final int priceIncrease;
  final int categoryId;
  final int intialPrice;
  final List images;

  final String image;
  final String currency;
  final List photos;
  final SharedPreferences prefs;
  final String categorylable;
  final String subCategorylable;
  MyAuctionItem(
      {this.id,
      this.title,
      this.labelCity,
      this.image,
      this.currency,
      this.period,
      this.desc,
      this.key,
      this.photos,
      this.priceIncrease,
      this.categoryId,
      this.intialPrice,
      this.images,
      this.subCategorylable,
      this.categorylable,
      this.prefs});

  @override
  _MyAuctionItemState createState() => _MyAuctionItemState();
}

class _MyAuctionItemState extends State<MyAuctionItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => AuctionsDetails(
                  id: widget.id,
                )));
      },
      child: ListTile(
        key: widget.key,
        leading: widget.image != null
            ? CachedNetworkImage(
                imageUrl: widget.image,
                fadeInDuration: Duration(seconds: 2),
                placeholder: (context, url) => CircleAvatar(
                  radius: 50,
                  backgroundImage: AssetImage(
                    'images/loading.gif',
                  ),
                ),
                imageBuilder: (context, provider) {
                  return CircleAvatar(radius: 50, backgroundImage: provider);
                },
              )
            : CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage(
                  'images/loading.gif',
                ),
              ),
        title: Text(
          widget.title,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        ),
        trailing: Container(
          width: 100,
          child: Row(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.edit),
                color: Theme.of(context).primaryColor,
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => EditAuctions(
                            details: widget.desc,
                            id: widget.id,
                            images: widget.images,
                            lableCity: widget.labelCity,
                            intialPrice: widget.intialPrice,
                            priceIncrease: widget.priceIncrease,
                            photos: widget.photos,
                            period: widget.period,
                            currency: widget.currency,
                            categorylable: widget.categorylable,
                            subCategorylable: widget.subCategorylable,
                            title: widget.title,
                            categoryId: widget.categoryId,
                          )));
                },
              ),
              IconButton(
                icon: Icon(Icons.delete),
                color: Theme.of(context).errorColor,
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (ctx) => AlertDialog(
                      title: Text(
                        localization.text('Are_you_sure'),
                        textDirection:
                            localization.currentLanguage.toString() == "en"
                                ? TextDirection.ltr
                                : TextDirection.rtl,
                      ),
                      content: Text(
                        localization
                            .text('do_you_want_to_remove_this_auction?'),
                        textDirection:
                            localization.currentLanguage.toString() == "en"
                                ? TextDirection.ltr
                                : TextDirection.rtl,
                      ),
                      actions: [
                        FlatButton(
                          child: Text(localization.text('no')),
                          onPressed: () {
                            Navigator.of(ctx).pop();
                          },
                        ),
                        FlatButton(
                          child: Text(localization.text('ok')),
                          onPressed: () async {
                            try {
                              await Provider.of<GetMyAductionProvider>(context,
                                      listen: false)
                                  .deleteAuctions(
                                      widget.prefs.getString('api_token'),
                                      widget.id,
                                      context);

                              Navigator.of(ctx).pop(true);
                            } catch (error) {
                              print(error);
                            }
                          },
                        ),
                      ],
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
