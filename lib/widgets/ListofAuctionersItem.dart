import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/provider/post/addedAuctionWinnerProvider.dart';
import 'package:rentstation/screens/userProfile.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';

class ListofAuctionersItem extends StatelessWidget {

  final int price;
  final String userPhoto;
  final int userid;
  final String userName;
  final dynamic auctionWinnerid;
  final int auctionId;
  final DateTime createdAt;
  ListofAuctionersItem(
      {this.userPhoto,

      this.userid,
      this.auctionWinnerid,
      this.price,
      this.auctionId,
      this.createdAt,
      this.userName});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => UserProfile(
                  userid: userid,
                )));
      },
      child: Card(
        child: Container(
          height: MediaQuery.of(context).size.height * 0.2,
          width: MediaQuery.of(context).size.width,
          child: Card(
            elevation: 10,
            clipBehavior: Clip.antiAlias,
            child: Container(
              height: 120,
              padding: const EdgeInsets.all(0),
              child: Row(children: [
                Expanded(
                  flex: 6,
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: CachedNetworkImageProvider(
                            userPhoto,
                          ),
                          fit: BoxFit.fill),
                    ),
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
                Expanded(
                  flex: 14,
                  child: Container(
                    padding: const EdgeInsets.only(top: 5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          alignment:
                              localization.currentLanguage.toString() == "en"
                                  ? Alignment.bottomLeft
                                  : Alignment.bottomRight,
                          width: MediaQuery.of(context).size.width * 0.5,
                          child: Text(userName,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              '${localization.text('price')}: ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15),
                            ),
                            Text(
                              '${intl.NumberFormat.currency(locale:localization.currentLanguage.toString() == "en"?null:'ar_EG',name:      Provider.of<SharedPref>(context, listen: false).currency, decimalDigits: 0).format(price)}',
                              style: TextStyle(fontSize: 15),
                            )
                          ],
                        ),
                        if (auctionWinnerid == null)
                          RaisedButton(
                            color: Colors.red,
                            onPressed: () async {
                              showDialog(
                                context: context,
                                builder: (ctx) => AlertDialog(
                                  title: Text(
                                    localization.text('Are_you_sure'),
                                    textDirection: localization.currentLanguage
                                                .toString() ==
                                            "en"
                                        ? TextDirection.ltr
                                        : TextDirection.rtl,
                                  ),
                                  content: Text(
                                    localization.text(
                                        'Are_you_sure_that_he_is_considered_the_winner_of_this_auction?'),
                                    textDirection: localization.currentLanguage
                                                .toString() ==
                                            "en"
                                        ? TextDirection.ltr
                                        : TextDirection.rtl,
                                  ),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text(localization.text('no')),
                                      onPressed: () {
                                        Navigator.of(ctx).pop(false);
                                      },
                                    ),
                                    FlatButton(
                                      child: Text(localization.text('ok')),
                                      onPressed: () {
                                        Provider.of<AddAuctionWinnerProvider>(
                                                context,
                                                listen: false)
                                            .addwinnerAuction(
                                                     Provider.of<SharedPref>(context, listen: false).token,
                                                auctionId,
                                                userid,
                                                context);
                                        Navigator.of(ctx).pop(true);
                                      },
                                    ),
                                  ],
                                ),
                              );
                            },
                            child: Text(
                              localization.text('Accept_bidding?'),
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        Align(
                            alignment:
                                localization.currentLanguage.toString() == "en"
                                    ? Alignment.bottomLeft
                                    : Alignment.bottomRight,
                            child: Text(
                              '${localization.text('ago')} ${DateTime.now().difference(DateTime(createdAt.year, createdAt.month, createdAt.day)).inDays} ${localization.text('day')}',
                            ))
                      ],
                    ),
                  ),
                ),
              ]),
            ),
          ),
        ),
      ),
    );
  }
}
