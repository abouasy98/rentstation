import 'package:flutter/material.dart';

import 'package:rentstation/provider/get/subCategoriesProvider.dart';

class SubCategoryItem extends StatefulWidget {
  final int index;
  SubCategoriesProvider subCategory;
  int itemCount;
  SubCategoryItem({this.index, this.subCategory, this.itemCount});
  @override
  _SubCategoryItemState createState() => _SubCategoryItemState();
}

class _SubCategoryItemState extends State<SubCategoryItem> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      primary: false, 
      shrinkWrap: true,
      itemCount: widget.itemCount,
      itemBuilder: (context, i) => Center(
        child: GestureDetector(
            child: Card(
              child: Text('${widget.subCategory.subcategories[i].name}'),
            ),
            onTap: () {
              setState(() {});
              print("bdan${widget.subCategory.subcategories[i].name}");
              Navigator.of(context)
                  .pop(widget.subCategory.subcategories[i].name);
            }),
      ),
    );
  }
}
