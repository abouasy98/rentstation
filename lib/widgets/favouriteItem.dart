import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/provider/get/getMyFavAdsProvider.dart';
import 'package:rentstation/screens/adDetails.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart' as intl;

class FavouriteItem extends StatelessWidget {
  final String title;
  final String photo;
  final int id;
  final int adId;

  final int price;
  final String city;
  final String userPhoto;
  final String userName;
  final DateTime createdAt;
  FavouriteItem(
      {this.title,
      this.city,
      this.createdAt,
      this.adId,
      this.id,
      this.photo,
      this.userPhoto,
      this.price,
      this.userName});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => AdDetails(
                  id: adId,
                )));
      },
      child: Dismissible(
        direction: DismissDirection.startToEnd,
        key: UniqueKey(),
        onDismissed: (direction) {
          if (direction == DismissDirection.startToEnd) {}
        },
        confirmDismiss: (direction) {
          return showDialog(
              context: context,
              builder: (ctx) => AlertDialog(
                    title: Text(
                      localization.text('Are_you_sure'),
                      textDirection:
                          localization.currentLanguage.toString() == "en"
                              ? TextDirection.ltr
                              : TextDirection.rtl,
                    ),
                    content: Text(
                      localization.text('ad_ask_delete'),
                      textDirection:
                          localization.currentLanguage.toString() == "en"
                              ? TextDirection.ltr
                              : TextDirection.rtl,
                    ),
                    actions: <Widget>[
                      FlatButton(
                        child: Text(
                          localization.text('no'),
                        ),
                        onPressed: () {
                          Navigator.of(ctx).pop(false);
                        },
                      ),
                      FlatButton(
                        child: Text(localization.text('ok')),
                        onPressed: () {
                          Provider.of<GetMyFavAdsProvider>(context,
                                  listen: false)
                              .deleteFavAd(
                                  context,
                                  true,
                                  adId,
                                  Provider.of<SharedPref>(context,
                                          listen: false)
                                      .token);
                          Navigator.of(ctx).pop(true);
                        },
                      ),
                    ],
                  ));
        },
        background: Container(
          color: Theme.of(context).errorColor,
          child: Icon(
            Icons.delete,
            color: Colors.white,
            size: 40,
          ),
          alignment: Alignment.centerRight,
          padding: EdgeInsets.only(right: 20),
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
        ),
        child: Container(
            height: MediaQuery.of(context).size.height * 0.2,
            width: MediaQuery.of(context).size.width,
            child: Card(
              elevation: 10,
              clipBehavior: Clip.antiAlias,
              child: Container(
                height: 120,
                padding: const EdgeInsets.all(0),
                child: Row(children: [
                  Expanded(
                    flex: 6,
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: CachedNetworkImageProvider(
                              photo,
                            ),
                            fit: BoxFit.fill),
                      ),
                    ),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  Expanded(
                    flex: 14,
                    child: Container(
                      padding: const EdgeInsets.only(top: 5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(title,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 20.0, fontWeight: FontWeight.bold)),
                          Row(
                            children: <Widget>[
                              Text(
                                '${localization.text('price')} : ',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15),
                              ),
                              Text(
                                '${intl.NumberFormat.currency(locale: localization.currentLanguage.toString() == "en" ? null : 'ar_EG', name: Provider.of<SharedPref>(context, listen: false).currency, decimalDigits: 0).format(price)}',
                                style: TextStyle(fontSize: 15),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              userPhoto != null
                                  ? CachedNetworkImage(
                                      imageUrl: userPhoto,
                                      fadeInDuration: Duration(seconds: 2),
                                      placeholder: (context, url) =>
                                          CircleAvatar(
                                              radius: 12,
                                              backgroundImage:
                                                  AssetImage('images/16.jpg')),
                                      imageBuilder: (context, provider) {
                                        return CircleAvatar(
                                            radius: 12,
                                            backgroundImage: provider);
                                      },
                                    )
                                  : CircleAvatar(
                                      radius: 12,
                                      backgroundImage:
                                          AssetImage('images/16.jpg')),
                              SizedBox(
                                width: 5,
                              ),
                              Container(
                                alignment:
                                    localization.currentLanguage.toString() ==
                                            "en"
                                        ? Alignment.bottomLeft
                                        : Alignment.bottomRight,
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: Text(userName,
                                    overflow: TextOverflow.ellipsis,
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                              ),
                            ],
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Row(
                              children: <Widget>[
                                Text(
                                  city,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Expanded(child: Text("")),
                                Text(
                                  '${localization.text('ago')} ${DateTime.now().difference(DateTime(createdAt.year, createdAt.month, createdAt.day)).inDays} ${localization.text('day')}',
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ]),
              ),
            )),
      ),
    );
  }
}
