import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/post/getAdsFilterByCity.dart' as a7a;
import 'package:rentstation/provider/get/addAdToArchiveProvider.dart';
import 'package:rentstation/provider/get/addAdToFavProvider.dart';
import 'package:rentstation/provider/get/getMyFavAdsProvider.dart';
import 'package:rentstation/screens/adDetails.dart';
import 'package:rentstation/screens/userProfile.dart';
import 'package:intl/intl.dart' as intel;
import 'package:provider/provider.dart';

class AdItem extends StatefulWidget {
  final List<a7a.Photo> image;
  final int id;
  final String title;
  final String desc;
  final int price;
  final String photo;
  final int sale;

  final int countryId;
  final String country;

  final int pinned;
  final String mobileNumber;
  final DateTime date;
  final String currency;
  final int userId;
  final String city;
  final DateTime createdAt;

  final Key key;
  final product;
  final String favouritecheck;
  final String userName;
  final String userPhoto;
  AdItem(
      {this.image,
      this.price,
      this.pinned,
      this.countryId,
      this.favouritecheck,
      this.createdAt,
      this.userId,
      this.sale,
      this.currency,
      this.product,
      this.mobileNumber,
      this.country,
      this.title,
      this.userName,
      this.key,
      this.date,
      this.userPhoto,
      this.photo,
      this.city,
      this.id,
      this.desc});
  @override
  _AdItemState createState() => _AdItemState();
}

class _AdItemState extends State<AdItem> {
  bool isFavourite = true;

  // CustomDialog _dialog = CustomDialog();
  @override
  void didChangeDependencies() {
    print(widget.countryId);
    if (widget.favouritecheck == '1') {
      setState(() {
        isFavourite = true;
      });
    } else {
      setState(() {
        isFavourite = false;
      });
    }

    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      key: widget.key,
      onTap: () {
        pushNewScreen(
          context,
          screen: AdDetails(
            id: widget.id,
          ),
          withNavBar: false, // OPTIONAL VALUE. True by default.
          pageTransitionAnimation: PageTransitionAnimation.cupertino,
        );

        // Navigator.of(context).push(MaterialPageRoute(
        //     builder: (context) => AdDetails(
        //           id: widget.id,
        //         )));
      },
      child: Card(
        elevation: 4,
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Stack(clipBehavior: Clip.none, children: [
                  Card(
                    elevation: 2,
                    child: Container(
                      width: MediaQuery.of(context).size.width / 2,
                      height: MediaQuery.of(context).size.height / 5,
                      child: GridTile(
                        child: widget.photo != null
                            ? CachedNetworkImage(
                                imageUrl: widget.photo,
                                fadeInDuration: Duration(seconds: 2),
                                placeholder: (context, url) => Container(
                                    decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage('images/loading.gif'),
                                      fit: BoxFit.fill),
                                )),
                                imageBuilder: (context, provider) {
                                  return Container(
                                      decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: provider, fit: BoxFit.fill),
                                  ));
                                },
                              )
                            : Container(
                                decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('images/loading.gif'),
                                    fit: BoxFit.fill),
                              )),
                        footer: GridTileBar(
                          backgroundColor: Colors.black45,
                          leading: Provider.of<SharedPref>(context,
                                          listen: false)
                                      .id ==
                                  widget.userId
                              ? Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.06,
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.archive,
                                    ),
                                    onPressed: () {
                                      showDialog(
                                        context: context,
                                        builder: (ctx) => AlertDialog(
                                          title: Text(
                                            localization.text("Are_you_sure"),
                                            textDirection: localization
                                                        .currentLanguage
                                                        .toString() ==
                                                    "en"
                                                ? TextDirection.ltr
                                                : TextDirection.rtl,
                                          ),
                                          content: Text(
                                            localization.text(
                                                'Are_you_sure_from_Archieve_this_ad'),
                                            textDirection: localization
                                                        .currentLanguage
                                                        .toString() ==
                                                    "en"
                                                ? TextDirection.ltr
                                                : TextDirection.rtl,
                                          ),
                                          actions: <Widget>[
                                            FlatButton(
                                              child:
                                                  Text(localization.text('No')),
                                              onPressed: () {
                                                Navigator.of(ctx).pop(false);
                                              },
                                            ),
                                            FlatButton(
                                              child: Text(
                                                  localization.text('yes')),
                                              onPressed: () async {
                                                Provider.of<AddAdToArchProvider>(
                                                        context,
                                                        listen: false)
                                                    .addAdToarch(
                                                        Provider.of<SharedPref>(
                                                                context,
                                                                listen: false)
                                                            .token,
                                                        widget.id,
                                                        context);
                                                Navigator.of(ctx).pop(true);
                                              },
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                    color: Colors.white,
                                  ),
                                )
                              : Container(),
                          title: AutoSizeText(
                            '${intel.NumberFormat.currency(locale: localization.currentLanguage.toString() == "en" ? null : 'ar_EG', name: widget.currency, decimalDigits: 0).format(widget.price)}',
                            softWrap: true,
                            maxLines: 1,
                            minFontSize: 20,
                            maxFontSize: 30,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ]),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.4,
                          child: Center(
                            child: Text(
                              widget.title,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          width: MediaQuery.of(context).size.width * 0.4,
                          child: GestureDetector(
                            onTap: () {
                              if (Provider.of<SharedPref>(context,
                                          listen: false)
                                      .token ==
                                  null) {
                                // _dialog.showOptionDialog(
                                //     context: context,
                                //     msg: localization.text('auth'),
                                //     okFun: () {
                                //       pushNewScreen(
                                //         context,
                                //         screen: SignInScreen(),
                                //         withNavBar:
                                //             false, // OPTIONAL VALUE. True by default.
                                //         pageTransitionAnimation:
                                //             PageTransitionAnimation.cupertino,
                                //       );
                                //       // Navigator.push(
                                //       //     context,
                                //       //     MaterialPageRoute(
                                //       //         builder: (c) =>
                                //       //             SignInScreen()));
                                //     },
                                //     okMsg: localization.text('yes'),
                                //     cancelMsg: localization.text('no'),
                                //     cancelFun: () {
                                //       return;
                                //     });
                              } else {
                                pushNewScreen(
                                  context,
                                  screen: UserProfile(
                                    userid: widget.userId,
                                  ),
                                  withNavBar:
                                      false, // OPTIONAL VALUE. True by default.
                                  pageTransitionAnimation:
                                      PageTransitionAnimation.cupertino,
                                );
                                // Navigator.of(context).push(MaterialPageRoute(
                                //     builder: (context) => UserProfile(
                                //           userid: widget.userId,
                                //         )));
                              }
                            },
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  widget.userPhoto != null
                                      ? CachedNetworkImage(
                                          imageUrl: widget.userPhoto,
                                          fadeInDuration: Duration(seconds: 2),
                                          placeholder: (context, url) =>
                                              CircleAvatar(
                                                  radius: 8,
                                                  backgroundImage: AssetImage(
                                                      'images/16.jpg')),
                                          imageBuilder: (context, provider) {
                                            return CircleAvatar(
                                                radius: 8,
                                                backgroundImage: provider);
                                          },
                                        )
                                      : CircleAvatar(
                                          radius: 8,
                                          backgroundImage:
                                              AssetImage('images/16.jpg')),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(widget.userName,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      )),
                                ]),
                          ),
                        ),
                        Expanded(
                          child: Row(children: [
                            Row(
                              children: [
                                Icon(
                                  Icons.location_on,
                                  size: 15,
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.14,
                                  child: Text(widget.city,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold)),
                                ),
                              ],
                            ),
                            Expanded(child: Text('')),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.12,
                              child: FittedBox(
                                child: Text(
                                    '${localization.text('ago')} ${DateTime.now().difference(DateTime(widget.createdAt.year, widget.createdAt.month, widget.createdAt.day)).inDays} ${localization.text('day')}',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 13),
                                    textAlign: localization.currentLanguage
                                                .toString() ==
                                            "en"
                                        ? TextAlign.right
                                        : TextAlign.left),
                              ),
                            ),
                            Icon(
                              Icons.timer,
                              size: 13,
                            )
                          ]),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            if (widget.sale == 1)
              Positioned(
                child: Container(
                  color: Colors.red,
                  child: Center(
                      child: FittedBox(
                    child: Text(
                      localization.text('price_status'),
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )),
                ),
                height: 50,
                left: 10,
              ),
            if (widget.pinned == 1)
              Positioned(
                child: Container(
                  color: Colors.white70,
                  child: Center(
                    child: Row(children: [
                      Icon(
                        Icons.star,
                        color: Colors.red,
                      ),
                      Text(
                        localization.text('Special'),
                        style: TextStyle(
                          color: Colors.red,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ]),
                  ),
                ),
                top: MediaQuery.of(context).size.height * 0.115,
              ),
            Positioned(
              child: IconButton(
                onPressed: () async {
                  if (Provider.of<SharedPref>(context, listen: false).token ==
                      null) {
                    // _dialog.showOptionDialog(
                    //     context: context,
                    //     msg: localization.text('auth'),
                    //     okFun: () {
                    //             pushNewScreen(
                    //               context,
                    //               screen:SignInScreen(),
                    //               withNavBar:
                    //                   false, // OPTIONAL VALUE. True by default.
                    //               pageTransitionAnimation:
                    //                   PageTransitionAnimation.cupertino,
                    //             );
                    //       // Navigator.push(
                    //       //     context,
                    //       //     MaterialPageRoute(
                    //       //         builder: (c) => SignInScreen()));
                    //     },
                    //     okMsg: localization.text('yes'),
                    //     cancelMsg: localization.text('no'),
                    //     cancelFun: () {
                    //       return;
                    //     });
                  } else if (isFavourite) {
                    await Provider.of<GetMyFavAdsProvider>(context,
                            listen: false)
                        .deleteFavAd(
                      context,
                      false,
                      widget.id,
                      Provider.of<SharedPref>(context, listen: false).token,
                    );
                    setState(() {
                      isFavourite = false;
                    });
                  } else {
                    await Provider.of<AddAdToFavProvider>(context,
                            listen: false)
                        .addAdToFav(
                            Provider.of<SharedPref>(context, listen: false)
                                .token,
                            widget.id,
                            context);
                    setState(() {
                      isFavourite = true;
                    });
                  }
                },
                icon:
                    Icon(isFavourite ? Icons.favorite : Icons.favorite_border),
                color: Colors.red,
              ),
              height: 40,
              right: 5,
            ),
          ],
        ),
      ),
    );
  }
}
