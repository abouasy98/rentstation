import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';

import 'package:rentstation/models/get/getCitiesModel.dart' as ads;
import 'package:rentstation/models/post/getAdsFilterByCity.dart' as a7a;
import 'package:rentstation/provider/get/getAddsProvider.dart';
import 'package:rentstation/provider/get/subCategoriesProvider.dart';
import 'package:rentstation/widgets/adItemGridView.dart';
import 'package:rentstation/widgets/searchAd.dart';
import 'package:provider/provider.dart';
import 'aditem.dart';
import 'package:fluttericon/mfg_labs_icons.dart';

class Adsbody extends StatefulWidget {
  String subCategory;
  int categoryId;
  int subCategoryId;
  final Key key;
  String query = "";
  // GetAdsModel getAddsModel;
  bool showFavourite;
  a7a.GetAddsByCityFilter getAddsByFilterModel;
  final ads.GetCities getCitiesByCountryIdModel;
  String category;

  Adsbody({
    this.key,
    this.categoryId,
    //  this.getAddsModel,
    this.query,
    this.getAddsByFilterModel,
    this.getCitiesByCountryIdModel,
    this.subCategoryId,
    this.showFavourite,
    this.category,
    this.subCategory,
  });
  @override
  _AdsbodyState createState() => _AdsbodyState();
}

class _AdsbodyState extends State<Adsbody> {
  GetAddsProvider getAdsProvider;

  bool _gridView = true;
  bool isInit = true;
  String dropVal1 = localization.text('price_high_to_low');
  bool myAds = false;
  List<a7a.Datum> productsAds;
  String city = localization.text("choose_city");
  SubCategoriesProvider subCategories;
  String _selected = localization.text("modern_to_old");

  List _items = [
    localization.text('price_high_to_low'),
    localization.text('price_low_to_high'),
    localization.text('old_to_modern'),
    localization.text("modern_to_old")
  ];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.getAddsByFilterModel.data != null) {
      productsAds = widget.category == 'الكل'
          ? widget.getAddsByFilterModel.data
              .where((element) =>
                  element.countryId ==
                  Provider.of<SharedPref>(context, listen: false).countryId)
              .toList()
          : widget.subCategory.isNotEmpty
              ? widget.getAddsByFilterModel.data
                  .where((element) =>
                      element.subCategoryId == widget.subCategoryId)
                  .toList()
              : widget.getAddsByFilterModel.data
                  .where((element) => element.categoryId == widget.categoryId)
                  .toList();

      sort();

      productsAds.sort(
        (family1, family2) => family2.pinned.compareTo(
          family1.pinned,
        ),
      );
    } else {
      print('a7teeeeeeeeeen');
      productsAds = [];
    }
  }

  void sort() {
    if (_selected == localization.text("modern_to_old")) {
      productsAds.sort(
        (family1, family2) => family1.createdAt.compareTo(
          family2.createdAt,
        ),
      );
    }
    if (_selected == localization.text('old_to_modern')) {
      productsAds.sort(
        (family1, family2) => family2.createdAt.compareTo(
          family1.createdAt,
        ),
      );
    } else if (_selected == localization.text('price_high_to_low')) {
      productsAds.sort(
        (family1, family2) => family2.price.compareTo(
          family1.price,
        ),
      );
    } else if (_selected == localization.text('price_low_to_high')) {
      productsAds.sort(
        (family1, family2) => family1.price.compareTo(
          family2.price,
        ),
      );
    }
  }

  mainBottomSheet(BuildContext context) {
    return showModalBottomSheet<dynamic>(
        isScrollControlled: false,
        backgroundColor: Colors.white,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        builder: (BuildContext bc) {
          return Directionality(
            textDirection: TextDirection.rtl,
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: ListView(children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        if (widget.getAddsByFilterModel.data != null) {
                          setState(() {
                            city = localization.text('all');
                            productsAds = widget.getAddsByFilterModel.data
                                .where((element) =>
                                    element.countryId ==
                                    Provider.of<SharedPref>(context,
                                            listen: false)
                                        .countryId)
                                .toList();
                          });
                        }
                        Navigator.of(context).pop(widget.category);
                      },
                      child: Text(
                        localization.text('all'),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                    Divider(
                      height: 1,
                      color: Colors.grey,
                    ),
                    Container(
                      color: Colors.white,
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemCount: widget.getCitiesByCountryIdModel.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              ListTile(
                                  onTap: () {
                                    print(
                                        'cityid=${widget.getCitiesByCountryIdModel.data[index].id}');
                                    if (widget.getAddsByFilterModel.data !=
                                        null) {
                                      setState(() {
                                        // productsAds = widget
                                        //     .getAddsByFilterModel.data
                                        //     .where((element) =>
                                        //         element.countryId ==
                                        //         Provider.of<SharedPref>(context,
                                        //                 listen: false)
                                        //             .countryId)
                                        //     .toList();
                                        city = widget.getCitiesByCountryIdModel
                                            .data[index].name;
                                        productsAds = widget
                                            .getAddsByFilterModel.data
                                            .where((element) =>
                                                element.countryId ==
                                                Provider.of<SharedPref>(context,
                                                        listen: false)
                                                    .countryId)
                                            .toList();
                                        productsAds = productsAds
                                            .where((element) =>
                                                element.cityId ==
                                                widget.getCitiesByCountryIdModel
                                                    .data[index].id)
                                            .toList();
                                      });
                                    }
                                    Navigator.of(context).pop(widget
                                        .getCitiesByCountryIdModel
                                        .data[index]
                                        .name);
                                  },
                                  title: Text(
                                    widget.getCitiesByCountryIdModel.data[index]
                                        .name,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )),
                              Divider(
                                height: 1,
                                color: Colors.grey,
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          );
        });
  }

  sortBottomSheet(BuildContext context) {
    return showModalBottomSheet<dynamic>(
        isScrollControlled: false,
        backgroundColor: Colors.white,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        builder: (BuildContext bc) {
          return Directionality(
            textDirection: TextDirection.rtl,
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: ListView(children: <Widget>[
                    Container(
                      color: Colors.white,
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemCount: _items.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              ListTile(
                                  onTap: () {
                                    setState(() {
                                      _selected = _items[index];
                                    });
                                    sort();
                                    Navigator.of(context).pop(_items[index]);
                                  },
                                  title: Text(
                                    _items[index],
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )),
                              Divider(
                                height: 1,
                                color: Colors.grey,
                              )
                            ],
                          );
                        },
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          );
        });
  }

  Widget headerBuild() {
    return Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 10, left: 18, right: 18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  setState(() {
                    _gridView = !_gridView;
                  });
                },
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.05,
                  child: Center(
                    child: Icon(
                      !_gridView
                          ? MfgLabs.th_thumb
                          : Icons.format_list_bulleted,
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.08,
              ),
              TextButton(
                  child: Text(city),
                  onPressed: () {
                    mainBottomSheet(context);
                  }),

              SizedBox(
                width: MediaQuery.of(context).size.width * 0.04,
              ),
              // Container(
              //   width: MediaQuery.of(context).size.width * 0.21,
              //   height: MediaQuery.of(context).size.height * 0.04,
              //   decoration: BoxDecoration(
              //     color: Colors.blueAccent,
              //     borderRadius: BorderRadius.circular(20),
              //   ),
              //   child: FlatButton(
              //       child: FittedBox(
              //         child: Text(
              //           Provider.of<SharedPref>(context, listen: false).token ==
              //                   null
              //               ? localization.text('all_ads')
              //               : !myAds
              //                   ? localization.text('my_ads_only')
              //                   : localization.text('all_ads'),
              //           style: TextStyle(
              //               color: Colors.white,
              //               fontSize: 12,
              //               fontWeight: FontWeight.bold),
              //         ),
              //       ),
              //       onPressed: () {
              //         setState(() {
              //           if (Provider.of<SharedPref>(context, listen: false)
              //                   .token ==
              //               null) {
              //             productsAds = widget.getAddsByFilterModel.data
              //                 .where((element) =>
              //                     element.countryId ==
              //                     Provider.of<SharedPref>(context,
              //                             listen: false)
              //                         .countryId)
              //                 .toList();
              //           } else if (!myAds) {
              //             productsAds = productsAds
              //                 .where((element) =>
              //                     element.userId ==
              //                     Provider.of<SharedPref>(context,
              //                             listen: false)
              //                         .id)
              //                 .toList();
              //             myAds = true;
              //           } else {
              //             productsAds = widget.getAddsByFilterModel.data
              //                 .where((element) =>
              //                     element.countryId ==
              //                     Provider.of<SharedPref>(context,
              //                             listen: false)
              //                         .countryId)
              //                 .toList();
              //             myAds = false;
              //           }
              //         });
              //       }),
              // ),
            ],
          ),
          Expanded(child: Text('')),
          GestureDetector(
            onTap: () {
              sortBottomSheet(context);
            },
            child: Container(
              height: MediaQuery.of(context).size.height * 0.045,
              width: MediaQuery.of(context).size.width * 0.48,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey,
                  width: 1,
                ),
                shape: BoxShape.rectangle,
              ),
              child: Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      _selected,
                      style: TextStyle(fontSize: 13),
                    ),
                    Icon(Icons.arrow_drop_down),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      if (widget.query == "") headerBuild(),
      Expanded(
        child: ((productsAds == null) || (productsAds.length <= 0))
            ? Container(
                height: MediaQuery.of(context).size.height * 0.3,
                child: Text(
                  localization.text('no_results'),
                  style: TextStyle(color: Colors.black),
                ),
              )
            : widget.query == ''
                ? _gridView
                    ? GridView.builder(
                        //reverse: true,

                        primary: false,
                        shrinkWrap: true,
                        itemCount: productsAds.length,
                        itemBuilder: (context, i) {
                          return AdItem(
                            photo: productsAds[i].photos.length == 0
                                ? ''
                                : productsAds[i].photos[0].photo,
                            image: productsAds[i].photos,
                            price: productsAds[i].price,
                            sale: productsAds[i].priceStatus,
                            country: productsAds[i].country,
                            createdAt: productsAds[i].createdAt,
                            title: productsAds[i].title,
                            desc: productsAds[i].description,
                            id: productsAds[i].id,
                            favouritecheck: productsAds[i].favorite,
                            pinned: productsAds[i].pinned,
                            key: ValueKey('key'),
                            countryId: productsAds[i].countryId,
                            product: productsAds[i],
                            userId: productsAds[i].userId,
                            city: productsAds[i].city,
                            userPhoto: productsAds[i].userPhoto,
                            mobileNumber: productsAds[i].phoneNumber,
                            currency: productsAds[i].currency,
                            userName: productsAds[i].user,
                            date: productsAds[i].createdAt,
                          );
                        },

                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            childAspectRatio: 0.7,
                            crossAxisCount: 2,
                            crossAxisSpacing: 5,
                            mainAxisSpacing: 5),
                      )
                    : ListView.builder(
                        //reverse: true,

                        primary: false,
                        shrinkWrap: true,
                        itemCount: productsAds.length,
                        itemBuilder: (context, i) {
                          return AdItemGridView(
                            photo: productsAds[i].photos.length == 0
                                ? ''
                                : productsAds[i].photos[0].photo,
                            image: productsAds[i].photos,
                            price: productsAds[i].price,
                            sale: productsAds[i].priceStatus,
                            country: productsAds[i].country,
                            createdAt: productsAds[i].createdAt,
                            title: productsAds[i].title,
                            desc: productsAds[i].description,
                            id: productsAds[i].id,
                            favouritecheck: productsAds[i].favorite,
                            pinned: productsAds[i].pinned,
                            categoryId: productsAds[i].categoryId,
                            key: ValueKey('key'),
                            category: productsAds[i].category,
                            countryId: productsAds[i].countryId,
                            product: productsAds[i],
                            userId: productsAds[i].userId,
                            city: productsAds[i].city,
                            userPhoto: productsAds[i].userPhoto,
                            mobileNumber: productsAds[i].phoneNumber,
                            currency: productsAds[i].currency,
                            userName: productsAds[i].user,
                            date: productsAds[i].createdAt,
                          );
                        },
                      )
                : SearchAdsIteams(
                    ads: productsAds,
                    gridView: _gridView,
                    getAddsByFilterModel: widget.getAddsByFilterModel,
                    query: widget.query,
                  ),
      ),
    ]);
  }
}
