// import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';
// import 'package:fluttericon/font_awesome_icons.dart';
// import 'package:herag/Repository/appLocalization.dart';
// import 'package:herag/helpers/sharedPref_helper.dart';

// import 'package:herag/models/get/getAdsWithoutAuth.dart'as getAdsWithoutAuth;
// import 'package:herag/models/get/getCitiesModel.dart' as ads;
// import 'package:herag/provider/get/getAddsProvider.dart';
// import 'package:herag/provider/get/subCategoriesProvider.dart';
// import 'package:herag/widgets/searchAd.dart';
// import 'package:provider/provider.dart';
// import 'aditem.dart';
// import 'package:fluttericon/mfg_labs_icons.dart';

// import 'aditemWithOutAuth.dart';

// class AdsbodywithOutAuth extends StatefulWidget {
//   String subCategory;
// getAdsWithoutAuth.GetAdsWithoutAuth getAddsModel;
//   bool showFavourite;
//   String query = "";
//   final ads.GetCities getCitiesByCountryIdModel;
//   final String category;

//   int categoryId;
//   int subCategoryId;
//   final Key key;
//   AdsbodywithOutAuth({
//     this.key,
//     this.categoryId,
//     this.getAddsModel,
//     this.query,
//     this.getCitiesByCountryIdModel,
//     this.subCategoryId,
//     this.showFavourite,
//     this.category,
//     this.subCategory,
//   });
//   @override
//   _AdsbodywithOutAuthState createState() => _AdsbodywithOutAuthState();
// }

// class _AdsbodywithOutAuthState extends State<AdsbodywithOutAuth> {
//   GetAddsProvider getAdsProvider;

//   bool _gridView = true;
//   bool isInit = true;
//   String dropVal1 = localization.text('price_high_to_low');
//   bool myAds = false;
//   List<getAdsWithoutAuth.Datum> productsAds;

//   SubCategoriesProvider subCategories;
//   String _selected = localization.text("modern_to_old");

//   List _items = [
//     localization.text('price_high_to_low'),
//     localization.text('price_low_to_high'),
//     localization.text('old_to_modern'),
//     localization.text("modern_to_old")
//   ];
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     print(
//         'countryId=${Provider.of<SharedPref>(context, listen: false).countryId}');
//     productsAds = widget.category == 'الكل'
//         ? widget.getAddsModel.data
//             .where((element) =>
//                 element.countryId ==
//                 Provider.of<SharedPref>(context, listen: false).countryId)
//             .toList()
//         : widget.getAddsModel.data != null
//             ? widget.subCategory.isNotEmpty
//                 ? widget.getAddsModel.data
//                     .where((element) =>
//                         element.subCategoryId == widget.subCategoryId)
//                     .toList()
//                 : widget.getAddsModel.data
//                     .where((element) => element.categoryId == widget.categoryId)
//                     .toList()
//             : widget.getAddsModel.data
//                 .where((element) =>
//                     element.countryId ==
//                     Provider.of<SharedPref>(context, listen: false).countryId)
//                 .toList();
//     sort();
//     productsAds.sort(
//       (family1, family2) => family2.pinned.compareTo(
//         family1.pinned,
//       ),
//     );
//   }

//   void sort() {
//     if (_selected == localization.text("modern_to_old")) {
//       productsAds.sort(
//         (family1, family2) => family1.createdAt.compareTo(
//           family2.createdAt,
//         ),
//       );
//     }
//     if (_selected == localization.text('old_to_modern')) {
//       productsAds.sort(
//         (family1, family2) => family2.createdAt.compareTo(
//           family1.createdAt,
//         ),
//       );
//     } else if (_selected == localization.text('price_high_to_low')) {
//       productsAds.sort(
//         (family1, family2) => family2.price.compareTo(
//           family1.price,
//         ),
//       );
//     } else if (_selected == localization.text('price_low_to_high')) {
//       productsAds.sort(
//         (family1, family2) => family1.price.compareTo(
//           family2.price,
//         ),
//       );
//     }
//   }

//   mainBottomSheet(BuildContext context) {
//     return showModalBottomSheet<dynamic>(
//         isScrollControlled: false,
//         backgroundColor: Colors.white,
//         context: context,
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.only(
//               topLeft: Radius.circular(20), topRight: Radius.circular(20)),
//         ),
//         builder: (BuildContext bc) {
//           return Directionality(
//             textDirection: TextDirection.rtl,
//             child: Stack(
//               children: <Widget>[
//                 Padding(
//                   padding: const EdgeInsets.only(top: 20.0),
//                   child: ListView(children: <Widget>[
//                     Container(
//                       color: Colors.white,
//                       child: ListView.builder(
//                         shrinkWrap: true,
//                         physics: ScrollPhysics(),
//                         itemCount: widget.getCitiesByCountryIdModel.data.length,
//                         itemBuilder: (BuildContext context, int index) {
//                           return Column(
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             children: <Widget>[
//                               ListTile(
//                                   onTap: () {
//                                     print(
//                                         'cityid=${widget.getCitiesByCountryIdModel.data[index].id}');

//                                     setState(() {
//                                       productsAds = widget.getAddsModel.data
//                                           .where((element) =>
//                                               element.countryId ==
//                                               Provider.of<SharedPref>(context,
//                                                       listen: false)
//                                                   .countryId)
//                                           .toList();

//                                       productsAds = productsAds
//                                           .where((element) =>
//                                               element.cityId ==
//                                               widget.getCitiesByCountryIdModel
//                                                   .data[index].id)
//                                           .toList();
//                                     });

//                                     Navigator.of(context).pop(widget
//                                         .getCitiesByCountryIdModel
//                                         .data[index]
//                                         .name);
//                                   },
//                                   title: Text(
//                                     widget.getCitiesByCountryIdModel.data[index]
//                                         .name,
//                                     textAlign: TextAlign.center,
//                                     style: TextStyle(
//                                       fontWeight: FontWeight.bold,
//                                     ),
//                                   )),
//                               Divider(
//                                 height: 1,
//                                 color: Colors.grey,
//                               )
//                             ],
//                           );
//                         },
//                       ),
//                     ),
//                   ]),
//                 ),
//               ],
//             ),
//           );
//         });
//   }

//   sortBottomSheet(BuildContext context) {
//     return showModalBottomSheet<dynamic>(
//         isScrollControlled: false,
//         backgroundColor: Colors.white,
//         context: context,
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.only(
//               topLeft: Radius.circular(20), topRight: Radius.circular(20)),
//         ),
//         builder: (BuildContext bc) {
//           return Directionality(
//             textDirection: TextDirection.rtl,
//             child: Stack(
//               children: <Widget>[
//                 Padding(
//                   padding: const EdgeInsets.only(top: 20.0),
//                   child: ListView(children: <Widget>[
//                     Container(
//                       color: Colors.white,
//                       child: ListView.builder(
//                         shrinkWrap: true,
//                         physics: ScrollPhysics(),
//                         itemCount: _items.length,
//                         itemBuilder: (BuildContext context, int index) {
//                           return Column(
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             children: <Widget>[
//                               ListTile(
//                                   onTap: () {
//                                     setState(() {
//                                       _selected = _items[index];
//                                     });
//                                     sort();
//                                     Navigator.of(context).pop(_items[index]);
//                                   },
//                                   title: Text(
//                                     _items[index],
//                                     textAlign: TextAlign.center,
//                                     style: TextStyle(
//                                       fontWeight: FontWeight.bold,
//                                     ),
//                                   )),
//                               Divider(
//                                 height: 1,
//                                 color: Colors.grey,
//                               )
//                             ],
//                           );
//                         },
//                       ),
//                     ),
//                   ]),
//                 ),
//               ],
//             ),
//           );
//         });
//   }

//   Widget headerBuild() {
//     return Padding(
//       padding: const EdgeInsets.only(top: 10, bottom: 10, left: 18, right: 18),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.start,
//         children: [
//           Row(
//             mainAxisAlignment: MainAxisAlignment.start,
//             children: [
//               InkWell(
//                 onTap: () {
//                   setState(() {
//                     _gridView = !_gridView;
//                   });
//                 },
//                 child: Container(
//                   width: MediaQuery.of(context).size.width * 0.05,
//                   child: Center(
//                     child: Icon(
//                       !_gridView
//                           ? MfgLabs.th_thumb
//                           : Icons.format_list_bulleted,
//                       color: Colors.blueAccent,
//                     ),
//                   ),
//                 ),
//               ),
//               SizedBox(
//                 width: MediaQuery.of(context).size.width * 0.01,
//               ),
//               Container(
//                 width: MediaQuery.of(context).size.width * 0.05,
//                 child: IconButton(
//                     icon: Icon(
//                       FontAwesome.filter,
//                       color: Colors.blueAccent,
//                     ),
//                     onPressed: () {
//                       mainBottomSheet(context);
//                     }),
//               ),
//               SizedBox(
//                 width: MediaQuery.of(context).size.width * 0.04,
//               ),
//               Container(
//                 width: MediaQuery.of(context).size.width * 0.21,
//                 height: MediaQuery.of(context).size.height * 0.04,
//                 decoration: BoxDecoration(
//                   color: Colors.blueAccent,
//                   borderRadius: BorderRadius.circular(20),
//                 ),
//                 child: FlatButton(
//                     child: FittedBox(
//                       child: Text(
//                         !myAds
//                             ? localization.text('my_ads_only')
//                             : localization.text('all_ads'),
//                         style: TextStyle(
//                             color: Colors.white,
//                             fontSize: 12,
//                             fontWeight: FontWeight.bold),
//                       ),
//                     ),
//                     onPressed: () {
//                       setState(() {
//                         if (!myAds) {
//                           productsAds = productsAds
//                               .where((element) =>
//                                   element.userId ==
//                                   Provider.of<SharedPref>(context,
//                                           listen: false)
//                                       .id)
//                               .toList();
//                           myAds = true;
//                         } else {
//                           productsAds = widget.getAddsModel.data
//                               .where((element) =>
//                                   element.countryId ==
//                                   Provider.of<SharedPref>(context,
//                                           listen: false)
//                                       .countryId)
//                               .toList();
//                           myAds = false;
//                         }
//                       });
//                     }),
//               )
//             ],
//           ),
//           Expanded(child: Text('')),
//           GestureDetector(
//             onTap: () {
//               sortBottomSheet(context);
//             },
//             child: Container(
//               height: MediaQuery.of(context).size.height * 0.045,
//               width: MediaQuery.of(context).size.width * 0.48,
//               decoration: BoxDecoration(
//                 border: Border.all(
//                   color: Colors.grey,
//                   width: 1,
//                 ),
//                 shape: BoxShape.rectangle,
//               ),
//               child: Padding(
//                 padding: const EdgeInsets.only(right: 8.0),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Text(
//                       _selected,
//                       style: TextStyle(fontSize: 13),
//                     ),
//                     Icon(Icons.arrow_drop_down),
//                   ],
//                 ),
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Column(children: [
//       headerBuild(),
//       Expanded(
//         child: ((productsAds == null) || (productsAds.length <= 0))
//             ? Container(
//                 height: MediaQuery.of(context).size.height * 0.6,
//                 child: Center(
//                   child: Text(
//                     localization.text('no_results'),
//                     style: TextStyle(color: Colors.black),
//                   ),
//                 ),
//               )
//             : widget.query == ''
//                 ? GridView.builder(
//                     //reverse: true,
//                     key: UniqueKey(),
//                     primary: false,
//                     shrinkWrap: true,
//                     itemCount: productsAds.length,
//                     itemBuilder: (context, i) {
//                       return AdItemwithOutAuth(
//                         photo: productsAds[i].photos.length == 0
//                             ? ''
//                             : productsAds[i].photos[0].photo,
//                         image: productsAds[i].photos,
//                         price: productsAds[i].price,
//                         sale: productsAds[i].priceStatus,
//                         country: productsAds[i].country,
//                         createdAt: productsAds[i].createdAt,
//                         title: productsAds[i].title,
//                         desc: productsAds[i].description,
//                         id: productsAds[i].id,
//                         favouritecheck: productsAds[i].favorite,
//                         pinned: productsAds[i].pinned,
//                         key: ValueKey('key'),
//                         category: productsAds[i].category,
//                         countryId: productsAds[i].countryId,
//                         product: productsAds[i],
//                         categoryId: productsAds[i].categoryId,
//                         userId: productsAds[i].userId,
//                         city: productsAds[i].city,
//                         gridView: _gridView,
//                         userPhoto: productsAds[i].userPhoto,
//                         mobileNumber: productsAds[i].phoneNumber,
//                         currency: productsAds[i].currency,
//                         userName: productsAds[i].user,
//                         date: productsAds[i].createdAt,
//                       );
//                     },

//                     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                         childAspectRatio: _gridView ? 0.7 : 1.4,
//                         crossAxisCount: _gridView ? 2 : 1,
//                         crossAxisSpacing: 5,
//                         mainAxisSpacing: 5),
//                   )
//                 : SearchAdsIteams(
//                     ads: productsAds,
//                     gridView: _gridView,
//                     key: UniqueKey(),
//                     query: widget.query,
//                   ),
//       ),
//     ]);
//   }
// }
