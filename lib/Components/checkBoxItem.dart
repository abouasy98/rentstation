import 'package:flutter/material.dart';

class CheckBoxItem extends StatefulWidget {
  final String label;
  final ValueChanged<bool> onChange;
  final bool init;
  const CheckBoxItem({Key key, this.label, this.onChange, this.init})
      : super(key: key);

  @override
  _CheckBoxItemState createState() => _CheckBoxItemState();
}

class _CheckBoxItemState extends State<CheckBoxItem> {
  bool _checked = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _checked = widget.init == null ? false : widget.init;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 0, left: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text(
            widget.label,
              style: TextStyle(
                  fontSize: 14,
                  fontFamily: "RB",

                  fontWeight: FontWeight.bold,
                  color: Colors.black)
          ),
          Checkbox(
            value: _checked,
            onChanged: (value) {
              widget.onChange(value);
              setState(() {
                _checked = !_checked;
              });
            },
            activeColor: Theme.of(context).primaryColor,
          ),
        ],
      ),
    );
  }
}
