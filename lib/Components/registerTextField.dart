import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rentstation/Repository/appLocalization.dart';


class RegisterTextField extends StatefulWidget {
  final String label;
  final  icon;
  final TextInputType type;
  final String hint;
  final Function error;
  final Function onChange;
  final String init;
  final int border;
  final int minline;
  final VoidCallback onPressed;
  final align ;
  final password ;
  final int maxline;
  const RegisterTextField(
      {Key key,
      this.icon,
      this.label,
        this.onPressed,
        this.password,
        this.align,
      this.type,
      this.hint,
      this.error,
        this.maxline,
        this.minline,
      this.onChange,
      this.init, this.border})
      : super(key: key);

  @override
  _RegisterTextFieldState createState() => _RegisterTextFieldState();
}

class _RegisterTextFieldState extends State<RegisterTextField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 0),
      child: Directionality(
        textDirection:
        localization.currentLanguage.toString() == "en"
            ? TextDirection.ltr
            :
        TextDirection.rtl,
        child: TextFormField(





            style: TextStyle(
            fontSize: 16,
            fontFamily: "RB",

            fontWeight: FontWeight.bold,
            color: Colors.black54),

          maxLines: widget.maxline,
          minLines: widget.minline,
          keyboardType: widget.type,
          onChanged: widget.onChange,
          initialValue: widget.init,
//          textAlign:
//          localization.currentLanguage.toString() == "en"
//              ? TextAlign.start
//              : TextAlign.end,
//          widget.align ?? ,

          inputFormatters: widget.label == 'رقم الجوال'
              ? <TextInputFormatter>[
                  WhitelistingTextInputFormatter.digitsOnly
                ]
              : <TextInputFormatter>[],
          validator: widget.error,
          decoration: InputDecoration(

            icon: widget.icon ,



            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            hintText: widget.hint == null ? '' : widget.hint,

//            hintStyle:  TextStyle(
//
//                fontSize: 16,
//
//                fontFamily: "RB",
//                fontWeight: FontWeight.bold,
//                color: Colors.grey[400]),
//                suffixIcon: widget.icon != null
//                    ? Padding(
//                        padding: const EdgeInsets.all(6),
//                        child: Container(
//                          decoration: BoxDecoration(
//                              shape: BoxShape.circle,
//                              color: Color.fromRGBO(242, 242, 242, 1)),
//                          child: Icon(widget.icon),
//                        ),
//                      )
//                    : null,
//            contentPadding: EdgeInsets.only(top: 20, right: 10,left: 20),
//                border: widget.border == 0 ? InputBorder.none :new OutlineInputBorder(
//                  borderRadius: const BorderRadius.all(Radius.circular(10)),
//                )
          ),
        ),
      ),
    );
  }
}
