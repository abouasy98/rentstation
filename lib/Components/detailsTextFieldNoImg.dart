import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';


class DetailsTextFieldNoImg extends StatefulWidget {
  final String label;
  final String hint;
  final Function onChange;
  final String init;
  final int border;
  final icon;

  const DetailsTextFieldNoImg(
      {Key key, this.label, this.hint, this.onChange, this.init, this.border,this.icon})
      : super(key: key);

  @override
  _DetailsTextFieldNoImgState createState() => _DetailsTextFieldNoImgState();
}

class _DetailsTextFieldNoImgState extends State<DetailsTextFieldNoImg> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Directionality(
          textDirection: localization.currentLanguage.toString() == "en"
              ? TextDirection.ltr
              : TextDirection.rtl,
          child: TextFormField(
            style: TextStyle(
                fontSize: 16,
                fontFamily: "RB",
                fontWeight: FontWeight.bold,
                color: Colors.black54),
            textDirection: localization.currentLanguage.toString() == "en"
                ? TextDirection.ltr
                : TextDirection.rtl,
            initialValue: widget.init,
            minLines: 3,
            maxLines: null,
            onChanged: widget.onChange,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              icon: widget.icon,
                hintText: widget.hint,
//                      contentPadding:
//                          EdgeInsets.only(top: 20, right: 10, left: 10),
                border: widget.border == 0
                    ? InputBorder.none
                    : new OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                      )),
          ),
        ),
        Positioned(
          right: 20,
          left: 20,
          child: Directionality(
            textDirection: localization.currentLanguage.toString() == "en"
                ? TextDirection.ltr
                : TextDirection.rtl,
            child: Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.only(right: 10, left: 20),
                child: Text(widget.label,
                    style: TextStyle(fontSize: 12, color: Colors.grey)),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
