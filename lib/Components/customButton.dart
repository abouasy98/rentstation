import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  final String text;
  final Function onTap;

  const CustomButton({
    Key key,
    @required this.text,
    @required this.onTap,
  }) : super(key: key);

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 75),
        child: Container(
          height: 50,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Color(0xffE4C02C)),
          child: MaterialButton(
            onPressed: widget.onTap,
//            color: Theme
//                .of(context)
//                .primaryColor,
//            shape:
//            RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Center(
              child: Text(
                widget.text,
                style: TextStyle(
                    fontFamily: 'RB',
                    color: Colors.grey[900],
                    fontWeight: FontWeight.w700,
                    fontSize: 17,
                    letterSpacing: .2),
              ),
            ),
          ),
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 75),
        child: Container(
          height: 50,

          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Color(0xffE4C02C)),
          child: MaterialButton(
            onPressed: widget.onTap,

            child: Center(
              child: Text(
                widget.text,
                style: TextStyle(
                    fontFamily: 'RB',
                    color: Colors.grey[900],
                    fontWeight: FontWeight.w700,
                    fontSize: 17,
                    letterSpacing: .2),
              ),
            ),
          ),
        ),
      );
    }
  }
}
