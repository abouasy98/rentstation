import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/CommsionModel.dart';

class CommisionProvider with ChangeNotifier {
  String content;

  NetworkUtil _utils = new NetworkUtil();
  CommsionModel aboutUsModel;
  Future< CommsionModel> getCommision(BuildContext context) async {
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
    };
    Response response =
        await _utils.get("commission_text", context, headers: headers);
    if (response.statusCode == 200) {
      print("get terms sucsseful");

      aboutUsModel = CommsionModel.fromJson(response.data);
      content = aboutUsModel.data[0].commissionText;

      notifyListeners();
      return  CommsionModel.fromJson(response.data);
    } else {
      print("error get terms data");
      return  CommsionModel.fromJson(response.data);
    }
  }
}
