import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/getAppLogoModel.dart';

class GetAppLogoProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  String name;
  String logo;

  GetAppLogo getTheUndertaking;
  Future<GetAppLogo> getAppLogoProvider(
      BuildContext context) async {
    Response response = await _utils.get(
      "get_app_logo",
      context,
    );
    if (response.statusCode == 200) {
      print("GetAppLogo sucsseful");
      getTheUndertaking = GetAppLogo.fromJson(response.data);
      name = getTheUndertaking.data.appName;
      print("$name");
      logo = getTheUndertaking.data.logo;

      notifyListeners();
      return GetAppLogo.fromJson(response.data);
    } else {
      print("error GetAppLogog data");
      return GetAppLogo.fromJson(response.data);
    }
  }
}
