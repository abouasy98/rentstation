import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/myWinedAuction.dart';

class MyWinedAuctionProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  List<Favourites> _favourite = [];

  List<Favourites> get favourite {
    return [..._favourite];
  }

  clear() {
    myWinedAuction = null;
    notifyListeners();
  }

  MyWinedAuction myWinedAuction;
  Future<MyWinedAuction> getMyWinedAuction(String token,BuildContext context) async {
    final List<Favourites> favouritesAdList = [];
    Map<String, String> headers = {"Authorization": "Bearer $token",  "Accept-Language": localization.currentLanguage.toString(),};
    Response response = await _utils.get("my_wined_auction",context, headers: headers);
    if (response.statusCode == 200) {
      myWinedAuction = MyWinedAuction.fromJson(response.data);
      myWinedAuction.data.forEach((e) {
        favouritesAdList.add(Favourites(
          id: e.id,
          createdAt: e.createdAt,
          city: e.city,
          country: e.country,
          archive: e.archive,
          auctionWinner: e.auctionWinner,
          auctionWinnerId: e.auctionWinnerId,
          auctionWinnerPhoto: e.auctionWinnerPhoto,
          cityId: e.cityId,
          countryId: e.countryId,
          currency: e.currency,
          description: e.description,
          details: e.details,
          email: e.email,
          heighPrice: e.heighPrice,
          initialPrice: e.initialPrice,
          memberDate: e.memberDate,
          name: e.name,
          period: e.period,
          phoneNumber: e.phoneNumber,
          photos: e.photos,
          priceIncrease: e.priceIncrease,
          prices: e.prices,
          user: e.user,
          userPhoto: e.userPhoto,
          userId: e.userId,
          
        ));
      });
      _favourite = favouritesAdList.reversed.toList();
      print("get my_wined_auction sucsseful");
      notifyListeners();
      return MyWinedAuction.fromJson(response.data);
    } else {
      myWinedAuction = MyWinedAuction.fromJson(response.data);
      _favourite = favouritesAdList.reversed.toList();
      print("error get my_wined_auction data");
      return MyWinedAuction.fromJson(response.data);
    }
  }
}

class Favourites {
  Favourites({
    @required this.id,
    @required this.archive,
    @required this.auctionWinner,
    @required this.userId,
    @required this.auctionWinnerId,
    @required this.auctionWinnerPhoto,
    @required this.cityId,
    @required this.countryId,
    @required this.city,
    @required this.country,
    this.currency,
    this.createdAt,
    this.description,
    this.details,
    this.email,
    this.heighPrice,
    this.initialPrice,
    this.memberDate,
    this.name,
    this.period,
    this.phoneNumber,
    this.photos,
    this.priceIncrease,
    this.prices,
    this.user,
    this.userPhoto,

  });

  int id;
  int userId;
  String user;
  String userPhoto;
  String email;
  String phoneNumber;
  DateTime memberDate;
  int countryId;
  String country;
  int cityId;
  String city;
  String name;
  int period;
  String details;
  dynamic description;
  int initialPrice;
  int priceIncrease;
  String currency;
  int archive;
  List<Price> prices;
  List<Photo> photos;
  int auctionWinnerId;
  String auctionWinner;
  String auctionWinnerPhoto;
  int heighPrice;
  DateTime createdAt;
}
