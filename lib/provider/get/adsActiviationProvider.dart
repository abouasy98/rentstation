import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/adActvionsmodel.dart';



class GetAdsActivationProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<AdActivitionmodelDart> getAuctionsActivationProvider(BuildContext context) async {

    Response response = await _utils.get("ad_activation",context,);
    if (response.statusCode == 200) {
      print("get get_ads sucsseful");
      return AdActivitionmodelDart.fromJson(response.data);
    } else {
      print("error get get_ads data");
      return AdActivitionmodelDart.fromJson(response.data);
    }
  }
}
