import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/categoriesModels.dart';

class CategoriesProvider with ChangeNotifier {
  List<CategoriesModel> _categories = [];

  List<CategoriesModel> get categories {
    return [..._categories];
  }

  NetworkUtil _utils = new NetworkUtil();
  CategoriesModels categoriesModel;
  Future<CategoriesModels> getCategories(BuildContext context) async {
    final List<CategoriesModel> loadedCategories = [];
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      
    };
    Response response = await _utils.get("get_categories",context, headers: headers);
    if (response.statusCode == 200) {
      print("get categories sucsseful");

      categoriesModel = CategoriesModels.fromJson(response.data);

      categoriesModel.data.forEach((element) {
        loadedCategories.add(CategoriesModel(
            dateTime: element.createdAt,
            id: element.id,
            photo: element.photo,
            name: element.name));
      });
      _categories = loadedCategories.reversed.toList();
      notifyListeners();
      return CategoriesModels.fromJson(response.data);
    } else {
      print("error get categories data");
      return CategoriesModels.fromJson(response.data);
    }
  }
}

class CategoriesModel {
  final int id;
  final String name;
  final String photo;
  final DateTime dateTime;

  CategoriesModel({@required this.id, @required this.name, @required this.dateTime,this.photo});
}
