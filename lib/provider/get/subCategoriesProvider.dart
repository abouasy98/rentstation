import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/subCategoriesModel.dart';

class SubCategoriesProvider with ChangeNotifier {
  List<SubCategoriesModel> _subcategories = [];

  List<SubCategoriesModel> get subcategories {
    return [..._subcategories];
  }

  NetworkUtil _utils = new NetworkUtil();
  SubCategoriesModels categoriesModel;
  Future<SubCategoriesModels> getSubCategories(int id,BuildContext context) async {
    final List<SubCategoriesModel> loadedCategories = [];
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString()
    };
    Response response =
        await _utils.get("get_sub_categories/$id",context, headers: headers);
    if (response.statusCode == 200) {
      print("get sub_categories sucsseful");

      categoriesModel = SubCategoriesModels.fromJson(response.data);

      categoriesModel.data.forEach((element) {
        loadedCategories.add(SubCategoriesModel(
            dateTime: element.createdAt,
            id: element.id,
            
            name: element.name));
      });
      _subcategories = loadedCategories.reversed.toList();
      notifyListeners();
      return SubCategoriesModels.fromJson(response.data);
    } else {
      print("error get sub_categories data");
      return SubCategoriesModels.fromJson(response.data);
    }
  }
}

class SubCategoriesModel {
  final int id;
  final String name;
  final DateTime dateTime;

  SubCategoriesModel(
      {@required this.id, @required this.name, @required this.dateTime});
}
