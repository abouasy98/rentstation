import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/deletead.dart';
import 'package:rentstation/models/get/myAds.dart';

class GetMyAdsProvider with ChangeNotifier {
  DeleteAdmodel deleteAdModel;
  List<Orders> _myAds = [];

  List<Orders> get myAds {
    return [..._myAds];
  }

  clear() {
    cartsModel = null;
    notifyListeners();
  }

  deleteAd(String token, int id, BuildContext context) async {
    Map<String, String> headers = {
      "Authorization": "Bearer $token",
      "Accept-Language": localization.currentLanguage.toString(),
    };
    Response response =
        await _utils.get("delete_ad/$id", context, headers: headers);
    final exitingDataIndex = _myAds.indexWhere((prod) => prod.id == id);
    var exitingData = _myAds[exitingDataIndex];
    _myAds.removeAt(exitingDataIndex);
    if (response.statusCode == 200) {
      print("get delete_ad sucsseful");
      deleteAdModel = DeleteAdmodel.fromJson(response.data);
    } else {
      print("error get delete_ad data");
      _myAds.insert(exitingDataIndex, exitingData);
      deleteAdModel = DeleteAdmodel.fromJson(response.data);
    }

    notifyListeners();
  }

  NetworkUtil _utils = new NetworkUtil();
  MyAds cartsModel;
  Future<MyAds> getMyAds(String token, BuildContext context) async {
    final List<Orders> loadedAds = [];

    Map<String, String> headers = {
      "Authorization": "Bearer $token",
      "Accept-Language": localization.currentLanguage.toString(),
    };
    Response response = await _utils.get("my_ads", context, headers: headers);
    if (response.statusCode == 200) {
      print("get-myAds sucsseful");

      cartsModel = MyAds.fromJson(response.data);
      cartsModel.data.forEach((e) {
        loadedAds.add(Orders(
          id: e.id,
          createdAt: e.createdAt,
          category: e.category,
          categoryId: e.categoryId,
          city: e.city,
          cityId: e.cityId,
          title: e.title,
          description: e.description,
          email: e.email,
          phoneNumber: e.phoneNumber,
          photos: e.photos,
          price: e.price,
          priceStatus: e.priceStatus,
          subCategory: e.subCategory,
          countryId: e.countryId,
          subCategoryId: e.subCategoryId,
          user: e.user,
          userPhoto: e.userPhoto,
          memberDate: e.memberDate,
          userId: e.userId,
        ));
      });

      _myAds = loadedAds.reversed.toList();
      print("get-myAds sucsseful=${_myAds.length}");
      notifyListeners();
      return MyAds.fromJson(response.data);
    } else {
      print("error get get-myAds data");
      cartsModel = MyAds.fromJson(response.data);

      _myAds = loadedAds.reversed.toList();
      notifyListeners();
      return MyAds.fromJson(response.data);
    }
  }
}

class Orders {
  Orders(
      {this.id,
      this.category,
      this.categoryId,
      this.city,
      this.cityId,
      this.countryId,
      this.createdAt,
      this.description,
      this.email,
      this.memberDate,
      this.phoneNumber,
      this.photos,
      this.price,
      this.priceStatus,
      this.subCategory,
      this.subCategoryId,
      this.title,
      this.user,
      this.userId,
      this.userPhoto});

  int id;
  int userId;
  String user;
  String userPhoto;
  String email;
  String phoneNumber;
  DateTime memberDate;
  int cityId;
  String city;
  int countryId;
  int categoryId;
  String category;
  int subCategoryId;
  String subCategory;
  String title;
  int price;
  int priceStatus;
  String description;
  String additionalDetails;
  List<Photo> photos;
  DateTime createdAt;
}
