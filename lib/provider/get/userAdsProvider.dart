import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/userAdsModel.dart';

class GetUserData with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<UserAdsModel> getUserData(String token,int id,BuildContext context) async {
  Map<String, String> headers = {
    "Authorization": "Bearer $token",
    "Accept-Language": localization.currentLanguage.toString()
  };
    Response response = await _utils.get("user_ads/$id",context, headers: headers);

    if (response.statusCode == 200) {
      print("get  getUserData sucsseful");
      print('response${response.data}');
      return UserAdsModel.fromJson(response.data);
    } else {
      print("error getUserData");
      return UserAdsModel.fromJson(response.data);
    }
  }

  notifyListeners();
}
