import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/auctionsActivationModel.dart';


class GetAuctionsActivationProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<AuctionsActivationModel> getAuctionsActivationProvider(BuildContext context) async {

    Response response = await _utils.get("auctions_activation",context,);
    if (response.statusCode == 200) {
      print("get get_ads sucsseful");
      return AuctionsActivationModel.fromJson(response.data);
    } else {
      print("error get get_ads data");
      return AuctionsActivationModel.fromJson(response.data);
    }
  }
}
