import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/myArchivedAds.dart';
import 'package:rentstation/models/get/removeArchieveAds.dart';

class GetmyArchieveAdsProvider with ChangeNotifier {
  List<Archieve> _archieve = [];

  List<Archieve> get archieve {
    return [..._archieve];
  }

  clear() {
    archieveModel = null;
    notifyListeners();
  }

  RemoveAdFromArchive deleteAdModel;
  NetworkUtil _utils = new NetworkUtil();
  MyArchivedAds archieveModel;
  deleteArchieveAd(BuildContext context, int id, String token) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response =
        await _utils.get("remove_ad_from_archive/$id", context,headers: headers);
    final exitingDataIndex = _archieve.indexWhere((prod) => prod.id == id);

    _archieve.removeAt(exitingDataIndex );
    print(_archieve.length);
    if (response.statusCode == 200) {
      notifyListeners();
      deleteAdModel = RemoveAdFromArchive.fromJson(response.data);
    } else {
      // _favourite.insert(exitingDataIndex, exitingData);

      print("error get Archieve data");
      deleteAdModel = RemoveAdFromArchive.fromJson(response.data);
    }
    if (deleteAdModel.code == 200) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  deleteAdModel.data.value,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    } else {
      print('error remove_ad_Archieve');
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  localization.text("ad_is_found"),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    }
    notifyListeners();
  }

  Future<MyArchivedAds> getMyArchieveAds(String token,BuildContext context) async {
    final List<Archieve> favouritesAdList = [];

    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response = await _utils.get("my_archived_ads", context, headers: headers);
    if (response.statusCode == 200) {
      print("get get-Archieve sucsseful");

      archieveModel = MyArchivedAds.fromJson(response.data);
      archieveModel.data.forEach((e) {
        favouritesAdList.add(Archieve(
            id: e.id,
            createdAt: e.createdAt,
            orderDetails: e.description,
            photo: e.photos,
            providertitle: e.title,
            city: e.city,
            country: e.country,
            providerPhoto: e.userPhoto,
            userId: e.userId,
            userName: e.user,
            price: e.price));
      });
      _archieve = favouritesAdList.reversed.toList();

      notifyListeners();
      return MyArchivedAds.fromJson(response.data);
    } else {
      print("error get get-Archieve data");
      archieveModel = MyArchivedAds.fromJson(response.data);

      _archieve = favouritesAdList.reversed.toList();
      notifyListeners();
      return MyArchivedAds.fromJson(response.data);
    }
  }
}

class Archieve {
  Archieve({
    @required this.id,
    @required this.orderDetails,
    @required this.photo,
    @required this.userId,
    @required this.userName,
    @required this.providertitle,
    @required this.providerPhoto,
    @required this.city,
    @required this.country,
    this.price,
    @required this.createdAt,
  });

  int id;
  String orderDetails;
  int price;
  List photo;
  String country;
  String city;
  int userId;
  String userName;
  String providertitle;
  String providerPhoto;
  DateTime createdAt;
}
