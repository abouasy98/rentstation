import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/getCitiesModel.dart';

class GetCitiesByCountryIdProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<GetCities> getCities(int id,BuildContext context) async {
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString()
    };

    Response response = await _utils.get("get_cities/$id",context, headers: headers);
    if (response.statusCode == 200) {
      print("get get_cities_by_country_id sucsseful");
      return GetCities.fromJson(response.data);
    } else {
      print("error get get_cities_by_country_id data");
      return GetCities.fromJson(response.data);
    }
  }
}
