import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/getUserById.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GetUserDataProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  GetUserById userDataModel;
  Future<GetUserById> getUserData(
      String token, int id, BuildContext context) async {
    Map<String, String> headers = {
      "Authorization": "Bearer $token",
      "Accept-Language": localization.currentLanguage.toString(),
    };
    Response response =
        await _utils.get("get_user_by_id/$id", context, headers: headers);
    if (response.statusCode == 200) {
      print("get user-data sucsseful");

      userDataModel = GetUserById.fromJson(response.data);
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      await _prefs.setString('name', userDataModel.data.name);
      await _prefs.setString('email', userDataModel.data.email);
      await _prefs.setString("image", userDataModel.data.image);
      await _prefs.setString('phone', userDataModel.data.phoneNumber);
      await _prefs.setString('api_token', userDataModel.data.apiToken);
      await _prefs.setInt('id', userDataModel.data.id);
      await _prefs.setString('image', userDataModel.data.image);
      await _prefs.setInt('active', userDataModel.data.active);
      await _prefs.setString('currency', userDataModel.data.currency);
      await _prefs.setString('country', userDataModel.data.country);
      await _prefs.setInt('countryId', userDataModel.data.countryId);
      notifyListeners();
      return GetUserById.fromJson(response.data);
    } else {
      print("error get user-data data");
      return GetUserById.fromJson(response.data);
    }
  }
}
