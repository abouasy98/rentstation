import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/myArchivedAuctionModel.dart';
import 'package:rentstation/models/post/removeAuctionFromArchive.dart';

class GetmyArchieveAuctionsProvider with ChangeNotifier {
  List<Archieve> _archieve = [];

  List<Archieve> get archieve {
    return [..._archieve];
  }

  clear() {
    archieveModel = null;
    notifyListeners();
  }

  RemoveAuctionFromArchive deleteAdModel;
  NetworkUtil _utils = new NetworkUtil();
  MyArchivedAuctionModel archieveModel;
  deleteArchieveAuctions(BuildContext context, int id, String token) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
        FormData formData = FormData.fromMap({
      "created_at":DateTime.now().toString().substring(0,19),
    });

    Response response =
        await _utils.post("remove_auction_from_archive/$id", context,headers: headers,body: formData);
    final exitingDataIndex = _archieve.indexWhere((prod) => prod.id == id);

    _archieve.removeAt(exitingDataIndex);
    print(_archieve.length);
    if (response.statusCode == 200) {
      notifyListeners();
      deleteAdModel = RemoveAuctionFromArchive.fromJson(response.data);
    } else {
      // _favourite.insert(exitingDataIndex, exitingData);

      print("error get Archieve data");
      deleteAdModel = RemoveAuctionFromArchive.fromJson(response.data);
    }
    if (deleteAdModel.code == 200) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  localization.text('auction_archieved_is_removed'),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    } else {
      print('error remove_Auctions_Archieve');
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  localization.text("error"),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    }
    notifyListeners();
  }

  Future<MyArchivedAuctionModel> getMyArchieveAuctions(String token,BuildContext context) async {
    final List<Archieve> favouritesAdList = [];

    Map<String, String> headers = {
      "Authorization": "Bearer $token",
      "Accept-Language": localization.currentLanguage.toString(),
    };
    
    Response response =
        await _utils.get("my_archived_auction", context,headers: headers);
    if (response.statusCode == 200) {
      print("get get-Archieve sucsseful");

      archieveModel = MyArchivedAuctionModel.fromJson(response.data);
      archieveModel.data.forEach((e) {
        favouritesAdList.add(Archieve(
            id: e.id,
            createdAt: e.createdAt,
            orderDetails: e.description,
            photo: e.photos,
            providertitle: e.name,
            city: e.city,
            country: e.country,
            heighPrice: e.heighPrice,
            winnerNamePhoto: e.auctionWinnerPhoto,
            providerPhoto: e.userPhoto,
            userId: e.userId,
            userName: e.user,
            intialPrice: e.initialPrice,
            period: e.period,
            priceInrease: e.priceIncrease,
            winnerName: e.auctionWinner,
            prices: e.prices,
            currency: e.currency,
            winnerId: e.auctionWinnerId));
      });
      _archieve = favouritesAdList.reversed.toList();

      notifyListeners();
      return MyArchivedAuctionModel.fromJson(response.data);
    } else {
      print("error get get-Archieve data");
      archieveModel = MyArchivedAuctionModel.fromJson(response.data);

      _archieve = favouritesAdList.reversed.toList();
      notifyListeners();
      return MyArchivedAuctionModel.fromJson(response.data);
    }
  }
}

class Archieve {
  Archieve({
    @required this.id,
    @required this.orderDetails,
    @required this.photo,
    @required this.userId,
    @required this.userName,
    @required this.providertitle,
    @required this.providerPhoto,
    @required this.city,
    @required this.country,
    this.period,
    this.winnerName,
    this.priceInrease,
    this.intialPrice,
    @required this.createdAt,
    this.prices,
    this.currency,
    this.heighPrice,
    this.winnerNamePhoto,
    this.winnerId,
  });

  int id;
  String orderDetails;
  int intialPrice;
  List photo;
  String country;
  String city;
  int userId;
  String currency;
  int priceInrease;
  int period;
  List prices;
  int winnerId;
  String winnerName;
  String winnerNamePhoto;
  int heighPrice;
  String userName;
  String providertitle;
  String providerPhoto;
  DateTime createdAt;
}
