import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/listNotifactions.dart';

class NotoficationProvider with ChangeNotifier {
  List<Notification> _notifications = [];

  List<Notification> get notfications {
    return [..._notifications];
  }

  NetworkUtil _utils = new NetworkUtil();
  ListNotificationsModel notificationModel;
  bool error = false;
  Future<ListNotificationsModel> removeNotifacion(
      int id, String token, BuildContext context) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response = await _utils.post("delete_Notifications/$id", context,
        headers: headers);
    final exitingDataIndex = _notifications.indexWhere((prod) => prod.id == id);
    var exitingData = _notifications[exitingDataIndex];
    _notifications.removeAt(exitingDataIndex);
    print(_notifications.length);
    if (response.statusCode == 200) {
      notifyListeners();
      return ListNotificationsModel.fromJson(response.data);
    } else {
      _notifications.insert(exitingDataIndex, exitingData);
      print("error get notification data");
      return ListNotificationsModel.fromJson(response.data);
    }
  }

  Future<ListNotificationsModel> getNotification(
      String token, BuildContext context) async {
    final List<Notification> loadedNotifications = [];
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    Response response =
        await _utils.get("list-notifications", context,headers: headers);
    if (response.statusCode == 200) {
      print("get notification data sucsseful");
      notificationModel = ListNotificationsModel.fromJson(response.data);
      notificationModel.data.forEach((element) {
        loadedNotifications.add(Notification(
            id: element.id,
            arMessage: element.message,
            arTitle: element.title,
            createdAt: element.createdAt,
            auctionId: element.auctionId,
            type: element.type,
            adId: element.adId));
      });
      _notifications = loadedNotifications.reversed.toList();
      error = false;
      notifyListeners();
      return ListNotificationsModel.fromJson(response.data);
    } else {
      print("error get notification data");
      return ListNotificationsModel.fromJson(response.data);
    }
  }
}

class Notification {
  Notification({
    @required this.id,
    @required this.type,
    @required this.arTitle,
    @required this.arMessage,
    @required this.adId,
    this.auctionId,
    @required this.createdAt,
  });

  int id;
  int type;
  String arTitle;
  String arMessage;
  int adId;
  int auctionId;
  DateTime createdAt;
}
