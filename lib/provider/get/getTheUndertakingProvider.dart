import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/getTheUndertakingModel.dart';

class GetTheUndertakingProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  String content;

  GetTheUndertaking getTheUndertaking;
  Future<GetTheUndertaking> getTheUndertakingProvider(
      BuildContext context) async {
    Response response = await _utils.get(
      "get_the_undertaking",
      context,
    );
    if (response.statusCode == 200) {
      print("GetTheUndertaking sucsseful");
      getTheUndertaking = GetTheUndertaking.fromJson(response.data);
      content = getTheUndertaking.data.value;

      notifyListeners();
      return GetTheUndertaking.fromJson(response.data);
    } else {
      print("error GetTheUndertaking data");
      return GetTheUndertaking.fromJson(response.data);
    }
  }
}
