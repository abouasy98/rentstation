import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/AddAdToFavorite.dart';

class AddAdToFavProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  AddAdToFavorite addAdToFavModel;
  addAdToFav(String token, int id, BuildContext context) async {
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    Response response =
        await _utils.get("add_ad_to_favorite/$id",context, headers: headers);
    if (response.statusCode == 200) {
      print("get make_ad_favrouit sucsseful");
      addAdToFavModel = AddAdToFavorite.fromJson(response.data);
    } else {
      print("error get make_ad_favrouit data");
      addAdToFavModel = AddAdToFavorite.fromJson(response.data);
    }
    if (addAdToFavModel.code == 200) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  addAdToFavModel.data.value,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    } else {
      print('error get make_ad_favrouit');
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  localization.text("ad_is_found"),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    }
    notifyListeners();
  }
}
