import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/myConversation.dart';

class GetChatsProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<MyConversations> getChats(String token,BuildContext context) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response = await _utils.get("my_conversations",context, headers: headers);
    if (response.statusCode == 200) {
      print("get my_ad_chats sucsseful");
      return MyConversations.fromJson(response.data);
    } else {
      print("error get my_ad_chats data");
      return MyConversations.fromJson(response.data);
    }
  }
}
