import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/getAuctionsModel.dart';

class GetAuctionsProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<GetAuctionsModel> getAuctions(String token,BuildContext context) async {
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    Response response = await _utils.get("get_auctions", context,headers: headers);
    if (response.statusCode == 200) {
      print("get get_auctions sucsseful");
      return GetAuctionsModel.fromJson(response.data);
    } else {
      print("error get get_auctions data");
      return GetAuctionsModel.fromJson(response.data);
    }
  }
}
