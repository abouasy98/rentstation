import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/getAdsModel.dart';

class GetAddsProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<GetAdsModel> getAdds(String token,BuildContext context) async {
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    Response response = await _utils.get("get_ads",context, headers: headers);
    if (response.statusCode == 200) {
      print("get get_ads sucsseful");
      return GetAdsModel.fromJson(response.data);
    } else {
      print("error get get_ads data");
      return GetAdsModel.fromJson(response.data);
    }
  }
}
