import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/AddAdToFavorite.dart';
import 'package:rentstation/models/get/getFavModel.dart';

class GetMyFavAdsProvider with ChangeNotifier {
  List<Favourites> _favourite = [];

  List<Favourites> get favourite {
    return [..._favourite];
  }

  clear() {
    cartsModel = null;
    notifyListeners();
  }

  AddAdToFavorite deleteAdModel;
  NetworkUtil _utils = new NetworkUtil();
  GetFavModel cartsModel;
  deleteFavAd(BuildContext context,bool list ,int id, String token) async {
    Map<String, String> headers = {
      "Authorization": "Bearer $token",
      "Accept-Language": localization.currentLanguage.toString(),
    };
    Response response = await _utils.get("remove_ad_from_favorite/$id", context,
        headers: headers);
    final exitingDataIndex = _favourite.indexWhere((prod) => prod.adId == id);

   if(list==true){ _favourite.removeAt(exitingDataIndex);
   }
    print(_favourite.length);
    if (response.statusCode == 200) {
      notifyListeners();
      deleteAdModel = AddAdToFavorite.fromJson(response.data);
    } else {
      // _favourite.insert(exitingDataIndex, exitingData);

      print("error get notification data");
      deleteAdModel = AddAdToFavorite.fromJson(response.data);
    }
    if (deleteAdModel.code == 200) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  deleteAdModel.data.value,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    } else {
      print('error remove_ad_favrouit');
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  localization.text("ad_is_found"),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    }
    notifyListeners();
  }

  Future<GetFavModel> getMyFavAds(String token, BuildContext context) async {
    final List<Favourites> favouritesAdList = [];

    Map<String, String> headers = {
      "Authorization": "Bearer $token",
      "Accept-Language": localization.currentLanguage.toString(),
    };
    Response response =
        await _utils.get("my_favorites_ads", context, headers: headers);
    if (response.statusCode == 200) {
      print("get get-Fav sucsseful");

      cartsModel = GetFavModel.fromJson(response.data);
      cartsModel.data.forEach((e) {
        favouritesAdList.add(Favourites(
            id: e.id,
            createdAt: e.createdAt,
            orderDetails: e.description,
            photo: e.photos,
            adId: e.adId,
            providertitle: e.title,
            city: e.city,
            country: e.country,
            providerPhoto: e.userPhoto,
            userId: e.userId,
            userName: e.user,
            price: e.price));
        print('favouriteid=${e.adId}');
      });
      _favourite = favouritesAdList.reversed.toList();

      notifyListeners();
      return GetFavModel.fromJson(response.data);
    } else {
      print("error get get-Fav data");
      cartsModel = GetFavModel.fromJson(response.data);

      _favourite = favouritesAdList.reversed.toList();
      notifyListeners();
      return GetFavModel.fromJson(response.data);
    }
  }
}

class Favourites {
  Favourites({
    @required this.id,
    @required this.orderDetails,
    @required this.photo,
    @required this.userId,
    @required this.userName,
    @required this.adId,
    @required this.providertitle,
    @required this.providerPhoto,
    @required this.city,
    @required this.country,
    this.price,
    @required this.createdAt,
  });

  int id;

  String orderDetails;
  int price;
  List photo;
  String country;
  String city;
  int userId;
  String userName;
  int adId;
  String providertitle;
  String providerPhoto;
  DateTime createdAt;
}
