import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/adCommentModel.dart';
import 'package:rentstation/models/get/userDeleteCommentModel.dart';
import 'package:rentstation/screens/addComment.dart';

class GetCommentProvider with ChangeNotifier {
  List<Comment> _comment = [];

  List<Comment> get comment {
    return [..._comment];
  }

  clear() {
    commentModel = null;
    notifyListeners();
  }

  UserDeleteCommentModel deleteCommentModel;
  NetworkUtil _utils = new NetworkUtil();
  GetAdComment commentModel;
  deleteCommentAd(BuildContext context, int id, int adId,String token) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response =
        await _utils.get("user_delete_comment/$id", context, headers: headers);
    // final exitingDataIndex = _comment.indexWhere((prod) => prod.adId == id);

    //_comment.removeAt(exitingDataIndex);
    print(_comment.length);
    if (response.statusCode == 200) {
      notifyListeners();
      deleteCommentModel = UserDeleteCommentModel.fromJson(response.data);
    } else {
      // _favourite.insert(exitingDataIndex, exitingData);

      print("error get notification data");
      deleteCommentModel = UserDeleteCommentModel.fromJson(response.data);
    }
    if (deleteCommentModel.code == 200) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  deleteCommentModel.data.value,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => AddComment(
                                id:adId,
                              )));
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    } else {
      print('error remove_ad_favrouit');
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  localization.text("ad_is_found"),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    }
    notifyListeners();
  }

  Future<GetAdComment> getCommentAds(
      String token, BuildContext context, int id) async {
    final List<Comment> favouritesAdList = [];

    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response =
        await _utils.get("ad_comments/$id", context, headers: headers);
    if (response.statusCode == 200) {
      print("get get-Fav sucsseful");

      commentModel = GetAdComment.fromJson(response.data);
      commentModel.data.forEach((e) {
        favouritesAdList.add(Comment(
          id: e.id,
          createdAt: e.createdAt,
          adId: e.adId,
          ad: e.ad,
          comment: e.comment,
          email: e.email,
          phoneNumber: e.phoneNumber,
          replays: e.replays,
          user: e.user,
          userPhoto: e.userPhoto,
          userId: e.userId,
        ));
        print('favouriteid=${e.adId}');
      });
      _comment = favouritesAdList.toList();

      notifyListeners();
      return GetAdComment.fromJson(response.data);
    } else {
      print("error get get-Fav data");
      commentModel = GetAdComment.fromJson(response.data);

      _comment = favouritesAdList.reversed.toList();
      notifyListeners();
      return GetAdComment.fromJson(response.data);
    }
  }
}
