import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/addAdToArchive.dart';
import 'package:rentstation/screens/tab_bar.dart';
import '../../helpers/sharedPref_helper.dart';
import '../../screens/adTabBarScreen.dart';
import '../../screens/auctionTabScreen.dart';
import '../../screens/noAction&AdsTab.dart';

class AddAdToArchProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  
  AddAdToArchive addAdToArchModel;
  addAdToarch(String token, int id, BuildContext context) async {
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    Response response =
        await _utils.get("add_ad_to_archive/$id",context, headers: headers);
    if (response.statusCode == 200) {
      print("get make_ad_archieve sucsseful");
      addAdToArchModel = AddAdToArchive.fromJson(response.data);
    } else {
      print("error get make_ad_archieve data");
      addAdToArchModel = AddAdToArchive.fromJson(response.data);
    }
    if (addAdToArchModel.code == 200) {
          
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  addAdToArchModel.data.value,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                 if (Provider.of<SharedPref>(context, listen: false)
                    .noadsNoAuctions ==
                true) {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => NoAuctionOrAdTabScreen(),
                ),
                (Route<dynamic> route) => false,
              );
            } else if (Provider.of<SharedPref>(context, listen: false)
                    .auction ==
                true) {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => AuctionTabScreen(),
                ),
                (Route<dynamic> route) => false,
              );
            } else if (Provider.of<SharedPref>(context, listen: false).ads ==
                true) {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => AdTabScreen(),
                ),
                (Route<dynamic> route) => false,
              );
            } else {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => TabScreen(),
                ),
                (Route<dynamic> route) => false,
              );
            }
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    } else {
      print('error get make_ad_archieve');
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  localization.text("ad_is_found"),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    }
    notifyListeners();
  }
}
