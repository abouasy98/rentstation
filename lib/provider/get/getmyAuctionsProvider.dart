import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/deleteAuction.dart';
import 'package:rentstation/models/get/getmyAuctionsModels.dart';

class GetMyAductionProvider with ChangeNotifier {
  DeleteAuction deleteAuctionModel;
  List<MyAuction> _myAuctions = [];

  List<MyAuction> get myAuctions {
    return [..._myAuctions];
  }

  clear() {
    auctionsModel = null;
    notifyListeners();
  }

  deleteAuctions(String token, int id, BuildContext context) async {
    Map<String, String> headers = {
      "Authorization": "Bearer $token",
      "Accept-Language": localization.currentLanguage.toString(),
    };
    Response response =
        await _utils.get("delete_auction/$id", context, headers: headers);
    final exitingDataIndex = _myAuctions.indexWhere((prod) => prod.id == id);
    var exitingData = _myAuctions[exitingDataIndex];
    _myAuctions.removeAt(exitingDataIndex);
    if (response.statusCode == 200) {
      print("get delete_ad sucsseful");
      deleteAuctionModel = DeleteAuction.fromJson(response.data);
    } else {
      print("error get delete_ad data");
      _myAuctions.insert(exitingDataIndex, exitingData);
      deleteAuctionModel = DeleteAuction.fromJson(response.data);
    }

    notifyListeners();
  }

  NetworkUtil _utils = new NetworkUtil();
  GetmyAuctionsModels auctionsModel;
  Future<GetmyAuctionsModels> getMyAuctions(
      String token, BuildContext context) async {
    final List<MyAuction> loadedAuctions = [];

    Map<String, String> headers = {
      "Authorization": "Bearer $token",
      "Accept-Language": localization.currentLanguage.toString(),
    };
    Response response =
        await _utils.get("my_auctions", context, headers: headers);
    if (response.statusCode == 200) {
      print("get get-myAuctionss sucsseful");

      auctionsModel = GetmyAuctionsModels.fromJson(response.data);
      auctionsModel.data.forEach((e) {
        loadedAuctions.add(MyAuction(
          id: e.id,
          createdAt: e.createdAt,
          cityId: e.cityId,
          phoneNumber: e.phoneNumber,
          photos: e.photos,
          auctionWinner: e.auctionWinner,
          auctionWinnerId: e.auctionWinnerId,
          city: e.city,
          country: e.country,
          countryId: e.countryId,
          currency: e.currency,
          description: e.description,
          details: e.details,
          email: e.email,
          initialPrice: e.initialPrice,
          name: e.name,
          priceIncrease: e.priceIncrease,
          prices: e.prices,
          user: e.user,
          userPhoto: e.userPhoto,
          period: e.period,
          memberDate: e.memberDate,
          archive: e.archive,
          auctionWinnerPhoto: e.auctionWinnerPhoto,
          category: e.category,
          categoryId: e.categoryId,
          heighPrice: e.heighPrice,
          pinned: e.pinned,
          subCategory: e.subCategory,
          subCategoryId: e.subCategoryId,
          userId: e.userId,
        ));
      });
      _myAuctions = loadedAuctions.reversed.toList();
      notifyListeners();
      return GetmyAuctionsModels.fromJson(response.data);
    } else {
      print("error get get-myAuctionss data");
      auctionsModel = GetmyAuctionsModels.fromJson(response.data);

      _myAuctions = loadedAuctions.reversed.toList();
      notifyListeners();
      return GetmyAuctionsModels.fromJson(response.data);
    }
  }
}
