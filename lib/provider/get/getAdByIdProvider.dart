import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/getAdbyIdModel.dart';


class GetAddsByIdProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<GetAdbyIdModel> getAdds(int id,String token,BuildContext context) async {
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
        "Authorization": "Bearer $token"
    };
    Response response =
        await _utils.get("get_ad_by_id/$id",context, headers: headers);
    if (response.statusCode == 200) {
      print("get get_ad_by_id sucsseful");
      return GetAdbyIdModel.fromJson(response.data);
    } else {
      print("error get get_ad_by_id data");
      return GetAdbyIdModel.fromJson(response.data);
    }
  }
}
