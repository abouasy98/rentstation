import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/registertype.dart';


class GetRegisterType with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Map<String, String> headers = {
    "Accept-Language": localization.currentLanguage.toString()
  };
  Future<RegisterType> getRegisterType(BuildContext context) async {

    Response response = await _utils.get("register_type",context, headers: headers);

    if (response.statusCode == 200) {
      print("get  getRegisterType sucsseful");
      print('response${response.data}');
      return RegisterType.fromJson(response.data);
    } else {

      print("error getRegisterType");
      return RegisterType.fromJson(response.data);
    }
  }

  notifyListeners();
}
