import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/deleteAdPhotoModel.dart';
import 'package:rentstation/models/get/getAdPhotosbyidModel.dart';

class GetphotosAdsbyIdProvider with ChangeNotifier {
  List<Photos> _photos = [];

  List<Photos> get photos {
    return [..._photos];
  }

  clear() {
    photoModel = null;
    notifyListeners();
  }

  DeleteAdPhotoModel deleteAdPhotoModel;
  NetworkUtil _utils = new NetworkUtil();
  GetAdPhotosbyidModel photoModel;
  deletePhotobyId(BuildContext context, int id, String token,) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response =
        await _utils.get("delete_ad_photo/$id",context, headers: headers);
    final exitingDataIndex = _photos.indexWhere((prod) => prod.adId == id);

    _photos.removeAt(exitingDataIndex+1);
    print(_photos.length);
    if (response.statusCode == 200) {
      notifyListeners();
      deleteAdPhotoModel = DeleteAdPhotoModel.fromJson(response.data);
    } else {

      print("error get remove_ad_photo data");
      deleteAdPhotoModel = DeleteAdPhotoModel.fromJson(response.data);
    }

    notifyListeners();
  }

  Future<GetAdPhotosbyidModel> getphotosbyId(String token, int id,BuildContext context) async {
    final List<Photos> photoAdList = [];

    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response = await _utils.get("get_ad_photos/$id", context,headers: headers);
    if (response.statusCode == 200) {
      print("get get-photos sucsseful");

      photoModel = GetAdPhotosbyidModel.fromJson(response.data);
      photoModel.data.forEach((e) {
        photoAdList.add(Photos(
          id: e.id,
          createdAt: e.createdAt,
          ad: e.ad,
          photo: e.photo,
          adId: e.adId,
        ));
      });
      _photos = photoAdList.reversed.toList();

      notifyListeners();
      return GetAdPhotosbyidModel.fromJson(response.data);
    } else {
      print("error get get-photo data");
      photoModel = GetAdPhotosbyidModel.fromJson(response.data);

      _photos = photoAdList.reversed.toList();
      notifyListeners();
      return GetAdPhotosbyidModel.fromJson(response.data);
    }
  }
}

class Photos {
  Photos({
    @required this.id,
    @required this.ad,
    @required this.adId,
    @required this.photo,
    @required this.createdAt,
  });

  int id;

  String ad;

  String country;

  int adId;
  String photo;

  DateTime createdAt;
}
