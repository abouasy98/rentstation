import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/getAuctionById.dart';

class GetAuctionsByIdProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<GetAuctionById> getAuctionsByid(int id, String token,BuildContext context) async {
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    Response response = await _utils.get("get_auction_by_id/$id", context,headers: headers);
    if (response.statusCode == 200) {
      print("get get_auctions_by_id sucsseful");
      return GetAuctionById.fromJson(response.data);
    } else {
      print("error get get_auctions_by_id data");
      return GetAuctionById.fromJson(response.data);
    }
  }
}
