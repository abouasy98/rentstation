import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/custom_progress_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/auth/resendmodel.dart';
import 'package:rentstation/provider/auth/phoneVerificationProvider.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';


class ResendCodeProvider with ChangeNotifier {
  String phone;
  String email;
  int countryId;

  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  Resend model;
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  resendCode(BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString()
    };
    FormData formData = FormData.fromMap(
        {"phone_number": phone, 'email': email, "country_id": countryId});

    Response response =
        await _utils.post("resend_code", context,body: formData, headers: headers);
    if (response == null) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        print('error resend-code mobile res == null');
        dialog.showWarningDialog(
          btnOnPress: () {},
          context: context,
          msg: localization.text("internet"),
        );
      });
      return;
    }
    if (response.statusCode == 200) {
      print("resend-code sucsseful");
      model = Resend.fromJson(response.data);
    } else {
      print("error resend-code");
      model = Resend.fromJson(response.data);
    }
    if (model.code == 200) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      Provider.of<PhoneVerificationProvider>(context, listen: false).phone =
          phone;
      Provider.of<PhoneVerificationProvider>(context, listen: false).email =
          email;
      // Navigator.of(context).pushReplacement(MaterialPageRoute(
      //     builder: (context) => ConfirmCode(
      //           //  phoneNumber: phone,
      //           stateOfVerificationCode: 1,
      //         )));
    } else {
      print('error resend-code');
      // Future.delayed(Duration(seconds: 1), () {
      //   customProgressDialog.hidePr();
      //   dialog.showErrorDialog(
      //     btnOnPress: () {},
      //     context: context,
      //     msg: model.error[0].value,
      //     ok: localization.text("ok"),
      //   );
      // });
    }
    notifyListeners();
  }
}
