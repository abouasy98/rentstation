import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/custom_progress_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/auth/LoginModel.dart';
import 'package:rentstation/screens/adTabBarScreen.dart';
import 'package:rentstation/screens/auctionTabScreen.dart';
import 'package:rentstation/screens/noAction&AdsTab.dart';
import 'package:rentstation/screens/tab_bar.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class LoginProvider with ChangeNotifier {
  String phone;
  String password;
  String email;
  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  LoginModel _model;
  SharedPreferences _prefs;
  void clear() {
    phone = null;
    password = null;
  }

  login(String deviceToken, BuildContext context, bool drawer) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();

    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
    };
    FormData formData = FormData.fromMap({
      "phone_number": phone,
      "password": password,
      "email": email,
      "device_token": deviceToken,
    });

    Response response =
        await _utils.post("login", context, body: formData, headers: headers);
    if (response == null) {
      print('error login res == null');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showWarningDialog(
          btnOnPress: () {},
          context: context,
          msg: localization.text("internet"),
        );
      });

      return;
    }

    if (response.statusCode == 200) {
      print("login sucsseful");
      _model = LoginModel.fromJson(response.data);
    } else {
      print("error login");
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      _model = LoginModel.fromJson(response.data);
    }
    if (_model.code == 200) {
      await Future.delayed(Duration(seconds: 1), () async {
        await customProgressDialog.hidePr();
      });
      _prefs = await SharedPreferences.getInstance();

      print('done');
      //  clear();
      _prefs.setString('name', _model.data.name);
      _prefs.setString('phone', _model.data.phoneNumber);
      _prefs.setString('api_token', _model.data.apiToken);
      _prefs.setInt('id', _model.data.id);
      _prefs.setInt('active', _model.data.active);
      _prefs.setString('image', _model.data.image);
      _prefs.setString('email', _model.data.email);
      _prefs.setString('currency', _model.data.currency);
      _prefs.setInt('countryId', _model.data.countryId);
      _prefs.setString('country', _model.data.country);
      notifyListeners();
      print(_prefs.getBool('Auctions'));
      print(_prefs.getBool('Ads'));

      if (_prefs.getBool('NoAdsNoAuctions') == true) {
        if (!drawer) {
          pushNewScreen(
            context,
            screen: NoAuctionOrAdTabScreen(),
            // customPageRoute:
            //     PageRouteBuilder(pageBuilder: (context, _, n) => AdTabScreen()),
            withNavBar: false, // OPTIONAL VALUE. True by default.
            pageTransitionAnimation: PageTransitionAnimation.cupertino,
          );
        } else {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => NoAuctionOrAdTabScreen(),
            ),
            (Route<dynamic> route) => false,
          );
        }
      } else if (_prefs.getBool('Auctions') == true) {
        if (!drawer) {
          pushNewScreen(
            context,
            screen: AuctionTabScreen(),
            // customPageRoute:
            //     PageRouteBuilder(pageBuilder: (context, _, n) => AdTabScreen()),
            withNavBar: false, // OPTIONAL VALUE. True by default.
            pageTransitionAnimation: PageTransitionAnimation.cupertino,
          );
        } else {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => AuctionTabScreen(),
            ),
            (Route<dynamic> route) => false,
          );
        }
      } else if (_prefs.getBool('Ads') == true) {
        if (!drawer) {
          pushNewScreen(
            context,
            screen: AdTabScreen(),
            // customPageRoute:
            //     PageRouteBuilder(pageBuilder: (context, _, n) => AdTabScreen()),
            withNavBar: false, // OPTIONAL VALUE. True by default.
            pageTransitionAnimation: PageTransitionAnimation.cupertino,
          );
        } else {
          Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
              builder: (BuildContext context) {
                return AdTabScreen();
              },
            ),
            (Route<dynamic> route) => false,
          );
        }
      } else {
        if (!drawer) {
          pushNewScreen(
            context,
            screen: TabScreen(),
            // customPageRoute:
            //     PageRouteBuilder(pageBuilder: (context, _, n) => AdTabScreen()),
            withNavBar: false, // OPTIONAL VALUE. True by default.
            pageTransitionAnimation: PageTransitionAnimation.cupertino,
          );
        } else {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => TabScreen(),
            ),
            (Route<dynamic> route) => false,
          );
        }
      }

      return _prefs;
    } else {
      print('error login');

      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();

        Toast.show(_model.error[0].value, context,
            duration: 1, gravity: Toast.CENTER, textColor: Colors.white);
      });
    }
  }

  notifyListeners();
}
