import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/custom_progress_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:progress_dialog/progress_dialog.dart';

class LogoutProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  String deviceToken;
  String apiToken;

  logout(BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {'Authorization': 'Bearer $apiToken'};
    FormData formData = FormData.fromMap({
      "device_token": deviceToken,
    });

    Response response = await _utils.post(
      "logout",context,
      body: formData,
      headers: headers,
    );
    if (response == null) {
      print('error confirm_reset_code');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showWarningDialog(
          btnOnPress: () {},
          context: context,
          msg: localization.text("internet"),
        );
      });

      return;
    }

    Future.delayed(Duration(seconds: 1), () {
      customProgressDialog.hidePr();
    });

    notifyListeners();
  }
}
