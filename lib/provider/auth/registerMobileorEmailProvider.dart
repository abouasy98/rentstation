import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/custom_progress_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/auth/registerMobileorEmailModel.dart';
import 'package:rentstation/provider/auth/phoneVerificationProvider.dart';
import 'package:rentstation/provider/auth/resendCode.dart';
import 'package:rentstation/provider/auth/signUpProvider.dart';
import 'package:rentstation/screens/confirmCode.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';

class RegisterMobileorEmailProvider with ChangeNotifier {
  String email;
  String phone;
  int countryId;

  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  RegisterMobileorEmailModel model;
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  registerMobile(BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString()
    };
    FormData formData = FormData.fromMap({
      "email": email,
      "phone_number": phone,
      "country_id": countryId,
    });

    Response response = await _utils.post("register_mobile_or_email",context,
        body: formData, headers: headers);
    if (response == null) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        print('error register mobile res == null');
        dialog.showWarningDialog(
          btnOnPress: () {},
          context: context,
          msg: localization.text("internet"),
        );
      });
      return;
    }
    if (response.statusCode == 200) {
      print("register_mobile sucsseful");
      model = RegisterMobileorEmailModel.fromJson(response.data);
    } else {
      print("error register_mobile+${response.statusCode}");
      model = RegisterMobileorEmailModel.fromJson(response.data);
    }
    if (model.code == 200) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      if (phone != null) {
        Provider.of<PhoneVerificationProvider>(context, listen: false).phone =
            phone;
      } else {
        Provider.of<PhoneVerificationProvider>(context, listen: false).email =
            email;
      }
      Provider.of< ResendCodeProvider>(context, listen: false).countryId = countryId;
      Provider.of<SignUpProvider>(context, listen: false).countryId = countryId;
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => ConfirmCode(
                //  phoneNumber: phone,
                stateOfVerificationCode: 1,
              )));
    } else {
      print('error register_mobile');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
          btnOnPress: () {},
          context: context,
          msg: model.error[0].value,
          ok: localization.text("ok"),
        );
      });
    }
    notifyListeners();
  }
}
