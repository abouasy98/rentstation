import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/custom_progress_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/auth/signUpModel.dart';
import 'package:rentstation/screens/adTabBarScreen.dart';
import 'package:rentstation/screens/auctionTabScreen.dart';
import 'package:rentstation/screens/noAction&AdsTab.dart';
import 'package:rentstation/screens/tab_bar.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'logoutProvider.dart';

class SignUpProvider with ChangeNotifier {
  String name;
  int gender;
  String email;
  String phoneNumber;
  int countryId;
  String password;
  String passwordConfirmation;
  String country;
  File image;

  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;

  SignUpModel _model;
  SharedPreferences _prefs;
  signUp(String token, BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    print('a7ten=$image');
    Map<String, String> headers = {
            "Accept-Language": localization.currentLanguage.toString(),
    };
    FormData formData = FormData.fromMap({
      "email": email,
      "phone_number": phoneNumber,
      "name": name,
      "image": image != null ? await MultipartFile.fromFile(image.path) : null,
      "country_id": countryId,
      "password": password,
      "password_confirmation": passwordConfirmation,
      "device_token": token,
      "country": country,
    });

    Response response = await _utils.post(
      "register",
      context,
      body: formData,
      headers: headers,
    );
    if (response == null) {
      print('error register res == null');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showWarningDialog(
          btnOnPress: () {},
          context: context,
          msg: localization.text("internet"),
        );
      });

      return;
    }
    if (response.statusCode == 200) {
      print("register sucsseful");
      _model = SignUpModel.fromJson(response.data);
    } else {
      print("error register");
      _model = SignUpModel.fromJson(response.data);
    }
    if (_model.code == 200) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      _prefs = await SharedPreferences.getInstance();
      Provider.of<LogoutProvider>(context, listen: false).deviceToken = token;
      print('done');
      print("${_model.data.name}...................");
      print("${_model.data.countryId}...................");
      print("${_model.data.apiToken}...................");
      print("${_model.data.id}...................");
      print("${_model.data.image}...................");
      await _prefs.setString('name', _model.data.name);
      await _prefs.setString('email', _model.data.email);
      await _prefs.setString("image", _model.data.image);
      await _prefs.setString('phone', _model.data.phoneNumber);
      await _prefs.setString('api_token', _model.data.apiToken);
      await _prefs.setInt('id', _model.data.id);
      await _prefs.setString('image', _model.data.image);
      await _prefs.setInt('active', _model.data.active);
      await _prefs.setString('currency', _model.data.currency);
      await _prefs.setString('country', _model.data.country);
      await _prefs.setInt('countryId', _model.data.countryId);
      if (_prefs.getBool('NoAdsNoAuctions') == true) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => NoAuctionOrAdTabScreen(),
          ),
          (Route<dynamic> route) => false,
        );
      }
    else  if (_prefs.getBool('Auctions') == true) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => AuctionTabScreen(),
          ),
          (Route<dynamic> route) => false,
        );
      } else if (_prefs.getBool('Ads') == true) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => AdTabScreen(),
          ),
          (Route<dynamic> route) => false,
        );
      } else {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => TabScreen(),
          ),
          (Route<dynamic> route) => false,
        );
      }
      return _prefs;
    } else {
      print('error register');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
            btnOnPress: () {},
            context: context,
            msg: _model.error[0].value,
            ok: localization.text("ok"),
            code: _model.code);
      });
    }
  }

  notifyListeners();
}
