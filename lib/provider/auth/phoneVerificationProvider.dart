import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/custom_progress_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/auth/phoneVerificationModel.dart';
import 'package:rentstation/provider/auth/signUpProvider.dart';
import 'package:rentstation/screens/register.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';

class PhoneVerificationProvider with ChangeNotifier {
  String phone;
  String code;
  String email;
  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  PhoneVerificationModel _model;
  phoneVerification(BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    // Map<String, String> headers = {
    //   "X-localization": localization.currentLanguage.toString()
    // };
    FormData formData = FormData.fromMap({
      "phone_number": phone,
      "code": code,
      'email': email,
    });

    Response response = await _utils.post(
      "phone_email_verification",context,
      body: formData,
    );
    if (response == null) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        print('error phone_verification');
        dialog.showWarningDialog(
          btnOnPress: () {},
          context: context,
          msg: localization.text("internet"),
        );
      });

      return;
    }
    if (response.statusCode == 200) {
      print("phone_verification sucsseful");
      _model = PhoneVerificationModel.fromJson(response.data);
    } else {
      print("error phone_verification");
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      _model = PhoneVerificationModel.fromJson(response.data);
    }
    if (_model.code == 200) {
      print('success phone_verification');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      if (phone != null) {
       
        Provider.of<SignUpProvider>(context, listen: false).phoneNumber = phone;
        print('3a7at=$phone');
      } else {
        Provider.of<SignUpProvider>(context, listen: false).email = email;
      }

      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => Register()));
    } else {
      print('error phone_verification');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
          btnOnPress: () {},
          context: context,
          msg: _model.error[0].value,
          ok: localization.text("ok"),
        );
      });
    }
    notifyListeners();
  }
}
