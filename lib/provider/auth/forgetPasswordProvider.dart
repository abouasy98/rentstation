import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/custom_progress_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/auth/forgetPasswordModel.dart';
import 'package:rentstation/provider/auth/resendCode.dart';
import 'package:rentstation/screens/confirmCode.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';

import 'confirmResetCodeProvider.dart';

class ForgetPasswordProvider with ChangeNotifier {
  String phone;
  String email;
  int countryId;
  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;

  ForgetPasswordModel model;
  forgetPassword(BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString()
    };
    FormData formData = FormData.fromMap({
      "phone_number": phone,
      "email": email,
    });

    Response response =
        await _utils.post("forget_password", context,body: formData, headers: headers);
    if (response == null) {
      print('error forgetPassword');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showWarningDialog(
          btnOnPress: () {},
          context: context,
          msg: localization.text("internet"),
        );
      });

      return;
    }
    if (response.statusCode == 200) {
      print("forget_password sucsseful");
      model = ForgetPasswordModel.fromJson(response.data);
    } else {
      print("error forget_password");
      model = ForgetPasswordModel.fromJson(response.data);
    }
    if (model.code == 200) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      Provider.of<ResendCodeProvider>(context, listen: false).countryId = countryId;
      Provider.of<ConfirmResetCodeProvider>(context, listen: false).phone =
          phone;
      Provider.of<ConfirmResetCodeProvider>(context, listen: false).email =
          email;
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => ConfirmCode(
                stateOfVerificationCode: 2,
              )));
    } else {
      print('error forgetPassword');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
          btnOnPress: () {},
          context: context,
          msg: model.error[0].value,
          ok: localization.text("ok"),
        );
      });
    }
    notifyListeners();
  }
}
