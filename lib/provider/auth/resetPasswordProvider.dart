import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/custom_progress_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/auth/resetPasswordModel.dart';
import 'package:rentstation/screens/login.dart';
import 'package:progress_dialog/progress_dialog.dart';

class ResetPasswordProvider with ChangeNotifier {
  String phone;
  String password;
  String passwordConfirmation;
  String email;
  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  ResetPasswordModel _model;
  resetPassword(BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {};
    FormData formData = FormData.fromMap({
      "phone_number": phone,
      "password": password,
      "password_confirmation": passwordConfirmation,
      "email":email
    });

    Response response =
        await _utils.post("reset_password",context, body: formData, headers: headers);
    if (response == null) {
      print('error reset_password');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showWarningDialog(
          btnOnPress: () {},
          context: context,
          msg: localization.text("internet"),
        );
      });

      return;
    }
    if (response.statusCode == 200) {
      print("reset_password sucsseful");
      _model = ResetPasswordModel.fromJson(response.data);
    } else {
      print("error reset_password");
      _model = ResetPasswordModel.fromJson(response.data);
    }
    if (_model.code == 200) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showSuccessDialog(
          btnOnPress: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => SignInScreen(true)));
          },
          context: context,
          msg: localization.text("password_changed"),
          btnMsg: localization.text("ok"),
        );
      });
    } else {
      print('error reset_password');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
          btnOnPress: () {},
          context: context,
          msg: _model.error[0].value,
          ok: localization.text("ok"),
        );
      });
    }
    notifyListeners();
  }
}
