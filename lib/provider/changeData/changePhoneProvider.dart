import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/custom_progress_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/ChangeData/changePasswordModel.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:rentstation/screens/confirmCode.dart';
import 'package:rentstation/screens/transferScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'changePhoneCodeProvider.dart';

class ChangeMobileProvider with ChangeNotifier {
  String phone;

  NetworkUtil _utils = new NetworkUtil();
  SharedPreferences _prefs;
  CustomDialog dialog = CustomDialog();
  ChangePasswordModel model;
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  changeMobile(String token, BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    FormData formData = FormData.fromMap({
      "phone_number": phone,
    });

    Response response = await _utils.post("change_phone_number", context,
        body: formData, headers: headers);
    if (response == null) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        print('error change mobile res == null');
        dialog.showWarningDialog(
          btnOnPress: () {},
          context: context,
          msg: localization.text("internet"),
        );
      });
      return;
    }
    if (response.statusCode == 200) {
      print("change_mobile sucsseful");
      model = ChangePasswordModel.fromJson(response.data);
    } else {
      print("error change_mobile");
      model = ChangePasswordModel.fromJson(response.data);
    }
    if (model.code == 200) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      Provider.of<ChangePhoneCodeProvider>(context, listen: false).phone =
          phone;

      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => ConfirmCode(
            phoneNumber: phone,
            stateOfVerificationCode: 3,
          ),
        ),
      );
      // pushNewScreen(
      //   context,
      //   screen: TrasnferCodeScreen(
      //     phone: phone,
      //     stateOfVerificationCode: 3,
      //   ),
      // withNavBar: false, // OPTIONAL VALUE. True by default.
      //pageTransitionAnimation: PageTransitionAnimation.cupertino,
      //);
      // Navigator.of(context).pushReplacement(MaterialPageRoute(
      //     builder: (context) => ConfirmCode(
      //           phoneNumber: phone,
      //              key: UniqueKey(),
      //           stateOfVerificationCode: 3,
      //         )));
    } else {
      print('error change_mobile');
      _prefs = await SharedPreferences.getInstance();
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
          btnOnPress: () {
            pushNewScreen(
              context,
              screen: TrasnferScreen(
                prefs: _prefs,
              ),
              withNavBar: false, // OPTIONAL VALUE. True by default.
              //  pageTransitionAnimation: PageTransitionAnimation.cupertino,
            );
          },
          context: context,
          msg: model.error[0].value,
          ok: localization.text("ok"),
        );
      });
    }
    notifyListeners();
  }
}
