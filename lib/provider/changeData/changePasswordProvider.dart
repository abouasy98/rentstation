import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/custom_progress_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/ChangeData/changePasswordModel.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rentstation/screens/transferScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangePasswordProvider with ChangeNotifier {
  String oldPassword;
  String password;
  String passwordConfirmation;

  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  SharedPreferences _prefs;
  ChangePasswordModel _model;
  changePassword(String token, BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    FormData formData = FormData.fromMap({
      "current_password": oldPassword,
      "new_password": password,
      "password_confirmation": passwordConfirmation,
    });

    Response response = await _utils.post("change_password", context,
        body: formData, headers: headers);
    if (response == null) {
      print('error change_password');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showWarningDialog(
          btnOnPress: () {},
          context: context,
          msg: localization.text("internet"),
        );
      });

      return;
    }
    if (response.statusCode == 200) {
      _prefs = await SharedPreferences.getInstance();
      print("change_password sucsseful");
      _model = ChangePasswordModel.fromJson(response.data);
    } else {
      print("error change_password");
      _model = ChangePasswordModel.fromJson(response.data);
    }
    if (_model.code == 200) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showSuccessDialog(
          btnOnPress: () {
            pushNewScreen(
              context,
              screen: TrasnferScreen(
                prefs: _prefs,
              ),
              withNavBar: false, // OPTIONAL VALUE. True by default.
              //  pageTransitionAnimation: PageTransitionAnimation.cupertino,
            );
          },
          context: context,
          msg: localization.text("password_changed"),
          btnMsg: localization.text("ok"),
        );
      });
    } else {
      print('error change_password');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
          btnOnPress: () {
             pushNewScreen(
              context,
              screen: TrasnferScreen(
                prefs: _prefs,
              ),
              withNavBar: false, // OPTIONAL VALUE. True by default.
              //  pageTransitionAnimation: PageTransitionAnimation.cupertino,
            );
          },
          context: context,
          msg: _model.error[0].value,
          ok: localization.text("ok"),
        );
      });
    }
    notifyListeners();
  }
}
