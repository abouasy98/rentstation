import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/custom_progress_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/ChangeData/ChangeUserDataModel.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rentstation/screens/transferScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ChangeUserDataProvider with ChangeNotifier {
  String name;

  int countryId;
  File image;

  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();

  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  ChangeUserDataModel _model;
  SharedPreferences _prefs;
  changeUserData(String token, BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    FormData formData = FormData.fromMap({
      "name": name,
      "country_id": countryId,
      "image": image == null ? null : await MultipartFile.fromFile(image.path),
    });

    Response response = await _utils.post("change_user_data", context,
        body: formData, headers: headers);
    if (response == null) {
      print('error change_password');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showWarningDialog(
          btnOnPress: () {},
          context: context,
          msg: localization.text("internet"),
        );
      });

      return;
    }
    if (response.statusCode == 200) {
      print("edit_account data sucsseful");
      _model = ChangeUserDataModel.fromJson(response.data);
    } else {
      print("error  edit_account data");

      _model = ChangeUserDataModel.fromJson(response.data);
    }
    if (_model.code == 200) {
      _prefs = await SharedPreferences.getInstance();
      Future.delayed(Duration(seconds: 1), () async {
        customProgressDialog.hidePr();
        await _prefs.setString('name', _model.data.name);
        await _prefs.setString('phone', _model.data.phoneNumber);
        await _prefs.setString('api_token', _model.data.apiToken);
        await _prefs.setInt('id', _model.data.id);
        await _prefs.setInt('active', _model.data.active);
        await _prefs.setString('image', _model.data.image);
        await _prefs.setString('email', _model.data.email);
        await _prefs.setString('currency', _model.data.currency);
        await _prefs.setInt('countryId', _model.data.countryId);
        await _prefs.setString('country', _model.data.country);
        dialog.showSuccessDialog(
          btnOnPress: () {
            pushNewScreen(
              context,
              screen: TrasnferScreen(
                prefs: _prefs,
              ),
              withNavBar: false, // OPTIONAL VALUE. True by default.
              //  pageTransitionAnimation: PageTransitionAnimation.cupertino,
            );
          },
          context: context,
          msg: localization.text("edit_success"),
          btnMsg: localization.text("ok"),
        );
      });
      return true;
    } else {
      print("error  edit_account data");
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
          btnOnPress: () {
            pushNewScreen(
              context,
              screen: TrasnferScreen(
                prefs: _prefs,
              ),
              withNavBar: false, // OPTIONAL VALUE. True by default.
              //  pageTransitionAnimation: PageTransitionAnimation.cupertino,
            );
          },
          context: context,
          msg: _model.error[0].value,
          ok: localization.text("ok"),
        );
      });
    }
    notifyListeners();
  }
}
