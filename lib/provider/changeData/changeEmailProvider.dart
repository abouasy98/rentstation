import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/custom_progress_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/ChangeData/ChangeEmailModel.dart';
import 'package:rentstation/provider/changeData/changeEmailCodeProvider.dart';

import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:rentstation/screens/confirmCode.dart';
import 'package:rentstation/screens/transferScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangeEmailProvider with ChangeNotifier {
  String email;

  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  SharedPreferences _prefs;
  ChangeEmailModel _model;
  changeEmail(String token, BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    FormData formData = FormData.fromMap({
      "email": email,
    });

    Response response = await _utils.post("change_email", context,
        body: formData, headers: headers);
    if (response == null) {
      print('error change_email');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showWarningDialog(
          btnOnPress: () {},
          context: context,
          msg: localization.text("internet"),
        );
      });

      return;
    }
    if (response.statusCode == 200) {
      print("change_email sucsseful");
      _model = ChangeEmailModel.fromJson(response.data);
    } else {
      print("error change_email");
      _model = ChangeEmailModel.fromJson(response.data);
    }
    if (_model.code == 200) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      Provider.of<ChangeEmailCodeProvider>(context, listen: false).email =
          email;

    Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => ConfirmCode(
              email: email,
              stateOfVerificationCode: 4,
            ),
          ),
        );
    //  return await pushNewScreen(
    //     context,
    //     screen: TrasnferCodeScreen(
    //       email: email,
    //       stateOfVerificationCode: 4,
    //     ),
    //     withNavBar: false,
    //   );
    } else {
      print('error change_email');
      _prefs = await SharedPreferences.getInstance();
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
          btnOnPress: () {
            pushNewScreen(
              context,
              screen: TrasnferScreen(
                prefs: _prefs,
              ),
              withNavBar: false, // OPTIONAL VALUE. True by default.
              //  pageTransitionAnimation: PageTransitionAnimation.cupertino,
            );
          },
          context: context,
          msg: _model.error[0].value,
          ok: localization.text("ok"),
        );
      });
    }
    notifyListeners();
  }
}
