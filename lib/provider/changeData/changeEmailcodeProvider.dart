import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/custom_progress_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/ChangeData/checkCodeChangeEmailModel.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:rentstation/screens/transferScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangeEmailCodeProvider with ChangeNotifier {
  String email;
  String code;

  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  CheckCodeChangeEmailModel _model;
  SharedPreferences _prefs;
  changeEmailCode(String token, BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    FormData formData = FormData.fromMap({
      "email": email,
      "code": code,
    });

    Response response = await _utils.post("check_code_change_email", context,
        body: formData, headers: headers);
    if (response == null) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        print('error check-code-change-email');
        dialog.showWarningDialog(
          btnOnPress: () {},
          context: context,
          msg: localization.text("internet"),
        );
      });

      return;
    }
    if (response.statusCode == 200) {
      print("check-code-change-email sucsseful");
      _model = CheckCodeChangeEmailModel.fromJson(response.data);
    } else {
      print("error check-code-change-email");
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      _model = CheckCodeChangeEmailModel.fromJson(response.data);
    }
    if (_model.code == 200) {
      _prefs = await SharedPreferences.getInstance();
      print('success check-code-change-email');
      _prefs.setString("email", email);

      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showSuccessDialog(
          btnOnPress: () {
               pushNewScreen(
              context,
              screen: TrasnferScreen(
                prefs: _prefs,
              ),
              withNavBar: false, // OPTIONAL VALUE. True by default.
              //  pageTransitionAnimation: PageTransitionAnimation.cupertino,
            );
          },
          context: context,
          msg: localization.text("email_changed"),
          btnMsg: localization.text("ok"),
        );
      });
    } else {
      print('error check-code-change-email');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
          btnOnPress: () {
              pushNewScreen(
              context,
              screen: TrasnferScreen(
                prefs: _prefs,
              ),
              withNavBar: false, // OPTIONAL VALUE. True by default.
              //  pageTransitionAnimation: PageTransitionAnimation.cupertino,
            );
          },
          context: context,
          msg: _model.error[0].value,
          ok: localization.text("ok"),
        );
      });
    }
    notifyListeners();
  }
}
