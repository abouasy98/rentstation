import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/get/contuctUsModel.dart';

class ContuctUsProvider with ChangeNotifier {
  String content;
  String phone;
  NetworkUtil _utils = new NetworkUtil();
  ContuctUsModel aboutUsModel;
  Future<ContuctUsModel> getContuct(BuildContext context) async {
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
    };
    Response response =
        await _utils.get("contact_us_text", context, headers: headers);
    if (response.statusCode == 200) {
      print("get terms sucsseful");

      aboutUsModel = ContuctUsModel.fromJson(response.data);
      content = aboutUsModel.data[0].contactUsText;
      phone = aboutUsModel.data[0].contactUsNumber;
      notifyListeners();
      return ContuctUsModel.fromJson(response.data);
    } else {
      print("error get terms data");
      return ContuctUsModel.fromJson(response.data);
    }
  }
}
