import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/aboutUsModel.dart';

class TermsProvider with ChangeNotifier {
  String content;
  String title;
  NetworkUtil _utils = new NetworkUtil();
  AboutUsModel aboutUsModel;
  Future<AboutUsModel> getTerms(BuildContext context) async {
    Map<String, String> headers = {
       "Accept-Language": localization.currentLanguage.toString(),
    };
    Response response =
        await _utils.get("terms-and-conditions",context, headers: headers);
    if (response.statusCode == 200) {
      print("get terms sucsseful");

      aboutUsModel = AboutUsModel.fromJson(response.data);
      content = aboutUsModel.data.content;
      title = aboutUsModel.data.title;
      notifyListeners();
      return AboutUsModel.fromJson(response.data);
    } else {
      print("error get terms data");
      return AboutUsModel.fromJson(response.data);
    }
  }
}
