import 'package:flutter/material.dart';

class ChoseLangeProvider with ChangeNotifier {
  bool selectLang = false;
  String lang;
  int id;
  void changeLang(String language) {
    selectLang = true;
    lang = language;
    notifyListeners();
  }

  void changeCountry(int id2) {
   
    id = id2;
    notifyListeners();
  }
}
