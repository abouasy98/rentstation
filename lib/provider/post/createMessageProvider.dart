import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Components/custom_alert.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/createMessageModel.dart';
import 'package:rentstation/screens/Chat/chat_room.dart';

class CreateMessageProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  createMessage(int userId, String token, BuildContext context) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    FormData formData = FormData.fromMap({});
    CreateMessageModel createMessageModel;
    Response response = await _utils.post("create_conversation/$userId",context,
        headers: headers, body: formData);
    if (response.statusCode == 200) {
      print("get create_conversation sucsseful");
      createMessageModel = CreateMessageModel.fromJson(response.data);
    } else {
      print("error get create_conversation data");
      createMessageModel = CreateMessageModel.fromJson(response.data);
    }
    if (createMessageModel.code == 200) {
      Navigator.push(context, MaterialPageRoute(builder: (c)=> ChatRoom(
        chateId: createMessageModel.data[0].id,
        // userName: createMessageModel.data[0].,
      )));
      return true;
    } else {
      print('error confirmed');
      CustomAlert().toast(
        context: context,
        title: "حدث خطا يرجي اعادة المحاولة ",
      );
    }
    notifyListeners();
  }
}
