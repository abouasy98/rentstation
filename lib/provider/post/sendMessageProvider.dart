import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/post/sendMessage.dart';

class ChatsendProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<SendMessage> sendMessage(
     int userId, String message, String token,BuildContext context) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    FormData formData = FormData.fromMap({
      "user_id": userId,
      "message": message,
    });
    Response response =
        await _utils.post("send_message",context, headers: headers, body: formData);
    if (response.statusCode == 200) {
      print("get ad_chat_demand sucsseful");
      return SendMessage.fromJson(response.data);
    } else {
      print("error get ad_chat_demand data");
      return SendMessage.fromJson(response.data);
    }
  }
}
