import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/post/addCommentModel.dart';

class AddCommentProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<AddCommentModel> addComment(
      String token, String comment, int id, BuildContext context) async {
    Map<String, String> headers = {
      'Authorization': 'Bearer $token',
    };
    FormData formData = FormData.fromMap({
      "ad_id": id,
      "comment": comment,
    });

    Response response = await _utils.post("ads/add_comment", context,
        body: formData, headers: headers);
    if (response.statusCode == 200) {
      print("add comment data sucsseful");
      return AddCommentModel.fromJson(response.data);
    } else {
      print("error add comment data");
      return AddCommentModel.fromJson(response.data);
    }
  }
}
