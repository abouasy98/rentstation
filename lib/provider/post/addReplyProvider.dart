import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/post/addReplayModel.dart';

class AddReplyProvider with ChangeNotifier {
  List<Replay> _reply = [];

  List<Replay> get reply {
    return [..._reply];
  }

  clear() {
    addReplayModel = null;

    _reply.clear();
    reply.clear();
    notifyListeners();
  }

  NetworkUtil _utils = new NetworkUtil();
  AddReplayModel addReplayModel;
  Future<AddReplayModel> addReply(
      String token, String comment, int id, BuildContext context) async {
    final List<Replay> replyAdList = [];
    Map<String, String> headers = {
      'Authorization': 'Bearer $token',
    };
    FormData formData = FormData.fromMap({
      "comment_id": id,
      "replay": comment,
    });

    Response response = await _utils.post("ads/add_replay", context,
        body: formData, headers: headers);
    if (response.statusCode == 200) {
      print("add comment data sucsseful");
      addReplayModel = AddReplayModel.fromJson(response.data);
      addReplayModel.data.replays.forEach((e) {
        replyAdList.add(Replay(
          id: e.id,
          createdAt: e.createdAt,
          email: e.email,
          phoneNumber: e.phoneNumber,
          commentId: e.commentId,
          replay: e.replay,
          user: e.user,
          userPhoto: e.userPhoto,
          userId: e.userId,
        ));
        print('replyid=${e.id}');
      });
      _reply = replyAdList.toList();
      notifyListeners();
      return addReplayModel;
    } else {
      print("error add comment data");
      return AddReplayModel.fromJson(response.data);
    }
  }
}
