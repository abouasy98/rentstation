import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/post/rateModel.dart';


class SendLocationProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  RateModel rateModel;
  CustomDialog dialog = CustomDialog();

  sendLocation(
    String token,
    double longitude,

    double latitude,
    // String orderId,
    String driverId,
    BuildContext context,
  ) async {
        print("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh i get my position");

    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    FormData formData = FormData.fromMap({
      "longitude": longitude,
      "latitude": latitude,
      // "order_id": orderId,
      "driver_id": driverId,
    });
    Response response =
        await _utils.post("track",context, body: formData, headers: headers);
    if (response == null) {
      print('error track');
      dialog.showWarningDialog(
        btnOnPress: () {},
        context: context,
        msg: localization.text("internet"),
      );
    }
    if (response.statusCode == 200) {
      print("get track sucsseful");
      rateModel = RateModel.fromJson(response.data);
    } else {
      print("error get track data");
      rateModel = RateModel.fromJson(response.data);
    }
    if (rateModel.code == 200) {
      // Fluttertoast.showToast(
      //     msg: localization.text("success_rating"),
      //     toastLength: Toast.LENGTH_LONG,
      //     timeInSecForIosWeb: 1,
      //     fontSize: 16.0);
      return true;
    } else {
      print('error track');
      // Fluttertoast.showToast(
      //     msg: localization.text("error"),
      //     toastLength: Toast.LENGTH_LONG,
      //     timeInSecForIosWeb: 1,
      //     fontSize: 16.0);
    }
    notifyListeners();
  }
}
