import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/post/adReport.dart';
class ReportCommentProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  String blockReason;
  Future<AdReport> reportComment(
      int id, String token, BuildContext context) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    FormData formData = FormData.fromMap({
      "comment_id": id,
      "report": blockReason,
    });
    Response response = await _utils.post("report_comment", context,
        headers: headers, body: formData);
    if (response.statusCode == 200) {
      print("get ad_chat_demand sucsseful");
      // Navigator.pushAndRemoveUntil(
      //   context,
      //   MaterialPageRoute(
      //     builder: (context) => TabScreen(),
      //   ),
      //   (Route<dynamic> route) => false,
      // );
      return AdReport.fromJson(response.data);
    } else {
      print("error get ad_chat_demand data");
      return AdReport.fromJson(response.data);
    }
  }
}
