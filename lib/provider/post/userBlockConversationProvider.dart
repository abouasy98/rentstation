import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/post/userBlockConversation.dart';
import 'package:rentstation/screens/tab_bar.dart';
import '../../helpers/sharedPref_helper.dart';
import '../../screens/adTabBarScreen.dart';
import '../../screens/auctionTabScreen.dart';
import '../../screens/noAction&AdsTab.dart';

class UserBlockConversationProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  String blockReason;

  Future<UserBlockConversation> sendMessage(
      int id, String token, BuildContext context) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    FormData formData = FormData.fromMap({
      " block_reason": blockReason,
    });
    Response response = await _utils.post(
        "user_block_conversation/$id", context,
        headers: headers, body: formData);
    if (response.statusCode == 200) {
      print("get ad_chat_demand sucsseful");

         if (Provider.of<SharedPref>(context, listen: false)
                    .noadsNoAuctions ==
                true) {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => NoAuctionOrAdTabScreen(),
                ),
                (Route<dynamic> route) => false,
              );
            } else if (Provider.of<SharedPref>(context, listen: false)
                    .auction ==
                true) {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => AuctionTabScreen(),
                ),
                (Route<dynamic> route) => false,
              );
            } else if (Provider.of<SharedPref>(context, listen: false).ads ==
                true) {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => AdTabScreen(),
                ),
                (Route<dynamic> route) => false,
              );
            } else {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => TabScreen(),
                ),
                (Route<dynamic> route) => false,
              );
            }

      return UserBlockConversation.fromJson(response.data);
    } else {
      print("error get ad_chat_demand data");
      return UserBlockConversation.fromJson(response.data);
    }
  }
}
