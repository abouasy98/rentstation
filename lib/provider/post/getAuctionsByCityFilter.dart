import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/post/getAuctionsByFilter.dart';

class GetAuctionsByCityFilterProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  int countryId;
  Future<GetAuctionsByCityFilter> getAuctions(
      double lat, double long, String token, BuildContext context) async {
    if (countryId == null) {
      countryId = Provider.of<SharedPref>(context, listen: false).countryId;
    }
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    FormData formData = FormData.fromMap({
      "latitude": lat != null ? lat : 30.7981684,
      "longitude": long != null ? long : 31.0067622,
      "country_id": countryId != null ? countryId : 1,
    });
    Response response = await _utils.post("get_auctions_city_filter", context,
        headers: headers, body: formData);
    if (response.statusCode == 200) {
      print("get get_ads sucsseful");
      return GetAuctionsByCityFilter.fromJson(response.data);
    } else {
      print("error get get_ads data");
      return GetAuctionsByCityFilter.fromJson(response.data);
    }
  }
}
