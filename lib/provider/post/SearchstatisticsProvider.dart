import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/post/searchstatisticsModel.dart';

class SearchstatisticsProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<Searchstatistics> searchstatistics(
      int id,  BuildContext context) async {

    FormData formData = FormData.fromMap({
      "category_id": id,
    });
    Response response = await _utils.post("search_statistics", context,
 body: formData);
    if (response.statusCode == 200) {
      print("get searchstatistics sucsseful");
      return Searchstatistics.fromJson(response.data);
    } else {
      print("error searchstatistics data");
      return Searchstatistics.fromJson(response.data);
    }
  }
}
