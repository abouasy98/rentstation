import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/post/createAuctionModel.dart';
import 'package:rentstation/screens/tab_bar.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:flutter/services.dart';
import 'package:dio/dio.dart';
import '../../helpers/sharedPref_helper.dart';
import '../../screens/adTabBarScreen.dart';
import '../../screens/auctionTabScreen.dart';
import '../../screens/noAction&AdsTab.dart';

class CreatAuctionProvider with ChangeNotifier {
  bool loading = false;
  NetworkUtil _utils = new NetworkUtil();

  CreateAuctionModel model;
  creatAuction(
      {String token,
      String title,
      int initialPrice,
      String details,
      String notes,
      String cityId,
      int period,
      int priceIncrease,
      int categoryId,
      String subCategoryId,
      List<Asset> photos,
      BuildContext context}) async {
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    List<MultipartFile> _photos = [];

    for (int i = 0; i < photos.length; i++) {
      if (photos[i] == null) {
      } else {
        ByteData byteData = await photos[i].getByteData();
        List<int> imageData = byteData.buffer.asUint8List();
        MultipartFile multipartFile = MultipartFile.fromBytes(imageData,
            filename: '${photos[i].toString()}.jpg');
        _photos.add(multipartFile);
      }
    }
    FormData formData = FormData.fromMap({
      "name": title,
      "period": period,
      "details": details,
      "notes": notes,
      "city_id": cityId,
      "initial_price": initialPrice,
      "price_increase": priceIncrease,
      "photo": _photos,
      "created_at": DateTime.now().toString().substring(0, 19),
      "category_id": categoryId,
      "sub_cat_id": subCategoryId,
    });

    Response response = await _utils.post("create_auction", context,
        body: formData, headers: headers);
    if (response.statusCode == 200) {
      print("create_auction sucsseful");
      model = CreateAuctionModel.fromJson(response.data);
      //return LoginModel.fromJson(response.data);
    } else {
      print("error create_auction");
      model = CreateAuctionModel.fromJson(response.data);
      // return LoginModel.fromJson(response.data);
    }
    if (model.code == 200) {
      loading = false;

      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  localization.text("auction_added"),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      if (Provider.of<SharedPref>(context, listen: false)
                              .noadsNoAuctions ==
                          true) {
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (context) => NoAuctionOrAdTabScreen(),
                          ),
                          (Route<dynamic> route) => false,
                        );
                      } else if (Provider.of<SharedPref>(context, listen: false)
                              .auction ==
                          true) {
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AuctionTabScreen(),
                          ),
                          (Route<dynamic> route) => false,
                        );
                      } else if (Provider.of<SharedPref>(context, listen: false)
                              .ads ==
                          true) {
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AdTabScreen(),
                          ),
                          (Route<dynamic> route) => false,
                        );
                      } else {
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (context) => TabScreen(index: 1,),
                          ),
                          (Route<dynamic> route) => false,
                        );
                      }
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    } else {
      loading = false;
      print('error create auction');
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  model.error[0].value,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    }
    notifyListeners();
  }
}
