import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/networkUtlis.dart';
import 'package:rentstation/models/post/addAuctionWinnerModel.dart';
import 'package:rentstation/screens/tab_bar.dart';
import '../../helpers/sharedPref_helper.dart';
import '../../screens/adTabBarScreen.dart';
import '../../screens/auctionTabScreen.dart';
import '../../screens/noAction&AdsTab.dart';

class AddAuctionWinnerProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  AddAuctionWinnerModel addAdToArchModel;
 
  addwinnerAuction(String token, int auctionId, int userId,BuildContext context) async {
    Map<String, String> headers = {
      "Accept-Language": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
     FormData formData = FormData.fromMap({
      "user_id": userId,
     
    });
    Response response =
        await _utils.post("add_auction_winner/$auctionId",context, headers: headers,body: formData);
    if (response.statusCode == 200) {
      print("get make_auction_archieve sucsseful");
      addAdToArchModel =   AddAuctionWinnerModel.fromJson(response.data);
    } else {
      print("error get make_auction_archieve data");
      addAdToArchModel =   AddAuctionWinnerModel.fromJson(response.data);
    }
    if (addAdToArchModel.code == 200) {
         
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                  localization.text('auction_is_end'),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                 if (Provider.of<SharedPref>(context, listen: false)
                    .noadsNoAuctions ==
                true) {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => NoAuctionOrAdTabScreen(),
                ),
                (Route<dynamic> route) => false,
              );
            } else if (Provider.of<SharedPref>(context, listen: false)
                    .auction ==
                true) {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => AuctionTabScreen(),
                ),
                (Route<dynamic> route) => false,
              );
            } else if (Provider.of<SharedPref>(context, listen: false).ads ==
                true) {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => AdTabScreen(),
                ),
                (Route<dynamic> route) => false,
              );
            } else {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => TabScreen(
                     index: 1,
                  ),
                ),
                (Route<dynamic> route) => false,
              );
            }
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    } else {
      print('error get make_auction_archieve');
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              backgroundColor: Colors.white,
              elevation: 3,
              contentPadding: EdgeInsets.all(5),
              children: <Widget>[
                Text(
                 addAdToArchModel.error[0].value,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    elevation: 3,
                    height: 45,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      localization.text("ok"),
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
    }
    notifyListeners();
  }
}
