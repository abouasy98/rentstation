import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/models/aboutUsModel.dart';
import 'package:rentstation/Repository/networkUtlis.dart';

class AboutUsProvider with ChangeNotifier {
  String content;
  String title='0';
  NetworkUtil _utils = new NetworkUtil();
  AboutUsModel aboutUsModel;
  Future<AboutUsModel> getAboutUs(BuildContext context) async {
    Map<String, String> headers = {
       "Accept-Language": localization.currentLanguage.toString(),
    };
    Response response = await _utils.get("about_us",context, headers: headers);
    if (response.statusCode == 200) {
      print("get about_us sucsseful");

      aboutUsModel = AboutUsModel.fromJson(response.data);
      content = aboutUsModel.data.content;
      title = aboutUsModel.data.title;
      notifyListeners();
      return AboutUsModel.fromJson(response.data);
    } else {
      print("error get about_us data");
      return AboutUsModel.fromJson(response.data);
    }
  }
}
