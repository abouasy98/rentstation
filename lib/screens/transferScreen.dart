import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:rentstation/screens/adTabBarScreen.dart';
import 'package:rentstation/screens/auctionTabScreen.dart';
import 'package:rentstation/screens/noAction&AdsTab.dart';
import 'package:rentstation/screens/tab_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TrasnferScreen extends StatelessWidget {
  final SharedPreferences prefs;
  TrasnferScreen({this.prefs});

  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      if (prefs.getBool('NoAdsNoAuctions') == true) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => NoAuctionOrAdTabScreen(
              index: 0,
            ),
          ),
          (Route<dynamic> route) => false,
        );
      } else if (prefs.getBool('Auctions') == true) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => AuctionTabScreen(
              index: 0,
            ),
          ),
          (Route<dynamic> route) => false,
        );
      } else if (prefs.getBool('Ads') == true) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => AdTabScreen(
              index: 0,
            ),
          ),
          (Route<dynamic> route) => false,
        );
      } else {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => TabScreen(
              index: 0,
            ),
          ),
          (Route<dynamic> route) => false,
        );
      }
    });

    return Container();
  }
}
