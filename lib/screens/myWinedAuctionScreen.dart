import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/get/myWinedAuctionProvider.dart';
import 'package:rentstation/widgets/MyWinedAuctionItem.dart';

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyWinedAuctionScreen extends StatelessWidget {
  final SharedPreferences prefs;
  MyWinedAuctionScreen({this.prefs});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Provider.of<MyWinedAuctionProvider>(context, listen: false)
            .getMyWinedAuction(prefs.getString('api_token'),context),
        builder: (context, snapShot) {
          if (snapShot.connectionState == ConnectionState.waiting) {
            return Center(child: Text(localization.text('waiting')));
          } else if (!snapShot.hasData) {
            return Center(child: Text(localization.text('no_posts')));
          } else {
            return Consumer<MyWinedAuctionProvider>(
              builder: (context, myWindeAuction, _) => ListView.builder(
                  primary: false,
                  shrinkWrap: true,
                  itemCount: myWindeAuction.favourite.length,
                  itemBuilder: (BuildContext context, int i) =>
                      MyWinedAuctionItem(
                        createdAt: myWindeAuction.favourite[i].createdAt,
                        id: myWindeAuction.favourite[i].id,
                        photo: myWindeAuction.favourite[i].photos[0].photo,
                        auctionWinner:
                            myWindeAuction.favourite[i].auctionWinner,
                        prefs: prefs,
                        period: myWindeAuction.favourite[i].period,
                        title: myWindeAuction.favourite[i].name,
                        prices: myWindeAuction.favourite[i].prices,
                        heighPrice: myWindeAuction.favourite[i].heighPrice,
                        winnerNamePhoto:
                            myWindeAuction.favourite[i].auctionWinnerPhoto,
                            auctionOwner: myWindeAuction.favourite[i].user ,
                        currency: myWindeAuction.favourite[i].currency,
                      )),
            );
          }
        });
  }
}
