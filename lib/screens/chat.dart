// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:rentstation/models/get/conversationMessage.dart';
// import 'package:rentstation/provider/get/getMessagesProvider.dart';
// import 'package:provider/provider.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import '../widgets/chat/message.dart';
// import '../widgets/chat/new_message.dart';

// class Chat extends StatefulWidget {
//   final String userPhoto;
//   final String name;
//   final int userId;
//   final SharedPreferences prefs;
//   final int id;

//   Chat({
//     this.userPhoto,
//     this.userId,
//     this.prefs,
//     this.name,
//     this.id,
//   });
//   @override
//   _ChatState createState() => _ChatState();
// }

// bool isInit = true;

// class _ChatState extends State<Chat> {
//   final formKey = GlobalKey<FormState>();
//   ConversationsMessages chatModel;
//   @override
//   void didChangeDependencies() async {
//     if (isInit) {
//       await Provider.of<GetMessagesProvider>(context, listen: false)
//           .getMessages(widget.id, widget.prefs.getString('api_token'), context)
//           .then((value) => setState(() {
//                 chatModel = value;
//               }));
//       setState(() {
//         isInit = false;
//       });
//     }
//     // TODO: implement didChangeDependencies
//     super.didChangeDependencies();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         titleSpacing: 1,
//         leading: IconButton(
//           icon: Icon(
//             Icons.arrow_back,
//             color: Colors.blue,
//           ),
//           onPressed: () {
//             Navigator.of(context).pop();
//           },
//         ),
//         backgroundColor: Colors.white,
//         elevation: 2,
//         title: SafeArea(
//           child: Padding(
//             padding: const EdgeInsets.only(bottom: 25),
//             child: ListTile(
//               leading: CircleAvatar(
//                 backgroundImage: CachedNetworkImageProvider(
//                   widget.userPhoto,
//                 ),
//                 radius: MediaQuery.of(context).size.height * 0.028,
//                 backgroundColor: Colors.grey[300],
//               ),
//               title: Text(
//                 widget.name,
//                 style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
//               ),
//             ),
//           ),
//         ),
       
//       ),
//       body: isInit
//           ? SpinKitThreeBounce(
//               color: Theme.of(context).primaryColor,
//               size: 22,
//             )
//           : Column(
//               children: <Widget>[
//                 Expanded(
//                   child: Message(
//                     chatModel: chatModel,
//                     imageUrl: widget.userPhoto,
//                   ),
//                 ),
//                 new_message(
//                   prefs: widget.prefs,
//                   idTo: widget.userId,
//                 ),
//               ],
//             ),
//     );
//   }
// }
