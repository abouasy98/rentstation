import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CircleUserImage extends StatelessWidget {
  final String userPhoto;

  const CircleUserImage({Key key, this.userPhoto}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 40,
      child: CachedNetworkImage(
        imageUrl: userPhoto,
        errorWidget: (context, url, error) => ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: Image.asset('images/16.jpg', fit: BoxFit.contain)),
        fadeInDuration: Duration(seconds: 2),
        placeholder: (context, url) => ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: Image.asset('images/16.jpg', fit: BoxFit.contain)),
        imageBuilder: (context, provider) {
          return ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: Image(
                image: provider,
                fit: BoxFit.cover,
              ));
        },
      ),
    );
  }
}
