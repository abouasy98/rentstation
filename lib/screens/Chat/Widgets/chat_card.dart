
// import 'package:flutter/material.dart';
// import 'package:tmam/BLoCs/get_from_shared_bloc.dart';
// import 'package:tmam/Models/my_chats_model.dart';
// import 'package:tmam/helpers/date_helper.dart';

// import '../chat_room.dart';

// class ChatCard extends StatelessWidget {
//   final Chat chat;

//   const ChatCard({Key key, this.chat}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return ListView(
//       shrinkWrap: true,
//       physics: ScrollPhysics(),
//       children: <Widget>[
//         InkWell(
//           onTap: () => Navigator.push(
//               context,
//               MaterialPageRoute(
//                   builder: (_) => ChatRoom(
//                       uniqueId: chat.orderId,
//                       phone: chat.phone,
//                       secondUserID: chat.secondUserId))),
//           child: Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: Directionality(
//               textDirection: TextDirection.rtl,
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: <Widget>[
//                   Row(
//                     children: <Widget>[
//                       CircleAvatar(
//                         backgroundImage: NetworkImage(chat.photo),
//                         radius: 25,
//                       ),
//                       SizedBox(width: 15),
//                       Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           Text(
//                               getFromShared.id == chat.userId
//                                   ? chat.secondUser
//                                   : chat.user,
//                               style: TextStyle(fontSize: 15)),
//                           Text(
//                             chat.lastMessage == null
//                                 ? "لا يوجد رسايل الان"
//                                 : chat.lastMessage == "null"
//                                   ? "صورة"
//                                   : chat.lastMessage  ,
//                             style: TextStyle(fontSize: 12),
//                           ),
//                         ],
//                       ),
//                     ],
//                   ),
//                   Text(
//                     TextHelper().formatDate(date: chat.createdAt),
//                     style: TextStyle(fontSize: 12),
//                     textAlign: TextAlign.end,
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//         Divider(height: 1, color: Colors.grey)
//       ],
//     );
//   }
// }
