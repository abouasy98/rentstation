import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/App/app_event.dart';
import 'package:rentstation/Bloc/chat_bloc.dart';
import 'package:rentstation/Components/customBtn.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/msg_model.dart';
import 'package:rentstation/provider/post/userBlockConversationProvider.dart';
import 'package:rentstation/widgets/custom_bottom_sheet.dart';
import 'package:rentstation/widgets/register_text_field.dart';
import 'package:provider/provider.dart';
import 'Widgets/chat_field.dart';
import 'Widgets/msg_card.dart';

class ChatRoom extends StatefulWidget {
  final int chateId;
  final String userName;

  const ChatRoom({Key key, this.chateId, this.userName}) : super(key: key);
  @override
  _ChatPageState createState() => new _ChatPageState();
}

class _ChatPageState extends State<ChatRoom> with TickerProviderStateMixin {
  List<MsgModel> _messages = [];

  ScrollController _scrollController;
  double _scrollPosition;

  _scrollListener() {
    setState(() {
      _scrollPosition = _scrollController.position.pixels;
    });
    if (_scrollPosition > 200.0) {
      FocusScope.of(context).requestFocus(FocusNode());
    }
  }

  @override
  void initState() {
    chatBloc.updateContext(context);
    chatBloc.updateChatID(widget.chateId);
    chatBloc.add(Init());
    super.initState();

    chatBloc.messages.listen((event) {
      setState(() {
        _messages.insert(0, event);
      });
      // Future.delayed(Duration(microseconds: 300), () {

      // });
    });
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    super.dispose();
    chatBloc.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          child: Icon(Icons.arrow_back_ios),
        ),
        centerTitle: true,
        title: Text(widget.userName ?? ""),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              radius: 15,
              backgroundColor: Colors.red[400],
              child: IconButton(
                iconSize: 15,
                onPressed: () async {
                  CustomBottomSheet().show(
                      context: context,
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            BlockTextField(
                              onChange: (v) {
                                Provider.of<UserBlockConversationProvider>(
                                        context,
                                        listen: false)
                                    .blockReason = v;
                              },
                              label: localization.text('block_Reason'),
                              icon: Icons.label,
                              type: TextInputType.text,
                            ),
                            SizedBox(height: 20),
                            CustomBtn(
                              text: localization.text('confirm_block'),
                              color: Colors.red,
                              onTap: () async {
                                Navigator.pop(context);
                                await Provider.of<
                                            UserBlockConversationProvider>(
                                        context,
                                        listen: false)
                                    .sendMessage(
                                        widget.chateId,
                                        Provider.of<SharedPref>(context,
                                                listen: false)
                                            .token,
                                        context);
                              },
                              txtColor: Colors.white,
                            )
                          ],
                        ),
                      ));
                },
                icon: Icon(
                  Icons.priority_high,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                child: ListView.builder(
                    controller: _scrollController,
                    shrinkWrap: true,
                    reverse: true,
                    itemCount: _messages.length,
                    padding: new EdgeInsets.all(6.0),
                    itemBuilder: (_, int index) {
                      return MsgCard(
                        model: _messages[index],
                      );
                    }),
              ),
              ChatField()
            ],
          ),
        ],
      ),
    );
  }
}
