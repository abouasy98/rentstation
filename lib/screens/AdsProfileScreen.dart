import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'adDetails.dart';

class AdsProfileScreen extends StatelessWidget {
  final int adid;
  final String adphoto;
  final String title;
  final String city;
  final DateTime createdAt;
  final int price;
 
  AdsProfileScreen(
      {this.adid,
      this.adphoto,
      this.city,
      this.createdAt,
  
      this.price,
      this.title});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => AdDetails(
                  id: adid,
                )));
      },
      child: Container(
          height: MediaQuery.of(context).size.height * 0.15,
          width: MediaQuery.of(context).size.width,
          child: Card(
            elevation: 10,
            clipBehavior: Clip.antiAlias,
            child: Container(
              height: 120,
              padding: const EdgeInsets.all(0),
              child: Row(children: [
                Expanded(
                  flex: 6,
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: CachedNetworkImageProvider(
                            adphoto,
                          ),
                          fit: BoxFit.fill),
                    ),
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
                Expanded(
                  flex: 14,
                  child: Container(
                    padding: const EdgeInsets.only(top: 5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(title,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.bold)),
                        Row(
                          children: <Widget>[
                            Text(
                              '${localization.text('price')} : ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15),
                            ),
                            Text(
                                '${NumberFormat.currency(locale: localization.currentLanguage.toString() == "en"?null:'ar_EG',name: Provider.of<SharedPref>(context, listen: false).currency, decimalDigits: 0).format(price)}',
                                style: TextStyle(fontSize: 15)),
                          ],
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                city,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(
                                '${localization.text('ago')} ${DateTime.now().difference(DateTime(createdAt.year, createdAt.month, createdAt.day)).inDays} ${localization.text('day')}',
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ]),
            ),
          )),
    );
  }
}
