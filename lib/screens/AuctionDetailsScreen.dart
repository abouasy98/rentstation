import 'dart:io';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/provider/get/getAuctionsByIdWithoutProvider.dart';
import 'package:rentstation/provider/post/auctionAddPriceProvider.dart';
import 'package:rentstation/screens/userProfile.dart';
import 'package:rentstation/widgets/showGeneralDialog.dart';
import 'package:rentstation/models/get/getAuctionById.dart';
import 'package:rentstation/provider/get/getAuctionByIdProvider.dart';
import 'package:rentstation/screens/login.dart';
import 'package:rentstation/screens/newsPhotoGallary.dart';
import 'package:rentstation/widgets/weekcountdawn.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'ListofAuctionersScreen.dart';

class AuctionsDetails extends StatefulWidget {
  final int id;

  const AuctionsDetails({Key key, this.id}) : super(key: key);

  @override
  _AuctionsDetailsState createState() => _AuctionsDetailsState();
}

class _AuctionsDetailsState extends State<AuctionsDetails> {
  GetAuctionById _model;
  String token;
  var _loadedSpinner = false;
  bool hiddenNumber = true;
  int price;
  CustomDialog _dialog = CustomDialog();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _form = GlobalKey<FormState>();

  List<String> photos = [];
  _getShared() async {
    if (Provider.of<SharedPref>(context, listen: false).token != null) {
      Provider.of<GetAuctionsByIdProvider>(context, listen: false)
          .getAuctionsByid(widget.id,
              Provider.of<SharedPref>(context, listen: false).token, context)
          .then((res) {
        setState(() {
          _model = res;
        });
        for (int i = 0; i < _model.data.photos.length; i++) {
          photos.add(_model.data.photos[i].photo);
        }
      });
    } else {
      Provider.of<GetAuctionsByIdWithOutAuthProvider>(context, listen: false)
          .getAuctionsByidwithoutAuth(widget.id, context)
          .then((res) {
        setState(() {
          _model = res;
        });
        for (int i = 0; i < _model.data.photos.length; i++) {
          photos.add(_model.data.photos[i].photo);
        }
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _createDynamicLink();
    _getShared();
  }

  String shareLink;
  Future<void> _createDynamicLink() async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://rentstation.page.link',
      link: Uri.parse('https://rentstation.page.link/0/${widget.id}'),
      androidParameters: AndroidParameters(
        packageName: 'com.tqnee.rentstation',
        minimumVersion: 0,
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      iosParameters: IosParameters(
        bundleId: 'com.tqnee.rentstation',
        minimumVersion: "4",
      ),
    );

    Uri url;
    final ShortDynamicLink shortLink = await parameters.buildShortLink();
    url = shortLink.shortUrl;
    setState(() {
      shareLink = url.toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Directionality(
      textDirection: localization.currentLanguage.toString() == "en"
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(localization.text('auction_details')),
          centerTitle: true,
          backgroundColor: Theme.of(context).primaryColor,
          leading: InkWell(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back_ios),
          ),
          actions: <Widget>[
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                  onPressed: () async {
                    String _msg;
                    StringBuffer _sb = new StringBuffer();
                    setState(() {
                      _sb.write("عنوان الاعلان : ${_model.data.name} \n");

                      _sb.write(shareLink);

                      _msg = _sb.toString();
                    });

                    if (_model.data.photos.length != 0) {
                      var request = await HttpClient()
                          .getUrl(Uri.parse(_model.data.photos[0].photo));
                      var response = await request.close();
                      Uint8List bytes =
                          await consolidateHttpClientResponseBytes(response);
                      await Share.file(
                          'ESYS AMLOG', 'amlog.jpg', bytes, 'image/jpg',
                          text: _msg);
                    } else {
                      Share.text("title", _msg, 'text/plain');
                    }
                  },
                  icon: Icon(Icons.share),
                )),
          ],
        ),
        backgroundColor: Colors.white,
        floatingActionButton: _model == null
            ? Center(
                child: SpinKitThreeBounce(
                  size: 25,
                  color: Colors.white,
                ),
              )
            : (_model.data.archive == 1 &&
                        _model.data.userId !=
                            Provider.of<SharedPref>(context, listen: false)
                                .id &&
                        _model.data.auctionWinnerId !=
                            Provider.of<SharedPref>(context, listen: false)
                                .id) ||
                    (_model.data.auctionWinnerId ==
                        Provider.of<SharedPref>(context, listen: false).id)
                ? Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                  )
                : FloatingActionButton(
                    backgroundColor: Theme.of(context).primaryColor,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        FaIcon(FontAwesome.hammer),
                        Text(
                          _model.data.userId ==
                                  Provider.of<SharedPref>(context,
                                          listen: false)
                                      .id
                              ? localization.text('Bids')
                              : localization.text('Bid_Now'),
                          style: TextStyle(fontSize: 10),
                        ),
                      ],
                    ),
                    onPressed: () {
                      if (Provider.of<SharedPref>(context, listen: false)
                              .token ==
                          null) {
                        _dialog.showOptionDialog(
                            context: context,
                            msg: localization.text('should_login'),
                            okFun: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (c) => SignInScreen(true)));
                            },
                            okMsg: localization.text('ok'),
                            cancelMsg: localization.text('no'),
                            cancelFun: () {
                              return;
                            });
                      } else if (_model.data.userId ==
                          Provider.of<SharedPref>(context, listen: false).id) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ListofAuctioners(
                                  prices: _model.data.prices,
                                  model: _model,
                                )));
                      } else {
                        GeneraDialog().show(
                          context,
                          Material(
                            child: Container(
                              height: screenSize.height * 0.35,
                              width: screenSize.width * 0.9,
                              decoration: BoxDecoration(
                                color: Colors.black54,
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SizedBox(
                                    height: screenSize.height * 0.05,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(
                                      12.0,
                                    ),
                                    child: Center(
                                      child: Column(children: [
                                      if (_model.data.heighPrice == null)
                                          Text(
                                            '${intl.NumberFormat.currency(locale: localization.currentLanguage.toString() == "en" ? null : 'ar_EG', name: _model.data.currency, decimalDigits: 0).format(_model.data.initialPrice + _model.data.priceIncrease)}${localization.text('Your_price_should_not_be_less_than')}',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        if (_model.data.heighPrice != null)
                                          Text(
                                            '${intl.NumberFormat.currency(locale: localization.currentLanguage.toString() == "en" ? null : 'ar_EG', name: _model.data.currency, decimalDigits: 0).format(_model.data.heighPrice + _model.data.priceIncrease)}${localization.text('Your_price_should_not_be_less_than')}',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        SizedBox(
                                          height: screenSize.height * 0.01,
                                        ),
                                        Container(
                                          height: screenSize.height * 0.1,
                                          width: screenSize.width * 0.37,
                                          child: Form(
                                            key: _form,
                                            child: TextFormField(
                                              keyboardType:
                                                  TextInputType.number,
                                              textAlign: TextAlign.right,
                                              validator: (value) {
                                                if (value.isEmpty) {
                                                  return '${localization.text('please_add_price')}';
                                                }
                                                if (_model.data.prices.length >
                                                    0) {
                                                  if (int.parse(value) <
                                                      (_model
                                                              .data
                                                              .prices[_model
                                                                      .data
                                                                      .prices
                                                                      .length -
                                                                  1]
                                                              .price +
                                                          _model.data
                                                              .priceIncrease)) {
                                                    return '${_model.data.currency} ${_model.data.prices[_model.data.prices.length - 1].price + _model.data.priceIncrease}\t${localization.text('Your_price_should_not_be_less_than')}';
                                                  }
                                                }
                                                if (_model.data.prices.length ==
                                                    0) {
                                                  if (int.parse(value) <
                                                      (_model.data
                                                              .priceIncrease +
                                                          _model.data
                                                              .initialPrice)) {
                                                    return '${_model.data.currency} ${_model.data.priceIncrease + _model.data.initialPrice}\t${localization.text('Your_price_should_not_be_less_than')}';
                                                  }
                                                }
                                                return null;
                                              },
                                              onChanged: (value) {
                                                price = int.parse(value);
                                              },
                                              decoration: InputDecoration(
                                                contentPadding: EdgeInsets.only(
                                                    top: 10, right: 10),
                                                border: new OutlineInputBorder(
                                                  borderRadius:
                                                      const BorderRadius.all(
                                                          Radius.circular(10)),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ]),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      bottom: 12,
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          height: screenSize.height * 0.06,
                                          width: screenSize.width * 0.25,
                                          child: _loadedSpinner
                                              ? Center(
                                                  child:
                                                      CircularProgressIndicator(),
                                                )
                                              : RaisedButton(
                                                  elevation: 8,
                                                  color: Colors.red,
                                                  onPressed: () async {
                                                    final _isValid = _form
                                                        .currentState
                                                        .validate();
                                                    if (!_isValid) {
                                                      return;
                                                    }
                                                    _form.currentState.save();

                                                    setState(() {
                                                      _loadedSpinner = true;
                                                    });

                                                    await Provider.of<
                                                                AuctionAddPrice>(
                                                            context,
                                                            listen: false)
                                                        .auctionAddPrice(
                                                            context: context,
                                                            id: _model.data.id,
                                                            token: Provider.of<
                                                                        SharedPref>(
                                                                    context,
                                                                    listen:
                                                                        false)
                                                                .token,
                                                            price: price);
                                                    setState(() {
                                                      _loadedSpinner = false;
                                                    });
                                                  },
                                                  child: Text(
                                                    localization
                                                        .text('Bid_Now'),
                                                    style:
                                                        TextStyle(fontSize: 15),
                                                  ),
                                                  textColor: Colors.white,
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15),
                                                  ),
                                                ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }
                    },
                  ),
        body: _model == null
            ? Center(
                child: SpinKitThreeBounce(
                  size: 25,
                  color: Theme.of(context).primaryColor,
                ),
              )
            : (_model.data.archive == 1 &&
                    _model.data.userId !=
                        Provider.of<SharedPref>(context, listen: false).id &&
                    _model.data.auctionWinnerId !=
                        Provider.of<SharedPref>(context, listen: false).id)
                ? Center(
                    child: Text(
                    localization.text('This_auction_is_archived'),
                    style: TextStyle(color: Colors.black),
                  ))
                : ListView(
                    children: <Widget>[
                      SizedBox(height: 20),
                      Directionality(
                        textDirection:
                            localization.currentLanguage.toString() == "en"
                                ? TextDirection.ltr
                                : TextDirection.rtl,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 10, left: 10),
                          child: Padding(
                            padding: const EdgeInsets.only(right: 10, left: 10),
                            child: Text(
                              _model.data.name,
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold),
                              textAlign:
                                  localization.currentLanguage.toString() ==
                                          "en"
                                      ? TextAlign.left
                                      : TextAlign.right,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      _imageSlider(),
                      SizedBox(height: 30),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.black54,
                        ),
                        height: MediaQuery.of(context).size.height * 0.15,
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  if (hiddenNumber) {
                                                    if (Provider.of<SharedPref>(
                                                                context,
                                                                listen: false)
                                                            .token ==
                                                        null) {
                                                      _dialog.showOptionDialog(
                                                          context: context,
                                                          msg: localization.text(
                                                              'should_login'),
                                                          okFun: () {
                                                            Navigator.push(
                                                                context,
                                                                MaterialPageRoute(
                                                                    builder: (c) =>
                                                                        SignInScreen(true)));
                                                          },
                                                          okMsg: localization
                                                              .text('yes'),
                                                          cancelMsg:
                                                              localization
                                                                  .text('no'),
                                                          cancelFun: () {
                                                            return;
                                                          });
                                                    } else {
                                                      hiddenNumber = false;
                                                    }
                                                  } else {
                                                    hiddenNumber = true;
                                                  }
                                                });
                                              },
                                              child: Text(
                                                  hiddenNumber
                                                      ? ' ${_model.data.phoneNumber.replaceRange(3, 8, '*****')} '
                                                      : ' ${_model.data.phoneNumber} ',
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                  textAlign: localization
                                                              .currentLanguage
                                                              .toString() ==
                                                          "en"
                                                      ? TextAlign.left
                                                      : TextAlign.right),
                                            ),
                                            GestureDetector(
                                              onTap: () async {
                                                if (Provider.of<SharedPref>(
                                                            context,
                                                            listen: false)
                                                        .token ==
                                                    null) {
                                                  _dialog.showOptionDialog(
                                                      context: context,
                                                      msg: localization
                                                          .text('should_login'),
                                                      okFun: () {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder: (c) =>
                                                                    SignInScreen(true)));
                                                      },
                                                      okMsg: localization
                                                          .text('yes'),
                                                      cancelMsg: localization
                                                          .text('no'),
                                                      cancelFun: () {
                                                        return;
                                                      });
                                                } else {
                                                  UrlLauncher.launch(
                                                      'tel: ${_model.data.phoneNumber.toString()}');
                                                }
                                              },
                                              child: Icon(
                                                Icons.call,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ]),
                                      InkWell(
                                        onTap: () {
                                          if (Provider.of<SharedPref>(context,
                                                      listen: false)
                                                  .token ==
                                              null) {
                                            _dialog.showOptionDialog(
                                                context: context,
                                                msg: localization
                                                    .text('should_login'),
                                                okFun: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (c) =>
                                                              SignInScreen(true)));
                                                },
                                                okMsg: localization.text('yes'),
                                                cancelMsg:
                                                    localization.text('no'),
                                                cancelFun: () {
                                                  return;
                                                });
                                          } else {
                                            Navigator.of(context).push(
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        UserProfile(
                                                          userid: _model
                                                              .data.userId,
                                                        )));
                                          }
                                        },
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            _model.data.userPhoto != null
                                                ? CachedNetworkImage(
                                                    imageUrl:
                                                        _model.data.userPhoto,
                                                    fadeInDuration:
                                                        Duration(seconds: 2),
                                                    placeholder: (context,
                                                            url) =>
                                                        CircleAvatar(
                                                            backgroundImage:
                                                                AssetImage(
                                                                    'images/16.jpg')),
                                                    imageBuilder:
                                                        (context, provider) {
                                                      return CircleAvatar(
                                                          backgroundImage:
                                                              provider);
                                                    },
                                                  )
                                                : CircleAvatar(
                                                    backgroundImage: AssetImage(
                                                        'images/16.jpg')),
                                            SizedBox(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.02),
                                            Container(
                                              alignment: localization
                                                          .currentLanguage
                                                          .toString() ==
                                                      "en"
                                                  ? Alignment.centerLeft
                                                  : Alignment.centerRight,
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.45,
                                              child: Text(_model.data.user,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ]),
                                Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                          '${localization.text('ago')} ${DateTime.now().difference(DateTime(_model.data.createdAt.year, _model.data.createdAt.month, _model.data.createdAt.day)).inDays} ${localization.text('day')}',
                                          style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.white,
                                          ),
                                          textAlign: localization
                                                      .currentLanguage
                                                      .toString() ==
                                                  "en"
                                              ? TextAlign.right
                                              : TextAlign.left),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Container(
                                            alignment: localization
                                                        .currentLanguage
                                                        .toString() ==
                                                    "en"
                                                ? Alignment.centerRight
                                                : Alignment.centerLeft,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.25,
                                            child: Text(
                                              _model.data.city,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                          Icon(
                                            Icons.location_on,
                                            color: Colors.white,
                                          ),
                                        ],
                                      ),
                                    ]),
                              ]),
                        ),
                      ),
                      if (_model.data.auctionWinnerId ==
                              Provider.of<SharedPref>(context, listen: false)
                                  .id &&
                          Provider.of<SharedPref>(context, listen: false)
                                  .token !=
                              null)
                        SizedBox(
                          height: 50,
                        ),
                      if (_model.data.auctionWinnerId !=
                              Provider.of<SharedPref>(context, listen: false)
                                  .id ||
                          Provider.of<SharedPref>(context, listen: false)
                                  .token ==
                              null)
                        Container(
                            child: WeekCountdown(
                                createdAt: _model.data.createdAt,
                                period: _model.data.period),
                            decoration: BoxDecoration(color: Colors.red)),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.15,
                        decoration: BoxDecoration(
                          color: Colors.black54,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            '${localization.text('Current_Price')}: ',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13),
                                          ),
                                          Text(
                                            _model.data.heighPrice == null
                                                ? '${intl.NumberFormat.currency(locale: localization.currentLanguage.toString() == "en"?null:'ar_EG',name: _model.data.currency, decimalDigits: 0).format(_model.data.initialPrice)}'
                                                : '${intl.NumberFormat.currency(locale: localization.currentLanguage.toString() == "en"?null:'ar_EG',name: _model.data.currency, decimalDigits: 0).format(_model.data.heighPrice)}',
                                            style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          '${localization.text('Bids')} :',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 13),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          '${_model.data.prices.length}',
                                          style: TextStyle(
                                            fontSize: 13,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Container(
                                  alignment:
                                      localization.currentLanguage.toString() ==
                                              "en"
                                          ? Alignment.centerLeft
                                          : Alignment.centerRight,
                                  width:
                                      MediaQuery.of(context).size.width * 0.5,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        '${localization.text('Min_Increment')}: ',
                                        textAlign: localization.currentLanguage
                                                    .toString() ==
                                                "en"
                                            ? TextAlign.left
                                            : TextAlign.right,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white,
                                            fontSize: 13),
                                      ),
                                      Text(
                                        '${intl.NumberFormat.currency(locale: localization.currentLanguage.toString() == "en"?null:'ar_EG',name: _model.data.currency, decimalDigits: 0).format(_model.data.priceIncrease)}',
                                        style: TextStyle(
                                          fontSize: 13,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ]),
                        ),
                      ),
                      SizedBox(height: 30),
                      Directionality(
                        textDirection: TextDirection.rtl,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            _model.data.details,
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                            textAlign: TextAlign.right,
                          ),
                        ),
                      ),
                      SizedBox(height: 100),
                    ],
                  ),
      ),
    );
  }

  Widget _imageSlider() {
    return InkWell(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => NewsPhotoGallary(
                    images: photos,
                  ))),
      child: Container(
        height: 300,
        child: Carousel(
          boxFit: BoxFit.cover,
          images: List.generate(_model.data.photos.length, (int index) {
            return _model.data.photos[index].photo != null
                ? CachedNetworkImage(
                    imageUrl: _model.data.photos[index].photo,
                    fadeInDuration: Duration(seconds: 2),
                    placeholder: (context, url) => Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('images/loading.gif'),
                                fit: BoxFit.fill),
                          ),
                        ),
                    imageBuilder: (context, provider) {
                      return Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: provider, fit: BoxFit.fill),
                        ),
                      );
                    })
                : Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('images/loading.gif'), fit: BoxFit.fill),
                    ),
                  );
          }),
          autoplay: false,
          dotSize: 4,
          dotSpacing: _model.data.photos.length > 13
              ? 300 / _model.data.photos.length
              : 20,
          autoplayDuration: Duration(seconds: 1),
          dotBgColor: Colors.black26,
          animationCurve: Curves.decelerate,
          animationDuration: Duration(milliseconds: 1000),
          indicatorBgPadding: 10,
          dotColor: Colors.white,
        ),
      ),
    );
  }
}
