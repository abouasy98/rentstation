import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Components/buttonSignIn.dart';
import 'package:rentstation/Components/customBtn.dart';
import 'package:rentstation/Components/inputTextField.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/models/get/registertype.dart';
import 'package:rentstation/provider/auth/loginProvider.dart';
import 'package:rentstation/provider/get/getregistertype.dart';
import 'package:rentstation/screens/registerType.dart';
import 'package:provider/provider.dart';
import 'forgetPassword.dart';

class SignInScreen extends StatefulWidget {
  static const routeName = 'SignInScreen';
  final bool drawer;
  SignInScreen(this.drawer);
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  FirebaseMessaging _fcm = FirebaseMessaging();
  String _deviceToken;

  @override
  void initState() {
    _fcm.getToken().then((response) {
      setState(() {
        _deviceToken = response;
      });
      print('The device Token is :' + _deviceToken);
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  String x;
  bool a7a = true;
  RegisterType txstfieldprovider;
  @override
  void didChangeDependencies() async {
    if (a7a) {
      txstfieldprovider =
          await Provider.of<GetRegisterType>(context, listen: false)
              .getRegisterType(context);

      String email = txstfieldprovider.data.value;
      if (email == 'email' || email == 'بريد الكتروني') {
        x = localization.text('email');
      } else {
        x = localization.text('phone_number');
      }

      setState(() {
        a7a = false;
      });
    }
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var logain = Provider.of<LoginProvider>(context, listen: false);
    return Scaffold(
      // appBar: AppBar(
      //   title: Text(localization.text('login')),
      //   centerTitle: true,
      //   actions: [
      //     Padding(
      //       padding: const EdgeInsets.only(left: 8.0, right: 10),
      //       child: IconButton(
      //         onPressed: () {
      //           Navigator.push(
      //               context,
      //               MaterialPageRoute(
      //                   builder: (_) => LanguageSelect(
      //                         changelang: true,
      //                       )));
      //         },
      //         icon: Icon(
      //           FontAwesomeIcons.language,
      //           color: Colors.black87,
      //           size: 35,
      //         ),
      //       ),
      //     ),
      //   ],
      // ),
      resizeToAvoidBottomInset: true,
      body: a7a
          ? SpinKitThreeBounce(
              color: Theme.of(context).primaryColor,
              size: 22,
            )
          : ListView(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              children: <Widget>[
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height * .95,
                    width: MediaQuery.of(context).size.width * .9,
                    alignment: Alignment.center,
                    child: Form(
                      autovalidate: true,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: MediaQuery.of(context).size.height * .02,
                          ),
                          InputFieldArea(
                            labelTxt: x == localization.text('email')
                                ? localization.text("email")
                                : localization.text("phone_number"),
                            // 'رقم الجوال',
                            hint: localization.text("write"), //"اكتب هنا",
                            //  errorTxt: "",
                            //  errorTxt: "",
                            changedFunction: (v) {
                              x == localization.text('email')
                                  ? logain.email = v
                                  : logain.phone = v;
                            },
                            textInputType: x == localization.text('email')
                                ? TextInputType.text
                                : TextInputType.number,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          InputFieldArea(
                            labelTxt: localization.text("password"),

                            //  'كلمة المرور',
                            hint: localization.text("write"), //"اكتب هنا",
                            textInputType: TextInputType.text,
                            show: false,
                            inputFieldWithBorder: false,
                            changedFunction: (v) {
                              logain.password = v;
                            },
                            // errorTxt: "",
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          SignInButton(
                            txtColor: Colors.white,
                            onPressSignIn: () {
                              print('deviceToken=$_deviceToken');
                              logain.login(_deviceToken, context, widget.drawer);
                            },
                            btnWidth: MediaQuery.of(context).size.width,
                            btnHeight: MediaQuery.of(context).size.height * .07,
                            btnColor: Theme.of(context).primaryColor,
                            buttonText: localization.text("login"),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: FlatButton(
                              child: Text(localization.text("forget_password"),
                                  // 'نسيت كلمة المرور ',
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor)),
                              onPressed: () {
                                pushNewScreen(
                                  context,
                                  screen: ForgetPassword(),
                                  withNavBar:
                                      false, // OPTIONAL VALUE. True by default.
                                  pageTransitionAnimation:
                                      PageTransitionAnimation.cupertino,
                                );
                              },
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: Container(
                              width: MediaQuery.of(context).size.width * .6,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Expanded(
                                      child: Divider(
                                    thickness: 1,
                                  )),
                                  Text(localization.text('or')),
                                  Expanded(
                                      child: Divider(
                                    thickness: 1,
                                  )),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Center(
                            child: Container(
                              width: MediaQuery.of(context).size.width * .4,
                              child: CustomBtn(
                                txtColor: Colors.white,
                                onTap: () {
                                  pushNewScreen(
                                    context,

                                    screen: RegisterTypeScreen(),
                                    withNavBar:
                                        false, // OPTIONAL VALUE. True by default.
                                    pageTransitionAnimation:
                                        PageTransitionAnimation.cupertino,
                                  );

                                  //  Navigator.of(context).push(MaterialPageRoute(
                                  //     builder: (_) => SignUpScreen()));
                                },
                                color: Colors.grey,
                                text: localization.text("sign_up"),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
