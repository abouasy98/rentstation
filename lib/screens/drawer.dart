import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/provider/auth/logoutProvider.dart';
import 'package:rentstation/provider/get/GetAppLogoProvider.dart';
import 'package:rentstation/screens/about.dart';
import 'package:rentstation/screens/login.dart';
import 'package:rentstation/screens/myAdScreen.dart';
import 'package:rentstation/screens/myAuctionsScreen.dart';
import 'package:rentstation/screens/tab_bar.dart';
import 'package:rentstation/screens/terms.dart';

import 'package:provider/provider.dart';
import 'package:rentstation/widgets/custom_option_card.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Intro/CounrtySelection.dart';
import 'Intro/languageSelect.dart';
import 'adTabBarScreen.dart';
import 'auctionTabScreen.dart';
import 'commision.dart';
import 'contuctUsScreen.dart';
import 'favourites.dart';
import 'noAction&AdsTab.dart';

class Mydrawrer extends StatefulWidget {
  @override
  _MydrawrerState createState() => _MydrawrerState();
}

class _MydrawrerState extends State<Mydrawrer> {
  SharedPreferences _preferences;
  FirebaseMessaging _fcm = FirebaseMessaging();
  String _deviceToken;
  CustomOptionCard _optionCard = CustomOptionCard();
  bool loaded = true;
  @override
  void initState() {
    _fcm.getToken().then((response) {
      setState(() {
        _deviceToken = response;
      });
      print('The device Token is :' + _deviceToken);
    });
    super.initState();
  }

  Widget drawerIcon({String text, Function func, IconData icon}) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Column(children: <Widget>[
        InkWell(
          onTap: func,
          child: ListTile(
            title: Text(
              text,
              style: TextStyle(color: Colors.black, fontSize: 20),
            ),
            leading: Icon(
              icon,
              color: Colors.blueAccent,
            ),
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 18,
              color: Colors.black,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            right: 15,
            left: 15,
          ),
          child: Divider(
            color: Colors.grey[500],
          ),
        ),
      ]),
    );
  }

  @override
  void didChangeDependencies() async {
    if (loaded) {
      var _instance = await SharedPreferences.getInstance();
      setState(() {
        _preferences = _instance;
      });
      setState(() {
        loaded = false;
      });
    }
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return loaded
        ? SpinKitThreeBounce(
            color: Theme.of(context).primaryColor,
            size: 22,
          )
        : Container(
            child: Drawer(
              child: Directionality(
                textDirection: localization.currentLanguage.toString() == "en"
                    ? TextDirection.ltr
                    : TextDirection.rtl,
                child: ListView(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.04,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CircleAvatar(
                            radius: 35,
                            backgroundImage:
                                _preferences.getString('image') != null
                                    ? CachedNetworkImageProvider(
                                        _preferences.getString('image'),
                                      )
                                    : AssetImage(
                                        'images/16.jpg',
                                      ),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.height * 0.02,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                _preferences.getString('name') != null
                                    ? ' ${_preferences.getString('name')}'
                                    : '${localization.text('welcome')}\t${Provider.of<GetAppLogoProvider>(context, listen: false).name}',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 13),
                              ),
                              Visibility(
                                visible: Provider.of<SharedPref>(context,
                                            listen: false)
                                        .token !=
                                    null,
                                child: Text(
                                    _preferences.getString('phone') != null
                                        ? _preferences.getString('phone')
                                        : 'رقم الهاتف',
                                    style: TextStyle(color: Colors.grey)),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: Provider.of<SharedPref>(context, listen: false)
                                  .token !=
                              null &&
                          (_preferences.getBool('Auctions') != true),
                      child: _optionCard.optionCard(
                          label: localization.text('my_ads_only'),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => MyAdScreen(
                                      prefs: _preferences,
                                    )));
                          },
                          icon: Icons.home),
                    ),
                    Visibility(
                        visible:
                            (Provider.of<SharedPref>(context, listen: false)
                                        .token !=
                                    null) &&
                                (_preferences.getBool('Ads') != true),
                        child: _optionCard.optionCard(
                            label: localization.text('my_auctions'),
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => MyAuctionScreen(
                                        prefs: _preferences,
                                      )));
                            },
                            icon: FontAwesome.hammer)),
                    Visibility(
                        visible: Provider.of<SharedPref>(context, listen: false)
                                .token !=
                            null,
                        child: _optionCard.optionCard(
                            label: localization.text('favourite'),
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => Favourites()));
                            },
                            icon: Icons.favorite)),
                    _optionCard.optionCard(
                        label: localization.text('change_country'),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => CountrySelection(
                                        changelang: true,
                                        key: UniqueKey(),
                                      )));
                        },
                        icon: Icons.language),
                    _optionCard.optionCard(
                        label: localization.text('languageAnCountry'),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => LanguageSelect(
                                        changelang: true,
                                        key: UniqueKey(),
                                      )));
                        },
                        icon: Icons.language),
                    _optionCard.optionCard(
                        label: localization.text('commision'),
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (_) => Commision()));
                        },
                        icon: Icons.language),
                    _optionCard.optionCard(
                        label: localization.text('contact_us'),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ContuctUs()));
                        },
                        icon: Icons.home_repair_service),
                    _optionCard.optionCard(
                        label: localization.text('terms'),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => TermsApp()));
                        },
                        icon: Icons.home_repair_service),
                    _optionCard.optionCard(
                        label: localization.text('about'),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => AboutApp()));
                        },
                        icon: Icons.message),
                    Visibility(
                      visible: Provider.of<SharedPref>(context, listen: false)
                              .token !=
                          null,
                      child: _optionCard.optionCard(
                          label: localization.text('logout'),
                          onTap: () {
                            Provider.of<LogoutProvider>(context, listen: false)
                                .deviceToken = _deviceToken;

                            Provider.of<LogoutProvider>(context, listen: false)
                                .apiToken = _preferences.getString('api_token');
                            String deviceToken = Provider.of<LogoutProvider>(
                                    context,
                                    listen: false)
                                .deviceToken;

                            showDialog(
                              context: context,
                              builder: (ctx) => AlertDialog(
                                title: Text(
                                  localization.text('Are_you_sure'),
                                  textDirection:
                                      localization.currentLanguage.toString() ==
                                              "en"
                                          ? TextDirection.ltr
                                          : TextDirection.rtl,
                                ),
                                content: Text(
                                  localization.text('logout_ask'),
                                  textDirection:
                                      localization.currentLanguage.toString() ==
                                              "en"
                                          ? TextDirection.ltr
                                          : TextDirection.rtl,
                                ),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text(localization.text('no')),
                                    onPressed: () {
                                      Navigator.of(ctx).pop(false);
                                    },
                                  ),
                                  FlatButton(
                                    child: Text(localization.text('ok')),
                                    onPressed: () async {
                                      Provider.of<LogoutProvider>(context,
                                              listen: false)
                                          .logout(context);
                                      await _preferences.remove('api_token');
                                      await _preferences.remove('id');
                                      await _preferences.remove('image');
                                      await _preferences.remove('phone');
                                      await _preferences.remove('email');
                                      await _preferences.remove('name');

                                      await _preferences.remove('countryV');
                                      Navigator.of(ctx).pop(true);
                                      if (_preferences
                                              .getBool('NoAdsNoAuctions') ==
                                          true) {
                                        Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                NoAuctionOrAdTabScreen(),
                                          ),
                                          (Route<dynamic> route) => false,
                                        );
                                      } else if (_preferences
                                              .getBool('Auctions') ==
                                          true) {
                                        Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                AuctionTabScreen(),
                                          ),
                                          (Route<dynamic> route) => false,
                                        );
                                      } else if (_preferences.getBool('Ads') ==
                                          true) {
                                        Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => AdTabScreen(),
                                          ),
                                          (Route<dynamic> route) => false,
                                        );
                                      } else {
                                        Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => TabScreen(),
                                          ),
                                          (Route<dynamic> route) => false,
                                        );
                                      }
                                      print('yaa7ayany=$deviceToken');
                                    },
                                  ),
                                ],
                              ),
                            );
                          },
                          icon: Icons.logout),
                    ),
                    Visibility(
                      visible: Provider.of<SharedPref>(context, listen: false)
                              .token ==
                          null,
                      child: _optionCard.optionCard(
                          label: localization.text('login'),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (c) => SignInScreen(true)));
                          },
                          icon: Icons.login),
                    ),
                  ],
                ),
              ),
            ),
          );
  }
}
