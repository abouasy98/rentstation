import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rentstation/Components/buttonSignIn.dart';
import 'package:rentstation/Components/inputTextField.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/models/get/countriesModel.dart';
import 'package:rentstation/models/get/registertype.dart';
import 'package:rentstation/provider/auth/forgetPasswordProvider.dart';
import 'package:rentstation/provider/get/getcountries.dart';
import 'package:rentstation/provider/get/getregistertype.dart';
import 'package:provider/provider.dart';

class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  @override
  void initState() {
    // Provider.of<ForgetPasswordProvider>(context, listen: false).context = context;
    super.initState();
  }

  RegisterType txstfieldprovider;
  String x;
  String selecetedCountry = localization.text('choose_country');
  int counteyId;
  bool a7a = true;
  CountriesModel getCountries;
  mainBottomSheet(BuildContext context) {
    return showModalBottomSheet<dynamic>(
        isScrollControlled: false,
        backgroundColor: Colors.white,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        builder: (BuildContext bc) {
          return Directionality(
            textDirection: TextDirection.rtl,
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: ListView(children: <Widget>[
                    Container(
                      color: Colors.white,
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemCount: getCountries.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              ListTile(
                                  onTap: () {
                                    print(getCountries.data[index].id);
                                    setState(() {
                                      selecetedCountry =
                                          getCountries.data[index].name;

                                      counteyId = getCountries.data[index].id;

                                      print('index=$index');
                                    });

                                    Navigator.of(context)
                                        .pop(getCountries.data[index].name);
                                  },
                                  title: Text(
                                    getCountries.data[index].name,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )),
                              Divider(
                                height: 1,
                                color: Colors.grey,
                              )
                            ],
                          );
                        },
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          );
        });
  }

  @override
  void didChangeDependencies() async {
    if (a7a) {
      txstfieldprovider =
          await Provider.of<GetRegisterType>(context, listen: false)
              .getRegisterType(context);
      getCountries =
          await Provider.of<GetCountriesprovider>(context, listen: false)
              .getCountries(context);
      String email = txstfieldprovider.data.value;
      if (email == 'email' || email == 'بريد الكتروني') {
        x = localization.text('email');
      } else {
        x = localization.text('phone_number');
      }
      print("555555${txstfieldprovider.data.value}");
      print("77777777${getCountries.data}");

      setState(() {
        a7a = false;
      });
    }
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var forgetPassword =
        Provider.of<ForgetPasswordProvider>(context, listen: false);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: a7a
          ? SpinKitThreeBounce(
              color: Theme.of(context).primaryColor,
              size: 22,
            )
          : Stack(
              children: <Widget>[
                ListView(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  children: <Widget>[
                    Center(
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width - 100,
                        alignment: Alignment.center,
                        child: Form(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Center(
                                  child: Text(
                                x == localization.text('email')
                                    ? localization.text('enter_email_and_send')
                                    : localization.text("enter_phone_and_send"),
                                // "فضلا ادخل رقم الجوال",
                                style: TextStyle(
                                    color: Colors.black87, fontSize: 20),
                              )),
                              SizedBox(
                                height: 5,
                              ),
                              Center(
                                  child: Text(
                                localization.text("for_reset_password"),
                                // "لاستعادة كلمة المرور",
                                style: TextStyle(
                                    color: Colors.black87, fontSize: 15),
                              )),
                              SizedBox(
                                height: MediaQuery.of(context).size.height * .1,
                              ),
                              GestureDetector(
                                onTap: () async {
                                  return mainBottomSheet(context);
                                },
                                child: Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.06,
                                  margin: EdgeInsets.only(
                                      bottom: 10, left: 20, right: 20),
                                  padding: EdgeInsets.only(left: 20, right: 20),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(25)),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Icon(Icons.arrow_drop_down),
                                      Text(selecetedCountry),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              InputFieldArea(
                                labelTxt: x == localization.text('email')
                                    ? localization.text('email')
                                    : localization.text("phone_number"),
                                // 'رقم الجوال',
                                hint: localization.text("write"),
                                //  "اكتب هنا",
                                textInputType: x == localization.text('email')
                                    ? TextInputType.emailAddress
                                    : TextInputType.number,
                                changedFunction: (v) {
                                  x == localization.text('email')
                                      ? forgetPassword.email = v
                                      : forgetPassword.phone = v;
                                },
                                //  errorTxt: snapshot.error,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              SignInButton(
                                  txtColor: Colors.white,
                                  onPressSignIn: () {
                                    // _forgetPasswordMethod();
                                    forgetPassword.countryId = counteyId;
                                    forgetPassword.forgetPassword(context);
                                  },
                                  btnWidth: MediaQuery.of(context).size.width,
                                  btnHeight:
                                      MediaQuery.of(context).size.height * .07,
                                  btnColor: Theme.of(context).primaryColor,
                                  buttonText: localization.text("send")),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Positioned(
                  top: 40,
                  left: 20,
                  child: IconButton(
                    onPressed: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      Navigator.of(context).pop();
                    },
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.black87,
                    ),
                  ),
                )
              ],
            ),
    );
  }
}
