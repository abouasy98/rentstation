import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/models/categoriesModels.dart';
import 'package:rentstation/models/get/getAdbyIdModel.dart';
import 'package:rentstation/models/get/getCitiesModel.dart';
import 'package:rentstation/models/subCategoriesModel.dart';
import 'package:rentstation/provider/get/categoriesProvider.dart';
import 'package:rentstation/provider/get/getCitiesByIdProvider.dart';
import 'package:rentstation/provider/get/getTheUndertakingProvider.dart';
import 'package:rentstation/provider/get/subCategoriesProvider.dart';
import 'package:rentstation/provider/post/createAdProvider.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateAdScreen extends StatefulWidget {
  @override
  _CreateAdScreenState createState() => _CreateAdScreenState();
}

class _CreateAdScreenState extends State<CreateAdScreen> {
  List<Asset> _multiImages = [
    null,
    null,
    null,
    null,
    null,
    null,
  ];
  String initProductState;
  int categoryId;
  String subCategoryId;
  final _priceFoucsNode = FocusNode();
  final _descriptionFoucsNode = FocusNode();
  CustomDialog dialog = CustomDialog();
  bool sale = false;
  String lable = localization.text("choose_city");
  String categorylable = localization.text("choose_categorie");
  String subCategorylable = localization.text("choose_sub_categorie");
  String title;
  String details;
  int price;
  String cityId;
  bool isChecked = false;
  var _isInit = true;
  List<CitiesModel> cities = [];
  List<CitiesModel> categories = [];
  List<CitiesModel> subCategories = [];
  var _loadedSpinner = false;
  final _form = GlobalKey<FormState>();

  bool loaded = true;
  String path;
  int priceStatus = 0;

  List<Asset> images = List<Asset>();
  List<Asset> resultList;
  bool get initbool {
    if (priceStatus == 1) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> _loadAssets(int index) async {
    resultList =
        await MultiImagePicker.pickImages(maxImages: 1, enableCamera: false);

    Asset photo = resultList[0];

    setState(() {
      _multiImages[index] = photo;
    });
  }

  CategoriesModels getCategoriesModel;
  SubCategoriesModels getSubCategoriesModel;
  GetCities getCitiesByCountryIdModel;
  SharedPreferences _prefs;
  _getShared() async {
    var _instance = await SharedPreferences.getInstance();
    setState(() {
      _prefs = _instance;
    });
    await Provider.of<GetCitiesByCountryIdProvider>(context, listen: false)
        .getCities(_prefs.get("countryId"), context)
        .then((res) {
      setState(() {
        getCitiesByCountryIdModel = res;
      });
      if (getCitiesByCountryIdModel.data != null) {
        for (int i = 0; i < getCitiesByCountryIdModel.data.length; i++) {
          cities.add(new CitiesModel(
            id: getCitiesByCountryIdModel.data[i].id,
            label: getCitiesByCountryIdModel.data[i].name,
          ));
        }
      }
    });
  }

  GetAdbyIdModel getAddsByIDModel;

  void didChangeDependencies() async {
    if (_isInit) {
      await _getShared();
      await Provider.of<GetTheUndertakingProvider>(context, listen: false)
          .getTheUndertakingProvider(context);
      await Provider.of<CategoriesProvider>(context, listen: false)
          .getCategories(context)
          .then((res) {
        setState(() {
          getCategoriesModel = res;
        });
        if (getCategoriesModel.data != null) {
          for (int i = 0; i < getCategoriesModel.data.length; i++) {
            categories.add(new CitiesModel(
              id: getCategoriesModel.data[i].id,
              label: getCategoriesModel.data[i].name,
              //selected: false
            ));
          }
        }
      });

      setState(() {
        _isInit = false;
      });
    }

    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void dispose() {
    _priceFoucsNode.dispose();
    _descriptionFoucsNode.dispose();

    super.dispose();
  }

  Future<void> _saveForm() async {
    if (isChecked == false) {
      dialog.showErrorDialog(
        btnOnPress: () {},
        context: context,
        msg: localization.text("should_accept_undertaking_letter"),
        //  "يحب الموافقة علي الشروط والاحكام",
        ok: localization.text("ok"),
      );
      return;
    }
    final _isValid = _form.currentState.validate();
    if (!_isValid) {
      return;
    }
    _form.currentState.save();

    setState(() {
      _loadedSpinner = true;
    });

    await Provider.of<CreatAdProvider>(context, listen: false).creatAd(
        token: _prefs.get("api_token"),
        photos: _multiImages,
        title: title,
        price: price,
        categoryId: categoryId,
        subCategoryId: subCategoryId,
        cityId: cityId,
        context: context,
        description: details,
        priceStatus: priceStatus);
    setState(() {
      _loadedSpinner = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: localization.currentLanguage.toString() == "en"
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          actions: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Center(
                  child: GestureDetector(
                child: _loadedSpinner
                    ? Center(
                        child: SpinKitThreeBounce(
                          size: 25,
                          color: Colors.white,
                        ),
                      )
                    : Text(localization.text('post')),
                onTap: _saveForm,
              )),
            )
          ],
          title: Text(localization.text('add_ads')),
        ),
        body: _isInit
            ? SpinKitThreeBounce(
                color: Theme.of(context).primaryColor,
                size: 22,
              )
            : Padding(
                padding: const EdgeInsets.all(15.0),
                child: Form(
                  key: _form,
                  child: ListView(
                    shrinkWrap: true,
                    primary: false,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          height: 250,
                          child: GridView.count(
                            crossAxisCount: 3,
                            physics: NeverScrollableScrollPhysics(),
                            mainAxisSpacing: 5,
                            crossAxisSpacing: 5,
                            children: List.generate(6, (index) {
                              //  Asset asset = _multiImages[index];
                              return Stack(
                                children: <Widget>[
                                  _multiImages[index] != null
                                      ? AssetThumb(
                                          asset: _multiImages[index],
                                          width: 300,
                                          height: 300,
                                        )
                                      : Container(
                                          child: Icon(
                                            FontAwesomeIcons.camera,
                                            size: 50,
                                            color: Colors.white,
                                          ),
                                          color: Colors.blue,
                                          width: 300,
                                          height: 300,
                                        ),
                                  Positioned(
                                    child: _multiImages[index] == null
                                        ? InkWell(
                                            onTap: () {
                                              setState(() {
                                                // _multiImages.removeAt(index);
                                                _loadAssets(index);
                                              });
                                            },
                                            child: Icon(
                                              Icons.add_circle_outline,
                                              color: Colors.green[700],
                                              size: 30,
                                            ))
                                        : InkWell(
                                            onTap: () {
                                              setState(() {
                                                _multiImages[index] = null;
                                              });
                                            },
                                            child: Icon(
                                              Icons.delete,
                                              color: Colors.red,
                                              size: 20,
                                            ),
                                          ),
                                    bottom: 5,
                                    left: 5,
                                  ),
                                ],
                              );
                            }),
                          ),
                        ),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: localization.text('title'),
                        ),
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context)
                              .requestFocus(_descriptionFoucsNode);
                        },
                        onSaved: (value) {
                          title = value;
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return localization.text('please_add_title');
                          }
                          if (value.length < 5) {
                            return localization.text(
                                'Please_field_must_not_be_less_than_5_characters');
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: localization.text('description'),
                        ),
                        onFieldSubmitted: (_) {
                          FocusScope.of(context).requestFocus(_priceFoucsNode);
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return localization.text('please_add_desc');
                          }
                          if (value.length < 5) {
                            return localization.text(
                                'Please_field_must_not_be_less_than_5_characters');
                          }
                          return null;
                        },
                        maxLines: 3,
                        keyboardType: TextInputType.multiline,
                        focusNode: _descriptionFoucsNode,
                        onSaved: (value) {
                          details = value;
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Text(
                          localization
                              .text('Describe_what_makes_your_ad_unique'),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Text(
                                  localization.text('price'),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.2,
                                  child: TextFormField(
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.right,
                                    onChanged: (value) {
                                      price = int.parse(value);
                                    },
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return localization
                                            .text("please_add_price");
                                      }

                                      if (int.parse(value) <= 0) {
                                        return localization
                                            .text("please_add_price");
                                      }
                                      return null;
                                    },
                                    focusNode: _priceFoucsNode,
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.only(top: 10, right: 10),
                                      border: new OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(10)),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(_prefs.get("currency")),
                              ],
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.03,
                            ),
                            Text(
                              localization.text("discussion_price"),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                            Checkbox(
                                value: initbool,
                                onChanged: (_) {
                                  if (priceStatus == 1) {
                                    setState(() {
                                      priceStatus = 0;
                                    });
                                  } else {
                                    setState(() {
                                      priceStatus = 1;
                                    });
                                  }
                                }),
                          ],
                        ),
                      ),
                      Directionality(
                        textDirection:
                            localization.currentLanguage.toString() == "en"
                                ? TextDirection.ltr
                                : TextDirection.ltr,
                        child: RaisedButton(
                          elevation: 0,
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          onPressed: () {
                            mainBottomSheet(context, categories, "تصنيف");
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 textDirection:
                                localization.currentLanguage.toString() == "en"
                                    ? TextDirection.ltr
                                    : TextDirection.rtl,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(categorylable,
                                        textAlign: TextAlign.end,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: "RB",
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ),
                                  Icon(
                                    Icons.category,
                                    size: 21,
                                    color: Color(0xff2542a6),
                                  )
                                ],
                              ),
                              Icon(
                                Icons.arrow_back_ios,
                                color: Colors.grey,
                              )
                            ],
                          ),
                        ),
                      ),
                      Directionality(
                        textDirection:
                            localization.currentLanguage.toString() == "en"
                                ? TextDirection.ltr
                                : TextDirection.ltr,
                        child: RaisedButton(
                          elevation: 0,
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          onPressed: () {
                            if (categoryId != null) {
                              mainBottomSheet(
                                  context, subCategories, "تصنيف فرعي");
                            } else {
                              return Fluttertoast.showToast(
                                  msg: localization.text(
                                      'You_must_choose_the_main_category_first'),
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            }
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                textDirection:
                                localization.currentLanguage.toString() == "en"
                                    ? TextDirection.ltr
                                    : TextDirection.rtl,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(subCategorylable,
                                        textAlign: TextAlign.end,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: "RB",
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ),
                                  Icon(
                                    Icons.category_rounded,
                                    size: 21,
                                    color: Color(0xff2542a6),
                                  )
                                ],
                              ),
                              Icon(
                                Icons.arrow_back_ios,
                                color: Colors.grey,
                              )
                            ],
                          ),
                        ),
                      ),
                      Directionality(
                        textDirection:
                            localization.currentLanguage.toString() == "en"
                                ? TextDirection.ltr
                                : TextDirection.ltr,
                        child: RaisedButton(
                          elevation: 0,
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          onPressed: () {
                            mainBottomSheet(context, cities, "المدينة");
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            textDirection:
                                localization.currentLanguage.toString() == "en"
                                    ? TextDirection.ltr
                                    : TextDirection.rtl,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(lable,
                                        textAlign: TextAlign.end,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: "RB",
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ),
                                  Icon(
                                    Icons.folder,
                                    size: 21,
                                    color: Color(0xff2542a6),
                                  )
                                ],
                              ),
                              Icon(
                                Icons.arrow_back_ios,
                                color: Colors.grey,
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Column(
                        crossAxisAlignment:
                            localization.currentLanguage.toString() == "en"
                                ? CrossAxisAlignment.end
                                : CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            Provider.of<GetTheUndertakingProvider>(context,
                                    listen: false)
                                .content,
                            // 'الموافقه علي الشروط والاحكام',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontFamily: 'cairo',
                            ),
                          ),
                          new Checkbox(
                              value: isChecked,
                              activeColor: Theme.of(context).primaryColor,
                              onChanged: (bool newValue) {
                                setState(() {
                                  isChecked = !isChecked;
                                });
                              }),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  mainBottomSheet(BuildContext context, List<CitiesModel> list, String title) {
    return showModalBottomSheet<dynamic>(
        isScrollControlled: false,
        backgroundColor: Colors.white,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        builder: (BuildContext bc) {
          return title == "تصنيف فرعي"
              ? FutureBuilder(
                  future:
                      Provider.of<SubCategoriesProvider>(context, listen: false)
                          .getSubCategories(categoryId, context),
                  builder: (context, snapShot) {
                    if (snapShot.connectionState == ConnectionState.waiting) {
                      return SpinKitThreeBounce(
                        color: Theme.of(context).primaryColor,
                        size: 22,
                      );
                    }

                    getSubCategoriesModel = snapShot.data;

                    if (getSubCategoriesModel.data != null &&
                        subCategories.isEmpty) {
                      for (int i = 0;
                          i < getSubCategoriesModel.data.length;
                          i++) {
                        subCategories.add(new CitiesModel(
                          id: getSubCategoriesModel.data[i].id,
                          label: getSubCategoriesModel.data[i].name,
                        ));
                      }
                    }

                    loaded = false;
                    print('subCategorieslength${subCategories.length}');
                    return loaded
                        ? SpinKitThreeBounce(
                            color: Theme.of(context).primaryColor,
                            size: 22,
                          )
                        : Directionality(
                            textDirection: TextDirection.rtl,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top: 20.0),
                                  child: ListView(children: <Widget>[
                                    Container(
                                      color: Colors.white,
                                      child: ListView.builder(
                                        shrinkWrap: true,
                                        physics: ScrollPhysics(),
                                        itemCount: list.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              ListTile(
                                                  onTap: () {
                                                    print(list[index].id);
                                                    Navigator.pop(context);

                                                    setState(() {
                                                      if (title == "المدينة") {
                                                        cityId = list[index]
                                                            .id
                                                            .toString();
                                                        print("citeid $cityId");
                                                        lable =
                                                            list[index].label;
                                                      } else if (title ==
                                                          "تصنيف") {
                                                        categoryId =
                                                            list[index].id;
                                                        print(
                                                            "catrgory $categoryId");
                                                        categorylable =
                                                            list[index].label;
                                                      } else {
                                                        subCategoryId =
                                                            list[index]
                                                                .id
                                                                .toString();
                                                        print(
                                                            "catrgory $categoryId");
                                                        subCategorylable =
                                                            list[index].label;
                                                        subCategories.clear();
                                                      }
                                                    });
                                                  },
                                                  title: Text(
                                                    list[index].label,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  )),
                                              Divider(
                                                height: 1,
                                                color: Colors.grey,
                                              )
                                            ],
                                          );
                                        },
                                      ),
                                    ),
                                  ]),
                                ),
                              ],
                            ),
                          );
                  },
                )
              : Directionality(
                  textDirection: TextDirection.rtl,
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: ListView(children: <Widget>[
                          Container(
                            color: Colors.white,
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: ScrollPhysics(),
                              itemCount: list.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    ListTile(
                                        onTap: () {
                                          print(list[index].id);
                                          Navigator.pop(context);
                                          setState(() {
                                            if (title == "المدينة") {
                                              cityId =
                                                  list[index].id.toString();
                                              print("cityid $cityId");
                                              lable = list[index].label;
                                            } else if (title == "تصنيف") {
                                              categoryId = list[index].id;
                                              print("catrgory $categoryId");
                                              categorylable = list[index].label;

                                              print(
                                                  "catrgorylabel $categorylable");
                                              subCategories.clear();
                                            }
                                          });
                                        },
                                        title: Text(
                                          list[index].label,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        )),
                                    Divider(
                                      height: 1,
                                      color: Colors.grey,
                                    ),
                                  ],
                                );
                              },
                            ),
                          ),
                        ]),
                      ),
                    ],
                  ),
                );
        });
  }
}

class CitiesModel {
  int id;
  String label;

  CitiesModel({
    this.id,
    this.label,
  });
}
