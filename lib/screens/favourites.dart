import 'package:flutter/material.dart';

import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/provider/get/getMyFavAdsProvider.dart';
import 'package:rentstation/widgets/favouriteItem.dart';
import 'package:provider/provider.dart';

class Favourites extends StatelessWidget {
  @override

 
  


  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: localization.currentLanguage.toString() == "en"
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text(localization.text('favourite')),
          centerTitle: true,
        ),
        body: FutureBuilder(
                future: Provider.of<GetMyFavAdsProvider>(context, listen: false)
                    .getMyFavAds(Provider.of<SharedPref>(context, listen: false).token,context),
                builder: (context, snapShot) {
                  print(Provider.of<SharedPref>(context, listen: false).currency);
                  if (snapShot.connectionState == ConnectionState.waiting) {
                    return Center(child: Text(localization.text('waiting')));
                  } else if (!snapShot.hasData) {
                    return Center(child: Text(localization.text('no_posts')));
                  } else {
                    return Consumer<GetMyFavAdsProvider>(
                        builder: (context, favouriteData, _) {
                      if (favouriteData.favourite.length <= 0) {
                        return Center(child: Text(localization.text('no_posts')));
                      }
                      return ListView.builder(
                          primary: false,
                          shrinkWrap: true,
                          itemCount: favouriteData.favourite.length,
                          itemBuilder: (BuildContext context, int i) =>
                              FavouriteItem(
                                city: favouriteData.favourite[i].city,
                                createdAt: favouriteData.favourite[i].createdAt,
                                id: favouriteData.favourite[i].id,
                                photo:
                                    favouriteData.favourite[i].photo[0].photo,
                       
                                adId: favouriteData.favourite[i].adId,
                                price: favouriteData.favourite[i].price,
                                userPhoto:
                                    favouriteData.favourite[i].providerPhoto,
                                title: favouriteData.favourite[i].providertitle,
                                userName: favouriteData.favourite[i].userName,
                              ));
                    });
                  }
                }),
      ),
    );
  }
}
