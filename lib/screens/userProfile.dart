import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/get/userAdsModel.dart';
import 'package:rentstation/provider/post/createMessageProvider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:rentstation/provider/get/userAdsProvider.dart';
import 'package:rentstation/widgets/custom_app_bar_back_ground.dart';
import 'package:provider/provider.dart';

import 'AdsProfileScreen.dart';

class UserProfile extends StatefulWidget {
  final int userid;
  UserProfile({this.userid});
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  UserAdsModel _model;

  _getShared() async {
    Provider.of<GetUserData>(context, listen: false)
        .getUserData(Provider.of<SharedPref>(context, listen: false).token,
            widget.userid, context)
        .then((res) {
      setState(() {
        _model = res;
      });
    });
  }

  bool hiddenNumber = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getShared();
  }

  @override
  Widget build(BuildContext context) {
    //  final mediaQuery = MediaQuery.of(context).size;
    return Directionality(
      textDirection: localization.currentLanguage.toString() == "en"
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: SafeArea(
        child: Scaffold(
          bottomNavigationBar: _model == null
              ? Center(
                  child: SpinKitThreeBounce(
                    size: 25,
                    color: Theme.of(context).primaryColor,
                  ),
                )
              : Container(
                  height: MediaQuery.of(context).size.height * 0.07,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Provider.of<CreateMessageProvider>(context,
                                  listen: false)
                              .createMessage(
                                  _model.data[0].userId,
                                  Provider.of<SharedPref>(context,
                                          listen: false)
                                      .token,
                                  context);
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.4,
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.only(left: 10, right: 10),
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                    blurRadius: 1,
                                    color: Colors.grey[100],
                                    offset: Offset(0, 1),
                                    spreadRadius: 1),
                              ],
                              color: Colors.lightBlueAccent,
                              borderRadius: BorderRadius.circular(25)),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Icon(
                                  Icons.chat,
                                  color: Colors.white,
                                ),
                                Text(
                                  localization.text('Contact_the_seller'),
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                    //fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ]),
                        ),
                      ),
                      Row(children: [
                        IconButton(
                          icon: Icon(
                            Icons.call,
                            color: Colors.white,
                          ),
                          color: Colors.white,
                          onPressed: () async {
                            UrlLauncher.launch(
                                'tel: ${_model.data[0].phoneNumber}');
                          },
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              if (hiddenNumber) {
                                hiddenNumber = false;
                              } else {
                                hiddenNumber = true;
                              }
                            });
                          },
                          child: Text(
                            hiddenNumber
                                ? ' ${_model.data[0].phoneNumber.replaceRange(3, 8, '*****')} '
                                : ' ${_model.data[0].phoneNumber} ',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ]),
                    ],
                  ),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 7,
                          color: Colors.grey[100],
                          offset: Offset(0, 3),
                          spreadRadius: 4,
                        ),
                      ],
                      //color: Colors.red[300],
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: <Color>[
                            Colors.blueAccent,
                            Colors.blueAccent[200],
                            Colors.blueAccent[200],
                            Colors.blueAccent,
                          ]),
                      borderRadius: BorderRadius.circular(20)),
                ),
          body: _model == null
              ? Center(
                  child: SpinKitThreeBounce(
                    size: 25,
                    color: Theme.of(context).primaryColor,
                  ),
                )
              : ListView(children: [
                  Stack(children: [
                    CustomAppBarBackGround(
                      widgets: <Widget>[
                        IconButton(
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Container(
                              height: 60,
                              alignment: Alignment.center,
                              child: Text(
                                _model.data[0].user,
                                // "تعديل حسابي",
                                textAlign: TextAlign.center,

                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * .05),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.white, width: 5)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Stack(
                              children: <Widget>[
                                _model.data[0].userPhoto != null
                                    ? CachedNetworkImage(
                                        imageUrl: _model.data[0].userPhoto,
                                        fadeInDuration: Duration(seconds: 2),
                                        placeholder: (context, url) =>
                                            CircleAvatar(
                                                radius: 60,
                                                backgroundImage: AssetImage(
                                                    'images/16.jpg')),
                                        imageBuilder: (context, provider) {
                                          return CircleAvatar(
                                              radius: 60,
                                              backgroundImage: provider);
                                        },
                                      )
                                    : CircleAvatar(
                                        radius: 60,
                                        backgroundImage:
                                            AssetImage('images/16.jpg')),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ]),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.black54,
                    ),
                    height: MediaQuery.of(context).size.height * 0.08,
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Icon(Icons.location_on, color: Colors.white),
                                Container(
                                  alignment:
                                      localization.currentLanguage.toString() ==
                                              "en"
                                          ? Alignment.centerLeft
                                          : Alignment.centerRight,
                                  width:
                                      MediaQuery.of(context).size.width * 0.3,
                                  child: Text(
                                      Provider.of<SharedPref>(context,
                                              listen: false)
                                          .country,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white)),
                                ),
                                Expanded(child: Text('')),
                                Text(
                                    '${localization.text('member')}  ${localization.text('ago')} ${DateTime.now().difference(DateTime(_model.data[0].memberDate.year, _model.data[0].memberDate.month, _model.data[0].memberDate.day)).inDays} ${localization.text('day')}',
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.white),
                                    textAlign: TextAlign.right),
                                SizedBox(
                                  width: 10,
                                ),
                                Icon(
                                  Icons.timer,
                                  color: Colors.white,
                                ),
                              ]),
                          // Container(
                          //   alignment: Alignment.centerRight,
                          //   child: Text(_model.data[0].email,
                          //       overflow: TextOverflow.ellipsis,
                          //       style: TextStyle(
                          //           fontWeight: FontWeight.bold,
                          //           color: Colors.white)),
                          // ),
                        ],
                      ),
                    ),
                  ),
                  ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: _model.data.length,
                      itemBuilder: (context, i) => AdsProfileScreen(
                            adid: _model.data[i].id,
                            adphoto: _model.data[i].photos[0].photo,
                            city: _model.data[i].city,
                            createdAt: _model.data[i].createdAt,
                            price: _model.data[i].price,
                            title: _model.data[i].title,
                          ))
                ]),
        ),
      ),
    );
  }
}
