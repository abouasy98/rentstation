// import 'package:flutter/material.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:herag/Repository/appLocalization.dart';
// import 'package:herag/helpers/sharedPref_helper.dart';
// import 'package:herag/models/get/getAdsModel.dart';
// import 'package:herag/models/get/getAdsWithoutAuth.dart';
// import 'package:herag/models/get/getCitiesModel.dart';
// import 'package:herag/models/get/getFavModel.dart';
// import 'package:herag/provider/get/categoriesProvider.dart';
// import 'package:herag/provider/get/getAddsProvider.dart';
// import 'package:herag/provider/get/getAddsWitioutAuth.dart';
// import 'package:herag/provider/get/getCitiesByIdProvider.dart';
// import 'package:herag/provider/get/subCategoriesProvider.dart';
// import 'package:herag/screens/createAdScreen.dart';
// import 'package:herag/widgets/adsBodyWithOutAuth.dart';
// import 'package:provider/provider.dart';
// import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
// import '../widgets/Adsbody.dart';

// class Home extends StatefulWidget {
//   Key key;

//   bool auctions;
//   Home({this.auctions, this.key});
//   @override
//   _HomeState createState() => _HomeState();
// }

// class _HomeState extends State<Home> {
//   String query = '';
//   GetFavModel getFavModel;
//   CategoriesProvider categoryProvider;
//   SubCategoriesProvider subCategories;
// GetAdsWithoutAuth getAddsModel;
//   GetCities getCitiesByCountryIdModel;
//   TextEditingController _searchController = TextEditingController();
//   _getShared() async {
//     getAddsModel = null;

//     await Provider.of<GetCitiesByCountryIdProvider>(context, listen: false)
//         .getCities(Provider.of<SharedPref>(context, listen: false).countryId,context)
//         .then((res) {
//       setState(() {
//         getCitiesByCountryIdModel = res;
//       });
//     });

//     await Provider.of< GetAddswithoutAuthProvider >(context, listen: false)
//         .getAdds(context)
//         .then((res) {
//       setState(() {
//         getAddsModel = res;
//       });
//     });
//   }

//   Widget searchWidget() {
//     return Container(
//       width: MediaQuery.of(context).size.width,
//       height: MediaQuery.of(context).size.height * 0.05,
//       decoration: BoxDecoration(
//         color: Colors.blueAccent[700],
//       ),
//       child: Directionality(
//           key: widget.key,
//           textDirection: TextDirection.rtl,
//           child: Padding(
//             padding: const EdgeInsets.only(right: 16.0),
//             child: TextFormField(
//               style: TextStyle(color: Colors.white, fontSize: 18),
//               controller: _searchController,
//               textAlign: localization.currentLanguage.toString() == "en"
//                   ? TextAlign.left
//                   : TextAlign.right,
//               textDirection: TextDirection.rtl,
//               onChanged: (val) {
//                 setState(() {
//                   query = val.trim();
//                 });
//               },
//               decoration: InputDecoration(
//                 focusedBorder: InputBorder.none,
//                 border: InputBorder.none,
//                 enabledBorder: InputBorder.none,
//                 hintText: localization.text("search_in_Herag_Tqnee"),
//                 hintStyle: TextStyle(color: Colors.white, fontSize: 14),
//                 suffixIcon: Container(
//                   child: query != ''
//                       ? IconButton(
//                           iconSize: 20,
//                           icon: Icon(
//                             Icons.close,
//                             color: Colors.white,
//                           ),
//                           onPressed: () {
//                             WidgetsBinding.instance.addPostFrameCallback(
//                                 (_) => _searchController.clear());
//                             setState(() {
//                               query = '';
//                             });
//                           },
//                         )
//                       : Icon(
//                           Icons.search,
//                           size: 20,
//                           color: Colors.white,
//                         ),
//                 ),
//               ),
//             ),
//           )),
//     );
//   }

//   bool isInit = true;

//   @override
//   void didChangeDependencies() async {
//     if (isInit) {
//       await Provider.of<CategoriesProvider>(context, listen: false)
//           .getCategories(context);

//       categoryProvider =
//           Provider.of<CategoriesProvider>(context, listen: false);
//       subCategories =
//           Provider.of<SubCategoriesProvider>(context, listen: false);
//       await _getShared();

//       setState(() {
//         isInit = false;
//       });
//     }
//     // TODO: implement didChangeDependencies
//     super.didChangeDependencies();
//   }

//   String category = 'الكل';
//   int categoryId;
//   int subCategoryId;
//   String subCategory = '';
//   @override
//   Widget build(BuildContext context) {
//     var mediaQuery = MediaQuery.of(context).size;
//     return SafeArea(
//       child: Directionality(
//         textDirection: localization.currentLanguage.toString() == "en"
//             ? TextDirection.ltr
//             : TextDirection.rtl,
//         child: isInit
//             ? SpinKitThreeBounce(
//                 color: Theme.of(context).primaryColor,
//                 size: 22,
//               )
//             : Scaffold(
//                 resizeToAvoidBottomPadding: false,
//                 //     resizeToAvoidBottomInset: false,
//                 appBar: PreferredSize(
//                   preferredSize:
//                       Size.fromHeight(MediaQuery.of(context).size.height * 0.1),
//                   child: AppBar(
//                     leading: Container(),
//                     backgroundColor: Colors.white,
//                     flexibleSpace: searchWidget(),
//                     bottom: PreferredSize(
//                       preferredSize: mediaQuery,
//                       child: Container(
//                           height: mediaQuery.height * 0.07,
//                           width: mediaQuery.width,
//                           child: Padding(
//                             padding:
//                                 localization.currentLanguage.toString() == "en"
//                                     ? const EdgeInsets.only(left: 16.0)
//                                     : const EdgeInsets.only(right: 16.0),
//                             child: ListView(
//                                 primary: false,
//                                 shrinkWrap: true,
//                                 scrollDirection: Axis.horizontal,
//                                 children: [
//                                   GestureDetector(
//                                     onTap: () {
//                                       setState(() {
//                                         category = 'الكل';
//                                       });
//                                     },
//                                     child: Center(
//                                       child: Text(
//                                         localization.text('all'),
//                                         style:
//                                             TextStyle(color: Color(0xff366775)),
//                                       ),
//                                     ),
//                                   ),
//                                   AnimationLimiter(
//                                     child: ListView.builder(
//                                         primary: false,
//                                         shrinkWrap: true,
//                                         scrollDirection: Axis.horizontal,
//                                         itemCount:
//                                             categoryProvider.categories.length,
//                                         itemBuilder: (ctx, i) {
//                                           return AnimationConfiguration
//                                               .staggeredList(
//                                             position: i,
//                                             delay: Duration(milliseconds: 400),
//                                             child: SlideAnimation(
//                                               duration:
//                                                   Duration(milliseconds: 400),
//                                               horizontalOffset: 50,
//                                               child: FadeInAnimation(
//                                                 child: GestureDetector(
//                                                   onTap: () async {
//                                                     setState(() {
//                                                       category =
//                                                           categoryProvider
//                                                               .categories[i]
//                                                               .name;
//                                                       print(
//                                                           'category=$category');
//                                                       categoryId =
//                                                           categoryProvider
//                                                               .categories[i].id;
//                                                       subCategory = '';
//                                                     });
//                                                     await subCategories
//                                                         .getSubCategories(
//                                                             categoryId,context);
//                                                   },
//                                                   child: Container(
//                                                     padding: localization
//                                                                 .currentLanguage
//                                                                 .toString() ==
//                                                             "en"
//                                                         ? const EdgeInsets.only(
//                                                             left: 16.0)
//                                                         : const EdgeInsets.only(
//                                                             right: 16.0),
//                                                     decoration: BoxDecoration(
//                                                       border: Border(
//                                                         bottom: category ==
//                                                                 categoryProvider
//                                                                     .categories[
//                                                                         i]
//                                                                     .name
//                                                             ? BorderSide(
//                                                                 color: Theme.of(
//                                                                         context)
//                                                                     .primaryColor,
//                                                                 width: 1.0,
//                                                               )
//                                                             : BorderSide(
//                                                                 color: Theme.of(
//                                                                         context)
//                                                                     .primaryColor,
//                                                                 width: 0.0,
//                                                               ),
//                                                       ),
//                                                       // borderRadius: BorderRadius.circular(20),
//                                                     ),
//                                                     child: Center(
//                                                       child: Text(
//                                                         categoryProvider
//                                                             .categories[i].name,
//                                                         maxLines: 1,
//                                                         style: TextStyle(
//                                                             color: Color(
//                                                                 0xff366775)),
//                                                       ),
//                                                     ),
//                                                   ),
//                                                 ),
//                                               ),
//                                             ),
//                                           );
//                                         }),
//                                   ),
//                                 ]),
//                           )),
//                     ),
//                   ),
//                 ),
//                 floatingActionButton: FloatingActionButton(
//                   onPressed: () {
//                     Navigator.of(context).push(MaterialPageRoute(
//                         builder: (context) => CreateAdScreen()));
//                   },
//                   child: Icon(Icons.add),
//                 ),
//                 body: (category == 'الكل')
//                     ? Container(
//                         height: mediaQuery.height,
//                         child: AdsbodywithOutAuth(
//                           key: UniqueKey(),
//                           categoryId: categoryId,
//                           getAddsModel: getAddsModel,
//                           subCategoryId: subCategoryId,
//                           query: query,
//                           getCitiesByCountryIdModel: getCitiesByCountryIdModel,
//                           category: category == null ? '' : category,
//                           subCategory: subCategory,
//                         ),
//                       )
//                     : SafeArea(
//                         child: SingleChildScrollView(
//                           primary: false,
//                           child: Column(
//                             children: [
//                               SizedBox(
//                                 height: mediaQuery.height * 0.01,
//                               ),
//                               Container(
//                                 height: mediaQuery.height * 0.07,
//                                 child: Consumer<SubCategoriesProvider>(
//                                   builder: (context, subCategoryData, _) =>
//                                       AnimationLimiter(
//                                     child: ListView.builder(
//                                       scrollDirection: Axis.horizontal,
//                                       itemCount:
//                                           subCategoryData.subcategories.length,
//                                       itemBuilder: (ctx, i) =>
//                                           AnimationConfiguration.staggeredList(
//                                         position: i,
//                                         delay: Duration(milliseconds: 400),
//                                         child: SlideAnimation(
//                                           duration: Duration(milliseconds: 400),
//                                           child: FadeInAnimation(
//                                             child: GestureDetector(
//                                               onTap: () {
//                                                 if (subCategory ==
//                                                     subCategoryData
//                                                         .subcategories[i]
//                                                         .name) {
//                                                   setState(() {
//                                                     subCategory = '';
//                                                   });
//                                                 } else {
//                                                   setState(() {
//                                                     subCategory =
//                                                         subCategoryData
//                                                             .subcategories[i]
//                                                             .name;
//                                                     subCategoryId =
//                                                         subCategoryData
//                                                             .subcategories[i]
//                                                             .id;
//                                                   });
//                                                   print(
//                                                       'subcategory$subCategory');
//                                                 }
//                                               },
//                                               child: Container(
//                                                 margin: EdgeInsets.only(
//                                                   left: 6,
//                                                   right: 6,
//                                                 ),
//                                                 decoration: BoxDecoration(
//                                                   borderRadius:
//                                                       BorderRadius.circular(10),
//                                                   color: subCategory ==
//                                                           subCategories
//                                                               .subcategories[i]
//                                                               .name
//                                                       ? Theme.of(context)
//                                                           .primaryColor
//                                                       : Colors.grey[200],
//                                                 ),
//                                                 padding: EdgeInsets.all(10),
//                                                 child: Center(
//                                                   child: Text(
//                                                     subCategories
//                                                         .subcategories[i].name,
//                                                     style: TextStyle(
//                                                       color: subCategory ==
//                                                               subCategories
//                                                                   .subcategories[
//                                                                       i]
//                                                                   .name
//                                                           ? Colors.white
//                                                           : Colors.grey,
//                                                     ),
//                                                   ),
//                                                 ),
//                                               ),
//                                             ),
//                                           ),
//                                         ),
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                               Container(
//                                 height: mediaQuery.height,
//                                 child: AdsbodywithOutAuth(
//                                   categoryId: categoryId,
//                                   subCategoryId: subCategoryId,
//                                   query: query,
//                                   key: UniqueKey(),
//                                   category: category == null ? '' : category,
//                                   getAddsModel: getAddsModel,
//                                   getCitiesByCountryIdModel:
//                                       getCitiesByCountryIdModel,
//                                   subCategory: subCategory,
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//               ),
//       ),
//     );
//   }
// }
