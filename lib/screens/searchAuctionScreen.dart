// import 'package:flutter/material.dart';
// import 'package:herag/Repository/appLocalization.dart';
// import 'package:herag/models/get/getAuctionsModel.dart';
// import 'package:herag/widgets/searchauction.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import '../utils/universal_variables.dart';

// class SearchAuctionsScreen extends StatefulWidget {
//   static const routedname = '/SearchScreen';
//   final SharedPreferences pref;
//   final GetAuctionsModel getAuctionsModel;

//   SearchAuctionsScreen({this.getAuctionsModel, this.pref});
//   @override
//   _SearchAuctionsScreenState createState() => _SearchAuctionsScreenState();
// }

// class _SearchAuctionsScreenState extends State<SearchAuctionsScreen> {
//   String query = "";
//   TextEditingController _searchController = TextEditingController();
//   searchAppBar(BuildContext context) {
//     return AppBar(
//       leading: IconButton(
//         icon: Icon(Icons.arrow_back),
//         onPressed: () => Navigator.pop(context),
//       ),
//       elevation: 0,
//       bottom: PreferredSize(
//         preferredSize: const Size.fromHeight(kToolbarHeight + 20),
//         child: Padding(
//           padding: EdgeInsets.only(left: 20),
//           child: TextField(
//             keyboardType: TextInputType.name,
//             textCapitalization: TextCapitalization.sentences,
//             textInputAction: TextInputAction.search,
//             controller: _searchController,
//             onChanged: (val) {
//               setState(() {
//                 query = val;
//               });
//             },
//             cursorColor: UniversalVariables.blackColor,
//             autofocus: true,
//             style: TextStyle(
//               fontWeight: FontWeight.bold,
//               color: Colors.white,
//               fontSize: 35,
//             ),
//             decoration: InputDecoration(
//               suffixIcon: IconButton(
//                 icon: Icon(Icons.close, color: Colors.white),
//                 onPressed: () {
//                   WidgetsBinding.instance
//                       .addPostFrameCallback((_) => _searchController.clear());
//                 },
//               ),
//               border: InputBorder.none,
//               hintText: localization.text('search_by_user_or_title'),
//               hintStyle: TextStyle(
//                 fontWeight: FontWeight.bold,
//                 fontSize: 20,
//                 color: Color(0x88ffffff),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Directionality(
//       textDirection: localization.currentLanguage.toString() == "en"
//           ? TextDirection.ltr
//           : TextDirection.rtl,
//       child: Scaffold(
//         appBar: searchAppBar(context),
//         body: Padding(
//           padding: const EdgeInsets.symmetric(horizontal: 20),
//           child: SearchAuctionsIteams(
//             getAuctionsModel: widget.getAuctionsModel,
//             query: query,
        
//           ),
//         ),
//       ),
//     );
//   }
// }
