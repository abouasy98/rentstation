import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/Components/buttonSignIn.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Components/inputTextField.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/models/get/registertype.dart';
import 'package:rentstation/provider/get/getregistertype.dart';
import 'package:rentstation/provider/auth/signUpProvider.dart';
import 'package:rentstation/provider/termsProvider.dart';
import 'package:rentstation/widgets/ImagePicker/image_picker_handler.dart';
import 'package:provider/provider.dart';

class Register extends StatefulWidget {
  static const routeName = '/Register';

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register>
    with TickerProviderStateMixin, ImagePickerListener {
  Color btnColor = Colors.grey;
  bool isChecked = false;
  final FirebaseMessaging _fcm = FirebaseMessaging();
  String _deviceToken = 'DEFULT_TOKEN';
  ImageProvider imageProvider;
  // ignore: unused_field
  File _image;
  AnimationController _controller;
  ImagePickerHandler imagePicker;
  RegisterType txstfieldprovider;
  // String _country;
  String x;
  bool a7a = true;
  @override
  // String _truck;
  // String _category;
  var rejister;
  @override
  void initState() {
    Provider.of<TermsProvider>(context, listen: false).getTerms(context);

    rejister = Provider.of<SignUpProvider>(context, listen: false);

    _fcm.getToken().then((response) {
      _deviceToken = response;
      print('The device Token is :' + _deviceToken);
    });

    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    imagePicker = new ImagePickerHandler(
        this, _controller, Color.fromRGBO(0, 150, 75, 1.0));
    imagePicker.init();
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    if (a7a) {
      txstfieldprovider =
          await Provider.of<GetRegisterType>(context, listen: false)
              .getRegisterType(context);

      String email = txstfieldprovider.data.value;
      if (email == 'email' || email == 'بريد الكتروني') {
        x = localization.text('email');
      } else {
        x = localization.text('phone_number');
      }

      setState(() {
        a7a = false;
      });
    }
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  CustomDialog dialog = CustomDialog();
  final _form = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final _blank = new FocusNode();
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(_blank);
        },
        child: Form(
          key: _form,
          child: Stack(
            children: <Widget>[
              Center(
                child: Container(
                  padding: EdgeInsets.all(20),
                  height: MediaQuery.of(context).size.height,
                  child: ListView(
                    children: <Widget>[
                      SizedBox(
                        height: 20,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () => imagePicker.showDialog(context),
                            child: Stack(
                              children: <Widget>[
                                _image == null
                                    ? Center(
                                        child: Image.asset(
                                        'images/16.jpg',
                                        height: 150,
                                        fit: BoxFit.cover,
                                      ))
                                    : Center(
                                        child: Image.file(
                                        _image,
                                        height: 150,
                                        fit: BoxFit.cover,
                                      )),
                                Positioned(
                                  bottom: 0,
                                  right: 80,
                                  child: CircleAvatar(
                                    child: Icon(
                                      Icons.add,
                                      color: Theme.of(context).primaryColor,
                                      size: 15,
                                    ),
                                    radius: 10,
                                    backgroundColor: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          Center(
                              child: Text(
                            localization.text("sign_up"),
                            // "تسجيل جديد",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 20),
                          )),
                          SizedBox(
                            height: 5,
                          ),
                          Center(
                              child: Text(
                            localization.text("complete_sign_up"),
                            // "فضلا اكمل بيانات التسجيل",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 13),
                            textAlign: TextAlign.center,
                          )),
                          SizedBox(
                            height: 40,
                          ),
                          InputFieldArea(
                            changedFunction: (v) {
                              Provider.of<SignUpProvider>(context,
                                      listen: false)
                                  .name = v;
                            },
                            lengthChar: 15,
                            textInputType: TextInputType.text,
                            labelTxt: localization.text("name"),
                            // labelTxt: 'الاسم الاول',

                            hint: localization.text("write"), //"اكتب هنا",
                            error: (value) {
                              if (value.isEmpty) {
                                return "${localization.text("name")} ${localization.text("required")}";
                              } else if (value.length >= 15) {
                                return 'لا يمكن ان يزيد الاسم عن 15 حرف';
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 10),
                          InputFieldArea(
                            changedFunction: (v) {
                              x == localization.text('email')
                                  ? Provider.of<SignUpProvider>(context,
                                          listen: false)
                                      .phoneNumber = v
                                  : Provider.of<SignUpProvider>(context,
                                          listen: false)
                                      .email = v;
                            },
                            textInputType: TextInputType.emailAddress,
                            labelTxt: x != localization.text('email')
                                ? localization.text("email")
                                : localization.text("phone_number"),
                            // 'البريد الإكتروني (إختياري)',
                            hint: localization.text("write"), //"اكتب هنا",
                            error: (value) {
                              if (value.isEmpty) {
                                return x != localization.text('email')
                                    ? "${localization.text("email")} ${localization.text("required")}"
                                    : "${localization.text("phone_number")} ${localization.text("required")}";
                              }
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          InputFieldArea(
                            textInputType: TextInputType.text,
                            labelTxt: localization
                                .text("new_password"), // 'كلمة المرور',
                            hint: localization.text("write"), //"اكتب هنا",
                            show: true,
                            changedFunction: (v) {
                              Provider.of<SignUpProvider>(context,
                                      listen: false)
                                  .password = v;
                            },
                            error: (value) {
                              if (value.isEmpty) {
                                return "${localization.text("new_password")} ${localization.text("required")}";
                              }
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          InputFieldArea(
                            textInputType: TextInputType.text,
                            labelTxt: localization.text(
                                "confirm_nem_password"), // 'تأكيد كلمة المرور',
                            hint: localization.text("write"), //"اكتب هنا",
                            show: true,
                            changedFunction: (v) {
                              Provider.of<SignUpProvider>(context,
                                      listen: false)
                                  .passwordConfirmation = v;
                            },
                            error: (value) {
                              if (value.isEmpty) {
                                return "${localization.text("confirm_nem_password")} ${localization.text("required")}";
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 20),
                          Container(
                            child: Directionality(
                              textDirection: TextDirection.rtl,
                              child: Row(
                                children: <Widget>[
                                  new Checkbox(
                                      value: isChecked,
                                      activeColor:
                                          Theme.of(context).primaryColor,
                                      onChanged: (bool newValue) {
                                        setState(() {
                                          isChecked = !isChecked;
                                        });
                                      }),
                                  InkWell(
                                      onTap: () => showDialog(
                                          context: context,
                                          builder: (_) => SimpleDialog(

                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                backgroundColor:
                                                    Theme.of(context)
                                                        .primaryColor,
                                                children: <Widget>[
                                                  IconButton(
                                                //    alignment: Alignment.centerLeft,
                                                      icon: Icon(
                                                        Icons.close,
                                                        size: 25,
                                                        color: Colors.red,
                                                      ),
                                                      onPressed: () {
                                                        Navigator.of(context)
                                                            .pop();
                                                      }),
                                                  Center(
                                                    child: Container(
                                                      height:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .height,
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              0.8,
                                                      child: ListView(
                                                        physics:
                                                            ScrollPhysics(),
                                                        children: <Widget>[
                                                          SizedBox(
                                                            height: 10,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(8.0),
                                                            child: Text(
                                                              Provider.of<TermsProvider>(
                                                                              context,
                                                                              listen:
                                                                                  false)
                                                                          .content ==
                                                                      null
                                                                  ? "المحتوى سوف يظهر في صفحة ويب"
                                                                  : Provider.of<
                                                                              TermsProvider>(
                                                                          context,
                                                                          listen:
                                                                              false)
                                                                      .content,
                                                              maxLines: 100,
                                                              softWrap: true,
                                                              textAlign:
                                                                  TextAlign
                                                                      .center,
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 12,
                                                                  fontFamily:
                                                                      'cairo'),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              )),
                                      child: Text(
                                        localization.text("terms"),
                                        // 'الموافقه علي الشروط والاحكام',
                                        style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontFamily: 'cairo',
                                        ),
                                      )),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          SignInButton(
                            txtColor: Colors.white,
                            onPressSignIn: () async {
                              if (isChecked == false) {
                                dialog.showErrorDialog(
                                  btnOnPress: () {},
                                  context: context,
                                  msg: localization.text("should_accept"),
                                  //  "يحب الموافقة علي الشروط والاحكام",
                                  ok: localization.text("ok"),
                                );
                                return;
                              }
                              final isValid = _form.currentState.validate();
                              if (!isValid) {
                                return;
                              }
                              _form.currentState.save();
                              // if (Provider.of<SignUpProvider>(context,
                              //                 listen: false)
                              //             .license ==
                              //         null ||
                              //     Provider.of<SignUpProvider>(context,
                              //                 listen: false)
                              //             .identity ==
                              //         null ||
                              //     Provider.of<SignUpProvider>(context,
                              //                 listen: false)
                              //             .carForm ==
                              //         null ||
                              //     Provider.of<SignUpProvider>(context,
                              //                 listen: false)
                              //             .transportationCard ==
                              //         null ||
                              //     Provider.of<SignUpProvider>(context,
                              //                 listen: false)
                              //             .insurance ==
                              //         null) {
                              //   dialog.showErrorDialog(
                              //     btnOnPress: () {},
                              //     context: context,
                              //     msg: localization.text("should_complete"),
                              //     ok: localization.text("ok"),
                              //   );
                              //   return;
                              // }

                              await Provider.of<SignUpProvider>(context,
                                      listen: false)
                                  .signUp(_deviceToken, context);
                            },
                            btnWidth: MediaQuery.of(context).size.width - 40,
                            btnHeight: MediaQuery.of(context).size.height * .07,
                            btnColor: btnColor,
                            buttonText: localization.text("sign_up"),
                            // 'تسجيل',
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  userImage(File _image) {
    setState(() {
      this._image = _image;
      Provider.of<SignUpProvider>(context, listen: false).image = _image;
    });
  }
}
