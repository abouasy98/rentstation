import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/provider/get/notificationProvider.dart';
import 'package:rentstation/screens/login.dart';
import 'package:provider/provider.dart';
import 'AuctionDetailsScreen.dart';
import 'adDetails.dart';

class Notifaions extends StatefulWidget {
  @override
  _NotifaionsState createState() => _NotifaionsState();
}

class _NotifaionsState extends State<Notifaions> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Provider.of<SharedPref>(context, listen: false).token == null
          ? SignInScreen(false)
          : FutureBuilder(
              future: Provider.of<NotoficationProvider>(context, listen: false)
                  .getNotification(
                      Provider.of<SharedPref>(context, listen: false).token,
                      context),
              builder: (context, snapShot) {
                if (snapShot.connectionState == ConnectionState.waiting) {
                  return Center(child: Text(localization.text('waiting')));
                } else if (!snapShot.hasData) {
                  return Center(child: Text(localization.text('no_posts')));
                } else {
                  return Consumer<NotoficationProvider>(
                      builder: (context, notifactionData, _) {
                    if (notifactionData.notfications.length <= 0) {
                      return Center(child: Text(localization.text('no_posts')));
                    }
                    return ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      reverse: true,
                      itemCount: notifactionData.notfications.length,
                      itemBuilder: (BuildContext context, int i) => Dismissible(
                        direction: DismissDirection.startToEnd,
                        key: UniqueKey(),
                        onDismissed: (direction) {
                          if (direction == DismissDirection.startToEnd) {}
                        },
                        confirmDismiss: (direction) {
                          return showDialog(
                              context: context,
                              builder: (ctx) => AlertDialog(
                                    title: Text(
                                      localization.text('Are_you_sure'),
                                      textDirection: localization
                                                  .currentLanguage
                                                  .toString() ==
                                              "en"
                                          ? TextDirection.ltr
                                          : TextDirection.rtl,
                                    ),
                                    content: Text(
                                      localization.text('notification_ask_delete'),
                                      textDirection: localization
                                                  .currentLanguage
                                                  .toString() ==
                                              "en"
                                          ? TextDirection.ltr
                                          : TextDirection.rtl,
                                    ),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text(localization.text('no')),
                                        onPressed: () {
                                          Navigator.of(ctx).pop(false);
                                        },
                                      ),
                                      FlatButton(
                                        child: Text(localization.text('ok')),
                                        onPressed: () {
                                          Provider.of<NotoficationProvider>(
                                                  context,
                                                  listen: false)
                                              .removeNotifacion(
                                                  notifactionData
                                                      .notfications[i].id,
                                                  Provider.of<SharedPref>(
                                                          context,
                                                          listen: false)
                                                      .token,
                                                  context);
                                          Navigator.of(ctx).pop(true);
                                        },
                                      ),
                                    ],
                                  ));
                        },
                        background: Container(
                          color: Theme.of(context).errorColor,
                          child: Icon(
                            Icons.delete,
                            color: Colors.white,
                            size: 40,
                          ),
                          alignment: Alignment.centerRight,
                          padding: EdgeInsets.only(right: 20),
                          margin:
                              EdgeInsets.symmetric(horizontal: 15, vertical: 4),
                        ),
                        child: GestureDetector(
                          onTap: () {
                            if (notifactionData.notfications[i].adId != null) {
                              pushNewScreen(
                                context,
                                screen: AdDetails(
                                  id: notifactionData.notfications[i].adId,
                                ),
                                withNavBar:
                                    false, // OPTIONAL VALUE. True by default.
                                pageTransitionAnimation:
                                    PageTransitionAnimation.cupertino,
                              );
                              // Navigator.of(context).push(MaterialPageRoute(
                              //     builder: (context) => AdDetails(
                              //           id: notifactionData
                              //               .notfications[i].adId,
                              //         )));
                            } else {
                              pushNewScreen(
                                context,
                                screen: AuctionsDetails(
                                  id: notifactionData.notfications[i].auctionId,
                                ),
                                withNavBar:
                                    false, // OPTIONAL VALUE. True by default.
                                pageTransitionAnimation:
                                    PageTransitionAnimation.cupertino,
                              );
                              // Navigator.of(context).push(MaterialPageRoute(
                              //     builder: (context) => AuctionsDetails(
                              //           id: notifactionData
                              //               .notfications[i].auctionId,
                              //         )));
                            }
                          },
                          child: Card(
                            child: Column(children: [
                              ListTile(
                                leading: Text(
                                  notifactionData.notfications[i].arTitle,
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 16),
                                ),
                                trailing: Text(
                                  '${DateTime.parse(notifactionData.notfications[i].createdAt.toString()).day}-${DateTime.parse(notifactionData.notfications[i].createdAt.toString()).month}-${DateTime.parse(notifactionData.notfications[i].createdAt.toString()).year}',
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 16),
                                ),
                              ),
                              Text(
                                notifactionData.notfications[i].arMessage,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16),
                              )
                            ]),
                          ),
                        ),
                      ),
                    );
                  });
                }
              }),
    );
  }
}
