import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/screens/editordeleteAds.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'createAdScreen.dart';
import 'myadsArchive.dart';

class MyAdScreen extends StatefulWidget {
  final SharedPreferences prefs;
  MyAdScreen({this.prefs});
  @override
  _MyAdScreenState createState() => _MyAdScreenState();
}

class _MyAdScreenState extends State<MyAdScreen> {
  List<Map<String, dynamic>> list = [
    {
      'selected': true,
      'name': localization.text('edit_ad'),
      'icon': Icons.edit,
    },
    {
      'selected': false,
      'name': localization.text('archive_ads'),
      'icon': Icons.archive,
    }
  ];

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return Directionality(
            textDirection: localization.currentLanguage.toString() == "en"
                    ? TextDirection.ltr
                    : TextDirection.rtl,
      child: Scaffold(
        //  resizeToAvoidBottomPadding: false,
          appBar: PreferredSize(
            preferredSize:
                Size.fromHeight(MediaQuery.of(context).size.height * 0.17),
            child: AppBar(
              bottom: PreferredSize(
                preferredSize: mediaQuery,
                child: Container(
                  decoration: BoxDecoration(color: Colors.white),
                  height: mediaQuery.height * 0.1,
                  width: mediaQuery.width,
                  child:  ListView.builder(
                      itemCount: list.length,
                      primary: false,
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (_, index) =>
                        InkWell(
                              onTap: () {
                                setState(() {
                                  list[index]['selected'] = true;
                                  for (int i = 0; i < list.length; i++) {
                                    if (i != index) {
                                      list[i]['selected'] = false;
                                    }
                                  }
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: list[index]['selected'] == true
                                      ? Colors.black54
                                      : Colors.white,
                                ),
                                width: MediaQuery.of(context).size.width / 2,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      height: 35,
                                      width: 35,
                                      child: Icon(
                                        list[index]['icon'],
                                        color: list[index]['selected'] == true
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(
                                      list[index]['name'],
                                      style: TextStyle(
                                          color: list[index]['selected'] == true
                                              ? Colors.white
                                              : Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                              ),
                            ),
                       
                    
                  ),
                ),
              ),
              title: Text(localization.text('my_ads_only')),
              actions: <Widget>[
                FlatButton(
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => CreateAdScreen()));
                  },
                ),
              ],
            ),
          ),
          body: list[0]['selected']
              ? EditordeleteAds(
                  prefs: widget.prefs,
                )
              : Archieve(widget.prefs)),
    );
  }
}
//  isInit
//             ? SpinKitThreeBounce(
//                 color: Theme.of(context).primaryColor,
//                 size: 22,
//               )
//             : Padding(
//                 padding: const EdgeInsets.only(top: 16.0, bottom: 16),
//                 child: Consumer<GetMyAdsProvider>(
//                   builder: (ctx, productData, _) => ListView.builder(
//                     itemCount: productData.myAds.length,
//                     itemBuilder: (_, i) => Card(
//                       child: UserProductsItem(
//                         id: productData.myAds[i].id,
//                         image: productData.myAds[i].photos[0].photo,
//                         title: productData.myAds[i].title,
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
