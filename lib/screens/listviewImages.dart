import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/get/getAuctionsPhotoProvider.dart';
import 'package:rentstation/provider/get/getadPhotosProvider.dart';
import 'package:rentstation/widgets/listViewImagesItem.dart';

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'fullPhoto.dart';

class ListViewImages extends StatelessWidget {
  final int adId;
  final int auctionId;

  final SharedPreferences prefs;
  ListViewImages({this.prefs, this.adId, this.auctionId});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: localization.currentLanguage.toString() == "en"
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            localization.text('ad_photos'),
            textAlign: TextAlign.right,
          ),
          centerTitle: true,
        ),
        body: Card(
          child: FutureBuilder(
              future: adId != null
                  ? Provider.of<GetphotosAdsbyIdProvider>(context,
                          listen: false)
                      .getphotosbyId(prefs.get('api_token'), adId, context)
                  : Provider.of<GetphotosAuctionsbyIdProvider>(context,
                          listen: false)
                      .getphotosbyId(
                          prefs.get('api_token'), auctionId, context),
              builder: (context, snapShot) {
                if (snapShot.connectionState == ConnectionState.waiting) {
                  return Center(child: Text(localization.text('waiting')));
                } else if (!snapShot.hasData) {
                  return Center(child: Text(localization.text('no_posts')));
                }
                if (adId != null) {
                  return Consumer<GetphotosAdsbyIdProvider>(
                      builder: (context, photo, _) {
                    if (photo.photos.length <= 0) {
                      return Center(child: Text(localization.text('no_posts')));
                    }
                    return ListView.builder(
                      key: UniqueKey(),
                      primary: false,
                      shrinkWrap: true,
                      itemCount: photo.photos.length,
                      itemBuilder: (context, i) => GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => FullPhoto(
                                      imageUrl: photo.photos[i].photo,
                                    )));
                          },
                          child: Dismissible(
                            direction: DismissDirection.startToEnd,
                            key: UniqueKey(),
                            onDismissed: (direction) {
                              if (direction == DismissDirection.startToEnd) {}
                            },
                            confirmDismiss: (direction) {
                              return showDialog(
                                  context: context,
                                  builder: (ctx) => AlertDialog(
                                        title: Text(
                                          localization.text('Are_you_sure'),
                                          textDirection: localization
                                                      .currentLanguage
                                                      .toString() ==
                                                  "en"
                                              ? TextDirection.ltr
                                              : TextDirection.rtl,
                                        ),
                                        content: Text(
                                          localization.text(
                                              'do_you_want_to_delete_this_photo"'),
                                          textDirection: localization
                                                      .currentLanguage
                                                      .toString() ==
                                                  "en"
                                              ? TextDirection.ltr
                                              : TextDirection.rtl,
                                        ),
                                        actions: <Widget>[
                                          FlatButton(
                                            child:
                                                Text(localization.text('no')),
                                            onPressed: () {
                                              Navigator.of(ctx).pop(false);
                                            },
                                          ),
                                          FlatButton(
                                            child:
                                                Text(localization.text('ok')),
                                            onPressed: () async {
                                              if (photo.photos.length <= 1) {
                                                Fluttertoast.showToast(
                                                    msg: localization.text(
                                                        'The_advertisement_cannot_be_without_at_least_one_image'));
                                                Navigator.of(ctx).pop(false);
                                              } else {
                                                await Provider.of<
                                                            GetphotosAdsbyIdProvider>(
                                                        context,
                                                        listen: false)
                                                    .deletePhotobyId(
                                                  context,
                                                  photo.photos[i].id,
                                                  prefs.getString('api_token'),
                                                );
                                                Navigator.of(ctx).pop(true);
                                              }
                                            },
                                          ),
                                        ],
                                      ));
                            },
                            background: Container(
                              color: Theme.of(context).errorColor,
                              child: Icon(
                                Icons.delete,
                                color: Colors.white,
                                size: 40,
                              ),
                              alignment: Alignment.centerRight,
                              padding: EdgeInsets.only(right: 20),
                              margin: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 4),
                            ),
                            child: Container(
                              padding: EdgeInsets.only(top: 10, bottom: 10),
                              height: 200,
                              child: GridTile(
                                child: photo.photos[i].photo != null
                                    ? CachedNetworkImage(
                                        imageUrl: photo.photos[i].photo,
                                        fadeInDuration: Duration(seconds: 2),
                                        placeholder: (context, url) =>
                                            Container(
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: AssetImage(
                                                        'images/loading.gif'),
                                                    fit: BoxFit.fill),
                                              ),
                                            ),
                                        imageBuilder: (context, provider) {
                                          return Container(
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: provider,
                                                  fit: BoxFit.fill),
                                            ),
                                          );
                                        })
                                    : Container(
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image:
                                                  AssetImage('images/loading.gif'),
                                              fit: BoxFit.fill),
                                        ),
                                      ),
                                footer: GridTileBar(
                                  backgroundColor: Colors.black87,
                                  title: Text(
                                    photo.photos[i].ad,
                                    textAlign: TextAlign.right,
                                  ),
                                ),
                              ),
                            ),
                          )),
                    );
                  });
                }
                return Consumer<GetphotosAuctionsbyIdProvider>(
                    builder: (context, photo, _) {
                  if (photo.photos.length <= 0) {
                    return Center(child: Text(localization.text('no_posts')));
                  }
                  return ListView.builder(
                      key: UniqueKey(),
                      primary: false,
                      shrinkWrap: true,
                      itemCount: photo.photos.length,
                      itemBuilder: (context, i) => ListViewImagesItem(
                            length: photo.photos.length,
                            photo: photo.photos[i].photo,
                            photoId: photo.photos[i].id,
                            prefs: prefs,
                          ));
                });
              }),
        ),
      ),
    );
  }
}
