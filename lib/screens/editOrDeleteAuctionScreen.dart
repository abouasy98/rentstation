import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/get/getmyAuctionsProvider.dart';
import 'package:rentstation/widgets/myAuctionItem.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditOrDeleteAuctionScreen extends StatelessWidget {
  final SharedPreferences prefs;
  EditOrDeleteAuctionScreen({this.prefs});
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Provider.of<GetMyAductionProvider>(context, listen: false)
          .getMyAuctions(prefs.getString('api_token'), context),
      builder: (context, snapShot) {
        if (snapShot.connectionState == ConnectionState.waiting)
          return SpinKitThreeBounce(
            color: Theme.of(context).primaryColor,
            size: 22,
          );
        if (!snapShot.hasData) {
          return Center(child: Text(localization.text('no_results')));
        }
        return Padding(
          padding: const EdgeInsets.only(top: 16.0, bottom: 16),
          child:
              Consumer<GetMyAductionProvider>(builder: (ctx, auctionData, _) {
            if (auctionData.myAuctions.length <= 0) {
              return Center(
                child: Text(localization.text('no_results')),
              );
            }
            return ListView.builder(
              primary: false,
              shrinkWrap: true,
              itemCount: auctionData.myAuctions.length,
              itemBuilder: (context, i) => Card(
                child: MyAuctionItem(
                  id: auctionData.myAuctions[i].id,
                  image: auctionData.myAuctions[i].photos[0].photo,
                  title: auctionData.myAuctions[i].name,
                  period: auctionData.myAuctions[i].period,
                  photos: auctionData.myAuctions[i].photos,
                  intialPrice: auctionData.myAuctions[i].initialPrice,
                  priceIncrease: auctionData.myAuctions[i].priceIncrease,
                  desc: auctionData.myAuctions[i].details,
                  labelCity: auctionData.myAuctions[i].city,
                  images: auctionData.myAuctions[i].photos,
                  currency: auctionData.myAuctions[i].currency,
                  categorylable: auctionData.myAuctions[i].category,
                  subCategorylable: auctionData.myAuctions[i].subCategory,
                  prefs: prefs,
                  categoryId:auctionData.myAuctions[i].categoryId ,
                ),
              ),
            );
          }),
        );
      },
    );
  }
}
