import 'package:flutter/material.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/get/adCommentModel.dart';
import 'package:rentstation/provider/post/addReplyProvider.dart';
import 'package:rentstation/widgets/replyCard.dart';

class AddReply extends StatefulWidget {
  final int id;
  final List<Replay> replays;
  const AddReply({Key key, this.id, this.replays}) : super(key: key);

  @override
  _AddReplyState createState() => _AddReplyState();
}

class _AddReplyState extends State<AddReply> {
  TextEditingController _controller = TextEditingController();
  // GetAdComment _model;
  // _getShared() async {
  //   setState(() {
  //     Provider.of<GetCommentProvider>(context, listen: false)
  //         .getCommentAds(
  //       Provider.of<SharedPref>(context, listen: false).token,
  //       context,
  //       widget.id,
  //     )
  //         .then((res) {
  //       setState(() {
  //         _model = res;
  //       });
  //     });
  //   });
  // }

  // @override
  // void initState() {
  //   super.initState();
  //   // _getShared();
  // }

  String comment;
  bool _load = false;
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      appBar: AppBar(
        title: Text(localization.text('shares')),
        centerTitle: true,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context, true);
          },
          child: Icon(Icons.arrow_back_ios),
        ),
      ),
      body: Column(
        children: <Widget>[
          if (widget.replays.length <= 0 &&
              Provider.of<AddReplyProvider>(context, listen: false).addReplayModel ==
                  null)
            Container(
              height: MediaQuery.of(context).size.height * 0.4,
              alignment: Alignment.bottomCenter,
              child: Text(
                localization.text('no_results'),
                style: TextStyle(color: Colors.black),
              ),
            ),
          Visibility(
            visible: Provider.of<AddReplyProvider>(context, listen: false)
                        .reply
                        .length ==
                    0 ||
                Provider.of<AddReplyProvider>(context, listen: false).reply ==
                    null,
            child: Expanded(
              child: SingleChildScrollView(
                child: ListView.builder(
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: widget.replays.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ReplyCard(
                      index: index,
                      idUser: widget.replays[index].userId,
                      comment: widget.replays[index].replay,
                      name: widget.replays[index].user,
                      data: intl.DateFormat.yMd()
                          .format(widget.replays[index].createdAt),
                      idComment: widget.replays[index].commentId,
                      img: widget.replays[index].userPhoto,
                    );
                  },
                ),
              ),
            ),
          ),
          Visibility(
            visible: Provider.of<AddReplyProvider>(context, listen: false)
                        .reply
                        .length >
                    0 &&
                Provider.of<AddReplyProvider>(context, listen: false).reply !=
                    null,
            child: Expanded(
              child: SingleChildScrollView(
                child: ListView.builder(
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                  itemCount:
                      Provider.of<AddReplyProvider>(context, listen: false)
                          .reply
                          .length,
                  itemBuilder: (BuildContext context, int index) {
                    return ReplyCard(
                      index: index,
                      idUser:
                          Provider.of<AddReplyProvider>(context, listen: false)
                              .reply[index]
                              .userId,
                      comment:
                          Provider.of<AddReplyProvider>(context, listen: false)
                              .reply[index]
                              .replay,
                      name:
                          Provider.of<AddReplyProvider>(context, listen: false)
                              .reply[index]
                              .user,
                      data: intl.DateFormat.yMd().format(
                          Provider.of<AddReplyProvider>(context, listen: false)
                              .reply[index]
                              .createdAt),
                      idComment:
                          Provider.of<AddReplyProvider>(context, listen: false)
                              .reply[index]
                              .commentId,
                      img: Provider.of<AddReplyProvider>(context, listen: false)
                          .reply[index]
                          .userPhoto,
                    );
                  },
                ),
              ),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.1,
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            decoration: BoxDecoration(color: Colors.grey[200]),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: Directionality(
                    //   textDirection: p.TextDirection.rtl,
                    textDirection: TextDirection.rtl,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                          controller: _controller,
                          maxLength: 200,
                          decoration: InputDecoration.collapsed(
                              hintText: localization.text('replay')),
                          onChanged: (value) {
                            comment = value;
                          }),
                    ),
                  ),
                ),

                _load == true
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width / 10,
                          child: Center(
                            child: Container(
                              width: 20,
                              height: 20,
                              child: Center(
                                child: CircularProgressIndicator(),
                              ),
                            ),
                          ),
                        ),
                      )
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width / 10,
                          child: InkWell(
                            onTap: () {
                              _saveForm();
                            },
                            child: Text(
                              localization.text('send'),
                              style: TextStyle(
                                  fontSize: 15,
                                  color: Theme.of(context).primaryColor,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      )
                // IconButton(
                //   icon: Icon(
                //     Icons.send,
                //     color: Theme.of(context).primaryColor,
                //   ),
                //   onPressed: () {
                //  print("${comment}.................");
                //      print("${widget.newsId}.................");
                //     _saveForm();

                //   },
                // ),
              ],
            ),
          )
        ],
      ),
    );
  }

  _saveForm() {
    Provider.of<AddReplyProvider>(context, listen: false)
        .addReply(Provider.of<SharedPref>(context, listen: false).token,
            comment, widget.id, context)
        .then((res) {
      setState(() {
        _load = false;
        comment = null;
        FocusScope.of(context).unfocus();
      });
      switch (res.code) {
        case 200:
          print("done");
          _controller.clear();

          //   setState(() {
          //  _model = null;
          //   });
          //  _getShared();
          break;
        case 400:
          print("data don't match");
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  backgroundColor: Colors.white,
                  elevation: 3,
                  contentPadding: EdgeInsets.all(5),
                  children: <Widget>[
                    Text(
                      res.error[0].value,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black, fontSize: 20),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: MaterialButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        elevation: 3,
                        height: 45,
                        color: Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        child: Text(
                          localization.text('ok'),
                          style: TextStyle(fontSize: 17, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                );
              });
          break;
        case 422:
          print(res.error[0].value);
          setState(() {
            //  _load = false;
          });
          break;
        default:
          setState(() {
            //_load = false;
          });
          print('error data');
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  backgroundColor: Colors.white,
                  elevation: 3,
                  contentPadding: EdgeInsets.all(5),
                  children: <Widget>[
                    Text(
                      localization.text('Please_enter_the_data_correctly'),
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black, fontSize: 20),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: MaterialButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        elevation: 3,
                        height: 45,
                        color: Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        child: Text(
                          localization.text('ok'),
                          style: TextStyle(fontSize: 17, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                );
              });
      }
    });
  }
}
