import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Components/buttonSignIn.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/Components/inputTextField.dart';
import 'package:rentstation/models/get/countriesModel.dart';
import 'package:rentstation/provider/changeData/changeEmailProvider.dart';
import 'package:rentstation/provider/changeData/changePhoneProvider.dart';
import 'package:rentstation/provider/changeData/changeUserDataProvider.dart';
import 'package:rentstation/screens/EditData/editPassword.dart';
import 'package:rentstation/screens/login.dart';
import 'package:rentstation/widgets/ImagePicker/image_picker_handler.dart';
import 'package:rentstation/widgets/custom_app_bar_back_ground.dart';
import 'package:rentstation/provider/get/getcountries.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile>
    with TickerProviderStateMixin, ImagePickerListener {
  final _form = GlobalKey<FormState>();
  SharedPreferences _preferences;
  CountriesModel getCountries;
  int counteyId;
  String selecetedCountry;

  // int gender;
  // bool male = true;
  bool isInit = true;
  // void genderFunc(bool newVal) {
  //   setState(() {
  //     male = newVal;
  //     print('male:$male');
  //   });
  // }

  ImageProvider imageProvider = AssetImage("images/16.jpg");

  File _image;
  TextEditingController _nameController;
  TextEditingController _emailController;

  TextEditingController _phoneController;

  AnimationController _controller;
  ImagePickerHandler imagePicker;

  @override
  void initState() {
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    imagePicker = new ImagePickerHandler(
        this, _controller, Color.fromRGBO(12, 169, 149, 1));
    imagePicker.init();

    super.initState();
  }

  mainBottomSheet(BuildContext context) {
    return showModalBottomSheet<dynamic>(
        isScrollControlled: false,
        backgroundColor: Colors.white,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        builder: (BuildContext bc) {
          return Directionality(
            textDirection: TextDirection.rtl,
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: ListView(children: <Widget>[
                    Container(
                      color: Colors.white,
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemCount: getCountries.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              ListTile(
                                  onTap: () {
                                    print(getCountries.data[index].id);
                                    setState(() {
                                      selecetedCountry =
                                          getCountries.data[index].name;

                                      counteyId = getCountries.data[index].id;
                                    });

                                    Navigator.of(context)
                                        .pop(getCountries.data[index].name);
                                  },
                                  title: Text(
                                    getCountries.data[index].name,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )),
                              Divider(
                                height: 1,
                                color: Colors.grey,
                              )
                            ],
                          );
                        },
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          );
        });
  }

  @override
  void didChangeDependencies() async {
    if (isInit) {
      _preferences = await SharedPreferences.getInstance();


     
      setState(() {
        isInit = false;
      });
    }

    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: isInit
            ? SpinKitThreeBounce(
                color: Theme.of(context).primaryColor,
                size: 22,
              )
            : Provider.of<SharedPref>(context, listen: false).token == null
                ? SignInScreen(false)
                : _preferences == null
                    ? Container()
                    : Stack(
                        children: <Widget>[
                          Center(
                            child: Container(
                              margin: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.width * .46),
                              child: Container(
                                padding: EdgeInsets.all(8),
                                width: MediaQuery.of(context).size.width * .9,
                                child: ListView(
                                  shrinkWrap: true,
                                  physics: ScrollPhysics(),
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Form(
                                          key: _form,
                                          child: InputFieldArea(
                                            border: false,
                                            error: (value) {
                                              if (value.isEmpty) {
                                                return "${localization.text("name")} ${localization.text("required")}";
                                              } else if (value.length >= 15) {
                                                return 'لا يمكن ان يزيد الاسم عن 15 حرف';
                                              }
                                              return null;
                                            },
                                            hint:
                                                localization.text("user_name"),
                                            //  "اسم المستخدم",
                                            changedFunction: (v) {
                                              Provider.of<ChangeUserDataProvider>(
                                                      context,
                                                      listen: false)
                                                  .name = v;
                                            },
                                            textInputType: TextInputType.text,
                                            controller: _nameController,
                                            lengthChar: 15,
                                            suffixIcon: Icon(
                                              Icons.edit,
                                              color: Theme.of(context)
                                                  .primaryColor,
                                            ),
                                            init: _preferences.get("name"),
                                          ),
                                        ),
                                        SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              .03,
                                        ),
                                        // Row(
                                        //   mainAxisAlignment:
                                        //       MainAxisAlignment.spaceEvenly,
                                        //   children: [
                                        //     Text(
                                        //       localization.text("female"),
                                        //       style: TextStyle(
                                        //           color: Colors.black,
                                        //           fontSize: 16),
                                        //     ),
                                        //     CupertinoSwitch(
                                        //       value: male,
                                        //       onChanged: genderFunc,
                                        //       activeColor: Colors.blueAccent,
                                        //       trackColor: Colors.red,
                                        //     ),
                                        //     Text(localization.text("male"),
                                        //         style: TextStyle(
                                        //             color: Colors.black,
                                        //             fontSize: 16)),
                                        //   ],
                                        // ),
                                        SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              .02,
                                        ),

                                        SizedBox(
                                          height: 10,
                                        ),
                                        SignInButton(
                                          txtColor: Colors.white,
                                          onPressSignIn: () {
                                            final _isValid =
                                                _form.currentState.validate();
                                            if (!_isValid) {
                                              return;
                                            }
                                            _form.currentState.save();
                                            // if (male == true) {
                                            //   gender = 1;
                                            // } else {
                                            //   gender = 0;
                                            // }
                                            // Provider.of<ChangeUserDataProvider>(
                                            //         context,
                                            //         listen: false)
                                            //     .gender = gender;

                                            Provider.of<ChangeUserDataProvider>(
                                                    context,
                                                    listen: false)
                                                .changeUserData(
                                                    _preferences
                                                        .get("api_token"),
                                                    context)
                                                .then((v) {
                                              if (v == true) {
                                                Provider.of<SharedPref>(context,
                                                        listen: false)
                                                    .getSharedHelper(
                                                        _preferences);
                                              }
                                            });
                                          },
                                          btnWidth: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              40,
                                          btnHeight: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              .07,
                                          btnColor:
                                              Theme.of(context).primaryColor,
                                          buttonText: localization
                                              .text("edit_provider_data"),

                                          // "تعديل بيانات المستخدم",
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        InputFieldArea(
                                          border: false,
                                          hint: localization.text("email"),
                                          //  "البريد الالكتروني",
                                          changedFunction: (v) {
                                            Provider.of<ChangeEmailProvider>(
                                                    context,
                                                    listen: false)
                                                .email = v;
                                          },
                                          textInputType: TextInputType.text,
                                          controller: _emailController,
                                          suffixIcon: Icon(
                                            Icons.edit,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                          init: _preferences.get("email"),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        SignInButton(
                                          txtColor: Colors.white,
                                          onPressSignIn: () {
                                            Provider.of<ChangeEmailProvider>(
                                                    context,
                                                    listen: false)
                                                .changeEmail(
                                                    _preferences
                                                        .get("api_token"),
                                                    context);
                                          },
                                          btnWidth: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              40,
                                          btnHeight: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              .07,
                                          btnColor:
                                              Theme.of(context).primaryColor,
                                          buttonText:
                                              localization.text("edit_email"),

                                          // "تعديل بيانات المستخدم",
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: InputFieldArea(
                                            changedFunction: (v) {
                                              Provider.of<ChangeMobileProvider>(
                                                      context,
                                                      listen: false)
                                                  .phone = v;
                                            },
                                            border: false,
                                            hint: _preferences.get("phone"),
                                            textInputType: TextInputType.number,
                                            controller: _phoneController,
                                            suffixIcon: Icon(
                                              Icons.edit,
                                              color: Theme.of(context)
                                                  .primaryColor,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        SignInButton(
                                          txtColor: Colors.white,
                                          onPressSignIn: () {
                                            Provider.of<ChangeMobileProvider>(
                                                    context,
                                                    listen: false)
                                                .changeMobile(
                                                    _preferences
                                                        .get("api_token"),
                                                    context);
                                          },
                                          btnWidth: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              40,
                                          btnHeight: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              .07,
                                          btnColor:
                                              Theme.of(context).primaryColor,
                                          buttonText:
                                              localization.text("edit_phone"),

                                          // 'تعديل رقم الجوال',
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Divider(
                                      height: 2,
                                      color: Colors.grey,
                                    ),
                                    EditPassword()
                                  ],
                                ),
                              ),
                            ),
                          ),
                          CustomAppBarBackGround(
                            widgets: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Container(
                                    height: 60,
                                    alignment: Alignment.center,
                                    child: Text(
                                      localization.text("edit_profile"),
                                      // "تعديل حسابي",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              margin: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * .05),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: Colors.white, width: 5)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Stack(
                                    children: <Widget>[
                                      InkWell(
                                        onTap: () =>
                                            imagePicker.showDialog(context),
                                        child: _image == null
                                            ? Provider.of<SharedPref>(context,
                                                            listen: false)
                                                        .photo !=
                                                    null
                                                ? CachedNetworkImage(
                                                    imageUrl:
                                                        Provider.of<SharedPref>(
                                                                context,
                                                                listen: false)
                                                            .photo,
                                                    fadeInDuration:
                                                        Duration(seconds: 2),
                                                    placeholder: (context,
                                                            url) =>
                                                        CircleAvatar(
                                                            radius: 60,
                                                            backgroundImage:
                                                                AssetImage(
                                                                    'images/16.jpg')),
                                                    imageBuilder:
                                                        (context, provider) {
                                                      return CircleAvatar(
                                                          radius: 60,
                                                          backgroundImage:
                                                              provider);
                                                    },
                                                  )
                                                : CircleAvatar(
                                                    radius: 60,
                                                    backgroundImage: AssetImage(
                                                        'images/16.jpg'))
                                            : Container(
                                                child: CircleAvatar(
                                                  radius: 60,
                                                  backgroundImage:
                                                      Image.file(_image).image,
                                                ),
                                              ),
                                      ),
                                      Positioned(
                                        bottom: 5,
                                        right: 10,
                                        child: InkWell(
                                          onTap: () =>
                                              imagePicker.showDialog(context),
                                          child: CircleAvatar(
                                            child: Icon(
                                              Icons.add,
                                              color: Colors.black,
                                              size: 15,
                                            ),
                                            radius: 10,
                                            backgroundColor: Colors.white,
                                          ),
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
      ),
    );
  }

  @override
  userImage(File _image) {
    setState(() {
      this._image = _image;
      Provider.of<ChangeUserDataProvider>(context, listen: false).image =
          _image;
    });
  }
}
