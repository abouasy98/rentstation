import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/models/get/getAuctionById.dart';
import 'package:rentstation/widgets/ListofAuctionersItem.dart';

class ListofAuctioners extends StatelessWidget {

  final GetAuctionById model;
  final List<Price> prices;
  ListofAuctioners({this.prices, this.model});

  @override
  Widget build(BuildContext context) {
    return Directionality(
            textDirection: localization.currentLanguage.toString() == "en"
            ? TextDirection.ltr
            : TextDirection.rtl,
          child: Scaffold(
          appBar: AppBar(
            title: Text(localization.text('Bids')),
            centerTitle: true,
          ),
          body: prices.length == 0
              ? Center(child: Text(localization.text('no_posts')))
              : ListView.builder(
                  primary: false,
                  shrinkWrap: true,
                  itemCount: prices.length,
                  itemBuilder: (BuildContext context, int i) =>
                      ListofAuctionersItem(
                        createdAt: prices[i].createdAt,
                  
                        price: prices[i].price,
                        userPhoto: prices[i].userPhoto,
                        auctionWinnerid: model.data.auctionWinnerId,
                        auctionId: prices[i].auctionId,
                        userid: prices[i].userId,
                     
                        userName: prices[i].user,
                      ))),
    );
  }
}
