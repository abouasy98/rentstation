import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/get/myConversation.dart';
import 'package:rentstation/provider/get/getChatsProvider.dart';
import 'package:rentstation/screens/Chat/chat_room.dart';
import 'package:rentstation/screens/login.dart';
import 'package:provider/provider.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  bool isInit = true;

  MyConversations menuChat;
  _getShared() async {
    menuChat = null;
    if (Provider.of<SharedPref>(context, listen: false).token != null) {
      await Provider.of<GetChatsProvider>(context, listen: false)
          .getChats(
              Provider.of<SharedPref>(context, listen: false).token, context)
          .then((res) {
        setState(() {
          menuChat = res;
        });
      });
    }
  }

  @override
  void didChangeDependencies() async {
    if (isInit) {
      await _getShared();
      setState(() {
        isInit = false;
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isInit
          ? SpinKitThreeBounce(
              color: Theme.of(context).primaryColor,
              size: 22,
            )
          : Provider.of<SharedPref>(context, listen: false).token == null
              ? SignInScreen(false)
              : menuChat.data == null
                  ? Container(
                      height: MediaQuery.of(context).size.height * 0.6,
                      child: Center(
                        child: Text(
                          localization.text('You_havenot_spoken_to_anyone_yet'),
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    )
                  : ListView.builder(
                      itemCount: menuChat.data.length,
                      itemBuilder: (context, i) => GestureDetector(
                        onTap: () {
                          pushNewScreen(
                            context,
                            screen: ChatRoom(
                              chateId: menuChat.data[i].id,
                              userName: menuChat.data[i].name,
                            ),
                            withNavBar:
                                false, // OPTIONAL VALUE. True by default.
                            pageTransitionAnimation:
                                PageTransitionAnimation.cupertino,
                          );
                          // Navigator.of(context).push(MaterialPageRoute(
                          //     builder: (context) => ChatRoom(
                          //           chateId: menuChat.data[i].id,
                          //           userName: menuChat.data[i].name,
                          //         )));
                        },
                        child: Card(
                          child: ListTile(
                            leading: CircleAvatar(
                              backgroundImage:
                                  NetworkImage(menuChat.data[i].photo ?? ""),
                            ),
                            title: Text(
                              menuChat.data[i].name ?? "",
                            ),
                            subtitle: Text(menuChat.data[i].lastMessage ?? ""),
                          ),
                        ),
                      ),
                    ),
    );
  }
}
