import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/screens/createAuctionScreen.dart';
import 'package:rentstation/screens/editOrDeleteAuctionScreen.dart';
import 'package:rentstation/screens/myAuctionsArchieve.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'myWinedAuctionScreen.dart';

class MyAuctionScreen extends StatefulWidget {
  final SharedPreferences prefs;
  MyAuctionScreen({this.prefs});
  @override
  _MyAuctionScreenState createState() => _MyAuctionScreenState();
}

class _MyAuctionScreenState extends State<MyAuctionScreen> {
  List<Map<String, dynamic>> list = [
    {
      'selected': true,
      'name': localization.text('edit_auction'),
      'icon': Icons.edit,
    },
    {
      'selected': false,
      'name': localization.text('archive_auctions'),
      'icon': Icons.archive,
    },
    {
      'selected': false,
      'name': localization.text('Auctions_you_won'),
      'icon': FontAwesome.hammer,
    }
  ];
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return Directionality(
      textDirection: localization.currentLanguage.toString() == "en"
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        //  resizeToAvoidBottomPadding: false,
          appBar: PreferredSize(
            preferredSize:
                Size.fromHeight(MediaQuery.of(context).size.height * 0.17),
            child: AppBar(
              bottom: PreferredSize(
                preferredSize: mediaQuery,
                child: Container(
                  decoration: BoxDecoration(color: Colors.white),
                  height: mediaQuery.height * 0.1,
                  width: mediaQuery.width,
                  child:  ListView.builder(
                      itemCount: list.length,
                      primary: false,
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (_, index) =>
                       InkWell(
                              onTap: () {
                                setState(() {
                                  list[index]['selected'] = true;
                                  for (int i = 0; i < list.length; i++) {
                                    if (i != index) {
                                      list[i]['selected'] = false;
                                    }
                                  }
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: list[index]['selected'] == true
                                      ? Colors.black54
                                      : Colors.white,
                                ),
                                width: MediaQuery.of(context).size.width / 3,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      height: 35,
                                      width: 35,
                                      child: list[index]['icon'] != null
                                          ? Icon(
                                              list[index]['icon'],
                                              color: list[index]['selected'] ==
                                                      true
                                                  ? Colors.white
                                                  : Colors.black,
                                            )
                                          : FaIcon(
                                              list[index]['icon'],
                                              color: list[index]['selected'] ==
                                                      true
                                                  ? Colors.white
                                                  : Colors.black,
                                            ),
                                    ),
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(
                                      list[index]['name'],
                                      style: TextStyle(
                                          color: list[index]['selected'] == true
                                              ? Colors.white
                                              : Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                              ),
                            ),
                     
                    
                  ),
                ),
              ),
              title: Text(localization.text('my_auctions')),
              actions: <Widget>[
                FlatButton(
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => CreateAuctionScreen()));
                  },
                ),
              ],
            ),
          ),
          body: list[0]['selected']
              ? EditOrDeleteAuctionScreen(
                  prefs: widget.prefs,
                )
              : list[1]['selected']
                  ? MyAuctionsArchieve(prefs: widget.prefs)
                  : MyWinedAuctionScreen(
                      prefs: widget.prefs,
                    )),
    );
  }
}
