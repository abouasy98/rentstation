import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';

import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/models/categoriesModels.dart';
import 'package:rentstation/models/get/getCitiesModel.dart';
import 'package:rentstation/models/subCategoriesModel.dart';
import 'package:rentstation/provider/get/categoriesProvider.dart';
import 'package:rentstation/provider/get/getCitiesByIdProvider.dart';
import 'package:rentstation/Components/detailsTextFieldNoImg.dart';
import 'package:rentstation/Components/registerTextField.dart';
import 'package:rentstation/Components/CustomButton.dart';
import 'package:rentstation/provider/get/subCategoriesProvider.dart';
import 'package:rentstation/provider/post/editAdProvider.dart';
import 'package:rentstation/screens/listviewImages.dart';

import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditAd extends StatefulWidget {
  final String lableCity;
  final String title;
  final String categorylable;
  final String subCategorylable;
  final int subCategoryId;
  final int categoryId;
  final bool checked;
  final String desc;
  final int price;
  final int id;
  final List photos;

  final List images;
  const EditAd(
      {Key key,
      this.checked,
      this.subCategorylable,
      this.images,
      this.title,
      this.categoryId,
      this.categorylable,
      this.photos,
      this.lableCity,
      this.desc,
      this.id,
      this.price,
      this.subCategoryId})
      : super(key: key);
  @override
  _EditAdState createState() => _EditAdState();
}

class _EditAdState extends State<EditAd> {
  List<Asset> _multiImages = [
    null,
    null,
    null,
    null,
    null,
    null,
  ];
  String lableCity;
  String categorylable;
  String subCategorylable;
  String title;
  String details;
  int cityId;
  int categoryId;
  CustomDialog dialog = CustomDialog();
  int subCategoryId;
  int price;
  String priceDiscussion;
  String productState;
  bool _checked;
  List<CitiesModel> cities = [];
  SubCategoriesModels getSubCategoriesModel;
  List<CitiesModel> categories = [];
  List<CitiesModel> subCategories = [];
  bool loaded = true;
  GetCities getCitiesByCountryIdModel;
  CategoriesModels getCategoriesModel;
  SharedPreferences _prefs;
  _getShared() async {
 
    var _instance = await SharedPreferences.getInstance();
    setState(() {
      _prefs = _instance;
    });
    await Provider.of<GetCitiesByCountryIdProvider>(context, listen: false)
        .getCities(_prefs.get("countryId"), context)
        .then((res) {
      setState(() {
        getCitiesByCountryIdModel = res;
      });
      if (getCitiesByCountryIdModel.data != null) {
        for (int i = 0; i < getCitiesByCountryIdModel.data.length; i++) {
          cities.add(new CitiesModel(
            id: getCitiesByCountryIdModel.data[i].id,
            label: getCitiesByCountryIdModel.data[i].name,
          ));
        }
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  bool isInit = true;

  @override
  void didChangeDependencies() async {
    if (isInit) {
      await _getShared();
      Provider.of<CategoriesProvider>(context, listen: false)
          .getCategories(context)
          .then((res) {
        setState(() {
          getCategoriesModel = res;
        });
        if (getCategoriesModel.data != null) {
          for (int i = 0; i < getCategoriesModel.data.length; i++) {
            categories.add(new CitiesModel(
              id: getCategoriesModel.data[i].id,
              label: getCategoriesModel.data[i].name,
            ));
          }
        }
      });
      categorylable = widget.categorylable;
      lableCity = widget.lableCity;
      subCategorylable = widget.subCategorylable;

      setState(() {
        isInit = false;
      });
    }
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: localization.currentLanguage.toString() == "en"
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(
            localization.text("edit_ad"),
          ),
          leading: InkWell(
            onTap: () => Navigator.pop(context),
            child: Container(
                color: Theme.of(context).primaryColor,
                child: Icon(Icons.arrow_back_ios)),
          ),
        ),
        backgroundColor: Colors.white,
        body: isInit
            ? SpinKitThreeBounce(
                color: Theme.of(context).primaryColor,
                size: 22,
              )
            : Container(
                color: Color.fromRGBO(240, 240, 240, 1),
                child: ListView(
                  shrinkWrap: true,
                  primary: false,
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height * 0.3,
                      child: Carousel(
                        images: widget.images
                            .map(
                              (e) => GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => ListViewImages(
                                            adId: widget.id,
                                            prefs: _prefs,
                                          )));
                                },
                                child: e.photo != null
                                    ? CachedNetworkImage(
                                        imageUrl: e.photo,
                                        fadeInDuration: Duration(seconds: 2),
                                        placeholder: (context, url) =>
                                            Container(
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: AssetImage(
                                                        'images/loading.gif'),
                                                    fit: BoxFit.fill),
                                              ),
                                            ),
                                        imageBuilder: (context, provider) {
                                          return Container(
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: provider,
                                                  fit: BoxFit.fill),
                                            ),
                                          );
                                        })
                                    : Container(
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'images/loading.gif'),
                                              fit: BoxFit.fill),
                                        ),
                                      ),
                              ),
                            )
                            .toList(),
                        dotSize: 4.0,
                        dotSpacing: 15.0,
                        dotColor: Colors.black,
                        indicatorBgPadding: 4.0,
                        dotIncreasedColor: Colors.blueAccent,
                        dotBgColor: Colors.white.withOpacity(0.5),
                        borderRadius: true,
                        moveIndicatorFromBottom: 180.0,
                        noRadiusForIndicator: true,
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      color: Colors.white,
                      child: Column(
                        children: <Widget>[
                          RegisterTextField(
                            border: 0,
                            type: TextInputType.text,
                            onChange: (value) {
                              title = value;
                            },
                            hint: localization.text("title"),
                            label: "",
                            init: widget.title,
                          ),
                          Divider(
                            height: 1,
                            color: Colors.grey,
                          ),
                          DetailsTextFieldNoImg(
                            border: 0,
                            label: "",
                            hint: localization.text("ad_details"),
                            onChange: (value) {
                              details = value;
                            },
                            init: widget.desc,
                          ),
                          Divider(
                            height: 1,
                            color: Colors.grey,
                          ),
                          RaisedButton(
                            elevation: 0,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0)),
                            onPressed: () {
                              mainBottomSheet(context, categories, "تصنيف");
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              textDirection:
                                  localization.currentLanguage.toString() ==
                                          "en"
                                      ? TextDirection.ltr
                                      : TextDirection.rtl,
                              children: <Widget>[
                                Text(categorylable),
                                Icon(Icons.arrow_back_ios)
                              ],
                            ),
                          ),
                          Divider(
                            height: 1,
                            color: Colors.grey,
                          ),
                          RaisedButton(
                            elevation: 0,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0)),
                            onPressed: () {
                              mainBottomSheet(
                                  context, subCategories, "تصنيف فرعي");
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              textDirection:
                                  localization.currentLanguage.toString() ==
                                          "en"
                                      ? TextDirection.ltr
                                      : TextDirection.rtl,
                              children: <Widget>[
                                Text(subCategorylable),
                                Icon(Icons.arrow_back_ios)
                              ],
                            ),
                          ),
                          Divider(
                            height: 1,
                            color: Colors.grey,
                          ),
                          RaisedButton(
                            elevation: 0,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0)),
                            onPressed: () {
                              mainBottomSheet(context, cities, "المدينة");
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              textDirection:
                                  localization.currentLanguage.toString() ==
                                          "en"
                                      ? TextDirection.ltr
                                      : TextDirection.rtl,
                              children: <Widget>[
                                Text(lableCity),
                                Icon(Icons.arrow_back_ios)
                              ],
                            ),
                          ),
                          Divider(
                            height: 1,
                            color: Colors.grey,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.all(20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Text(
                                  localization.text("price"),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  width: 80,
                                  child: TextFormField(
                                    keyboardType: TextInputType.number,
                                    initialValue: '${widget.price}',
                                    textAlign: TextAlign.right,
                                    onChanged: (value) {
                                      price = int.parse(value);
                                    },
                                    validator: (value) {
                                      if (int.parse(value) <= 0) {
                                        return localization
                                            .text('please_add_price');
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.only(top: 10, right: 10),
                                      border: new OutlineInputBorder(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(10)),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(_prefs.get("currency")),
                              ],
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.03,
                            ),
                            Text(
                              localization.text("discussion_price"),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                            Checkbox(
                                value: _checked != null
                                    ? _checked
                                    : widget.checked,
                                onChanged: (_) {
                                  if (priceDiscussion == '1') {
                                    setState(() {
                                      _checked = false;
                                      priceDiscussion = '0';
                                    });
                                  } else {
                                    setState(() {
                                      _checked = true;
                                      priceDiscussion = '1';
                                    });
                                  }
                                }),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 250,
                        child: GridView.count(
                          crossAxisCount: 3,
                          physics: NeverScrollableScrollPhysics(),
                          mainAxisSpacing: 5,
                          crossAxisSpacing: 5,
                          children: List.generate(6, (index) {
                            //  Asset asset = _multiImages[index];
                            return Stack(
                              children: <Widget>[
                                _multiImages[index] != null
                                    ? AssetThumb(
                                        asset: _multiImages[index],
                                        width: 300,
                                        height: 300,
                                      )
                                    : Container(
                                        child: Icon(
                                          FontAwesomeIcons.camera,
                                          size: 50,
                                          color: Colors.white,
                                        ),
                                        color: Colors.blue,
                                        width: 300,
                                        height: 300,
                                      ),
                                Positioned(
                                  child: _multiImages[index] == null
                                      ? InkWell(
                                          onTap: () {
                                            setState(() {
                                              // _multiImages.removeAt(index);
                                              _loadAssets(index);
                                            });
                                          },
                                          child: Icon(
                                            Icons.add_circle_outline,
                                            color: Colors.green[700],
                                            size: 30,
                                          ))
                                      : InkWell(
                                          onTap: () {
                                            setState(() {
                                              _multiImages[index] = null;
                                            });
                                          },
                                          child: Icon(
                                            Icons.delete,
                                            color: Colors.red,
                                            size: 20,
                                          ),
                                        ),
                                  bottom: 5,
                                  left: 5,
                                ),
                              ],
                            );
                          }),
                        ),
                      ),
                    ),
                    SizedBox(height: 50),
                    Provider.of<EditAdProvider>(context).loading == true
                        ? Center(
                            child: CircularProgressIndicator(),
                          )
                        : Padding(
                            padding: const EdgeInsets.only(left: 50, right: 50),
                            child: CustomButton(
                              onTap: () {
                                if (categoryId != null &&
                                    subCategoryId == null) {
                                  dialog.showErrorDialog(
                                    btnOnPress: () {},
                                    context: context,
                                    msg: localization.text(
                                        "If_you_modify_the_main_categories_you_must_also_modify_the_subcategories"),
                                    //  "يحب الموافقة علي الشروط والاحكام",
                                    ok: localization.text("ok"),
                                  );
                                  return;
                                }
                                setState(() {
                                  Provider.of<EditAdProvider>(context,
                                          listen: false)
                                      .loading = true;
                                });
                                Provider.of<EditAdProvider>(context,
                                        listen: false)
                                    .editAd(
                                  token: _prefs.getString('api_token'),
                                  title: title,
                                  description: details,
                                  price: price,
                                  photos: _multiImages,
                                  context: context,
                                  id: widget.id,
                                  priceStatus: priceDiscussion,
                                  categoryId: categoryId,
                                  cityId: cityId,
                                  subCategoryId: subCategoryId,
                                );
                              },
                              text: localization.text("edit"),
                            ),
                          ),
                    SizedBox(height: 20)
                  ],
                ),
              ),
      ),
    );
  }

  Future<void> _loadAssets(int index) async {
    List<Asset> resultList =
        await MultiImagePicker.pickImages(maxImages: 1, enableCamera: false);
    Asset photo = resultList[0];
    setState(() {
      _multiImages[index] = photo;
    });
  }

  mainBottomSheet(BuildContext context, List<CitiesModel> list, String title) {
    return showModalBottomSheet<dynamic>(
        isScrollControlled: false,
        backgroundColor: Colors.white,
        context: context,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        builder: (BuildContext bc) {
          return title == "تصنيف فرعي"
              ? FutureBuilder(
                  future: Provider.of<SubCategoriesProvider>(context,
                          listen: false)
                      .getSubCategories(
                          categoryId != null ? categoryId : widget.categoryId,
                          context),
                  builder: (context, snapShot) {
                    if (snapShot.connectionState == ConnectionState.waiting) {
                      return SpinKitThreeBounce(
                        color: Theme.of(context).primaryColor,
                        size: 22,
                      );
                    }

                    getSubCategoriesModel = snapShot.data;

                    if (getSubCategoriesModel.data != null &&
                        subCategories.isEmpty) {
                      for (int i = 0;
                          i < getSubCategoriesModel.data.length;
                          i++) {
                        subCategories.add(new CitiesModel(
                          id: getSubCategoriesModel.data[i].id,
                          label: getSubCategoriesModel.data[i].name,
                        ));
                      }
                    }

                    loaded = false;

                    return loaded
                        ? SpinKitThreeBounce(
                            color: Theme.of(context).primaryColor,
                            size: 22,
                          )
                        : Directionality(
                            textDirection: TextDirection.rtl,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top: 20.0),
                                  child: ListView(children: <Widget>[
                                    Container(
                                      color: Colors.white,
                                      child: ListView.builder(
                                        shrinkWrap: true,
                                        physics: ScrollPhysics(),
                                        itemCount: list.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              ListTile(
                                                  onTap: () {
                                                    print(list[index].id);
                                                    Navigator.pop(context);
                                                    setState(() {
                                                      if (title == "المدينة") {
                                                        cityId = list[index].id;
                                                        print("cityid $cityId");
                                                        lableCity =
                                                            list[index].label;
                                                      } else if (title ==
                                                          "تصنيف") {
                                                        categoryId =
                                                            list[index].id;
                                                        print(
                                                            "catrgory $categoryId");
                                                        categorylable =
                                                            list[index].label;
                                                      } else {
                                                        subCategoryId =
                                                            list[index].id;
                                                        print(
                                                            "catrgory $categoryId");
                                                        subCategorylable =
                                                            list[index].label;
                                                        subCategories.clear();
                                                      }
                                                    });
                                                  },
                                                  title: Text(
                                                    list[index].label,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  )),
                                              Divider(
                                                height: 1,
                                                color: Colors.grey,
                                              )
                                            ],
                                          );
                                        },
                                      ),
                                    ),
                                  ]),
                                ),
                              ],
                            ),
                          );
                  },
                )
              : Directionality(
                  textDirection: TextDirection.rtl,
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: ListView(children: <Widget>[
                          Container(
                            color: Colors.white,
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: ScrollPhysics(),
                              itemCount: list.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    ListTile(
                                        onTap: () {
                                          print(list[index].id);
                                          Navigator.pop(context);
                                          setState(() {
                                            if (title == "المدينة") {
                                              cityId = list[index].id;
                                              print("cityid $cityId");
                                              lableCity = list[index].label;
                                            } else {
                                              categoryId = list[index].id;
                                              print("catrgory $categoryId");
                                              categorylable = list[index].label;
                                              subCategories.clear();
                                            }
                                          });
                                        },
                                        title: Text(
                                          list[index].label,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        )),
                                    Divider(
                                      height: 1,
                                      color: Colors.grey,
                                    )
                                  ],
                                );
                              },
                            ),
                          ),
                        ]),
                      ),
                    ],
                  ),
                );
        });
  }
}

class CitiesModel {
  int id;
  String label;

  CitiesModel({
    this.id,
    this.label,
  });
}
