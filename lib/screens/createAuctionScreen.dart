import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/models/categoriesModels.dart';
import 'package:rentstation/models/get/getCitiesModel.dart';
import 'package:rentstation/models/subCategoriesModel.dart';
import 'package:rentstation/provider/get/categoriesProvider.dart';
import 'package:rentstation/provider/get/getCitiesByIdProvider.dart';
import 'package:rentstation/provider/get/getTheUndertakingProvider.dart';
import 'package:rentstation/provider/get/subCategoriesProvider.dart';
import 'package:rentstation/provider/post/createAuction.dart';
import 'package:rentstation/widgets/multiPhotos.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateAuctionScreen extends StatefulWidget {
  static const routedName = "/EditProductScreen";
  @override
  _CreateAuctionScreenState createState() => _CreateAuctionScreenState();
}

class _CreateAuctionScreenState extends State<CreateAuctionScreen> {
  String initProductState;
  final _descriptionFoucsNode = FocusNode();
  final _intialPriceFoucsNode = FocusNode();
  final _lowPriceadditionFoucsNode = FocusNode();
  bool isChecked = false;
  final _durationAuctionFoucsNode = FocusNode();
  String lable = localization.text("choose_city");
  String title;
  String details;
  int intialPrice;
  int categoryId;
  List<CitiesModel> categories = [];
  List<CitiesModel> subCategories = [];
  String subCategoryId;
  String categorylable = localization.text("choose_categorie");
  String subCategorylable = localization.text("choose_sub_categorie");
  int lowadditionValue;
  int duration;
  String cityId;
  var _isInit = true;
  CustomDialog dialog = CustomDialog();
  CategoriesModels getCategoriesModel;
  SubCategoriesModels getSubCategoriesModel;
  GetCities getCitiesByCountryIdModel;
  SharedPreferences _prefs;
  List<Asset> files = [];
  List<CitiesModel> cities = [];
  var _loadedSpinner = false;
  final _form = GlobalKey<FormState>();
  bool loaded = true;
  Widget buildPrices(
    String title1,
    String title2,
    FocusNode foucsNode,
    Function func,
    Function func2,
  ) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width * 0.3,
              child: Text(
                title1,
                style: TextStyle(fontSize: 17),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.3,
              child: TextFormField(
                keyboardType: TextInputType.number,
                textAlign: TextAlign.right,
                onChanged: func2,
                onFieldSubmitted: func,
                focusNode: foucsNode,
                validator: (value) {
                  if (value.isEmpty) {
                    return localization.text("please_add_price");
                  }

                  if (double.parse(value) <= 0 ||
                      double.tryParse(value) == null) {
                    return localization.text('please_add_price');
                  }
                  return null;
                },
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(top: 10, right: 10),
                  border: new OutlineInputBorder(
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                  ),
                ),
              ),
            ),
            Text(title2),
          ],
        ),
      ),
    );
  }

  _getShared() async {
    var _instance = await SharedPreferences.getInstance();
    setState(() {
      _prefs = _instance;
    });
    await Provider.of<GetCitiesByCountryIdProvider>(context, listen: false)
        .getCities(_prefs.get("countryId"), context)
        .then((res) {
      setState(() {
        getCitiesByCountryIdModel = res;
      });
      if (getCitiesByCountryIdModel.data != null) {
        for (int i = 0; i < getCitiesByCountryIdModel.data.length; i++) {
          cities.add(new CitiesModel(
            id: getCitiesByCountryIdModel.data[i].id,
            label: getCitiesByCountryIdModel.data[i].name,
            // selected: false
          ));
        }
      }
    });
  }

  void didChangeDependencies() async {
    if (_isInit) {
      await Provider.of<GetTheUndertakingProvider>(context, listen: false)
          .getTheUndertakingProvider(context);
      await _getShared();
      await Provider.of<CategoriesProvider>(context, listen: false)
          .getCategories(context)
          .then((res) {
        setState(() {
          getCategoriesModel = res;
        });
        if (getCategoriesModel.data != null) {
          for (int i = 0; i < getCategoriesModel.data.length; i++) {
            categories.add(new CitiesModel(
              id: getCategoriesModel.data[i].id,
              label: getCategoriesModel.data[i].name,
              //selected: false
            ));
          }
        }
      });
      setState(() {
        _isInit = false;
      });
    }

    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  void dispose() {
    _descriptionFoucsNode.dispose();

    _intialPriceFoucsNode.dispose();
    _lowPriceadditionFoucsNode.dispose();
    _durationAuctionFoucsNode.dispose();
    super.dispose();
  }

  Future<void> _saveForm() async {
    if (isChecked == false) {
      dialog.showErrorDialog(
        btnOnPress: () {},
        context: context,
        msg: localization.text("should_accept_undertaking_letter"),
        //  "يحب الموافقة علي الشروط والاحكام",
        ok: localization.text("ok"),
      );
      return;
    }
    final _isValid = _form.currentState.validate();
    if (!_isValid) {
      return;
    }
    _form.currentState.save();

    setState(() {
      _loadedSpinner = true;
    });

    await Provider.of<CreatAuctionProvider>(context, listen: false)
        .creatAuction(
            photos: files,
            context: context,
            token: _prefs.get("api_token"),
            cityId: cityId,
            details: details,
            title: title,
            period: duration,
            categoryId: categoryId,
            subCategoryId: subCategoryId,
            initialPrice: intialPrice,
            priceIncrease: lowadditionValue);
    setState(() {
      _loadedSpinner = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: localization.currentLanguage.toString() == "en"
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text(localization.text('add_auction')),
          centerTitle: true,
        ),
        body: _isInit
            ? SpinKitThreeBounce(
                color: Theme.of(context).primaryColor,
                size: 22,
              )
            : Padding(
                padding: const EdgeInsets.all(15.0),
                child: Form(
                  key: _form,
                  child: ListView(
                    shrinkWrap: true,
                    primary: false,
                    children: <Widget>[
                      MultiImagePickerBtn(
                        onGetImages: (f) {
                          setState(() {
                            files = f;
                          });
                        },
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: localization.text('title')),
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context)
                              .requestFocus(_descriptionFoucsNode);
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return '${localization.text('please_add_title')}';
                          }
                          if (value.length < 5) {
                            return '${localization.text('Please_field_must_not_be_less_than_5_characters')}';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          title = value;
                        },
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: localization.text('description'),
                        ),
                        onFieldSubmitted: (_) {
                          FocusScope.of(context)
                              .requestFocus(_intialPriceFoucsNode);
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return '${localization.text('please_add_desc')}';
                          }
                          if (value.length < 5) {
                            return '${localization.text('Please_field_must_not_be_less_than_5_characters')}';
                          }
                          return null;
                        },
                        maxLines: 3,
                        keyboardType: TextInputType.multiline,
                        focusNode: _descriptionFoucsNode,
                        onSaved: (value) {
                          details = value;
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                        child: Text(
                          localization
                              .text('Describe_what_makes_your_ad_unique'),
                        ),
                      ),
                      buildPrices(
                          localization.text('initial_price'),
                          "${_prefs.get("currency")}",
                          _intialPriceFoucsNode, (_) {
                        FocusScope.of(context)
                            .requestFocus(_lowPriceadditionFoucsNode);
                      }, (value) {
                        intialPrice = int.parse(value);
                      }),
                      buildPrices(
                          localization.text('Min_Increment'),
                          "${_prefs.get("currency")}",
                          _lowPriceadditionFoucsNode, (_) {
                        FocusScope.of(context)
                            .requestFocus(_durationAuctionFoucsNode);
                      }, (value) {
                        lowadditionValue = int.parse(value);
                      }),
                      buildPrices(
                          localization.text('period'),
                          localization.text('day'),
                          _durationAuctionFoucsNode,
                          (_) {}, (value) {
                        duration = int.parse(value);
                      }),
                      Directionality(
                        textDirection:
                            localization.currentLanguage.toString() == "en"
                                ? TextDirection.ltr
                                : TextDirection.ltr,
                        child: RaisedButton(
                          elevation: 0,
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          onPressed: () {
                            mainBottomSheet(context, categories, "تصنيف");
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            textDirection: TextDirection.rtl,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(categorylable,
                                        textAlign: TextAlign.end,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: "RB",
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ),
                                  Icon(
                                    Icons.category,
                                    size: 21,
                                    color: Color(0xff2542a6),
                                  )
                                ],
                              ),
                              Icon(
                                Icons.arrow_back_ios,
                                color: Colors.grey,
                              )
                            ],
                          ),
                        ),
                      ),
                      Directionality(
                        textDirection:
                            localization.currentLanguage.toString() == "en"
                                ? TextDirection.ltr
                                : TextDirection.ltr,
                        child: RaisedButton(
                          elevation: 0,
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          onPressed: () {
                            if (categoryId != null) {
                              mainBottomSheet(
                                  context, subCategories, "تصنيف فرعي");
                            } else {
                              return Fluttertoast.showToast(
                                  msg: localization.text(
                                      'You_must_choose_the_main_category_first'),
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            }
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            textDirection: TextDirection.rtl,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(subCategorylable,
                                        textAlign: TextAlign.end,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: "RB",
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ),
                                  Icon(
                                    Icons.category_rounded,
                                    size: 21,
                                    color: Color(0xff2542a6),
                                  )
                                ],
                              ),
                              Icon(
                                Icons.arrow_back_ios,
                                color: Colors.grey,
                              )
                            ],
                          ),
                        ),
                      ),
                      Directionality(
                        textDirection:
                            localization.currentLanguage.toString() == "en"
                                ? TextDirection.ltr
                                : TextDirection.ltr,
                        child: RaisedButton(
                          elevation: 0,
                          color: Colors.black45,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          onPressed: () {
                            mainBottomSheet(context, cities, "المدينة");
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            textDirection: TextDirection.rtl,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(lable,
                                        textAlign: TextAlign.end,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: "RB",
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white)),
                                  ),
                                  Icon(
                                    Icons.location_city,
                                    size: 21,
                                    color: Color(0xff2542a6),
                                  )
                                ],
                              ),
                              Icon(
                                Icons.arrow_back_ios,
                                color: Colors.white,
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Column(
                        crossAxisAlignment:
                            localization.currentLanguage.toString() == "en"
                                ? CrossAxisAlignment.end
                                : CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            Provider.of<GetTheUndertakingProvider>(context,
                                    listen: false)
                                .content,
                            // 'الموافقه علي الشروط والاحكام',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontFamily: 'cairo',
                            ),
                          ),
                          new Checkbox(
                              value: isChecked,
                              activeColor: Theme.of(context).primaryColor,
                              onChanged: (bool newValue) {
                                setState(() {
                                  isChecked = !isChecked;
                                });
                              }),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: Center(
                          child: _loadedSpinner
                              ? Center(
                                  child: CircularProgressIndicator(),
                                )
                              : RaisedButton(
                                  color: Colors.amberAccent,
                                  child: Text(
                                      localization.text('Publish_the_auction')),
                                  onPressed: _saveForm,
                                ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  mainBottomSheet(BuildContext context, List<CitiesModel> list, String title) {
    return showModalBottomSheet<dynamic>(
        isScrollControlled: false,
        backgroundColor: Colors.white,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        builder: (BuildContext bc) {
          return title == "تصنيف فرعي"
              ? FutureBuilder(
                  future:
                      Provider.of<SubCategoriesProvider>(context, listen: false)
                          .getSubCategories(categoryId, context),
                  builder: (context, snapShot) {
                    if (snapShot.connectionState == ConnectionState.waiting) {
                      return SpinKitThreeBounce(
                        color: Theme.of(context).primaryColor,
                        size: 22,
                      );
                    }

                    getSubCategoriesModel = snapShot.data;

                    if (getSubCategoriesModel.data != null &&
                        subCategories.isEmpty) {
                      for (int i = 0;
                          i < getSubCategoriesModel.data.length;
                          i++) {
                        subCategories.add(new CitiesModel(
                          id: getSubCategoriesModel.data[i].id,
                          label: getSubCategoriesModel.data[i].name,
                        ));
                      }
                    }

                    loaded = false;
                    print('subCategorieslength${subCategories.length}');
                    return loaded
                        ? SpinKitThreeBounce(
                            color: Theme.of(context).primaryColor,
                            size: 22,
                          )
                        : Directionality(
                            textDirection: TextDirection.rtl,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top: 20.0),
                                  child: ListView(children: <Widget>[
                                    Container(
                                      color: Colors.white,
                                      child: ListView.builder(
                                        shrinkWrap: true,
                                        physics: ScrollPhysics(),
                                        itemCount: list.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              ListTile(
                                                  onTap: () {
                                                    print(list[index].id);
                                                    Navigator.pop(context);

                                                    setState(() {
                                                      if (title == "المدينة") {
                                                        cityId = list[index]
                                                            .id
                                                            .toString();
                                                        print("citeid $cityId");
                                                        lable =
                                                            list[index].label;
                                                      } else if (title ==
                                                          "تصنيف") {
                                                        categoryId =
                                                            list[index].id;
                                                        print(
                                                            "catrgory $categoryId");
                                                        categorylable =
                                                            list[index].label;
                                                      } else {
                                                        subCategoryId =
                                                            list[index]
                                                                .id
                                                                .toString();
                                                        print(
                                                            "catrgory $categoryId");
                                                        subCategorylable =
                                                            list[index].label;
                                                        subCategories.clear();
                                                      }
                                                    });
                                                  },
                                                  title: Text(
                                                    list[index].label,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  )),
                                              Divider(
                                                height: 1,
                                                color: Colors.grey,
                                              )
                                            ],
                                          );
                                        },
                                      ),
                                    ),
                                  ]),
                                ),
                              ],
                            ),
                          );
                  },
                )
              : Directionality(
                  textDirection: TextDirection.rtl,
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: ListView(children: <Widget>[
                          Container(
                            color: Colors.white,
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: ScrollPhysics(),
                              itemCount: list.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    ListTile(
                                        onTap: () {
                                          print(list[index].id);
                                          Navigator.pop(context);
                                          setState(() {
                                            if (title == "المدينة") {
                                              cityId =
                                                  list[index].id.toString();
                                              print("cityid $cityId");
                                              lable = list[index].label;
                                            } else if (title == "تصنيف") {
                                              categoryId = list[index].id;
                                              print("catrgory $categoryId");
                                              categorylable = list[index].label;

                                              print(
                                                  "catrgorylabel $categorylable");
                                              subCategories.clear();
                                            }
                                          });
                                        },
                                        title: Text(
                                          list[index].label,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        )),
                                    Divider(
                                      height: 1,
                                      color: Colors.grey,
                                    )
                                  ],
                                );
                              },
                            ),
                          ),
                        ]),
                      ),
                    ],
                  ),
                );
        });
  }
}

class CitiesModel {
  int id;
  String label;

  CitiesModel({
    this.id,
    this.label,
  });
}
