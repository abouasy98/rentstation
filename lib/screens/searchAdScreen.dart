import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/models/get/getAdsModel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../utils/universal_variables.dart';

import '../widgets/searchAd.dart';

class SearchAdsScreen extends StatefulWidget {
  static const routedname = '/SearchScreen';
  final SharedPreferences pref;
  final GetAdsModel getAddsModel;

  SearchAdsScreen({this.getAddsModel,this.pref});
  @override
  _SearchAdsScreenState createState() => _SearchAdsScreenState();
}

class _SearchAdsScreenState extends State<SearchAdsScreen> {
  String query = "";
  TextEditingController _searchController = TextEditingController();
  searchAppBar(BuildContext context) {
    return AppBar(
      leading: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () => Navigator.pop(context),
      ),
      elevation: 0,
      bottom: PreferredSize(
        preferredSize: const Size.fromHeight(kToolbarHeight + 20),
        child: Padding(
          padding: EdgeInsets.only(left: 20),
          child: TextField(
            keyboardType: TextInputType.name,
            textCapitalization: TextCapitalization.sentences,
            textInputAction: TextInputAction.search,
            controller: _searchController,
            onChanged: (val) {
              setState(() {
                query = val;
              });
            },
            cursorColor: UniversalVariables.blackColor,
            autofocus: true,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.white,
              fontSize: 35,
            ),
            decoration: InputDecoration(
              suffixIcon: IconButton(
                icon: Icon(Icons.close, color: Colors.white),
                onPressed: () {
                  WidgetsBinding.instance
                      .addPostFrameCallback((_) => _searchController.clear());
                },
              ),
              border: InputBorder.none,
              hintText: localization.text('search_by_user_or_title'),
              hintStyle: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Color(0x88ffffff),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: searchAppBar(context),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: SearchAdsIteams(
         // ads: widget.getAddsModel,
          query: query,
         
        ),
      ),
    );
  }
}
