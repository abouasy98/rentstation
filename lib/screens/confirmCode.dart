import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/auth/confirmResetCodeProvider.dart';
import 'package:rentstation/provider/auth/phoneVerificationProvider.dart';
import 'package:rentstation/provider/auth/resendCode.dart';
import 'package:rentstation/provider/changeData/changeEmailCodeProvider.dart';
import 'package:rentstation/provider/changeData/changePhoneCodeProvider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:slide_countdown_clock/slide_countdown_clock.dart';

class ConfirmCode extends StatefulWidget {
    static const String routeName = '/ConfirmCode';
  final String phoneNumber;
  final String email;
  final int stateOfVerificationCode;

  const ConfirmCode(
      {Key key, this.phoneNumber, this.email, this.stateOfVerificationCode})
      : super(key: key);

  @override
  _ConfirmCodeState createState() => _ConfirmCodeState();
}

class _ConfirmCodeState extends State<ConfirmCode> {
  SharedPreferences _prefs;
  _getShared() async {
    var _instance = await SharedPreferences.getInstance();
    setState(() {
      _prefs = _instance;
    });
  }

  @override
  void initState() {
    _getShared();
    super.initState();
  }

  bool resend = false;
  int timer = 1;
  @override
  Widget build(BuildContext context) {
    var register =
        Provider.of<PhoneVerificationProvider>(context, listen: false);
    var confirmRessetCode =
        Provider.of<ConfirmResetCodeProvider>(context, listen: false);
    var changePhone =
        Provider.of<ChangePhoneCodeProvider>(context, listen: false);
    var changeEmail =
        Provider.of<ChangeEmailCodeProvider>(context, listen: false);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: <Widget>[
          ListView(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            children: <Widget>[
              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height * .9,
                  width: MediaQuery.of(context).size.width - 100,
                  alignment: Alignment.center,
                  child: Form(
                    autovalidate: true,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                        ),
                        Center(
                            child: Text(
                          localization.text("sign_up"),
                          // "تسجيل جديد",
                          style: TextStyle(color: Colors.black87, fontSize: 20),
                        )),
                        SizedBox(
                          height: 10,
                        ),
                        Center(
                            child: Text(
                          localization.text("enter_code"),
                          // "ادخل كود التفعيل المرسل اليك",
                          style: TextStyle(color: Colors.black87, fontSize: 13),
                          textAlign: TextAlign.center,
                        )),
                        SizedBox(
                          height: 10,
                        ),
                        if (widget.stateOfVerificationCode == 1)
                          Center(
                              child: Text(
                            "${register.phone}",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 13),
                            textAlign: TextAlign.center,
                          )),
                        if (widget.stateOfVerificationCode == 2)
                          Center(
                              child: Text(
                            "${confirmRessetCode.phone}",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 13),
                            textAlign: TextAlign.center,
                          )),
                        if (widget.stateOfVerificationCode != 2 &&
                            widget.stateOfVerificationCode != 1)
                          Center(
                              child: Text(
                            "${changePhone.phone}",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 13),
                            textAlign: TextAlign.center,
                          )),
                        SizedBox(
                          height: 40,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 30),
                          child: Center(
                            child: PinCodeTextField(
                              appContext: context,
                              length: 4,
                              backgroundColor: Color(
                                  0x00000000), //Theme.of(context).accentColor,
                              textStyle: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                              pinTheme: PinTheme(
                                activeColor: Theme.of(context).primaryColor,
                                selectedColor: Theme.of(context).accentColor,
                                inactiveColor: Theme.of(context).accentColor,
                              ),
                              onChanged: (code) {
                                if (widget.stateOfVerificationCode == 1)
                                  register.code = code;
                                else if (widget.stateOfVerificationCode == 2)
                                  confirmRessetCode.code = code;
                                else if (widget.stateOfVerificationCode == 3)
                                  Provider.of<ChangePhoneCodeProvider>(context,
                                          listen: false)
                                      .code = code;
                                else {
                                  Provider.of<ChangeEmailCodeProvider>(context,
                                          listen: false)
                                      .code = code;
                                }
                              },
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              keyboardType: TextInputType.number,

                              autoFocus: true,
                              onCompleted: (String value) {
                                if (widget.stateOfVerificationCode == 1)
                                  register.phoneVerification(context);
                                else if (widget.stateOfVerificationCode == 2)
                                  confirmRessetCode.confirmResetCode(context);
                                else if (widget.stateOfVerificationCode == 3) {
                                  changePhone.changePhoneCode(
                                      _prefs.get("api_token"), context);
                                } else {
                                  changeEmail.changeEmailCode(
                                      _prefs.get("api_token"), context);
                                }
                              },
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        //   if(widget.stateOfVerificationCode!=2)
                        Visibility(
                          visible: resend,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              InkWell(
                                  child: new Text(
                                    localization.text("resend_code"),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        fontSize: 13,
                                        fontFamily: 'cairo',
                                        fontWeight: FontWeight.w400),
                                  ),
                                  onTap: () {
                                    if (widget.stateOfVerificationCode == 1) {
                                      Provider.of<ResendCodeProvider>(context,
                                              listen: false)
                                          .email = register.email;
                                      Provider.of<ResendCodeProvider>(context,
                                              listen: false)
                                          .email = register.email;
                                      Provider.of<ResendCodeProvider>(context,
                                              listen: false)
                                          .phone = register.phone;
                                    } else if (widget.stateOfVerificationCode ==
                                        2) {
                                      Provider.of<ResendCodeProvider>(context,
                                              listen: false)
                                          .email = confirmRessetCode.email;
                                      Provider.of<ResendCodeProvider>(context,
                                              listen: false)
                                          .phone = confirmRessetCode.phone;
                                    } else {
                                      Provider.of<ResendCodeProvider>(context,
                                              listen: false)
                                          .email = changeEmail.email;
                                      Provider.of<ResendCodeProvider>(context,
                                              listen: false)
                                          .phone = changePhone.phone;
                                    }

                                    Provider.of<ResendCodeProvider>(context,
                                            listen: false)
                                        .resendCode(context);
                                    setState(() {
                                      resend = false;
                                    });
                                    // resendCodeBloc.add(Click());
                                  }),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                localization.text("send_code"),
                                // 'لم يصلك كود التفعيل؟',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 12,
                                    fontFamily: 'cairo',
                                    fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                        resend == true
                            ? Container()
                            : Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                textDirection: TextDirection.rtl,
                                children: <Widget>[
                                  Text(localization.text("confirm_code")
                                      // "سيصل الكود خلال"
                                      ),
                                  SizedBox(height: 10),
                                  Center(
                                    child: SlideCountdownClock(
                                      duration: Duration(minutes: timer),
                                      slideDirection: SlideDirection.Down,
                                      tightLabel: true,
                                      onDone: () {
                                        setState(() {
                                          resend = true;
                                        });
                                      },
                                      separator: ":",
                                      textStyle: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                        // SizedBox(height: 20),
                        // SignInButton(
                        //   txtColor: Colors.white,
                        //   onPressSignIn: () async{
                        //     if (widget.stateOfVerificationCode == 1) {
                        //       Provider.of<ResendCodeProvider>(context,
                        //               listen: false)
                        //           .email = register.email;
                        //       Provider.of<ResendCodeProvider>(context,
                        //               listen: false)
                        //           .email = register.email;
                        //       Provider.of<ResendCodeProvider>(context,
                        //               listen: false)
                        //           .phone = register.phone;
                        //     } else if (widget.stateOfVerificationCode == 2) {
                        //       Provider.of<ResendCodeProvider>(context,
                        //               listen: false)
                        //           .email = confirmRessetCode.email;
                        //       Provider.of<ResendCodeProvider>(context,
                        //               listen: false)
                        //           .phone = confirmRessetCode.phone;
                        //     } else {
                        //       Provider.of<ResendCodeProvider>(context,
                        //               listen: false)
                        //           .email = changeEmail.email;

                        //       Provider.of<ResendCodeProvider>(context,
                        //               listen: false)
                        //           .phone = changePhone.phone;
                        //     }

                        //    await Provider.of<ResendCodeProvider>(context,
                        //             listen: false)
                        //         .resendCode(context);
                        //     setState(() {
                        //       resend = false;
                        //     });
                        //   },
                        //   btnWidth: MediaQuery.of(context).size.width,
                        //   btnHeight: MediaQuery.of(context).size.height * .07,
                        //   btnColor: Theme.of(context).primaryColor,
                        //   buttonText: 'تفعيل',
                        // ),
                        SizedBox(height: 20),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            top: 40,
            left: 20,
            child: InkWell(
                onTap: () => Navigator.pop(context),
                child: Icon(Icons.arrow_back, color: Colors.black87)),
          ),
        ],
      ),
    );
  }
}
