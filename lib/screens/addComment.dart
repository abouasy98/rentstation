import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/get/adCommentModel.dart';
import 'package:rentstation/provider/get/getCommentProvider.dart';
import 'package:rentstation/provider/post/addCommenProvider.dart';
import 'package:rentstation/provider/post/addReplyProvider.dart';
import 'package:rentstation/widgets/commentCard.dart';

class AddComment extends StatefulWidget {
  final int id;

  const AddComment({Key key, this.id}) : super(key: key);

  @override
  _AddCommentState createState() => _AddCommentState();
}

class _AddCommentState extends State<AddComment> {
  GetAdComment _model;
  _getShared(BuildContext context) async {
    setState(() {
      Provider.of<GetCommentProvider>(context, listen: false)
          .getCommentAds(
        Provider.of<SharedPref>(context, listen: false).token,
        context,
        widget.id,
      )
          .then((res) {
        setState(() {
          _model = res;
        });
      });
    });
  }

  @override
  void initState() {
    super.initState();
  }

  String comment;

  bool _load = false;
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    _getShared(context);
  }

  @override
  Widget build(BuildContext context) {
    print(
        'rplies=${Provider.of<AddReplyProvider>(context, listen: false).reply.length}');
    return Scaffold(
      appBar: AppBar(
        title: Text(localization.text('comments')),
        centerTitle: true,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context, true);
          },
          child: Icon(Icons.arrow_back_ios),
        ),
      ),
      body: _model == null
          ? Center(
              child: SpinKitThreeBounce(
                size: 25,
                color: Theme.of(context).primaryColor,
              ),
            )
          : Container(
              height: MediaQuery.of(context).size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  if (_model.data == null || _model.data.length <= 0)
                    Container(
                      height: MediaQuery.of(context).size.height * 0.4,
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        localization.text('no_results'),
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  if (_model.data != null || _model.data.length > 0)
                    Flexible(
                      child: ListView.builder(
                        physics: ScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: _model.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return CommentCard(
                            index: index,
                            idUser: _model.data[index].userId,
                            comment: _model.data[index].comment,
                            name: _model.data[index].user,
                            data: intl.DateFormat.yMd()
                                .format(_model.data[index].createdAt),
                            idComment: _model.data[index].id,
                            img: _model.data[index].userPhoto,
                            replays: _model.data[index].replays,
                            idAd: _model.data[index].adId,
                          );
                        },
                      ),
                    ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 4.0),
                    decoration: BoxDecoration(color: Colors.grey[200]),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                          child: Directionality(
                            //   textDirection: p.TextDirection.rtl,
                            textDirection: TextDirection.rtl,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextField(
                                  keyboardType: TextInputType.multiline,
                                  maxLines: null,
                                  maxLength: 200,
                                  decoration: InputDecoration.collapsed(
                                      hintText: localization.text('comment')),
                                  onChanged: (value) {
                                    setState(() {
                                      comment = value;
                                    });
                                  }),
                            ),
                          ),
                        ),
                        _load == true
                            ? Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  width: MediaQuery.of(context).size.width / 10,
                                  child: Center(
                                    child: Container(
                                      width: 20,
                                      height: 20,
                                      child: Center(
                                        child: CircularProgressIndicator(),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            : Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  width: MediaQuery.of(context).size.width / 10,
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        _load = true;
                                      });
                                      _saveForm();
                                    },
                                    child: Text(
                                      localization.text('send'),
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Theme.of(context).primaryColor,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              )
                        // IconButton(
                        //   icon: Icon(
                        //     Icons.send,
                        //     color: Theme.of(context).primaryColor,
                        //   ),
                        //   onPressed: () {
                        //  print("${comment}.................");
                        //      print("${widget.newsId}.................");
                        //     _saveForm();

                        //   },
                        // ),
                      ],
                    ),
                  )
                ],
              ),
            ),
    );
  }

  _saveForm() {
    Provider.of<AddCommentProvider>(context, listen: false)
        .addComment(Provider.of<SharedPref>(context, listen: false).token,
            comment, widget.id, context)
        .then((res) {
      setState(() {
        _load = false;
        comment = null;
      });
      switch (res.code) {
        case 200:
          print("done");
          setState(() {
            _model = null;
          });
          _getShared(context);
          break;
        case 400:
          print("data don't match");
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  backgroundColor: Colors.white,
                  elevation: 3,
                  contentPadding: EdgeInsets.all(5),
                  children: <Widget>[
                    Text(
                      res.error[0].value,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black, fontSize: 20),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: MaterialButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        elevation: 3,
                        height: 45,
                        color: Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        child: Text(
                          localization.text('ok'),
                          style: TextStyle(fontSize: 17, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                );
              });
          break;
        case 422:
          print(res.error[0].value);
          setState(() {
            //  _load = false;
          });
          break;
        default:
          setState(() {
            //_load = false;
          });
          print('error data');
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  backgroundColor: Colors.white,
                  elevation: 3,
                  contentPadding: EdgeInsets.all(5),
                  children: <Widget>[
                    Text(
                      localization.text('Please_enter_the_data_correctly'),
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black, fontSize: 20),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: MaterialButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        elevation: 3,
                        height: 45,
                        color: Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        child: Text(
                          localization.text('ok'),
                          style: TextStyle(fontSize: 17, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                );
              });
      }
    });
  }
}
