import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/contuctUsProvider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.dart';

class ContuctUs extends StatefulWidget {
  @override
  _ContuctUsState createState() => _ContuctUsState();
}

class _ContuctUsState extends State<ContuctUs> {
  _launchURL() async {
    const url = 'https://tqnee.com.sa';
    launch(url);
  }

  @override
  void initState() {
    super.initState();
    Provider.of<ContuctUsProvider>(context, listen: false).getContuct(context);
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: localization.currentLanguage.toString() == "en"
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: Text(
            localization.text('contact_us'),
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
          ),
        ),
        //   floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
        floatingActionButton: Provider.of<ContuctUsProvider>(
                  context,
                ).phone ==
                null
            ? Container()
            : SpeedDial(
                /// both default to 16
                marginEnd: 18,
                marginBottom: 20,
                // animatedIcon: AnimatedIcons.menu_close,
                // animatedIconTheme: IconThemeData(size: 22.0),
                /// This is ignored if animatedIcon is non null
                icon: Icons.call,
                activeIcon: Icons.call,
                // iconTheme: IconThemeData(color: Colors.grey[50], size: 30),
                /// The label of the main button.
                // label: Text("Open Speed Dial"),
                /// The active label of the main button, Defaults to label if not specified.
                // activeLabel: Text("Close Speed Dial"),
                /// Transition Builder between label and activeLabel, defaults to FadeTransition.
                // labelTransitionBuilder: (widget, animation) => ScaleTransition(scale: animation,child: widget),
                /// The below button size defaults to 56 itself, its the FAB size + It also affects relative padding and other elements
                buttonSize: 56.0,
                visible: true,

                /// If true user is forced to close dial manually
                /// by tapping main button and overlay is not rendered.
                closeManually: false,

                /// If true overlay will render no matter what.
                renderOverlay: false,
                curve: Curves.bounceIn,
                overlayColor: Colors.black,
                overlayOpacity: 0.5,
                onOpen: () => print('OPENING DIAL'),
                onClose: () => print('DIAL CLOSED'),
                tooltip: 'Speed Dial',
                heroTag: 'speed-dial-hero-tag',
                backgroundColor: Colors.blue,
                foregroundColor: Colors.white,
                elevation: 8.0,
                shape: CircleBorder(),
                // orientation: SpeedDialOrientation.Up,
                // childMarginBottom: 2,
                // childMarginTop: 2,
                children: [
                  SpeedDialChild(
                    child: Icon(
                      Icons.phone_iphone,
                      color: Colors.blue,
                    ),
                    backgroundColor: Colors.white,
                    label: Provider.of<ContuctUsProvider>(
                      context,
                    ).phone,
                    labelStyle: TextStyle(fontSize: 18.0),
                    onTap: () {
                      UrlLauncher.launch('tel: ${Provider.of<ContuctUsProvider>(
                        context,
                      ).phone}');
                    },
                    onLongPress: () => print('FIRST CHILD LONG PRESS'),
                  ),
                  SpeedDialChild(
                    child: Icon(
                      FontAwesomeIcons.whatsapp,
                      color: Colors.green,
                    ),
                    backgroundColor: Colors.white,
                    label: localization.text('contact_us'),
                    labelStyle: TextStyle(fontSize: 18.0),
                    onTap: _sendWhatsApp,
                    onLongPress: () => print('SECOND CHILD LONG PRESS'),
                  ),
                  SpeedDialChild(
                    child: Icon(
                      Icons.copy,
                      color: Colors.blue,
                    ),
                    backgroundColor: Colors.white,
                    label: localization.text("copy_a_number"),
                    labelStyle: TextStyle(fontSize: 18.0),
                    onTap: () {
                      Clipboard.setData(new ClipboardData(
                          text: Provider.of<ContuctUsProvider>(
                        context,
                      ).phone));
                    },
                    onLongPress: () => print('SECOND CHILD LONG PRESS'),
                  ),
                ],
              ),
        body: Provider.of<ContuctUsProvider>(
                  context,
                ).content ==
                null
            ? SpinKitThreeBounce(
                color: Theme.of(context).primaryColor,
                size: 22,
              )
            : Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                      bottom: 40.0,
                    ),
                    child: ListView(
                      children: <Widget>[
                        Directionality(
                          textDirection: TextDirection.rtl,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                right: 8.0, left: 20, top: 8),
                            child: Html(
                              data:
                                  "${Provider.of<ContuctUsProvider>(context, listen: false).content}",
                              padding: EdgeInsets.all(8.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      color: Colors.white,
                      child: InkWell(
                        onTap: _launchURL,
                        child: Padding(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              localization.text('copy_rights'),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.blueAccent, fontSize: 12),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  _sendWhatsApp() async {
    var url =
        "https://wa.me/${Provider.of<ContuctUsProvider>(context, listen: false).phone}";
    await canLaunch(url) ? launch(url) : print('No WhatsAPP');
  }
}
