import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttericon/font_awesome5_icons.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/provider/get/GetAppLogoProvider.dart';
import 'package:rentstation/screens/AuctionDetailsScreen.dart';
import 'package:rentstation/screens/AuctionScreen.dart';
import 'package:rentstation/screens/adDetails.dart';
import 'package:rentstation/screens/EditData/editProfile.dart';
import 'package:provider/provider.dart';
import 'home.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'chatScreen.dart';
import './notifiactions.dart';
import 'drawer.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';

class TabScreen extends StatefulWidget {
  final int index;
  TabScreen({this.index});
  @override
  _TabScreenState createState() => _TabScreenState();
}

class _TabScreenState extends State<TabScreen> {
  GlobalKey<ScaffoldState> _keyDrawer = GlobalKey<ScaffoldState>();
  // int _selectedpage = 2;

  PersistentTabController _controller;
  bool _hideNavBar;
  List<Map<String, Object>> pages;
  SharedPreferences _preferences;
  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        title: localization.text('profile'),
        icon: Icon(
          Icons.person_outline,
        ),
        activeColorPrimary: Colors.blue,
        inactiveColorPrimary: Colors.grey,
      ),
      PersistentBottomNavBarItem(
        title: localization.text('auctions'),
        icon: FaIcon(FontAwesome.hammer),
        activeColorPrimary: Colors.blue,
        inactiveColorPrimary: Colors.grey,
      ),
      PersistentBottomNavBarItem(
        title: localization.text('centers'),
        icon: Icon(
          Icons.home_outlined,
          color: Colors.white70,
          size: 35,
        ),
        activeColorPrimary: Colors.blue,
        inactiveColorPrimary: Colors.grey,
      ),
      PersistentBottomNavBarItem(
        title: localization.text('notifications'),
        icon: Icon(Icons.notification_important_outlined),
        activeColorPrimary: Colors.blue,
        inactiveColorPrimary: Colors.grey,
      ),
      PersistentBottomNavBarItem(
        title: localization.text('my_chat'),
        icon: Icon(Icons.message),
        activeColorPrimary: Colors.blue,
        inactiveColorPrimary: Colors.grey,
        onPressed: (_) {
          Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
              builder: (BuildContext context) {
                return TabScreen(
                  index: 4,
                );
              },
            ),
            (_) => false,
          );
        },
      ),
    ];
  }

  BuildContext testContext;
  List<Widget> _buildScreens() {
    return [
      EditProfile(),
      AuctionScreen(),
      Home(
        auctions: false,
      ),
      Notifaions(),
      ChatScreen()
    ];
  }

  getShared() async {}

  @override
  void initState() {
    super.initState();
    _controller = PersistentTabController(
        initialIndex: widget.index != null ? widget.index : 2);
    _hideNavBar = false;
    _initDynamicLinks();
  }

  // void selectedpage(int index) {
  //   setState(() {
  //     _selectedpage = index;
  //   });
  // }

  bool isInit = true;
  @override
  void didChangeDependencies() async {
    if (isInit) {
      _preferences = await SharedPreferences.getInstance();
      await Provider.of<SharedPref>(context, listen: false)
          .getSharedHelper(_preferences);

      setState(() {
        isInit = false;
      });
    }
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _controller.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  void _initDynamicLinks() async {
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri deepLink = data?.link;

    if (deepLink != null) {
      print(' ---------- ' + deepLink.path);
      String _state = deepLink.path.substring(1);
      print('state >>>>> ' + _state);

      String _value = _state.substring(2);
      print('value >>>>> ' + _value);
      if (_state.contains("0/")) {
        print('state >>>>>>>>>>>>>> "Shop"' + _state);
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => AuctionsDetails(
                  key: UniqueKey(),
                  id: int.parse(_value),
                )));
      } else {
        print('state >>>>>>>>>>>>>>>>> "product" ' + _state);
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => AdDetails(
                  key: UniqueKey(),
                  id: int.parse(_value),
                )));
      }
    } else {}

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      final Uri deepLink = dynamicLink?.link;
      if (deepLink != null) {
        print(' ---------- ' + deepLink.path);
        String _state = deepLink.path.substring(1);
        print('state >>>>> ' + _state);

        String _value = _state.substring(2);
        print('value >>>>> ' + _value);
        if (_state.contains("0/")) {
          print('state >>>>>>>>>>>>>> "Shop"' + _state);
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => AuctionsDetails(
                    key: UniqueKey(),
                    id: int.parse(_value),
                  )));
        } else {
          print('state >>>>>>>>>>>>>>>>> "product" ' + _state);
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => AdDetails(
                    key: UniqueKey(),
                    id: int.parse(_value),
                  )));
        }
      } else {}
    }, onError: (OnLinkErrorException e) async {
      print('onLinkError');
      print(e.message);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: localization.currentLanguage.toString() == "en"
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        key: _keyDrawer,
        drawer: Mydrawrer(),
        appBar: AppBar(
          backgroundColor: Colors.blueAccent,
          flexibleSpace: new ClipRect(
            child: new Container(
              child: new BackdropFilter(
                filter: new ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
                child: new Container(
                  decoration: new BoxDecoration(
                    color: Colors.black.withOpacity(0.5),
                  ),
                ),
              ),
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                      image: new AssetImage(
                        "images/08.jpg",
                      ),
                      fit: BoxFit.fitWidth)),
            ),
          ),
          leading: IconButton(
            onPressed: () {
              _keyDrawer.currentState.openDrawer();
            },
            icon: Icon(FontAwesome5.align_right),
          ),
          title: Center(
              child: Text(
            Provider.of<GetAppLogoProvider>(context, listen: false).name,
            style: TextStyle(fontWeight: FontWeight.bold),
          )),
        ),
        body: isInit
            ? SpinKitThreeBounce(
                color: Theme.of(context).primaryColor,
                size: 22,
              )
            : PersistentTabView(
                context,
                controller: _controller,

                screens: _buildScreens(),
                items: _navBarsItems(),
                confineInSafeArea: true,

                backgroundColor: Colors.white,
                handleAndroidBackButtonPress: true,
                resizeToAvoidBottomInset: true,
                stateManagement: true,

                navBarHeight: MediaQuery.of(context).viewInsets.bottom > 0
                    ? 0.0
                    : kBottomNavigationBarHeight,
                hideNavigationBarWhenKeyboardShows: true,
                margin: EdgeInsets.all(0.0),

                popActionScreens: PopActionScreensType.all,
                bottomScreenMargin: MediaQuery.of(context).viewInsets.bottom > 0
                    ? 0.0
                    : kBottomNavigationBarHeight,
                padding: const NavBarPadding.all(null),
                // routeAndNavigatorSettings: RouteAndNavigatorSettings(
                //   initialRoute: '/',
                // ),
                // onWillPop: () async {
                //   await showDialog(
                //     context: context,
                //     useSafeArea: true,
                //     builder: (context) => Container(
                //       height: 50.0,
                //       width: 50.0,
                //       color: Colors.white,
                //       child: RaisedButton(
                //         child: Text("Close"),
                //         onPressed: () {
                //           Navigator.pop(context);
                //         },
                //       ),
                //     ),
                //   );
                //   return false;
                // },
                selectedTabScreenContext: (context) {
                  testContext = context;
                },
                hideNavigationBar: _hideNavBar,
                decoration: NavBarDecoration(
                  colorBehindNavBar: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                ),
                popAllScreensOnTapOfSelectedTab: true,
                itemAnimationProperties: ItemAnimationProperties(
                  duration: Duration(milliseconds: 400),
                  curve: Curves.ease,
                ),
                screenTransitionAnimation: ScreenTransitionAnimation(
                  animateTabTransition: true,
                  curve: Curves.ease,
                  duration: Duration(milliseconds: 200),
                ),
                navBarStyle: NavBarStyle
                    .style15, // Choose the nav bar style with this property
              ),
        resizeToAvoidBottomInset: false,
        //  resizeToAvoidBottomPadding: false,
        floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }
}
