import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/get/getmyArchieveAds.dart';
import 'package:rentstation/widgets/arhievedItem.dart';

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Archieve extends StatelessWidget {
  final SharedPreferences _prefs;
  Archieve(this._prefs);
  bool a7a = true;

  @override
  Widget build(BuildContext context) {
    return  FutureBuilder(
              future:
                  Provider.of<GetmyArchieveAdsProvider>(context, listen: false)
                      .getMyArchieveAds(_prefs.getString('api_token'),context),
              builder: (context, snapShot) {
                if (snapShot.connectionState == ConnectionState.waiting) {
                  return Center(child: Text(localization.text('waiting')));
                } else if (!snapShot.hasData) {
                  return Center(child: Text(localization.text('no_posts')));
                } else {
                  return Consumer<GetmyArchieveAdsProvider>(
                      builder: (context, archieveData, _) {
                    if (archieveData.archieve.length <= 0) {
                      return Center(child: Text(localization.text('no_posts')));
                    }
                    return ListView.builder(
                          primary: false,
                          shrinkWrap: true,
                          itemCount: archieveData.archieve.length,
                          itemBuilder: (BuildContext context, int i) =>
                              ArchievedItem(
                                city: archieveData.archieve[i].city,
                                createdAt: archieveData.archieve[i].createdAt,
                                id: archieveData.archieve[i].id,
                                photo: archieveData.archieve[i].photo[0].photo,
                                prefs: _prefs,
                                price: archieveData.archieve[i].price,
                                userPhoto: archieveData.archieve[i].providerPhoto,
                    
                                title: archieveData.archieve[i].providertitle,
                                userName: archieveData.archieve[i].userName,
                              ),
                    );
                  });
                }
              }
    );
  }
}
