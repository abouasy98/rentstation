import 'dart:async';

import 'package:flutter/material.dart';


import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:rentstation/screens/EditData/editProfile.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  bool switchValue = false;
  bool stateOfLogOut = false;
  //final _repo = Repository();
 // String _phone = '';

  void onChangedSwitch(value) {
    switchValue = value;
  }

//  _launchURLOs() async {
//    const url = "https://apps.apple.com/us/app/مخدووم/id1498378815?ls=1";
//    launch(url);
//  }
//  _launchURLAndroid() async {
//    const url = "https://play.google.com/store/apps/details?id=com.tqnee.makhdoum";
//    launch(url);
//  }

  String photo;

  String name = "";

  int userType = 0;
  bool changeUser = false;

  //String _deviceToken = 'DEFULT_TOKEN';

  bool loading = true;

  Future<Timer> _load() async {
    return Timer(Duration(seconds: 1), () {
      setState(() {
        loading = false;
      });
    });
  }

  @override
  void initState() {
    _load();

    // _repo.getApp('token').then((res) {
    //   if (res.code == 200) {
    //     setState(() {
    //       _phone = res.data.phone;
    //     });
    //   }
    // });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // final appbar = AppBar(
    //   elevation: 0.0,
    //   centerTitle: true,
    //   title: Text('حسابي', style: Theme.of(context).textTheme.button),
    //   iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
    // );
    return Scaffold(
      //    appBar: appbar,
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      name,
                      style: TextStyle(
                          fontSize: 20, color: Theme.of(context).primaryColor),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "مرحبا بك",
                      style: TextStyle(
                          fontSize: 20, color: Theme.of(context).primaryColor),
                    )
                  ],
                ),
                InkWell(
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => EditProfile(),
                    ),
                  ),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Icon(Icons.arrow_back),
                                      Text(
                                        "تعديل",
                                        style: TextStyle(
                                            fontSize: 16, fontFamily: "cairo"),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    "البيانات الشخصية",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 16, fontFamily: "cairo"),
                                  ),
                                ].reversed.toList(),
                              ),
                              Divider()
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Material(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20))),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Icon(
                              Icons.person,
                              color: Colors.white,
                            ),
                          ),
                          color: Color.fromRGBO(1, 91, 102, 1),
                        ),
                      ),
                    ].reversed.toList(),
                  ),
                ),
          
                Visibility(
                  visible: changeUser == false,
                  child: Row(
                    children: [
                      Expanded(
                          child: Column(
                        children: [
                          //AvailableSwitch(),
                          Divider()
                        ],
                      )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Material(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20))),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Icon(
                              Icons.event_available,
                              color: Colors.white,
                            ),
                          ),
                          color: Colors.grey,
                        ),
                      ),
                    ].reversed.toList(),
                  ),
                ),
                // InkWell(
                //   // onTap: () => Navigator.of(context).push(
                //   //     MaterialPageRoute(builder: (context) => EditPassword())),
                //   child: Row(
                //     children: <Widget>[
                //       Expanded(
                //         child: Padding(
                //           padding: const EdgeInsets.all(8.0),
                //           child: Column(
                //             children: <Widget>[
                //               Row(
                //                 mainAxisAlignment:
                //                     MainAxisAlignment.spaceBetween,
                //                 children: <Widget>[
                //                   Row(
                //                     children: <Widget>[
                //                       Icon(Icons.arrow_back),
                //                       Text("تغير",
                //                           style: TextStyle(
                //                               fontSize: 16,
                //                               fontFamily: "cairo")),
                //                     ],
                //                   ),
                //                   Text(
                //                     "كلمة المرور",
                //                     textAlign: TextAlign.start,
                //                     style: TextStyle(
                //                         fontSize: 16, fontFamily: "cairo"),
                //                   ),
                //                 ].reversed.toList(),
                //               ),
                //               Divider()
                //             ],
                //           ),
                //         ),
                //       ),
                //       Padding(
                //         padding: const EdgeInsets.all(8.0),
                //         child: Material(
                //           shape: RoundedRectangleBorder(
                //               borderRadius:
                //                   BorderRadius.all(Radius.circular(20))),
                //           child: Padding(
                //             padding: const EdgeInsets.all(4.0),
                //             child: Icon(
                //               Icons.vpn_key,
                //               color: Colors.white,
                //             ),
                //           ),
                //           color: Colors.green,
                //         ),
                //       ),
                //     ].reversed.toList(),
                //   ),
                // ),
                Visibility(
                  visible: changeUser == true ? false : true,
                  child: InkWell(
                    // onTap: () => Navigator.of(context).push(
                    //     MaterialPageRoute(builder: (context) => MyProfile())),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      'صفحتي كمزود خدمة',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 16, fontFamily: "cairo"),
                                    ),
                                  ],
                                ),
                                Divider()
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Material(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Icon(
                                Icons.person_pin,
                                color: Colors.white,
                              ),
                            ),
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                      ].reversed.toList(),
                    ),
                  ),
                ),
                // InkWell(
                //   // onTap: () => Navigator.of(context).push(
                //   //     MaterialPageRoute(builder: (context) => Favorite())),
                //   child: Row(
                //     children: <Widget>[
                //       Expanded(
                //         child: Padding(
                //           padding: const EdgeInsets.all(8.0),
                //           child: Column(
                //             children: <Widget>[
                //               Row(
                //                 mainAxisAlignment: MainAxisAlignment.start,
                //                 children: <Widget>[
                //                   Text(
                //                     "المفضلة",
                //                     textAlign: TextAlign.start,
                //                     style: TextStyle(
                //                         fontSize: 16, fontFamily: "cairo"),
                //                   ),
                //                 ],
                //               ),
                //               Divider()
                //             ],
                //           ),
                //         ),
                //       ),
                //       Padding(
                //         padding: const EdgeInsets.all(8.0),
                //         child: Material(
                //           shape: RoundedRectangleBorder(
                //               borderRadius:
                //                   BorderRadius.all(Radius.circular(20))),
                //           child: Padding(
                //             padding: const EdgeInsets.all(4.0),
                //             child: Icon(
                //               Icons.favorite,
                //               color: Colors.white,
                //             ),
                //           ),
                //           color: Colors.red,
                //         ),
                //       ),
                //     ].reversed.toList(),
                //   ),
                // ),
                InkWell(
                  // onTap: () => Navigator.of(context).push(
                  //     MaterialPageRoute(builder: (context) => MyDatesScreen())),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "مواعيدي",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 16, fontFamily: "cairo"),
                                  ),
                                ],
                              ),
                              Divider()
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Material(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20))),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Icon(
                              Icons.timer,
                              color: Colors.white,
                            ),
                          ),
                          color: Colors.green,
                        ),
                      ),
                    ].reversed.toList(),
                  ),
                ),
                InkWell(
                  // onTap: () => Navigator.push(context,
                  //     MaterialPageRoute(builder: (_) => MyPaymentScreen())),
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Material(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Icon(
                                Icons.monetization_on,
                                color: Colors.white,
                              ),
                            ),
                            color: Color.fromRGBO(1, 91, 102, 1),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Text(
                                      "معاملاتي",
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: "cairo",
                                      ),
                                      textAlign: TextAlign.start,
                                    ),
                                  ],
                                ),
                                Divider()
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                // InkWell(
                //   // onTap: () => Navigator.push(
                //   //     context,
                //   //     MaterialPageRoute(
                //   //         builder: (_) => NotificationPage(
                //   //               stateOfNotify: false,
                //   //             ))),
                //   child: Directionality(
                //     textDirection: TextDirection.rtl,
                //     child: Row(
                //       children: <Widget>[
                //         Padding(
                //           padding: const EdgeInsets.all(8.0),
                //           child: Material(
                //             shape: RoundedRectangleBorder(
                //                 borderRadius:
                //                     BorderRadius.all(Radius.circular(20))),
                //             child: Padding(
                //               padding: const EdgeInsets.all(4.0),
                //               // child: BlocBuilder(
                //               //     bloc: unReadNotificationBloC,
                //               //     builder: (_, state) {
                //               //       if (state is Done) {
                //               //         UnreadNotificationModel model =
                //               //             state.model;
                //               //         if (model.code == 200) {
                //               //           print(
                //               //               "_________________${model.data.unreadNotificationCount}_______________");
                //               //           unReadNotificationBloC
                //               //               .controllerPublic.sink
                //               //               .add(model
                //               //                   .data.unreadNotificationCount
                //               //                   .toString());
                //               //           return StreamBuilder<String>(
                //               //               initialData: "",
                //               //               stream: unReadNotificationBloC
                //               //                   .controllerPublic.stream,
                //               //               builder: (context, snapshot) {
                //               //                 return Badge(
                //               //                     color: Theme.of(context)
                //               //                         .primaryColor,
                //               //                     value: snapshot.data,
                //               //                     child: Icon(
                //               //                       Icons.notifications,
                //               //                       color: Colors.white,
                //               //                     ));
                //               //               });
                //               //         }
                //               //       }
                //               //       return Text("");
                //               //     }),
                //             ),
                //             color: Color.fromRGBO(166, 206, 57, 1.0),
                //           ),
                //         ),
                //         Expanded(
                //           child: Padding(
                //             padding: const EdgeInsets.all(8.0),
                //             child: Column(
                //               children: <Widget>[
                //                 Row(
                //                   children: <Widget>[
                //                     Expanded(
                //                       child: Text(
                //                         "الاشعارات الادارية",
                //                         style: TextStyle(
                //                           fontSize: 16,
                //                           fontFamily: "cairo",
                //                         ),
                //                         textAlign: TextAlign.start,
                //                       ),
                //                     ),
                //                   ],
                //                 ),
                //                 Divider()
                //               ],
                //             ),
                //           ),
                //         ),
                //       ],
                //     ),
                //   ),
                // ),
                InkWell(
                  //   onTap: () => _sendWhatsApp(),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "مركز المساعدة",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 16, fontFamily: "cairo"),
                                  ),
                                ].reversed.toList(),
                              ),
                              Divider()
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Material(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20))),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Icon(
                              FontAwesomeIcons.whatsapp,
                              color: Colors.white,
                            ),
                          ),
                          color: Colors.red,
                        ),
                      ),
                    ].reversed.toList(),
                  ),
                ),
                InkWell(
                  // onTap: () => Navigator.push(
                  //     context, MaterialPageRoute(builder: (_) => Terms())),
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Material(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Icon(
                                Icons.filter_none,
                                color: Colors.white,
                              ),
                            ),
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Text(
                                      "الشروط والاحكام",
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: "cairo",
                                      ),
                                      textAlign: TextAlign.start,
                                    ),
                                  ],
                                ),
                                Divider()
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                InkWell(
                  // onTap: () {
                  //   Theme.of(context).platform != TargetPlatform.iOS
                  //       ? shareTheApp()
                  //       : shareTheAppIOS();
                  // },
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Material(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Icon(
                                Icons.share,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                            color: Colors.white,
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Text(
                                      "شارك التطبيق",
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: "cairo",
                                      ),
                                      textAlign: TextAlign.start,
                                    ),
                                  ],
                                ),
                                Divider()
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                InkWell(
                  // onTap: () => Navigator.push(
                  //     context, MaterialPageRoute(builder: (_) => About())),
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Material(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Icon(
                                Icons.info,
                                color: Colors.white,
                              ),
                            ),
                            color: Color.fromRGBO(166, 206, 57, 1.0),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Text(
                                      "عن التطبيق",
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: "cairo",
                                      ),
                                      textAlign: TextAlign.start,
                                    ),
                                  ],
                                ),
                                Divider()
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                // BlocListener<AvailableBloC , AppState>(
                //   bloc: availableBloC,
                //   listener: (_ , state){
                //     if(state is Done){
                //      setState(() {
                //        stateOfLogOut = true;
                //      });
                //     }
                //   },
                //   child: BlocListener<UpdateOnlineStateBloC , AppState>(
                //     bloc: updateOnlineStateBloC,
                //     listener: (_,state){
                //       if(state is Done){
                //         if(stateOfLogOut){
                //           changeUserBloC.logout(_deviceToken).then((response) {
                //             if (response) {
                //               print("______________logOut____________________");
                //               _preferences.setBool("state", true);
                //             }
                //           });
                //           Navigator.of(context).pushAndRemoveUntil(
                //               MaterialPageRoute(builder: (_) => SignInScreen()),
                //                   (Route<dynamic> route) => false);
                //           _preferences.clear();
                //         }
                //       }
                //     },
                //     child: InkWell(
                //       onTap: () {
                //         updateOnlineStateBloC.stateChanged(0);
                //         availableBloC.availableChanged(0);
                //         updateOnlineStateBloC.add(Click());
                //         availableBloC.add(Click());
                //       },
                //       child: Directionality(
                //         textDirection: TextDirection.rtl,
                //         child: Row(
                //           children: <Widget>[
                //             Padding(
                //               padding: const EdgeInsets.all(8.0),
                //               child: Material(
                //                 shape: RoundedRectangleBorder(
                //                     borderRadius:
                //                         BorderRadius.all(Radius.circular(20))),
                //                 child: Padding(
                //                   padding: const EdgeInsets.all(4.0),
                //                   child: Icon(
                //                     FontAwesomeIcons.signOutAlt,
                //                     color: Colors.white,
                //                   ),
                //                 ),
                //                 color: Colors.redAccent,
                //               ),
                //             ),
                //             Expanded(
                //               child: Padding(
                //                 padding: const EdgeInsets.all(8.0),
                //                 child: Column(
                //                   children: <Widget>[
                //                     Row(
                //                       children: <Widget>[
                //                         Text(
                //                           "تسجيل خروج",
                //                           style: TextStyle(
                //                             fontSize: 16,
                //                             fontFamily: "cairo",
                //                           ),
                //                           textAlign: TextAlign.start,
                //                         ),
                //                       ],
                //                     ),
                //                     Divider()
                //                   ],
                //                 ),
                //               ),
                //             ),
                //           ],
                //         ),
                //       ),
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
          // CustomVisibility(
          //   loading: loading,
          // )
        ],
      ),
    );
  }
}
