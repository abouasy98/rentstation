import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/get/getMyAdds.dart';
import 'package:rentstation/widgets/myAdItem.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditordeleteAds extends StatelessWidget {
    final SharedPreferences prefs;

  const EditordeleteAds({Key key, this.prefs}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Provider.of<GetMyAdsProvider>(context, listen: false)
          .getMyAds(prefs.getString('api_token'),context),
      builder: (context, snapShot) {
        if (snapShot.connectionState == ConnectionState.waiting)
          return SpinKitThreeBounce(
            color: Theme.of(context).primaryColor,
            size: 22,
          );
        if (!snapShot.hasData) {
          print('data=${snapShot.data}');
          return Center(child: Text(localization.text('no_posts')));
        }
        return Padding(
          padding: const EdgeInsets.only(top: 16.0, bottom: 16),
          child: Consumer<GetMyAdsProvider>(builder: (ctx, productData, _) {
            if (productData.myAds.length <= 0) {
              return Center(
                child: Text(localization.text('no_posts')),
              );
            }
            return ListView.builder(
              primary: false,
              shrinkWrap: true,
              itemCount: productData.myAds.length,
              itemBuilder: (context, i) => Card(
                child: MyAdItem(
                  id: productData.myAds[i].id,
                  image: productData.myAds[i].photos[0].photo,
                  categoryId: productData.myAds[i].categoryId,
                  categorylable: productData.myAds[i].category,
                  checked: productData.myAds[i].priceStatus == 1 ? true : false,
                  subCategoryId: productData.myAds[i].subCategoryId,
                  subCategorylable: productData.myAds[i].subCategory,
                  title: productData.myAds[i].title,
                  desc: productData.myAds[i].description,
                  price: productData.myAds[i].price,
                  photos: productData.myAds[i].photos,
                  
                  labelCity: productData.myAds[i].city,
                  images: productData.myAds[i].photos,
                  prefs: prefs,
                ),
              ),
            );
          }),
        );
      },
    );
  }
}
