import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/models/categoriesModels.dart';
import 'package:rentstation/models/get/getCitiesModel.dart';
import 'package:rentstation/models/subCategoriesModel.dart';
import 'package:rentstation/provider/get/categoriesProvider.dart';
import 'package:rentstation/provider/get/getCitiesByIdProvider.dart';
import 'package:rentstation/Components/detailsTextFieldNoImg.dart';
import 'package:rentstation/Components/registerTextField.dart';
import 'package:rentstation/Components/CustomButton.dart';
import 'package:rentstation/provider/get/subCategoriesProvider.dart';
import 'package:rentstation/provider/post/editAuction.dart';
import 'package:rentstation/screens/listviewImages.dart';
import 'package:rentstation/widgets/multiPhotos.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditAuctions extends StatefulWidget {
  final String lableCity;
  final String title;
  final List photos;
  final int period;
  final int categoryId;
  final int intialPrice;
  final String details;
  final int priceIncrease;
  final int id;
  final String currency;
  final String categorylable;
  final String subCategorylable;
  final List images;
  const EditAuctions({
    Key key,
    this.details,
    this.photos,
    this.period,
    this.priceIncrease,
    this.intialPrice,
    this.images,
    this.title,
    this.currency,
    this.lableCity,
    this.id,
    this.categoryId,
    this.categorylable,
    this.subCategorylable,
  }) : super(key: key);
  @override
  _EditAuctionsState createState() => _EditAuctionsState();
}

class _EditAuctionsState extends State<EditAuctions> {
  String lableCity;
  List<Asset> files = [];
    CustomDialog dialog = CustomDialog();
  String title;
    List<CitiesModel> categories = [];
  List<CitiesModel> subCategories = [];
  String editedDetails;
    SubCategoriesModels getSubCategoriesModel;
  int cityId;
    String categorylable;
  String subCategorylable;
  int categoryId;
  int subCategoryId;
  final _form = GlobalKey<FormState>();
  int editedPeriod;
  int editedintialPrice;
  int editedIncreasePrice;
    CategoriesModels getCategoriesModel;
  List<CitiesModel> cities = [];
  bool loaded = true;
  GetCities getCitiesByCountryIdModel;
  Widget buildContainer(
      {String title, int intialValue, String title2, Function func}) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.3,
                  child: Text(
                    title,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.3,
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    initialValue: '$intialValue',
                    textAlign: TextAlign.right,
                    onChanged: func,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(top: 10, right: 10),
                      border: new OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.1,
                ),
                Text(title2),
              ],
            ),
          ],
        ),
      ),
    );
  }

  SharedPreferences _prefs;
  _getShared() async {
    var _instance = await SharedPreferences.getInstance();
    setState(() {
      _prefs = _instance;
    });
    await Provider.of<GetCitiesByCountryIdProvider>(context, listen: false)
        .getCities(_prefs.get("countryId"), context)
        .then((res) {
      setState(() {
        getCitiesByCountryIdModel = res;
      });
      if (getCitiesByCountryIdModel.data != null) {
        for (int i = 0; i < getCitiesByCountryIdModel.data.length; i++) {
          cities.add(new CitiesModel(
            id: getCitiesByCountryIdModel.data[i].id,
            label: getCitiesByCountryIdModel.data[i].name,
          ));
        }
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  bool isInit = true;

  @override
  void didChangeDependencies() async {
    if (isInit) {
    
      await _getShared();
  Provider.of<CategoriesProvider>(context, listen: false)
          .getCategories(context)
          .then((res) {
        setState(() {
          getCategoriesModel = res;
        });
        if (getCategoriesModel.data != null) {
          for (int i = 0; i < getCategoriesModel.data.length; i++) {
            categories.add(new CitiesModel(
              id: getCategoriesModel.data[i].id,
              label: getCategoriesModel.data[i].name,
            ));
          }
        }
      });
      categorylable = widget.categorylable;

      subCategorylable = widget.subCategorylable;
      lableCity = widget.lableCity;

      setState(() {
        isInit = false;
      });
    }
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Theme.of(context).primaryColor,
        title: Text(
          localization.text("edit_auction"),
        ),
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          child: Container(
              color: Theme.of(context).primaryColor,
              child: Icon(Icons.arrow_back_ios)),
        ),
      ),
      backgroundColor: Colors.white,
      body: isInit
          ? SpinKitThreeBounce(
              color: Theme.of(context).primaryColor,
              size: 22,
            )
          : Directionality(
              textDirection: localization.currentLanguage.toString() == "en"
                  ? TextDirection.ltr
                  : TextDirection.rtl,
              child: Container(
                color: Color.fromRGBO(240, 240, 240, 1),
                child: Form(
                  key: _form,
                  child: ListView(
                    shrinkWrap: true,
                    primary: false,
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height * 0.3,
                        child: Carousel(
                          images: widget.images
                              .map(
                                (e) => GestureDetector(
                                  onTap: () {
                                    print('auctionId=${widget.id}');
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ListViewImages(
                                                  auctionId: widget.id,
                                                  prefs: _prefs,
                                                )));
                                  },
                                  child: e.photo != null
                                      ? CachedNetworkImage(
                                          imageUrl: e.photo,
                                          fadeInDuration: Duration(seconds: 2),
                                          placeholder: (context, url) =>
                                              Container(
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          'images/loading.gif'),
                                                      fit: BoxFit.fill),
                                                ),
                                              ),
                                          imageBuilder: (context, provider) {
                                            return Container(
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: provider,
                                                    fit: BoxFit.fill),
                                              ),
                                            );
                                          })
                                      : Container(
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image:
                                                    AssetImage('images/loading.gif'),
                                                fit: BoxFit.fill),
                                          ),
                                        ),
                                ),
                              )
                              .toList(),
                          dotSize: 4.0,
                          dotSpacing: 15.0,
                          dotColor: Colors.black,
                          indicatorBgPadding: 4.0,
                          dotIncreasedColor: Colors.blueAccent,
                          dotBgColor: Colors.white.withOpacity(0.5),
                          borderRadius: true,
                          moveIndicatorFromBottom: 180.0,
                          noRadiusForIndicator: true,
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        color: Colors.white,
                        child: Column(
                          children: <Widget>[
                            RegisterTextField(
                              border: 0,
                              type: TextInputType.text,
                              onChange: (value) {
                                title = value;
                              },
                              hint: localization.text("title"),
                              label: "",
                              init: widget.title,
                            ),
                            Divider(
                              height: 1,
                              color: Colors.grey,
                            ),
                            DetailsTextFieldNoImg(
                              border: 0,
                              label: "",
                              hint: localization.text("ad_details"),
                              onChange: (value) {
                                editedDetails = value;
                              },
                              init: widget.details,
                            ),
                            Divider(
                              height: 1,
                              color: Colors.grey,
                            ),
                            Divider(
                              height: 1,
                              color: Colors.grey,
                            ),
                            RaisedButton(
                            elevation: 0,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0)),
                            onPressed: () {
                              mainBottomSheet(context, categories, "تصنيف");
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              textDirection:
                                  localization.currentLanguage.toString() ==
                                          "en"
                                      ? TextDirection.ltr
                                      : TextDirection.rtl,
                              children: <Widget>[
                                Text(categorylable),
                                Icon(Icons.arrow_back_ios)
                              ],
                            ),
                          ),
                          Divider(
                            height: 1,
                            color: Colors.grey,
                          ),
                          RaisedButton(
                            elevation: 0,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0)),
                            onPressed: () {
                              mainBottomSheet(
                                  context, subCategories, "تصنيف فرعي");
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              textDirection:
                                  localization.currentLanguage.toString() ==
                                          "en"
                                      ? TextDirection.ltr
                                      : TextDirection.rtl,
                              children: <Widget>[
                                Text(subCategorylable),
                                Icon(Icons.arrow_back_ios)
                              ],
                            ),
                          ),
                          Divider(
                            height: 1,
                            color: Colors.grey,
                          ),
                            ElevatedButton(

                              style: ButtonStyle(
                                elevation: MaterialStateProperty.all(0.0),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(0)),
                                ),
                                
                              ),

                              // elevation: 0,
                              // color: Colors.white,
                              // shape: RoundedRectangleBorder(
                              //     borderRadius: BorderRadius.circular(0)),
                              onPressed: () {
                                mainBottomSheet(context, cities, "المدينة");
                              },
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                 textDirection:
                                    localization.currentLanguage.toString() ==
                                            "en"
                                        ? TextDirection.ltr
                                        : TextDirection.rtl,
                                children: <Widget>[
                                  Text(lableCity),
                                  Icon(Icons.arrow_back_ios)
                                ],
                              ),
                            ),
                            Divider(
                              height: 1,
                              color: Colors.grey,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      buildContainer(
                          title: localization.text('period'),
                          intialValue: widget.period,
                          title2: localization.text('day'),
                          func: (value) {
                            editedPeriod = int.parse(value);
                          }),
                      buildContainer(
                          title: localization.text('initial_price'),
                          intialValue: widget.intialPrice,
                          title2: widget.currency,
                          func: (value) {
                            editedintialPrice = int.parse(value);
                          }),
                      buildContainer(
                          title: localization.text('Min_Increment'),
                          intialValue: widget.priceIncrease,
                          title2: widget.currency,
                          func: (value) {
                            editedIncreasePrice = int.parse(value);
                          }),
                      SizedBox(height: 20),
                      MultiImagePickerBtn(
                        onGetImages: (f) {
                          setState(() {
                            files = f;
                          });
                        },
                      ),
                      SizedBox(height: 50),
                      Provider.of<EditAuctionProvider>(context).loading == true
                          ? Center(
                              child: CircularProgressIndicator(),
                            )
                          : Padding(
                              padding:
                                  const EdgeInsets.only(left: 50, right: 50),
                              child: CustomButton(
                                onTap: () {
                                         if (categoryId != null &&
                                    subCategoryId == null) {
                                  dialog.showErrorDialog(
                                    btnOnPress: () {},
                                    context: context,
                                    msg: localization.text(
                                        "If_you_modify_the_main_categories_you_must_also_modify_the_subcategories"),
                                    //  "يحب الموافقة علي الشروط والاحكام",
                                    ok: localization.text("ok"),
                                  );
                                  return;
                                }
                                  setState(() {
                                    _form.currentState.save();
                                    Provider.of<EditAuctionProvider>(context,
                                            listen: false)
                                        .loading = true;
                                  });
                                  Provider.of<EditAuctionProvider>(context,
                                          listen: false)
                                      .editAd(
                                          token: _prefs.getString('api_token'),
                                          title: title,
                                          details: editedDetails,
                                          period: editedPeriod,
                                          photos: files,
                                          context: context,
                                          id: widget.id,
                                          cityId: cityId,
                                          intialPrice: editedintialPrice,
                                          subCategoryId: subCategoryId,
                                          priceIncrease: editedIncreasePrice);
                                },
                                text: localization.text("edit"),
                              ),
                            ),
                      SizedBox(height: 20)
                    ],
                  ),
                ),
              ),
            ),
    );
  }

mainBottomSheet(BuildContext context, List<CitiesModel> list, String title) {
    return showModalBottomSheet<dynamic>(
        isScrollControlled: false,
        backgroundColor: Colors.white,
        context: context,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        builder: (BuildContext bc) {
          return title == "تصنيف فرعي"
              ? FutureBuilder(
                  future: Provider.of<SubCategoriesProvider>(context,
                          listen: false)
                      .getSubCategories(
                          categoryId != null ? categoryId : widget.categoryId,
                          context),
                  builder: (context, snapShot) {
                    if (snapShot.connectionState == ConnectionState.waiting) {
                      return SpinKitThreeBounce(
                        color: Theme.of(context).primaryColor,
                        size: 22,
                      );
                    }

                    getSubCategoriesModel = snapShot.data;

                    if (getSubCategoriesModel.data != null &&
                        subCategories.isEmpty) {
                      for (int i = 0;
                          i < getSubCategoriesModel.data.length;
                          i++) {
                        subCategories.add(new CitiesModel(
                          id: getSubCategoriesModel.data[i].id,
                          label: getSubCategoriesModel.data[i].name,
                        ));
                      }
                    }

                    loaded = false;

                    return loaded
                        ? SpinKitThreeBounce(
                            color: Theme.of(context).primaryColor,
                            size: 22,
                          )
                        : Directionality(
                            textDirection: TextDirection.rtl,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top: 20.0),
                                  child: ListView(children: <Widget>[
                                    Container(
                                      color: Colors.white,
                                      child: ListView.builder(
                                        shrinkWrap: true,
                                        physics: ScrollPhysics(),
                                        itemCount: list.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              ListTile(
                                                  onTap: () {
                                                    print(list[index].id);
                                                    Navigator.pop(context);
                                                    setState(() {
                                                      if (title == "المدينة") {
                                                        cityId = list[index].id;
                                                        print("cityid $cityId");
                                                        lableCity =
                                                            list[index].label;
                                                      } else if (title ==
                                                          "تصنيف") {
                                                        categoryId =
                                                            list[index].id;
                                                        print(
                                                            "catrgory $categoryId");
                                                        categorylable =
                                                            list[index].label;
                                                      } else {
                                                        subCategoryId =
                                                            list[index].id;
                                                        print(
                                                            "catrgory $categoryId");
                                                        subCategorylable =
                                                            list[index].label;
                                                        subCategories.clear();
                                                      }
                                                    });
                                                  },
                                                  title: Text(
                                                    list[index].label,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  )),
                                              Divider(
                                                height: 1,
                                                color: Colors.grey,
                                              )
                                            ],
                                          );
                                        },
                                      ),
                                    ),
                                  ]),
                                ),
                              ],
                            ),
                          );
                  },
                )
              : Directionality(
                  textDirection: TextDirection.rtl,
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: ListView(children: <Widget>[
                          Container(
                            color: Colors.white,
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: ScrollPhysics(),
                              itemCount: list.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    ListTile(
                                        onTap: () {
                                          print(list[index].id);
                                          Navigator.pop(context);
                                          setState(() {
                                            if (title == "المدينة") {
                                              cityId = list[index].id;
                                              print("cityid $cityId");
                                              lableCity = list[index].label;
                                            } else {
                                              categoryId = list[index].id;
                                              print("catrgory $categoryId");
                                              categorylable = list[index].label;
                                              subCategories.clear();
                                            }
                                          });
                                        },
                                        title: Text(
                                          list[index].label,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        )),
                                    Divider(
                                      height: 1,
                                      color: Colors.grey,
                                    )
                                  ],
                                );
                              },
                            ),
                          ),
                        ]),
                      ),
                    ],
                  ),
                );
        });
  }
}


class CitiesModel {
  int id;
  String label;

  CitiesModel({
    this.id,
    this.label,
  });
}
