import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/map_helper.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/get/getCitiesModel.dart';
import 'package:rentstation/models/post/getAuctionsByFilter.dart';
import 'package:rentstation/provider/get/GetAppLogoProvider.dart';
import 'package:rentstation/provider/get/categoriesProvider.dart';
import 'package:rentstation/provider/get/getCitiesByIdProvider.dart';
import 'package:rentstation/provider/get/subCategoriesProvider.dart';
import 'package:rentstation/provider/post/getAuctionsByCityFilter.dart';
import 'package:rentstation/screens/createAuctionScreen.dart';
import 'package:rentstation/screens/login.dart';
import 'package:rentstation/widgets/AuctionsBody.dart';
import 'package:provider/provider.dart';

class AuctionScreen extends StatefulWidget {
  @override
  _AuctionScreenState createState() => _AuctionScreenState();
}

class _AuctionScreenState extends State<AuctionScreen> {
  TextEditingController _searchController = TextEditingController();
  String query = '';
  // GetAuctionsModel getAuctionssModel;
  GetCities getCitiesByCountryIdModel;
  String category = 'الكل';
  int categoryId;
  int subCategoryId;
  String subCategory = '';
  GetAuctionsByCityFilter getAuctionsByCityFilter;
  CategoriesProvider categoryProvider;
  SubCategoriesProvider subCategories;
  // CustomDialog _dialog = CustomDialog();
  _getShared() async {
    Provider.of<GetAuctionsByCityFilterProvider>(context, listen: false)
        .countryId = Provider.of<SharedPref>(context, listen: false).countryId;
    // getAuctionssModel = null;
    if (Provider.of<MapHelper>(context, listen: false).position == null) {
      await Provider.of<MapHelper>(context, listen: false).getLocation();
    }
    getAuctionsByCityFilter = null;
    await Provider.of<GetCitiesByCountryIdProvider>(context, listen: false)
        .getCities(
            Provider.of<SharedPref>(context, listen: false).countryId, context)
        .then((res) {
      setState(() {
        getCitiesByCountryIdModel = res;
      });
    });
    await Provider.of<GetAuctionsByCityFilterProvider>(context, listen: false)
        .getAuctions(
            Provider.of<MapHelper>(context, listen: false).position != null
                ? Provider.of<MapHelper>(context, listen: false)
                    .position
                    .latitude
                : 30.7982574,
            Provider.of<MapHelper>(context, listen: false).position != null
                ? Provider.of<MapHelper>(context, listen: false)
                    .position
                    .longitude
                : 31.0066411,
            Provider.of<SharedPref>(context, listen: false).token,
            context)
        .then((res) {
      setState(() {
        getAuctionsByCityFilter = res;
      });
    });
    // if (Provider.of<SharedPref>(context, listen: false).token != null) {
    //   await Provider.of<GetAuctionsProvider>(context, listen: false)
    //       .getAuctions(
    //           Provider.of<SharedPref>(context, listen: false).token, context)
    //       .then((res) {
    //     setState(() {
    //       getAuctionssModel = res;
    //     });
    //   });
    // } else {
    //   await Provider.of<GetAuctionsWihtoutAuthProvider>(context, listen: false)
    //       .getAuctionswithoutAuth(context)
    //       .then((res) {
    //     setState(() {
    //       getAuctionssModel = res;
    //     });
    //   });
    // }
  }

  Widget searchWidget() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.05,
      decoration: BoxDecoration(
        color: Colors.blueAccent[700],
      ),
      child: Directionality(
          key: widget.key,
          textDirection: TextDirection.rtl,
          child: Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: TextFormField(
              style: TextStyle(color: Colors.white, fontSize: 18),
              controller: _searchController,
              textAlign: localization.currentLanguage.toString() == "en"
                  ? TextAlign.left
                  : TextAlign.right,
              textDirection: TextDirection.rtl,
              onChanged: (val) {
                setState(() {
                         category = 'الكل';
                  query = val.trim();
                });
              },
              decoration: InputDecoration(
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                hintText:
                    '${localization.text("search_in_Herag_Tqnee")}\t${Provider.of<GetAppLogoProvider>(context, listen: false).name}',
                hintStyle: TextStyle(color: Colors.white, fontSize: 14),
                suffixIcon: query != ''
                    ? IconButton(
                        iconSize: 20,
                        icon: Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          WidgetsBinding.instance.addPostFrameCallback(
                              (_) => _searchController.clear());
                          setState(() {
                            query = '';
                          });
                        },
                      )
                    : Icon(
                        Icons.search,
                        size: 20,
                        color: Colors.white,
                      ),
              ),
            ),
          )),
    );
  }

  bool isInit = true;

  @override
  void didChangeDependencies() async {
    if (isInit) {
      await Provider.of<CategoriesProvider>(context, listen: false)
          .getCategories(context);

      categoryProvider =
          Provider.of<CategoriesProvider>(context, listen: false);
      subCategories =
          Provider.of<SubCategoriesProvider>(context, listen: false);
      await _getShared();

      setState(() {
        isInit = false;
      });
    }
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return SafeArea(
      child: Directionality(
        textDirection: localization.currentLanguage.toString() == "en"
            ? TextDirection.ltr
            : TextDirection.rtl,
        child: DefaultTabController(
          length: 8,
          child: isInit
              ? SpinKitThreeBounce(
                  color: Theme.of(context).primaryColor,
                  size: 22,
                )
              : Scaffold(
                  //  resizeToAvoidBottomPadding: false,
                  appBar: PreferredSize(
                       preferredSize:
                      query==""?Size.fromHeight(MediaQuery.of(context).size.height * 0.1):Size.fromHeight(MediaQuery.of(context).size.height * 0.05),
                    child: AppBar(
                      leading: Container(),
                      backgroundColor: Colors.white,
                      flexibleSpace: searchWidget(),
                      bottom: PreferredSize(
                        preferredSize: mediaQuery,
                        child: query==""?Container(
                            height: mediaQuery.height * 0.05,
                            width: mediaQuery.width,
                            child: Padding(
                              padding:
                                  localization.currentLanguage.toString() ==
                                          "en"
                                      ? const EdgeInsets.only(left: 16.0)
                                      : const EdgeInsets.only(right: 16.0),
                              child: ListView(
                                  primary: false,
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          category = 'الكل';
                                        });
                                      },
                                      child: Center(
                                        child: Text(
                                          localization.text('all'),
                                          style: TextStyle(
                                              color: Color(0xff366775)),
                                        ),
                                      ),
                                    ),
                                    ListView.builder(
                                        primary: false,
                                        shrinkWrap: true,
                                        scrollDirection: Axis.horizontal,
                                        itemCount:
                                            categoryProvider.categories.length,
                                        itemBuilder: (ctx, i) {
                                          return GestureDetector(
                                            onTap: () async {
                                              setState(() {
                                                category = categoryProvider
                                                    .categories[i].name;
                                                print('category=$category');
                                                categoryId = categoryProvider
                                                    .categories[i].id;
                                                subCategory = '';
                                              });
                                              await subCategories
                                                  .getSubCategories(
                                                      categoryId, context);
                                            },
                                            child: Container(
                                              padding: localization
                                                          .currentLanguage
                                                          .toString() ==
                                                      "en"
                                                  ? const EdgeInsets.only(
                                                      left: 16.0)
                                                  : const EdgeInsets.only(
                                                      right: 16.0),
                                              decoration: BoxDecoration(
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: category ==
                                                            categoryProvider
                                                                .categories[i]
                                                                .name
                                                        ? Theme.of(context)
                                                            .primaryColor
                                                            .withOpacity(0.5)
                                                        : Colors.white,
                                                    spreadRadius: 0,
                                                    blurRadius: 0,
                                                    offset: Offset(0,
                                                        0), // changes position of shadow
                                                  ),
                                                ],
                                                // borderRadius: BorderRadius.circular(20),
                                              ),
                                              child: Center(
                                                child: Text(
                                                  categoryProvider
                                                      .categories[i].name,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      color: Color(0xff366775)),
                                                ),
                                              ),
                                            ),
                                          );
                                        }),
                                  ]),
                            )):Container(),
                      ),
                    ),
                  ),
                  floatingActionButton:
                      Provider.of<SharedPref>(context, listen: false).auction ==
                              true
                          ? Container()
                          : FloatingActionButton(
                              heroTag: null,
                              onPressed: () async {
                                if (Provider.of<SharedPref>(context,
                                            listen: false)
                                        .token ==
                                    null) {
                                  // await _dialog.showOptionDialog(
                                  //     context: context,
                                  //     msg: localization.text('should_login'),
                                  //     okFun: () {
                                  //       Navigator.push(
                                  //           context,
                                  //           MaterialPageRoute(
                                  //               builder: (c) => SignInScreen()));
                                  //     },
                                  //     okMsg: localization.text('yes'),
                                  //     cancelMsg: localization.text('no'),
                                  //     cancelFun: () {
                                  //       return;
                                  //     });
                                  pushNewScreen(
                                    context,
                                    screen: SignInScreen(true),
                                    withNavBar:
                                        false, // OPTIONAL VALUE. True by default.
                                    pageTransitionAnimation:
                                        PageTransitionAnimation.cupertino,
                                  );
                                } else {
                                  pushNewScreen(
                                    context,
                                    screen: CreateAuctionScreen(),
                                    withNavBar:
                                        false, // OPTIONAL VALUE. True by default.
                                    pageTransitionAnimation:
                                        PageTransitionAnimation.cupertino,
                                  );
                                  //                         Navigator.of(context).push(MaterialPageRoute(
                                  //                             builder: (context) =>
                                  //                                 CreateAuctionScreen()));
                                }
                              },
                              child: Icon(Icons.add),
                            ),
                  body: (category == 'الكل')
                      ? Container(
                          height: mediaQuery.height,
                          child: AuctionsBody(
                            categoryId: categoryId,
                            subCategoryId: subCategoryId,
                            category: category == null ? '' : category,
                            key: UniqueKey(),
                            query: query,
                            subCategory: subCategory,
                            getAuctionsByCityFilter: getAuctionsByCityFilter,
                            getCitiesByCountryIdModel:
                                getCitiesByCountryIdModel,
                          ),
                        )
                      : SingleChildScrollView(
                          primary: false,
                          child: Column(
                            children: [
                              SizedBox(
                                height: mediaQuery.height * 0.01,
                              ),
                              Container(
                                height: mediaQuery.height * 0.07,
                                child: Consumer<SubCategoriesProvider>(
                                  builder: (context, subCategoryData, _) =>
                                      ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount:
                                        subCategoryData.subcategories.length,
                                    itemBuilder: (ctx, i) => GestureDetector(
                                      onTap: () {
                                        if (subCategory ==
                                            subCategoryData
                                                .subcategories[i].name) {
                                          setState(() {
                                            subCategory = '';
                                          });
                                        } else {
                                          setState(() {
                                            subCategory = subCategoryData
                                                .subcategories[i].name;
                                            subCategoryId = subCategoryData
                                                .subcategories[i].id;
                                          });
                                          print('subcategory$subCategory');
                                        }
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(
                                          left: 6,
                                          right: 6,
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: subCategory ==
                                                  subCategories
                                                      .subcategories[i].name
                                              ? Theme.of(context).primaryColor
                                              : Colors.grey[200],
                                        ),
                                        padding: EdgeInsets.all(10),
                                        child: Center(
                                          child: Text(
                                            subCategories.subcategories[i].name,
                                            style: TextStyle(
                                              color: subCategory ==
                                                      subCategories
                                                          .subcategories[i].name
                                                  ? Colors.white
                                                  : Colors.grey,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                height: mediaQuery.height,
                                child: AuctionsBody(
                                  categoryId: categoryId,
                                  subCategoryId: subCategoryId,
                                  category: category == null ? '' : category,
                                  key: UniqueKey(),
                                  query: query,
                                  subCategory: subCategory,
                                  getAuctionsByCityFilter:
                                      getAuctionsByCityFilter,
                                  getCitiesByCountryIdModel:
                                      getCitiesByCountryIdModel,
                                ),
                              ),
                            ],
                          ),
                        )),
        ),
      ),
    );
  }
}
