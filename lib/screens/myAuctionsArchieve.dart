import 'package:flutter/material.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/get/getmyAuctionsArchieveProvider.dart';
import 'package:rentstation/widgets/auctionArchievedItem.dart';

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyAuctionsArchieve extends StatelessWidget {
final   SharedPreferences prefs;

 
MyAuctionsArchieve({this.prefs});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: FutureBuilder(
              future:
                  Provider.of<GetmyArchieveAuctionsProvider>(context, listen: false)
                      .getMyArchieveAuctions(prefs.getString('api_token'),context),
              builder: (context, snapShot) {
                if (snapShot.connectionState == ConnectionState.waiting) {
                  return Center(child:  Text(localization.text('waiting')));
                } else if (!snapShot.hasData) {
                  return Center(child: Text(localization.text('no_posts')));
                } else {
                  return Consumer<GetmyArchieveAuctionsProvider>(
                      builder: (context, archieveData, _) {
                    if (archieveData.archieve.length <= 0) {
                      return Center(child: Text(localization.text('no_posts')));
                    }
                    return ListView.builder(
                        primary: false,
                        shrinkWrap: true,
                        itemCount: archieveData.archieve.length,
                        itemBuilder: (BuildContext context, int i) =>
                            AuctionArchievedItem(
                              createdAt: archieveData.archieve[i].createdAt,
                              id: archieveData.archieve[i].id,
                              photo: archieveData.archieve[i].photo[0].photo,
                              auctionWinner: archieveData.archieve[i].winnerName,
                              prefs: prefs,
                              period: archieveData.archieve[i].period,                    
                              title: archieveData.archieve[i].providertitle,
                              prices: archieveData.archieve[i].prices,
                              userId: archieveData.archieve[i].userId ,
                              intialPrice: archieveData.archieve[i].intialPrice,
                              heighPrice:archieveData.archieve[i].heighPrice ,
                              winnerNamePhoto:archieveData.archieve[i].winnerNamePhoto ,
                              currency:archieveData.archieve[i].currency ,
                            ));
                  });
                }
              }),
    );
  }
}
