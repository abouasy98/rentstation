import 'package:flutter/material.dart';
import 'package:rentstation/Components/buttonSignIn.dart';
import 'package:rentstation/Components/inputTextField.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/auth/resetPasswordProvider.dart';
import 'package:provider/provider.dart';


class ResetPasswordScreen extends StatefulWidget {
  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  @override
  void initState() {
    // Provider.of<ResetPasswordProvider>(context, listen: false).context =
    //     context;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var confirmRessetCode =
        Provider.of<ResetPasswordProvider>(context, listen: false);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: <Widget>[
          
          Center(
            child: Container(
              child: ListView(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                children: <Widget>[
              
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                       localization.text("edit_password"),
                      // 'تغير كلمة المرور',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontFamily: 'cairo'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InputFieldArea(
                      inputFieldWithBorder: true,
                      textInputType: TextInputType.text,
                    hint: localization.text("new_password"),
                      // 'كلمة المرور',
                      show: true,
                      changedFunction: (v) {
                        confirmRessetCode.password = v;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InputFieldArea(
                      inputFieldWithBorder: true,
                      textInputType: TextInputType.text,
                      hint: localization.text("confirm_nem_password"),
                      show: true,
                      changedFunction: (v) {
                        confirmRessetCode.passwordConfirmation = v;
                      },
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SignInButton(
                        txtColor: Colors.white,
                        onPressSignIn: () {
                          confirmRessetCode.resetPassword(context);
                        },
                        btnWidth: MediaQuery.of(context).size.width,
                        btnHeight: MediaQuery.of(context).size.height * .07,
                        btnColor: Theme.of(context).primaryColor,
                        buttonText: localization.text("send"),
                      )),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
