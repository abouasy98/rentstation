import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/provider/changeData/changeUserDataProvider.dart';
import 'package:rentstation/provider/post/getAdsByCityFilter.dart';
import 'package:rentstation/provider/post/getAuctionsByCityFilter.dart';
import '../CounrtySelection.dart';

import 'package:provider/provider.dart';

class CountryCard extends StatefulWidget {
  final List<Countrymodel> listCountries;

  const CountryCard({Key key, this.listCountries}) : super(key: key);

  @override
  _CountryCardState createState() => _CountryCardState();
}

class _CountryCardState extends State<CountryCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: ListView.builder(
        itemCount: widget.listCountries.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          print('listCountries.length${widget.listCountries.length}');
          return Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: InkWell(
              onTap: () async {
                setState(() {
                  for (int i = 0; i < widget.listCountries.length; i++) {
                    widget.listCountries[i].selected = false;
                  }

                  widget.listCountries[index].selected =
                      !widget.listCountries[index].selected;
                });
                Provider.of<SharedPref>(context, listen: false).countryId =
                    widget.listCountries[index].id;
                Provider.of<GetAddsByCityFilterProvider>(context, listen: false)
                    .countryId = widget.listCountries[index].id;
                Provider.of<GetAuctionsByCityFilterProvider>(context,
                        listen: false)
                    .countryId = widget.listCountries[index].id;
                Provider.of<SharedPref>(context, listen: false).currency =
                    widget.listCountries[index].currency;
                print(widget.listCountries[index].name);
                if (Provider.of<SharedPref>(context, listen: false).token !=
                    null) {
                  Provider.of<ChangeUserDataProvider>(context, listen: false)
                      .countryId = widget.listCountries[index].id;
                  print(Provider.of<ChangeUserDataProvider>(context,
                          listen: false)
                      .countryId);
                }
                // else {
                  // Provider.of<GetAddswithoutAuthProvider>(context,
                  //         listen: false)
                  //     .countryId = widget.listCountries[index].id;
                  // Provider.of<GetAuctionsWihtoutAuthProvider>(context,
                  //         listen: false)
                  //     .countryId = widget.listCountries[index].id;
                  // Provider.of<GetAddsByCityFilterProvider>(context,
                  //         listen: false)
                  //     .countryId = widget.listCountries[index].id;
              //  }

                // Provider.of<SharedPref>(context, listen: false)
                //     .getSharedHelper(_prefs);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  widget.listCountries[index].selected
                      ? Icon(Icons.check, color: Colors.black)
                      : Container(),
                  Row(
                    children: <Widget>[
                      Text(widget.listCountries[index].name,
                          style: TextStyle(fontSize: 20)),
                      SizedBox(width: 10),
                      CircleAvatar(
                          backgroundImage: CachedNetworkImageProvider(
                              widget.listCountries[index].flag),
                          radius: 30),
                    ],
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
