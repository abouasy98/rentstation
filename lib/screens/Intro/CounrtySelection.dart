import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rentstation/Components/customBtn.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/get/countriesModel.dart';
import 'package:rentstation/provider/changeData/changeUserDataProvider.dart';
import 'package:rentstation/provider/get/getcountries.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'SplashScreen.dart';
import 'Widgets/countryCard.dart';

class CountrySelection extends StatefulWidget {
  final bool changelang;
  const CountrySelection({Key key, this.changelang}) : super(key: key);
  @override
  _CountrySelectionState createState() => _CountrySelectionState();
}

class _CountrySelectionState extends State<CountrySelection> {
  // ignore: unused_field
  CountriesModel getCountries;
  List<Countrymodel> _countriies = [];

  @override
  void initState() {
    super.initState();
  }

  SharedPreferences pref;
  bool a7a = true;
  @override
  void didChangeDependencies() async {
    if (a7a) {
      pref = await SharedPreferences.getInstance();
      await Provider.of<GetCountriesprovider>(context, listen: false)
          .getCountries(context)
          .then((res) => setState(() {
                getCountries = res;
              }));

      for (int i = 0; i < getCountries.data.length; i++) {
        _countriies.add(Countrymodel(
            flag: getCountries.data[i].flag,
            selected: false,
            name: getCountries.data[i].name,
            currency: getCountries.data[i].currency,
            id: getCountries.data[i].id));
      }
      setState(() {
        a7a = false;
      });
    }
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: localization.currentLanguage.toString() == "en"
          ? TextDirection.rtl
          : TextDirection.ltr,
      child: Scaffold(
        body: a7a
            ? SpinKitThreeBounce(
                color: Theme.of(context).primaryColor,
                size: 22,
              )
            : Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListView(
                      children: <Widget>[
                        SizedBox(height: 50),
                        SizedBox(
                          height: 50,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Text(localization.text('change_country'),
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor)),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.grey[200],
                            ),
                            child: CountryCard(
                              listCountries: _countriies,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(30),
                          child: CustomBtn(
                            color: Theme.of(context).primaryColor,
                            txtColor: Colors.white,
                            onTap: () async {
                              await pref.setInt(
                                  "countryId",
                                  Provider.of<SharedPref>(context,
                                          listen: false)
                                      .countryId);
                              if (Provider.of<SharedPref>(context,
                                          listen: false)
                                      .token ==
                                  null) {
                                await pref.setBool("countryV", true);
                                Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Splash(),
                                  ),
                                  (Route<dynamic> route) => false,
                                );
                              } else {
                                Provider.of<ChangeUserDataProvider>(context,
                                        listen: false)
                                    .changeUserData(
                                        Provider.of<SharedPref>(context,
                                                listen: false)
                                            .token,
                                        context)
                                    .then((v) {
                                  if (v == true) {
                                    Provider.of<SharedPref>(context,
                                            listen: false)
                                        .getSharedHelper(pref);
                                  }
                                });
                              }
                            },
                            text: localization.text("save"),
                          ),
                        )
                      ],
                    ),
                  ),
                  Visibility(
                    visible: widget.changelang != null,
                    child: Positioned(
                      top: 40,
                      left: 20,
                      child: IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.black87,
                        ),
                      ),
                    ),
                  )
                ],
              ),
      ),
    );
  }
}

class Countrymodel {
  int id;
  String name;
  bool selected;
  String flag;
  String currency;

  Countrymodel({this.id, this.name, this.selected, this.flag, this.currency});
}
