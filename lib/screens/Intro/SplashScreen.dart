import 'dart:async';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/Repository/firebaseNotifications.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/get/adActvionsmodel.dart';
import 'package:rentstation/models/get/auctionsActivationModel.dart';
import 'package:rentstation/provider/get/GetAppLogoProvider.dart';
import 'package:rentstation/provider/get/adsActiviationProvider.dart';
import 'package:rentstation/provider/get/auctionsActivationProvider.dart';
import 'package:rentstation/provider/get/getUserByDataProvider.dart';
import 'package:rentstation/provider/get/getcountries.dart';
import 'package:rentstation/screens/Intro/languageSelect.dart';
import 'package:rentstation/screens/Intro/waitingScreen.dart';
import 'package:rentstation/screens/adTabBarScreen.dart';
import 'package:rentstation/screens/auctionTabScreen.dart';
import 'package:rentstation/screens/noAction&AdsTab.dart';
import 'package:rentstation/screens/tab_bar.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'CounrtySelection.dart';

class Splash extends StatefulWidget {
  final GlobalKey<NavigatorState> navigator;

  const Splash({Key key, this.navigator}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with SingleTickerProviderStateMixin {
  AnimationController controller;
  AuctionsActivationModel _model;
  AdActivitionmodelDart _model2;
  Animation animation;
  final _fcm = FirebaseMessaging();
  String fcmToken = "DEFAULT_TOKEN";
  SharedPreferences _prefs;

  _getShared() async {
    await Provider.of<GetAppLogoProvider>(context, listen: false)
        .getAppLogoProvider(context);
    _prefs = await SharedPreferences.getInstance();
    await Provider.of<SharedPref>(context, listen: false)
        .getSharedHelper(_prefs);
Provider.of<GetCountriesprovider>(context, listen: false).getCountries(context);
    if (_prefs.get("api_token") != null) {
      getUserData();

      print('Api _ Token=${_prefs.get("api_token")}');
    }
    if (_prefs.get("deviceToken") == null) {
      _fcm.getToken().then((response) {
        if (response != null) {
          setState(() {
            fcmToken = response;
          });
        }
      });
    }

    await Provider.of<GetAuctionsActivationProvider>(context, listen: false)
        .getAuctionsActivationProvider(context)
        .then((res) {
      setState(() {
        _model = res;
      });
    });
    await Provider.of<GetAdsActivationProvider>(context, listen: false)
        .getAuctionsActivationProvider(context)
        .then((res) {
      setState(() {
        _model2 = res;
      });
    });
  }

  getUserData() async {
    await Provider.of<GetUserDataProvider>(context, listen: false)
        .getUserData(_prefs.get("api_token"), _prefs.get("id"), context)
        .then((res) {
      if (res.code == 200) {
        Provider.of<SharedPref>(context, listen: false).getSharedHelper(_prefs);
      } else if (res.code == 401) {
        print('----- clear ----');
        _prefs.remove("name");
        _prefs.remove("phone");
        _prefs.remove("api_token");
        _prefs.remove("id");
        _prefs.remove("active");
        _prefs.remove("photo");
      }
    });
  }

  Future _loadData() async {
    String x = await _prefs.get('api_token');

    print('api_token=$x');
    print(
        'isLocationServiceEnabled=${await Geolocator.isLocationServiceEnabled()}');
    print(
        'Provider.of<SharedPref>(context, listen: false).countryId=${Provider.of<SharedPref>(context, listen: false).countryId}');
    // if (!await Geolocator.isLocationServiceEnabled()) {
    //   print('nit ');
    //   Navigator.of(context).pushAndRemoveUntil(
    //     MaterialPageRoute(builder: (c) => RequestLocationScreen()),
    //     (route) => false,
    //   );

    //   return false;
    // } else
    if (x == null) {
      print('go to login');
      bool lang = await _prefs.get('lang');
      bool country = await _prefs.get('countryV');
      print('lang=$lang');
      print('country=$country');
      if (Provider.of<SharedPref>(context, listen: false).countryId != null) {
        if (lang == true) {
          //     await _prefs.remove('countryV');
          if (_model != null &&
              _model.data.activation == 0 &&
              _model2 != null &&
              _model2.data.activation == 0) {
            _prefs.setBool('NoAdsNoAuctions', true);
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (context) => NoAuctionOrAdTabScreen()),
                (Route<dynamic> route) => false);
          } else if (_model != null && _model.data.activation == 0) {
            _prefs.setBool('Ads', true);
            await _prefs.remove('Auctions');
            await _prefs.remove('NoAdsNoAuctions');
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => AdTabScreen()),
                (Route<dynamic> route) => false);
          } else if (_model2 != null && _model2.data.activation == 0) {
            _prefs.setBool('Auctions', true);
            await _prefs.remove('Ads');
            await _prefs.remove('NoAdsNoAuctions');
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => AuctionTabScreen()),
                (Route<dynamic> route) => false);
          } else {
            await _prefs.remove('Auctions');
            await _prefs.remove('Ads');
            await _prefs.remove('NoAdsNoAuctions');
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => TabScreen()),
                (Route<dynamic> route) => false);
          }
        } else {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => LanguageSelect()),
              (Route<dynamic> route) => false);
        }
      } else {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => CountrySelection()),
            (Route<dynamic> route) => false);
      }
    } else {
      if (await _prefs.get('active') == 0) {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (_) => WaitingAccepting()));
      }
      if (_model != null &&
          _model.data.activation == 0 &&
          _model2 != null &&
          _model2.data.activation == 0) {
        _prefs.setBool('NoAdsNoAuctions', true);
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => NoAuctionOrAdTabScreen()),
            (Route<dynamic> route) => false);
      } else if (_model != null && _model.data.activation == 0) {
        _prefs.setBool('Ads', true);
        await _prefs.remove('Auctions');
        await _prefs.remove('NoAdsNoAuctions');
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => AdTabScreen()),
            (Route<dynamic> route) => false);
      } else if (_model2 != null && _model2.data.activation == 0) {
        _prefs.setBool('Auctions', true);
        await _prefs.remove('Ads');
        await _prefs.remove('NoAdsNoAuctions');
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => AuctionTabScreen()),
            (Route<dynamic> route) => false);
      } else {
        await _prefs.remove('Auctions');
        await _prefs.remove('Ads');
        await _prefs.remove('NoAdsNoAuctions');
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => TabScreen()),
            (Route<dynamic> route) => false);
      }
    }
  }

  @override
  void initState() {
    FirebaseNotifications().setUpFirebase(widget.navigator);
    // _getShared();
//    Provider.of<SettingProvider>(context, listen: false).getUserData(context);

    //_loadData();
    super.initState();
  }

  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  @override
  void didChangeDependencies() async {
    // FirebaseNotifications().setUpFirebase(widget.navigator);

    await _getShared();

    await _loadData();
    // await Provider.of<MapHelper>(context, listen: false).getLocation();
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      backgroundColor: Color.fromRGBO(84, 189, 255, 1),
      body: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(""),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'images/logo.png',
                      height: 200,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      Provider.of<GetAppLogoProvider>(context, listen: false)
                                  .name !=
                              null
                          ? '${localization.text('welcome')}\t${Provider.of<GetAppLogoProvider>(context, listen: false).name}'
                          : '${localization.text('welcome')}',
                      style: TextStyle(fontSize: 15, color: Colors.white),
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
