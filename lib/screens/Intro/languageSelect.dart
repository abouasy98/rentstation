import 'package:flutter/material.dart';
import 'package:rentstation/Components/customBtn.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/provider/choseLangProvider.dart';
import 'package:rentstation/screens/Intro/SplashScreen.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Widgets/selectCard.dart';

class LanguageSelect extends StatefulWidget {
  static bool selectLang = false;
  final bool changelang;

  const LanguageSelect({Key key, this.changelang}) : super(key: key);
  @override
  _LanguageSelectState createState() => _LanguageSelectState();
}

class _LanguageSelectState extends State<LanguageSelect> {
  // ignore: unused_field
  List<CategoryModel> _countries = [];
  List<CategoryModel> _lang = [];

  @override
  void initState() {
    super.initState();

    _lang.add(new CategoryModel(
        id: 1, label: 'العربية', image: 'images/eg.png', selected: false));
    _lang.add(new CategoryModel(
      id: 2,
      label: 'English',
      selected: false,
      image: 'images/en.jpg',
    ));
  }

  SharedPreferences _prefs;
  @override
  Widget build(BuildContext context) {
    return Directionality(
          textDirection: localization.currentLanguage.toString() == "en"
            ? TextDirection.rtl
            : TextDirection.ltr,
          child: Scaffold(
        body: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView(
                children: <Widget>[
                  SizedBox(height: 50),
                  SizedBox(
                    height: 50,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Text('إختر اللغة',
                        textAlign: TextAlign.right,
                        style: TextStyle(color: Theme.of(context).primaryColor)),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.grey[200],
                      ),
                      child: SelectCard(
                        list: _lang,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(30),
                    child: CustomBtn(
                      color: Theme.of(context).primaryColor,
                      txtColor: Colors.white,
                      onTap:
                          Provider.of<ChoseLangeProvider>(context).selectLang ==
                                  true
                              ? () async {
                                  _prefs = await SharedPreferences.getInstance();
                                  await _prefs.setBool("lang", true);
                                  await localization.setNewLanguage(
                                      Provider.of<ChoseLangeProvider>(context,
                                              listen: false)
                                          .lang,
                                      true);

                                  setState(() {
                                    Provider.of<ChoseLangeProvider>(context,
                                            listen: false)
                                        .selectLang = false;
                                  });
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Splash()));
                                }
                              : null,
                      text: localization.text("save"),
                    ),
                  )
                ],
              ),
            ),
            Visibility(
              visible: widget.changelang != null,
              child: Positioned(
                top: 40,
                left: 20,
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.black87,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CategoryModel {
  int id;
  String label;
  bool selected;
  String image;

  CategoryModel({this.id, this.label, this.selected, this.image});
}