// import 'dart:async';
// import 'package:connectivity/connectivity.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_app_badger/flutter_app_badger.dart';
// import 'package:herag/helpers/sharedPref_helper.dart';
// import 'package:herag/models/get/adActvionsmodel.dart';
// import 'package:herag/models/get/auctionsActivationModel.dart';
// import 'package:herag/provider/get/adsActiviationProvider.dart';
// import 'package:herag/provider/get/auctionsActivationProvider.dart';
// import 'package:herag/screens/login.dart';
// import 'package:herag/widgets/Connection/check_connection_screen.dart';
// import 'package:provider/provider.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import '../../Components/custom_new_dialog.dart';
// import '../../Repository/firebaseNotifications.dart';
// import '../adTabBarScreen.dart';
// import '../auctionTabScreen.dart';
// import './waitingScreen.dart';
// import '../tab_bar.dart';
// import 'languageSelect.dart';

// class Splash extends StatefulWidget {
//   final GlobalKey<NavigatorState> navigator;

//   const Splash({Key key, this.navigator}) : super(key: key);

//   @override
//   _SplashState createState() => _SplashState();
// }

// class _SplashState extends State<Splash> with SingleTickerProviderStateMixin {
//   AnimationController controller;

//   Animation animation;

//   SharedPreferences _preferences;
//   AuctionsActivationModel _model;
// AdActivitionmodelDart _model2;
//   _getShared() async {
//     _preferences = await SharedPreferences.getInstance();
//     await Provider.of<GetAuctionsActivationProvider>(context, listen: false)
//         .getAuctionsActivationProvider()
//         .then((res) {
//       setState(() {
//         _model = res;
//       });
//     });
//     await Provider.of<GetAdsActivationProvider>(context, listen: false)
//         .getAuctionsActivationProvider()
//         .then((res) {
//       setState(() {
//         _model2 = res;
//       });
//     });
//     _onDoneLoading();
//   }

//   Future<Timer> _loadData() async {
//     return Timer(Duration(seconds: 3), checkConnection);
//   }

//   CustomDialog dialog = CustomDialog();

//   checkConnection() async {
//     final result = await Connectivity().checkConnectivity();
//     if (result == ConnectivityResult.none) {
//       Navigator.of(context).pushAndRemoveUntil(
//           MaterialPageRoute(
//               builder: (_) => CheckConnectionScreen(
//                     state: false,
//                   )),
//           (Route<dynamic> route) => false);
//     } else if (result == ConnectivityResult.mobile) {
//       _getShared();
//     } else if (result == ConnectivityResult.wifi) {
//       _getShared();
//     }
//   }

//   _onDoneLoading() async {
//     String x = await _preferences.get('api_token');
//     print('api_token=$x');
//     if (x == null) {
//       print('go to login');
//       bool lang = await _preferences.get('lang');
//       print('lang=$lang');
//       if (lang == true) {
//         Navigator.of(context).pushAndRemoveUntil(
//             MaterialPageRoute(builder: (context) => SignInScreen()),
//             (Route<dynamic> route) => false);
//       } else
//         Navigator.of(context).pushReplacement(
//             MaterialPageRoute(builder: (context) => LanguageSelect()));
//     } else {
//       if (await _preferences.get('active') == 0) {
//         Navigator.of(context).pushReplacement(
//             MaterialPageRoute(builder: (_) => WaitingAccepting()));
//       } else if (_model.data.activation == 0 ) {
//         Navigator.of(context).pushAndRemoveUntil(
//             MaterialPageRoute(builder: (context) => AdTabScreen()),
//             (Route<dynamic> route) => false);
//       } 
      
//       else if( _model2.data.activation==0){

//         Navigator.of(context).pushAndRemoveUntil(
//             MaterialPageRoute(builder: (context) => AuctionTabScreen()),
//             (Route<dynamic> route) => false);
//       }
//       else {
//         Navigator.of(context).pushAndRemoveUntil(
//             MaterialPageRoute(builder: (context) => TabScreen()),
//             (Route<dynamic> route) => false);
//       }
//     }
//   }


//   _initPlatformState() async {
//     bool res = await FlutterAppBadger.isAppBadgeSupported();
//     if (res) {
//       print('Supported');
//     } else {
//       print('Not supported');
//     }
//     if (!mounted) return;
//   }

//   @override
//   void initState() {
//     Provider.of<SharedPref>(context, listen: false).getSharedHelper(_preferences);
//     FirebaseNotifications().setUpFirebase(widget.navigator);
//     _initPlatformState();
//     _loadData();
//     super.initState();
//   }

//   @override
//   void didChangeDependencies() {
//     super.didChangeDependencies();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: Container(
//         decoration: BoxDecoration(
//             image: DecorationImage(
//                 image: AssetImage('images/1.jpg'), fit: BoxFit.fill)),
//         width: MediaQuery.of(context).size.width,
//         height: MediaQuery.of(context).size.height,
//       ),
//     );
//   }
// }
