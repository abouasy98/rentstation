import 'dart:io';
import 'dart:typed_data';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rentstation/Components/customBtn.dart';
import 'package:rentstation/Components/custom_new_dialog.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/get/getAdbyIdModel.dart';
import 'package:rentstation/provider/get/addAdToFavProvider.dart';
import 'package:rentstation/provider/get/getAdByIdProvider.dart';
import 'package:rentstation/provider/get/getAdByIdWithoutAuth.dart';
import 'package:rentstation/provider/post/adReportProvider.dart';
import 'package:rentstation/provider/post/addReplyProvider.dart';
import 'package:rentstation/provider/post/createMessageProvider.dart';
import 'package:rentstation/screens/login.dart';
import 'package:rentstation/screens/newsPhotoGallary.dart';
import 'package:rentstation/screens/userProfile.dart';
import 'package:intl/intl.dart' as an;
import 'package:provider/provider.dart';
import 'package:rentstation/screens/addComment.dart';
import 'package:rentstation/widgets/custom_bottom_sheet.dart';
import 'package:rentstation/widgets/register_text_field.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

class AdDetails extends StatefulWidget {
  final int id;

  const AdDetails({Key key, this.id}) : super(key: key);

  @override
  _AdDetailsState createState() => _AdDetailsState();
}

class _AdDetailsState extends State<AdDetails> {
  GetAdbyIdModel _model;
  String token;
  bool hiddenNumber = true;
  var fifteenAgo;
  CustomDialog _dialog = CustomDialog();
  List<String> photos = [];
  _getShared() async {
    if (Provider.of<SharedPref>(context, listen: false).token != null) {
      Provider.of<GetAddsByIdProvider>(context, listen: false)
          .getAdds(widget.id,
              Provider.of<SharedPref>(context, listen: false).token, context)
          .then((res) {
        setState(() {
          _model = res;
        });
        for (int i = 0; i < _model.data.photos.length; i++) {
          photos.add(_model.data.photos[i].photo);
        }
      });
    } else {
      Provider.of<GetAddsByIdWithoutAuthProvider>(context, listen: false)
          .getAdds(widget.id, context)
          .then((res) {
        setState(() {
          _model = res;
        });
        for (int i = 0; i < _model.data.photos.length; i++) {
          photos.add(_model.data.photos[i].photo);
        }
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _createDynamicLink();

    _getShared();
  }

  String shareLink;
  Future<void> _createDynamicLink() async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://rentstation.page.link',
      link: Uri.parse('https://rentstation.page.link/1/${widget.id}'),
      androidParameters: AndroidParameters(
        packageName: 'com.tqnee.rentstation',
        minimumVersion: 0,
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      iosParameters: IosParameters(
        bundleId: 'com.tqnee.rentstation',
        minimumVersion: "4",
      ),
    );

    Uri url;
    final ShortDynamicLink shortLink = await parameters.buildShortLink();
    url = shortLink.shortUrl;
    setState(() {
      shareLink = url.toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: localization.currentLanguage.toString() == "en"
          ? TextDirection.ltr
          : TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text(localization.text('ad_details')),
          centerTitle:
              Provider.of<SharedPref>(context, listen: false).token != null
                  ? false
                  : true,
          backgroundColor: Theme.of(context).primaryColor,
          leading: InkWell(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back_ios),
          ),
          actions: <Widget>[
            Visibility(
              visible:
                  Provider.of<SharedPref>(context, listen: false).token != null,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  radius: 15,
                  backgroundColor: Colors.red[400],
                  child: IconButton(
                    iconSize: 15,
                    onPressed: () async {
                      CustomBottomSheet().show(
                          context: context,
                          child: Padding(
                            padding: const EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                BlockTextField(
                                  onChange: (v) {
                                    Provider.of<AdReportProvider>(context,
                                            listen: false)
                                        .blockReason = v;
                                  },
                                  label: localization.text('report_Reason'),
                                  icon: Icons.label,
                                  type: TextInputType.text,
                                ),
                                SizedBox(height: 20),
                                CustomBtn(
                                  text: localization.text('confirm_report'),
                                  color: Colors.red,
                                  onTap: () async {
                                    Navigator.pop(context);
                                    await Provider.of<AdReportProvider>(context,
                                            listen: false)
                                        .sendMessage(
                                            widget.id,
                                            Provider.of<SharedPref>(context,
                                                    listen: false)
                                                .token,
                                            context);
                                  },
                                  txtColor: Colors.white,
                                )
                              ],
                            ),
                          ));
                    },
                    icon: Icon(
                      Icons.priority_high,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                  onPressed: () async {
                    String _msg;
                    StringBuffer _sb = new StringBuffer();
                    setState(() {
                      _sb.write("عنوان الاعلان : ${_model.data.title} \n");

                      _sb.write(shareLink);

                      _msg = _sb.toString();
                    });

                    if (_model.data.photos.length != 0) {
                      var request = await HttpClient()
                          .getUrl(Uri.parse(_model.data.photos[0].photo));
                      var response = await request.close();
                      Uint8List bytes =
                          await consolidateHttpClientResponseBytes(response);
                      await Share.file(
                          'ESYS AMLOG', 'amlog.jpg', bytes, 'image/jpg',
                          text: _msg);
                    } else {
                      Share.text("title", _msg, 'text/plain');
                    }
                  },
                  icon: Icon(Icons.share),
                )),
            IconButton(
              onPressed: () {
                _saveForm();
              },
              icon: _model == null
                  ? Center(
                      child: SpinKitThreeBounce(
                        size: 10,
                        color: Theme.of(context).primaryColor,
                      ),
                    )
                  : Icon(
                      _model.data.favorite == '1'
                          ? Icons.favorite
                          : Icons.favorite_border,
                      color: Colors.red,
                    ),
            ),
          ],
        ),
        backgroundColor: Colors.white,
        floatingActionButton: _model == null
            ? Container()
            : SpeedDial(
                /// both default to 16
                marginEnd: 18,
                marginBottom: 20,
                // animatedIcon: AnimatedIcons.menu_close,
                // animatedIconTheme: IconThemeData(size: 22.0),
                /// This is ignored if animatedIcon is non null
                icon: Icons.call,
                activeIcon: Icons.call,
                // iconTheme: IconThemeData(color: Colors.grey[50], size: 30),
                /// The label of the main button.
                // label: Text("Open Speed Dial"),
                /// The active label of the main button, Defaults to label if not specified.
                // activeLabel: Text("Close Speed Dial"),
                /// Transition Builder between label and activeLabel, defaults to FadeTransition.
                // labelTransitionBuilder: (widget, animation) => ScaleTransition(scale: animation,child: widget),
                /// The below button size defaults to 56 itself, its the FAB size + It also affects relative padding and other elements
                buttonSize: 56.0,
                visible: true,

                /// If true user is forced to close dial manually
                /// by tapping main button and overlay is not rendered.
                closeManually: false,

                /// If true overlay will render no matter what.
                renderOverlay: false,
                curve: Curves.bounceIn,
                overlayColor: Colors.black,
                overlayOpacity: 0.5,
                onOpen: () => print('OPENING DIAL'),
                onClose: () => print('DIAL CLOSED'),
                tooltip: 'Speed Dial',
                heroTag: 'speed-dial-hero-tag',
                backgroundColor: Colors.blue,
                foregroundColor: Colors.white,
                elevation: 8.0,
                shape: CircleBorder(),
                // orientation: SpeedDialOrientation.Up,
                // childMarginBottom: 2,
                // childMarginTop: 2,
                children: [
                  SpeedDialChild(
                    child: Icon(
                      Icons.phone_iphone,
                      color: Colors.blue,
                    ),
                    backgroundColor: Colors.white,
                    label: _model.data.phoneNumber,
                    labelStyle: TextStyle(fontSize: 18.0),
                    onTap: () {
                      UrlLauncher.launch(
                          'tel: ${_model.data.phoneNumber.toString()}');
                    },
                    onLongPress: () => print('FIRST CHILD LONG PRESS'),
                  ),
                  SpeedDialChild(
                    child: Icon(
                      Icons.copy,
                      color: Colors.blue,
                    ),
                    backgroundColor: Colors.white,
                    label: localization.text("copy_a_number"),
                    labelStyle: TextStyle(fontSize: 18.0),
                    onTap: () {
                      Clipboard.setData(
                          new ClipboardData(text: _model.data.phoneNumber));
                    },
                    onLongPress: () => print('SECOND CHILD LONG PRESS'),
                  ),
                ],
              ),
        body: _model == null
            ? Center(
                child: SpinKitThreeBounce(
                  size: 25,
                  color: Theme.of(context).primaryColor,
                ),
              )
            : (_model.data.archive == 1 &&
                    _model.data.userId !=
                        Provider.of<SharedPref>(context, listen: false).id)
                ? Center(
                    child: Text(
                    localization.text('This_ad_is_archived'),
                    style: TextStyle(color: Colors.black),
                  ))
                : ListView(
                    children: <Widget>[
                      // SizedBox(height: 20),
                      // Directionality(
                      //   textDirection: TextDirection.rtl,

                      //     child: Padding(
                      //       padding: const EdgeInsets.only(right: 20, left: 10),
                      //       child: Text(
                      //         _model.data.title,
                      //         style: TextStyle(fontSize: 20),
                      //         textAlign: TextAlign.right,
                      //       ),
                      //     ),

                      // ),
                      // SizedBox(height: 10),
                      Stack(children: [
                        _imageSlider(),
                        if (_model.data.priceStatus == '1')
                          Positioned(
                            child: Container(
                              color: Colors.red[400],
                              child: Center(
                                  child: FittedBox(
                                child: Text(
                                  localization.text('price_status2'),
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              )),
                            ),
                            width: 35,
                            height: 80,
                            left: 10,
                          )
                      ]),
                      SizedBox(height: 20),
                      Container(
                        alignment: Alignment.topCenter,
                        padding: EdgeInsets.only(left: 10, right: 10),
                        // decoration: BoxDecoration(
                        //   color: Colors.black54,
                        // ),
                        height: MediaQuery.of(context).size.height * 0.18,
                        // child: Padding(
                        //   padding: const EdgeInsets.all(15.0),
                        child: Column(
                          children: [
                            Text(
                                '${an.NumberFormat.currency(locale: localization.currentLanguage.toString() == "en" ? null : 'ar_EG', name: _model.data.currency, decimalDigits: 0).format(_model.data.price)}',
                                style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54),
                                textAlign: TextAlign.center),
                            SizedBox(
                              height: 10,
                            ),
                            Card(
                              elevation: 2,
                              child: Padding(
                                padding: EdgeInsets.only(
                                  top: 10,
                                  bottom: 10,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(top: 10),
                                      child: Row(
                                        children: [
                                          Icon(Icons.location_on,
                                              size: 15, color: Colors.blue),
                                          Container(
                                            alignment: localization
                                                        .currentLanguage
                                                        .toString() ==
                                                    "en"
                                                ? Alignment.centerLeft
                                                : Alignment.centerRight,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.3,
                                            child: Text(_model.data.city,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.black)),
                                          ),
                                        ],
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        if (Provider.of<SharedPref>(context,
                                                    listen: false)
                                                .token ==
                                            null) {
                                          _dialog.showOptionDialog(
                                              context: context,
                                              msg: localization.text('auth'),
                                              okFun: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (c) =>
                                                            SignInScreen(true)));
                                              },
                                              okMsg: localization.text('yes'),
                                              cancelMsg:
                                                  localization.text('no'),
                                              cancelFun: () {
                                                return;
                                              });
                                        } else {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      UserProfile(
                                                        userid:
                                                            _model.data.userId,
                                                      )));
                                        }
                                      },
                                      child: Row(
                                        children: [
                                          Container(
                                            alignment: localization
                                                        .currentLanguage
                                                        .toString() ==
                                                    "en"
                                                ? Alignment.centerRight
                                                : Alignment.centerLeft,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.3,
                                            child: Text(_model.data.user,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.black)),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          _model.data.userPhoto != null
                                              ? CachedNetworkImage(
                                                  imageUrl:
                                                      _model.data.userPhoto,
                                                  fadeInDuration:
                                                      Duration(seconds: 2),
                                                  placeholder: (context, url) =>
                                                      CircleAvatar(
                                                          backgroundImage:
                                                              AssetImage(
                                                                  'images/16.jpg')),
                                                  imageBuilder:
                                                      (context, provider) {
                                                    return CircleAvatar(
                                                        backgroundImage:
                                                            provider);
                                                  },
                                                )
                                              : CircleAvatar(
                                                  backgroundImage: AssetImage(
                                                      'images/16.jpg'))
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        // ),
                      ),

                      Directionality(
                        textDirection: TextDirection.rtl,
                        child: Container(
                          alignment: Alignment.topRight,
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            // mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                (_model.data.title),
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                                textAlign: TextAlign.right,

                                // collapseText: 'show less',
                                // expandText: 'show more',
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                alignment: Alignment.topRight,
                                child: Text(
                                  _model.data.description,
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.grey[900]),
                                  textAlign: TextAlign.right,
                                  maxLines: 1,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),

                      SizedBox(height: 100),
                    ],
                  ),
        bottomNavigationBar: _model == null
            ? Center(
                child: SpinKitThreeBounce(
                  size: 25,
                  color: Theme.of(context).primaryColor,
                ),
              )
            : (_model.data.archive == 1 &&
                    _model.data.userId !=
                        Provider.of<SharedPref>(context, listen: false).id)
                ? Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                  )
                : Card(
                    elevation: 10,
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            onTap: () async {
                              if (Provider.of<SharedPref>(context,
                                          listen: false)
                                      .token ==
                                  null) {
                                await _dialog.showOptionDialog(
                                    context: context,
                                    msg: localization.text('should_login'),
                                    okFun: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (c) => SignInScreen(true)));
                                    },
                                    okMsg: localization.text('ok'),
                                    cancelMsg: localization.text('no'),
                                    cancelFun: () {
                                      return;
                                    });
                              } else {
                                Provider.of<CreateMessageProvider>(context,
                                        listen: false)
                                    .createMessage(
                                        _model.data.userId,
                                        Provider.of<SharedPref>(context,
                                                listen: false)
                                            .token,
                                        context);
                              }
                            },
                            child: AutoSizeText(
                              localization.text('Contact_the_seller'),
                              softWrap: true,
                              style: TextStyle(
                                  color: Colors.lightBlue, fontSize: 15
                                  //fontWeight: FontWeight.bold,
                                  ),
                            ),
                          ),
                          VerticalDivider(
                            thickness: 2,
                            width: 15,
                            color: Colors.black87,
                          ),
                          InkWell(
                            onTap: () {
                              if (Provider.of<SharedPref>(context,
                                          listen: false)
                                      .token ==
                                  null) {
                                _dialog.showOptionDialog(
                                    context: context,
                                    msg: localization.text('should_login'),
                                    okFun: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (c) => SignInScreen(true)));
                                    },
                                    okMsg: localization.text('ok'),
                                    cancelMsg: localization.text('no'),
                                    cancelFun: () {
                                      return;
                                    });
                              } else {
                                if (Provider.of<AddReplyProvider>(context,
                                                listen: false)
                                            .reply
                                            .length >
                                        0 ||
                                    Provider.of<AddReplyProvider>(context,
                                                listen: false)
                                            .reply !=
                                        null) {
                                  Provider.of<AddReplyProvider>(context,
                                          listen: false)
                                      .clear();
                                }
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => AddComment(
                                              id: widget.id,
                                            )));
                              }
                            },
                            child: AutoSizeText(
                              localization.text('comments'),
                              softWrap: true,
                              style: TextStyle(
                                  color: Colors.lightBlue, fontSize: 15
                                  //fontWeight: FontWeight.bold,
                                  ),
                            ),
                          ),
                        ],
                      ),
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 7,
                              color: Colors.grey[100],
                              offset: Offset(0, 3),
                              spreadRadius: 4,
                            ),
                          ],
                          //color: Colors.red[300],
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: <Color>[
                                Colors.white70,
                                Colors.grey[100],
                                Colors.grey[100],
                                Colors.white70,
                              ]),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20))),
                    ),
                  ),
      ),
    );
  }

  Widget _imageSlider() {
    return InkWell(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => NewsPhotoGallary(
                    images: photos,
                  ))),
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.3,
            child: Carousel(
              boxFit: BoxFit.cover,
              images: List.generate(_model.data.photos.length, (int index) {
                return _model.data.photos[index].photo != null
                    ? CachedNetworkImage(
                        imageUrl: _model.data.photos[index].photo,
                        fadeInDuration: Duration(seconds: 2),
                        placeholder: (context, url) => Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('images/loading.gif'),
                                    fit: BoxFit.fill),
                              ),
                            ),
                        imageBuilder: (context, provider) {
                          return Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: provider, fit: BoxFit.fill),
                            ),
                          );
                        })
                    : Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('images/16.png'),
                              fit: BoxFit.fill),
                        ),
                      );
              }),
              autoplay: false,
              dotSize: 4,
              dotSpacing: _model.data.photos.length > 13
                  ? 300 / _model.data.photos.length
                  : 20,
              autoplayDuration: Duration(seconds: 1),
              dotBgColor: Colors.black26,
              animationCurve: Curves.decelerate,
              animationDuration: Duration(milliseconds: 1000),
              indicatorBgPadding: 10,
              dotColor: Colors.white,
            ),
          ),
          Positioned(
            bottom: 30,
            child: Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              width: MediaQuery.of(context).size.width * .35,
              color: Colors.red,
              child: Row(
                  //textDirection: TextDirection.ltr,
                  mainAxisAlignment: MainAxisAlignment.start,
                  //crossAxisAlignment: CrossAxisAlignment. center,
                  children: [
                    Text(
                        '${localization.text('ago')} ${DateTime.now().difference(DateTime(_model.data.createdAt.year, _model.data.createdAt.month, _model.data.createdAt.day)).inDays} ${localization.text('day')}',
                        style: TextStyle(fontSize: 15, color: Colors.white),
                        textAlign: TextAlign.right),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(
                      Icons.timer,
                      color: Colors.white,
                    ),
                  ]),
            ),
          ),
        ],
      ),
    );
  }

  _saveForm() async {
    if (Provider.of<SharedPref>(context, listen: false).token == null) {
      await _dialog.showOptionDialog(
          context: context,
          msg: localization.text('auth'),
          okFun: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (c) => SignInScreen(true)));
          },
          okMsg: localization.text('yes'),
          cancelMsg: localization.text('no'),
          cancelFun: () {
            return;
          });
    } else {
      Provider.of<AddAdToFavProvider>(context, listen: false).addAdToFav(
          Provider.of<SharedPref>(context, listen: false).token,
          widget.id,
          context);
    }
  }
}
