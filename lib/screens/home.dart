import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/helpers/map_helper.dart';
import 'package:rentstation/helpers/sharedPref_helper.dart';
import 'package:rentstation/models/get/getCitiesModel.dart';
import 'package:rentstation/models/get/getFavModel.dart';
import 'package:rentstation/models/post/getAdsFilterByCity.dart';
import 'package:rentstation/provider/get/GetAppLogoProvider.dart';
import 'package:rentstation/provider/get/categoriesProvider.dart';
import 'package:rentstation/provider/get/getCitiesByIdProvider.dart';
import 'package:rentstation/provider/get/subCategoriesProvider.dart';
import 'package:rentstation/provider/post/SearchstatisticsProvider.dart';
import 'package:rentstation/provider/post/getAdsByCityFilter.dart';
import 'package:rentstation/screens/createAdScreen.dart';
import 'package:rentstation/screens/login.dart';
import 'package:provider/provider.dart';
import '../widgets/Adsbody.dart';

class Home extends StatefulWidget {
  bool auctions;
  Home({this.auctions});
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String query = "";
  GetFavModel getFavModel;
  CategoriesProvider categoryProvider;
  SubCategoriesProvider subCategories;
  // CustomDialog _dialog = CustomDialog();
  // GetAdsModel getAddsModel;
  GetAddsByCityFilter getAddsByFilterModel;
  GetCities getCitiesByCountryIdModel;
  TextEditingController _searchController = TextEditingController();
  _getShared() async {
    Provider.of<GetAddsByCityFilterProvider>(context, listen: false).countryId =
        Provider.of<SharedPref>(context, listen: false).countryId;
    //  getAddsModel = null;
    getAddsByFilterModel = null;
    if (Provider.of<MapHelper>(context, listen: false).position == null) {
      await Provider.of<MapHelper>(context, listen: false).getLocation();
    }
    await Provider.of<GetCitiesByCountryIdProvider>(context, listen: false)
        .getCities(
            Provider.of<SharedPref>(context, listen: false).countryId, context)
        .then((res) {
      setState(() {
        getCitiesByCountryIdModel = res;
      });
    });
    await Provider.of<GetAddsByCityFilterProvider>(context, listen: false)
        .getAdds(
            Provider.of<MapHelper>(context, listen: false).position != null
                ? Provider.of<MapHelper>(context, listen: false)
                    .position
                    .latitude
                : 30.7982574,
            Provider.of<MapHelper>(context, listen: false).position != null
                ? Provider.of<MapHelper>(context, listen: false)
                    .position
                    .longitude
                : 31.0066411,
            Provider.of<SharedPref>(context, listen: false).token,
            context)
        .then((res) {
      setState(() {
        getAddsByFilterModel = res;
      });
    });
    // if (Provider.of<SharedPref>(context, listen: false).token != null) {
    //   await Provider.of<GetAddsProvider>(context, listen: false)
    //       .getAdds(
    //           Provider.of<SharedPref>(context, listen: false).token, context)
    //       .then((res) {
    //     setState(() {
    //       getAddsModel = res;
    //     });
    //   });
    // } else {
    //   await Provider.of<GetAddswithoutAuthProvider>(context, listen: false)
    //       .getAdds(context)
    //       .then((res) {
    //     setState(() {
    //       getAddsModel = res;
    //     });
    //   });
    // }
  }

  Widget searchWidget() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.05,
      decoration: BoxDecoration(
        color: Colors.blueAccent[700],
      ),
      child: Directionality(
          key: widget.key,
          textDirection: TextDirection.rtl,
          child: Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: TextFormField(
              style: TextStyle(color: Colors.white, fontSize: 18),
              controller: _searchController,
              textAlign: localization.currentLanguage.toString() == "en"
                  ? TextAlign.left
                  : TextAlign.right,
              textDirection: TextDirection.rtl,
              onChanged: (val) {
                setState(() {
                  category = 'الكل';
                  query = val.trim();
                });
              },
              decoration: InputDecoration(
                focusedBorder: InputBorder.none,
                border: InputBorder.none,
                enabledBorder: InputBorder.none,
                hintText:
                    '${localization.text("search_in_Herag_Tqnee")}\t${Provider.of<GetAppLogoProvider>(context, listen: false).name}',
                hintStyle: TextStyle(color: Colors.white, fontSize: 14),
                suffixIcon: Container(
                  child: query != ''
                      ? IconButton(
                          iconSize: 20,
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            WidgetsBinding.instance.addPostFrameCallback(
                                (_) => _searchController.clear());
                            setState(() {
                              query = "";
                            });
                          },
                        )
                      : Icon(
                          Icons.search,
                          size: 20,
                          color: Colors.white,
                        ),
                ),
              ),
            ),
          )),
    );
  }

  bool isInit = true;

  @override
  void didChangeDependencies() async {
    if (isInit) {
      await Provider.of<CategoriesProvider>(context, listen: false)
          .getCategories(context);

      categoryProvider =
          Provider.of<CategoriesProvider>(context, listen: false);
      subCategories =
          Provider.of<SubCategoriesProvider>(context, listen: false);
      await _getShared();

      setState(() {
        isInit = false;
      });
    }
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  String category = 'الكل';
  int categoryId;
  int subCategoryId;
  String subCategory = '';
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return SafeArea(
      child: Directionality(
        textDirection: localization.currentLanguage.toString() == "en"
            ? TextDirection.ltr
            : TextDirection.rtl,
        child: isInit
            ? SpinKitThreeBounce(
                color: Theme.of(context).primaryColor,
                size: 22,
              )
            : Scaffold(
                //  resizeToAvoidBottomPadding: false,
                //     resizeToAvoidBottomInset: false,
                appBar: PreferredSize(
                  preferredSize: query == ""
                      ? Size.fromHeight(
                          MediaQuery.of(context).size.height * 0.2)
                      : Size.fromHeight(
                          MediaQuery.of(context).size.height * 0.05),
                  child: AppBar(
                    leading: Container(),
                    backgroundColor: Colors.white,
                    flexibleSpace: searchWidget(),
                    bottom: PreferredSize(
                      preferredSize: mediaQuery,
                      child: query == ""
                          ? Container(
                              height: mediaQuery.height * 0.15,
                              width: mediaQuery.width,
                              child: Padding(
                                padding:
                                    localization.currentLanguage.toString() ==
                                            "en"
                                        ? const EdgeInsets.only(left: 10.0)
                                        : const EdgeInsets.only(right: 10.0),
                                child: ListView(
                                    primary: false,
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            category = 'الكل';
                                          });
                                        },
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.25,
                                          child: Center(
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Card(
                                                  elevation: 10,
                                                  child: Card(
                                                      elevation: 10,
                                                      child: FadeInImage(
                                                        image: AssetImage(
                                                            'images/1.jpg'),
                                                        width: 75,
                                                        height: 53,
                                                        fit: BoxFit.fill,
                                                        fadeInDuration:
                                                            Duration(
                                                                seconds: 2),
                                                        placeholder: AssetImage(
                                                            'images/loading.gif'),
                                                      )),
                                                ),
                                                Text(
                                                  localization.text('all'),
                                                  style: TextStyle(
                                                      color: Color(0xff366775)),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      ListView.builder(
                                          primary: false,
                                          shrinkWrap: true,
                                          scrollDirection: Axis.horizontal,
                                          itemCount: categoryProvider
                                              .categories.length,
                                          itemBuilder: (ctx, i) {
                                            return GestureDetector(
                                              onTap: () async {
                                                setState(() {
                                                  category = categoryProvider
                                                      .categories[i].name;
                                                  print('category=$category');
                                                  categoryId = categoryProvider
                                                      .categories[i].id;
                                                  subCategory = '';
                                                });
                                                await subCategories
                                                    .getSubCategories(
                                                        categoryId, context);
                                                Provider.of<SearchstatisticsProvider>(
                                                        context,
                                                        listen: false)
                                                    .searchstatistics(
                                                        categoryProvider
                                                            .categories[i].id,
                                                        context);
                                              },
                                              child: Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.25,
                                                padding: localization
                                                            .currentLanguage
                                                            .toString() ==
                                                        "en"
                                                    ? const EdgeInsets.only(
                                                        left: 10.0)
                                                    : const EdgeInsets.only(
                                                        right: 10.0),
                                                decoration: BoxDecoration(
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: category ==
                                                              categoryProvider
                                                                  .categories[i]
                                                                  .name
                                                          ? Theme.of(context)
                                                              .primaryColor
                                                              .withOpacity(0.5)
                                                          : Colors.white,
                                                      spreadRadius: 0,
                                                      blurRadius: 0,
                                                      offset: Offset(0, 0),

                                                      // changes position of shadow
                                                    ),
                                                  ],
                                                  border: Border(
                                                    bottom: category ==
                                                            categoryProvider
                                                                .categories[i]
                                                                .name
                                                        ? BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColor,
                                                            width: 1.0,
                                                          )
                                                        : BorderSide(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColor,
                                                            width: 0.0,
                                                          ),
                                                  ),
                                                  // borderRadius: BorderRadius.circular(20),
                                                ),
                                                child: Center(
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Card(
                                                        elevation: 10,
                                                        child: Card(
                                                          elevation: 10,
                                                          child: categoryProvider
                                                                      .categories[
                                                                          i]
                                                                      .photo !=
                                                                  null
                                                              ? FadeInImage
                                                                  .assetNetwork(
                                                                  image: categoryProvider
                                                                      .categories[
                                                                          i]
                                                                      .photo,
                                                                  width: 75,
                                                                  height: 53,
                                                                  fit: BoxFit
                                                                      .fill,
                                                                  fadeInDuration:
                                                                      Duration(
                                                                          seconds:
                                                                              2),
                                                                  placeholder:
                                                                      'images/loading.gif',
                                                                  imageCacheHeight:
                                                                      500,
                                                                  imageCacheWidth:
                                                                      500,
                                                                )
                                                              : CircleAvatar(
                                                                  radius: 12,
                                                                  backgroundImage:
                                                                      AssetImage(
                                                                          'images/loading.gif'),
                                                                ),
                                                        ),
                                                      ),
                                                      Center(
                                                        child: Text(
                                                          categoryProvider
                                                              .categories[i]
                                                              .name,
                                                          style: TextStyle(
                                                              color: Color(
                                                                  0xff366775)),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            );
                                          }),
                                    ]),
                              ))
                          : Container(),
                    ),
                  ),
                ),
                floatingActionButton:
                    Provider.of<SharedPref>(context, listen: false).ads == true
                        ? Container()
                        : FloatingActionButton(
                            heroTag: null,
                            onPressed: () async {
                              if (Provider.of<SharedPref>(context,
                                          listen: false)
                                      .token ==
                                  null) {
                                // await _dialog.showOptionDialog(
                                //     context: context,
                                //     msg:  localization.text('should_login'),
                                //     okFun: () {
                                //       Navigator.push(
                                //           context,
                                //           MaterialPageRoute(
                                //               builder: (c) => SignInScreen()));
                                //     },
                                //     okMsg:  localization.text('yes'),
                                //     cancelMsg:  localization.text('no'),
                                //     cancelFun: () {
                                //       return;
                                //     });
                                pushNewScreen(
                                  context,
                                  screen: SignInScreen(true),
                                  withNavBar:
                                      false, // OPTIONAL VALUE. True by default.
                                  pageTransitionAnimation:
                                      PageTransitionAnimation.cupertino,
                                );
                              } else {
                                pushNewScreen(
                                  context,
                                  screen: CreateAdScreen(),
                                  withNavBar:
                                      false, // OPTIONAL VALUE. True by default.
                                  pageTransitionAnimation:
                                      PageTransitionAnimation.cupertino,
                                );
                                // Navigator.of(context).push(MaterialPageRoute(
                                //     builder: (context) => CreateAdScreen()));
                              }
                            },
                            child: Icon(Icons.add),
                          ),

                body: (category == 'الكل')
                    ? Container(
                        height: mediaQuery.height,
                        child: Adsbody(
                          key: UniqueKey(),
                          categoryId: categoryId,
                          // getAddsModel: getAddsModel,
                          getAddsByFilterModel: getAddsByFilterModel,
                          subCategoryId: subCategoryId,
                          query: query,
                          getCitiesByCountryIdModel: getCitiesByCountryIdModel,
                          category: category == null ? '' : category,
                          subCategory: subCategory,
                        ),
                      )
                    : SingleChildScrollView(
                        primary: false,
                        child: Column(
                          children: [
                            SizedBox(
                              height: mediaQuery.height * 0.01,
                            ),
                            Container(
                              height: mediaQuery.height * 0.07,
                              child: Consumer<SubCategoriesProvider>(
                                builder: (context, subCategoryData, _) =>
                                    ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount:
                                      subCategoryData.subcategories.length,
                                  itemBuilder: (ctx, i) => GestureDetector(
                                    onTap: () {
                                      if (subCategory ==
                                          subCategoryData
                                              .subcategories[i].name) {
                                        setState(() {
                                          subCategory = '';
                                        });
                                      } else {
                                        setState(() {
                                          subCategory = subCategoryData
                                              .subcategories[i].name;
                                          subCategoryId = subCategoryData
                                              .subcategories[i].id;
                                        });
                                        print('subcategory$subCategory');
                                      }
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(
                                        left: 6,
                                        right: 6,
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: subCategory ==
                                                subCategories
                                                    .subcategories[i].name
                                            ? Theme.of(context).primaryColor
                                            : Colors.grey[200],
                                      ),
                                      padding: EdgeInsets.all(10),
                                      child: Center(
                                        child: Text(
                                          subCategories.subcategories[i].name,
                                          style: TextStyle(
                                            color: subCategory ==
                                                    subCategories
                                                        .subcategories[i].name
                                                ? Colors.white
                                                : Colors.grey,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              height: mediaQuery.height,
                              child: Adsbody(
                                categoryId: categoryId,
                                subCategoryId: subCategoryId,
                                category: category == null ? '' : category,
                                key: UniqueKey(),
                                query: query,
                                subCategory: subCategory,
                                //   getAddsModel: getAddsModel,
                                getAddsByFilterModel: getAddsByFilterModel,
                                getCitiesByCountryIdModel:
                                    getCitiesByCountryIdModel,
                              ),
                            ),
                          ],
                        ),
                      ),
              ),
      ),
    );
  }
}
