// To parse this JSON data, do
//
//     final categoriesModels = categoriesModelsFromJson(jsonString);

import 'dart:convert';

CategoriesModels categoriesModelsFromJson(String str) => CategoriesModels.fromJson(json.decode(str));

String categoriesModelsToJson(CategoriesModels data) => json.encode(data.toJson());

class CategoriesModels {
    CategoriesModels({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    List<Datum> data;
    dynamic error;

    factory CategoriesModels.fromJson(Map<String, dynamic> json) => CategoriesModels(
        mainCode: json["mainCode"],
        code: json["code"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
    };
}

class Datum {
    Datum({
        this.id,
        this.name,
        this.photo,
        this.createdAt,
    });

    int id;
    String name;
    String photo;
    DateTime createdAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        name: json["name"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "photo": photo,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}
