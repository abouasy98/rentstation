// To parse this JSON data, do
//
//     final userBlockConversation = userBlockConversationFromJson(jsonString);

import 'dart:convert';

UserBlockConversation userBlockConversationFromJson(String str) => UserBlockConversation.fromJson(json.decode(str));

String userBlockConversationToJson(UserBlockConversation data) => json.encode(data.toJson());

class UserBlockConversation {
    UserBlockConversation({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    Data data;
    dynamic error;

    factory UserBlockConversation.fromJson(Map<String, dynamic> json) => UserBlockConversation(
        mainCode: json["mainCode"] == null ? null : json["mainCode"],
        code: json["code"] == null ? null : json["code"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode == null ? null : mainCode,
        "code": code == null ? null : code,
        "data": data == null ? null : data.toJson(),
        "error": error,
    };
}

class Data {
    Data({
        this.key,
        this.value,
    });

    String key;
    String value;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
    );

    Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
    };
}
