// To parse this JSON data, do
//
//     final getAuctionsByCityFilter = getAuctionsByCityFilterFromJson(jsonString);

import 'dart:convert';

GetAuctionsByCityFilter getAuctionsByCityFilterFromJson(String str) => GetAuctionsByCityFilter.fromJson(json.decode(str));

String getAuctionsByCityFilterToJson(GetAuctionsByCityFilter data) => json.encode(data.toJson());

class GetAuctionsByCityFilter {
    GetAuctionsByCityFilter({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    List<Datum> data;
   List<Error> error;

    factory GetAuctionsByCityFilter.fromJson(Map<String, dynamic> json) => GetAuctionsByCityFilter(
        mainCode: json["mainCode"] == null ? null : json["mainCode"],
        code: json["code"] == null ? null : json["code"],
        data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
         error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode == null ? null : mainCode,
        "code": code == null ? null : code,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
    };
}

class Datum {
    Datum({
        this.id,
        this.userId,
        this.user,
        this.userPhoto,
        this.email,
        this.phoneNumber,
        this.memberDate,
        this.countryId,
        this.country,
        this.cityId,
        this.city,
        this.categoryId,
        this.category,
        this.subCategoryId,
        this.subCategory,
        this.name,
        this.period,
        this.details,
        this.description,
        this.initialPrice,
        this.priceIncrease,
        this.currency,
        this.archive,
        this.prices,
        this.pinned,
        this.photos,
        this.auctionWinnerId,
        this.auctionWinner,
        this.auctionWinnerPhoto,
        this.heighPrice,
        this.createdAt,
    });

    int id;
    int userId;
    String user;
    String userPhoto;
    String email;
    String phoneNumber;
    DateTime memberDate;
    int countryId;
    String country;
    int cityId;
    String city;
    int categoryId;
    String category;
    int subCategoryId;
    String subCategory;
    String name;
    int period;
    String details;
    dynamic description;
    int initialPrice;
    int priceIncrease;
    String currency;
    int archive;
    List<dynamic> prices;
    int pinned;
    List<Photo> photos;
    dynamic auctionWinnerId;
    dynamic auctionWinner;
    dynamic auctionWinnerPhoto;
    dynamic heighPrice;
    DateTime createdAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        user: json["user"] == null ? null : json["user"],
        userPhoto: json["user_photo"] == null ? null : json["user_photo"],
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        memberDate: json["member_date"] == null ? null : DateTime.parse(json["member_date"]),
        countryId: json["country_id"] == null ? null : json["country_id"],
        country: json["country"] == null ? null : json["country"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        city: json["city"] == null ? null : json["city"],
        categoryId: json["category_id"] == null ? null : json["category_id"],
        category: json["category"] == null ? null : json["category"],
        subCategoryId: json["sub_category_id"] == null ? null : json["sub_category_id"],
        subCategory: json["sub_category"] == null ? null : json["sub_category"],
        name: json["name"] == null ? null : json["name"],
        period: json["period"] == null ? null : json["period"],
        details: json["details"] == null ? null : json["details"],
        description: json["description"],
        initialPrice: json["initial_price"] == null ? null : json["initial_price"],
        priceIncrease: json["price_increase"] == null ? null : json["price_increase"],
        currency: json["currency"] == null ? null : json["currency"],
        archive: json["archive"] == null ? null : json["archive"],
        prices: json["prices"] == null ? null : List<dynamic>.from(json["prices"].map((x) => x)),
        pinned: json["pinned"] == null ? null : json["pinned"],
        photos: json["photos"] == null ? null : List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        auctionWinnerId: json["auction_winner_id"],
        auctionWinner: json["auction_winner"],
        auctionWinnerPhoto: json["auction_winner_photo"],
        heighPrice: json["heigh_price"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "user": user == null ? null : user,
        "user_photo": userPhoto == null ? null : userPhoto,
        "email": email == null ? null : email,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "member_date": memberDate == null ? null : "${memberDate.year.toString().padLeft(4, '0')}-${memberDate.month.toString().padLeft(2, '0')}-${memberDate.day.toString().padLeft(2, '0')}",
        "country_id": countryId == null ? null : countryId,
        "country": country == null ? null : country,
        "city_id": cityId == null ? null : cityId,
        "city": city == null ? null : city,
        "category_id": categoryId == null ? null : categoryId,
        "category": category == null ? null : category,
        "sub_category_id": subCategoryId == null ? null : subCategoryId,
        "sub_category": subCategory == null ? null : subCategory,
        "name": name == null ? null : name,
        "period": period == null ? null : period,
        "details": details == null ? null : details,
        "description": description,
        "initial_price": initialPrice == null ? null : initialPrice,
        "price_increase": priceIncrease == null ? null : priceIncrease,
        "currency": currency == null ? null : currency,
        "archive": archive == null ? null : archive,
        "prices": prices == null ? null : List<dynamic>.from(prices.map((x) => x)),
        "pinned": pinned == null ? null : pinned,
        "photos": photos == null ? null : List<dynamic>.from(photos.map((x) => x.toJson())),
        "auction_winner_id": auctionWinnerId,
        "auction_winner": auctionWinner,
        "auction_winner_photo": auctionWinnerPhoto,
        "heigh_price": heighPrice,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    };
}

class Photo {
    Photo({
        this.id,
        this.auctionId,
        this.auction,
        this.photo,
        this.createdAt,
    });

    int id;
    int auctionId;
    String auction;
    String photo;
    DateTime createdAt;

    factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"] == null ? null : json["id"],
        auctionId: json["auction_id"] == null ? null : json["auction_id"],
        auction: json["auction"] == null ? null : json["auction"],
        photo: json["photo"] == null ? null : json["photo"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "auction_id": auctionId == null ? null : auctionId,
        "auction": auction == null ? null : auction,
        "photo": photo == null ? null : photo,
        "created_at": createdAt == null ? null : "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}
class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}
