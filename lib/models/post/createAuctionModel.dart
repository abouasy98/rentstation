// To parse this JSON data, do
//
//     final createAuctionModel = createAuctionModelFromJson(jsonString);

import 'dart:convert';

CreateAuctionModel createAuctionModelFromJson(String str) =>
    CreateAuctionModel.fromJson(json.decode(str));

class CreateAuctionModel {
  CreateAuctionModel({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  Data data;
  List<Error> error;

  factory CreateAuctionModel.fromJson(Map<String, dynamic> json) =>
      CreateAuctionModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );
}

class Data {
  Data({
    this.id,
    this.userId,
    this.user,
    this.userPhoto,
    this.email,
    this.phoneNumber,
    this.memberDate,
    this.cityId,
    this.city,
    this.name,
    this.period,
    this.details,
    this.notes,
    this.initialPrice,
    this.priceIncrease,
    this.prices,
    this.photos,
    this.auctionWinnerId,
    this.auctionWinner,
    this.createdAt,
    this.countryId,
    this.country,
    this.categoryId,
    this.category,
    this.subCategoryId,
    this.subCategory,
    this.archive,
    this.currency,
    this.pinned,
    this.auctionWinnerPhoto,
    this.heighPrice,
  });

  int id;
  int userId;
  String user;
  String userPhoto;
  String email;
  String phoneNumber;
  DateTime memberDate;
  int cityId;
  String city;
  String name;
  int period;
  String details;
  String notes;
  int initialPrice;
  int priceIncrease;
  List<dynamic> prices;
  List<Photo> photos;
  dynamic auctionWinnerId;
  dynamic auctionWinner;
  DateTime createdAt;
  int countryId;
  String country;
  int categoryId;
  String category;
  int subCategoryId;
  String subCategory;
  String currency;
  int archive;
  int pinned;
  dynamic auctionWinnerPhoto;
  dynamic heighPrice;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        userId: json["user_id"],
        user: json["user"],
        userPhoto: json["user_photo"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        memberDate: DateTime.parse(json["member_date"]),
        cityId: json["city_id"],
        city: json["city"],
        name: json["name"],
        period: json["period"],
        details: json["details"],
        notes: json["notes"],
        initialPrice: json["initial_price"],
        priceIncrease: json["price_increase"],
        prices: List<dynamic>.from(json["prices"].map((x) => x)),
        photos: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        auctionWinnerId: json["auction_winner_id"],
        auctionWinner: json["auction_winner"],
        createdAt: DateTime.parse(json["created_at"]),
        countryId: json["country_id"] == null ? null : json["country_id"],
        country: json["country"] == null ? null : json["country"],
        categoryId: json["category_id"] == null ? null : json["category_id"],
        category: json["category"] == null ? null : json["category"],
        subCategoryId:
            json["sub_category_id"] == null ? null : json["sub_category_id"],
        subCategory: json["sub_category"] == null ? null : json["sub_category"],
        currency: json["currency"] == null ? null : json["currency"],
        archive: json["archive"] == null ? null : json["archive"],
        auctionWinnerPhoto: json["auction_winner_photo"],
        heighPrice: json["heigh_price"],
        pinned: json["pinned"] == null ? null : json["pinned"],
      );
}

class Photo {
  Photo({
    this.id,
    this.auctionId,
    this.auction,
    this.photo,
    this.createdAt,
  });

  int id;
  int auctionId;
  String auction;
  String photo;
  DateTime createdAt;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        auctionId: json["auction_id"],
        auction: json["auction"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
      );
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}
