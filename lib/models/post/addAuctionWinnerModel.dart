// To parse this JSON data, do
//
//     final addAuctionWinnerModel = addAuctionWinnerModelFromJson(jsonString);

import 'dart:convert';

AddAuctionWinnerModel addAuctionWinnerModelFromJson(String str) => AddAuctionWinnerModel.fromJson(json.decode(str));

String addAuctionWinnerModelToJson(AddAuctionWinnerModel data) => json.encode(data.toJson());

class AddAuctionWinnerModel {
    AddAuctionWinnerModel({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    Data data;
 List<Error> error;

    factory AddAuctionWinnerModel.fromJson(Map<String, dynamic> json) => AddAuctionWinnerModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data:json["data"] == null ? null : Data.fromJson(json["data"]),
        error:json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": error,
    };
}

class Data {
    Data({
        this.id,
        this.userId,
        this.user,
        this.userPhoto,
        this.email,
        this.phoneNumber,
        this.memberDate,
        this.countryId,
        this.country,
        this.cityId,
        this.city,
        this.name,
        this.period,
        this.details,
        this.initialPrice,
        this.priceIncrease,
        this.currency,
        this.archive,
        this.prices,
        this.photos,
        this.auctionWinnerId,
        this.auctionWinner,
        this.createdAt,
    });

    int id;
    int userId;
    String user;
    String userPhoto;
    String email;
    String phoneNumber;
    DateTime memberDate;
    int countryId;
    String country;
    int cityId;
    String city;
    String name;
    int period;
    String details;
    int initialPrice;
    int priceIncrease;
    String currency;
    int archive;
    List<Price> prices;
    List<Photo> photos;
    int auctionWinnerId;
    String auctionWinner;
    DateTime createdAt;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        userId: json["user_id"],
        user: json["user"],
        userPhoto: json["user_photo"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        memberDate: DateTime.parse(json["member_date"]),
        countryId: json["country_id"],
        country: json["country"],
        cityId: json["city_id"],
        city: json["city"],
        name: json["name"],
        period: json["period"],
        details: json["details"],
        initialPrice: json["initial_price"],
        priceIncrease: json["price_increase"],
        currency: json["currency"],
        archive: json["archive"],
        prices: List<Price>.from(json["prices"].map((x) => Price.fromJson(x))),
        photos: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        auctionWinnerId: json["auction_winner_id"],
        auctionWinner: json["auction_winner"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "user": user,
        "user_photo": userPhoto,
        "email": email,
        "phone_number": phoneNumber,
        "member_date": "${memberDate.year.toString().padLeft(4, '0')}-${memberDate.month.toString().padLeft(2, '0')}-${memberDate.day.toString().padLeft(2, '0')}",
        "country_id": countryId,
        "country": country,
        "city_id": cityId,
        "city": city,
        "name": name,
        "period": period,
        "details": details,
        "initial_price": initialPrice,
        "price_increase": priceIncrease,
        "currency": currency,
        "archive": archive,
        "prices": List<dynamic>.from(prices.map((x) => x.toJson())),
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
        "auction_winner_id": auctionWinnerId,
        "auction_winner": auctionWinner,
        "created_at": createdAt.toIso8601String(),
    };
}

class Photo {
    Photo({
        this.id,
        this.auctionId,
        this.auction,
        this.photo,
        this.createdAt,
    });

    int id;
    int auctionId;
    String auction;
    String photo;
    DateTime createdAt;

    factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        auctionId: json["auction_id"],
        auction: json["auction"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "auction_id": auctionId,
        "auction": auction,
        "photo": photo,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}

class Price {
    Price({
        this.id,
        this.auctionId,
        this.auction,
        this.userId,
        this.user,
        this.userPhoto,
        this.price,
        this.createdAt,
    });

    int id;
    int auctionId;
    String auction;
    int userId;
    String user;
    String userPhoto;
    int price;
    DateTime createdAt;

    factory Price.fromJson(Map<String, dynamic> json) => Price(
        id: json["id"],
        auctionId: json["auction_id"],
        auction: json["auction"],
        userId: json["user_id"],
        user: json["user"],
        userPhoto: json["user_photo"],
        price: json["price"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "auction_id": auctionId,
        "auction": auction,
        "user_id": userId,
        "user": user,
        "user_photo": userPhoto,
        "price": price,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}
class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}
