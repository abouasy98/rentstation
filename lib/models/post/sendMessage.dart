// To parse this JSON data, do
//
//     final sendMessage = sendMessageFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

SendMessage sendMessageFromJson(String str) => SendMessage.fromJson(json.decode(str));

String sendMessageToJson(SendMessage data) => json.encode(data.toJson());

class SendMessage {
    SendMessage({
        @required this.mainCode,
        @required this.code,
        @required this.data,
        @required this.error,
    });

    final int mainCode;
    final int code;
    final Data data;
    final dynamic error;

    factory SendMessage.fromJson(Map<String, dynamic> json) => SendMessage(
        mainCode: json["mainCode"],
        code: json["code"],
        data: Data.fromJson(json["data"]),
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": error,
    };
}

class Data {
    Data({
        @required this.key,
        @required this.value,
    });

    final String key;
    final String value;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        key: json["key"],
        value: json["value"],
    );

    Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
    };
}
