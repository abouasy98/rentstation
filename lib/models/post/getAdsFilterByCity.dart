// To parse this JSON data, do
//
//     final getAddsByCityFilterProvider = getAddsByCityFilterProviderFromJson(jsonString);

import 'dart:convert';

GetAddsByCityFilter getAddsByCityFilterProviderFromJson(String str) =>
    GetAddsByCityFilter.fromJson(json.decode(str));

String getAddsByCityFilterProviderToJson(GetAddsByCityFilter data) =>
    json.encode(data.toJson());

class GetAddsByCityFilter {
  GetAddsByCityFilter({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  List<Datum> data;
  List<Error> error;

  factory GetAddsByCityFilter.fromJson(Map<String, dynamic> json) =>
      GetAddsByCityFilter(
        mainCode: json["mainCode"] == null ? null : json["mainCode"],
        code: json["code"] == null ? null : json["code"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode == null ? null : mainCode,
        "code": code == null ? null : code,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
      };
}

class Datum {
  Datum({
    this.id,
    this.userId,
    this.user,
    this.userPhoto,
    this.email,
    this.phoneNumber,
    this.memberDate,
    this.countryId,
    this.country,
    this.cityId,
    this.city,
    this.categoryId,
    this.category,
    this.subCategoryId,
    this.subCategory,
    this.title,
    this.price,
    this.priceStatus,
    this.description,
    this.currency,
    this.favorite,
    this.pinned,
    this.archive,
    this.photos,
    this.createdAt,
  });

  int id;
  int userId;
  String user;
  String userPhoto;
  String email;
  String phoneNumber;
  DateTime memberDate;
  int countryId;
  String country;
  int cityId;
  String city;
  int categoryId;
  String category;
  int subCategoryId;
  String subCategory;
  String title;
  int price;
  int priceStatus;
  String description;
  String currency;
  String favorite;
  int pinned;
  int archive;
  List<Photo> photos;
  DateTime createdAt;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        user: json["user"] == null ? null : json["user"],
        userPhoto: json["user_photo"] == null ? null : json["user_photo"],
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        memberDate: json["member_date"] == null
            ? null
            : DateTime.parse(json["member_date"]),
        countryId: json["country_id"] == null ? null : json["country_id"],
        country: json["country"] == null ? null : json["country"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        city: json["city"] == null ? null : json["city"],
        categoryId: json["category_id"] == null ? null : json["category_id"],
        category: json["category"] == null ? null : json["category"],
        subCategoryId:
            json["sub_category_id"] == null ? null : json["sub_category_id"],
        subCategory: json["sub_category"] == null ? null : json["sub_category"],
        title: json["title"] == null ? null : json["title"],
        price: json["price"] == null ? null : json["price"],
        priceStatus: json["price_status"] == null ? null : json["price_status"],
        description: json["description"] == null ? null : json["description"],
        currency: json["currency"] == null ? null : json["currency"],
        favorite: json["favorite"] == null ? null : json["favorite"],
        pinned: json["pinned"] == null ? null : json["pinned"],
        archive: json["archive"] == null ? null : json["archive"],
        photos: json["photos"] == null
            ? null
            : List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "user": user == null ? null : user,
        "user_photo": userPhoto == null ? null : userPhoto,
        "email": email == null ? null : email,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "member_date": memberDate == null
            ? null
            : "${memberDate.year.toString().padLeft(4, '0')}-${memberDate.month.toString().padLeft(2, '0')}-${memberDate.day.toString().padLeft(2, '0')}",
        "country_id": countryId == null ? null : countryId,
        "country": country == null ? null : countryValues.reverse[country],
        "city_id": cityId == null ? null : cityId,
        "city": city == null ? null : cityValues.reverse[city],
        "category_id": categoryId == null ? null : categoryId,
        "category": category == null ? null : category,
        "sub_category_id": subCategoryId == null ? null : subCategoryId,
        "sub_category": subCategory == null ? null : subCategory,
        "title": title == null ? null : title,
        "price": price == null ? null : price,
        "price_status": priceStatus == null ? null : priceStatus,
        "description": description == null ? null : description,
        "currency": currency == null ? null : currencyValues.reverse[currency],
        "favorite": favorite == null ? null : favorite,
        "pinned": pinned == null ? null : pinned,
        "archive": archive == null ? null : archive,
        "photos": photos == null
            ? null
            : List<dynamic>.from(photos.map((x) => x.toJson())),
        "created_at": createdAt == null
            ? null
            : "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

enum City { TALKA, SHOBRA_ELKHIMA }

final cityValues =
    EnumValues({"Shobra elkhima": City.SHOBRA_ELKHIMA, "talka": City.TALKA});

enum Country { EGYPT }

final countryValues = EnumValues({"Egypt": Country.EGYPT});

enum Currency { EGP }

final currencyValues = EnumValues({"EGP": Currency.EGP});

class Photo {
  Photo({
    this.id,
    this.adId,
    this.ad,
    this.photo,
    this.createdAt,
  });

  int id;
  int adId;
  String ad;
  String photo;
  DateTime createdAt;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"] == null ? null : json["id"],
        adId: json["ad_id"] == null ? null : json["ad_id"],
        ad: json["ad"] == null ? null : json["ad"],
        photo: json["photo"] == null ? null : json["photo"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "ad_id": adId == null ? null : adId,
        "ad": ad == null ? null : ad,
        "photo": photo == null ? null : photo,
        "created_at": createdAt == null
            ? null
            : "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}
