// To parse this JSON data, do
//
//     final auctionAddPriceModel = auctionAddPriceModelFromJson(jsonString);

import 'dart:convert';

AuctionAddPriceModel auctionAddPriceModelFromJson(String str) =>
    AuctionAddPriceModel.fromJson(json.decode(str));

String auctionAddPriceModelToJson(AuctionAddPriceModel data) =>
    json.encode(data.toJson());

class AuctionAddPriceModel {
  AuctionAddPriceModel({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  Data data;
  List<Error> error;

  factory AuctionAddPriceModel.fromJson(Map<String, dynamic> json) =>
      AuctionAddPriceModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": error,
      };
}

class Data {
  Data({
    this.id,
    this.userId,
    this.user,
    this.userPhoto,
    this.email,
    this.phoneNumber,
    this.memberDate,
    this.countryId,
    this.country,
    this.cityId,
    this.city,
    this.name,
    this.period,
    this.details,
    this.initialPrice,
    this.priceIncrease,
    this.currency,
    this.prices,
    this.photos,
    this.auctionWinnerId,
    this.auctionWinner,
    this.createdAt,
  });

  int id;
  int userId;
  AuctionWinner user;
  String userPhoto;
  String email;
  String phoneNumber;
  DateTime memberDate;
  int countryId;
  String country;
  int cityId;
  String city;
  Name name;
  int period;
  String details;
  int initialPrice;
  int priceIncrease;
  String currency;
  List<Price> prices;
  List<Photo> photos;
  int auctionWinnerId;
  AuctionWinner auctionWinner;
  DateTime createdAt;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        userId: json["user_id"],
        user: auctionWinnerValues.map[json["user"]],
        userPhoto: json["user_photo"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        memberDate: DateTime.parse(json["member_date"]),
        countryId: json["country_id"],
        country: json["country"],
        cityId: json["city_id"],
        city: json["city"],
        name: nameValues.map[json["name"]],
        period: json["period"],
        details: json["details"],
        initialPrice: json["initial_price"],
        priceIncrease: json["price_increase"],
        currency: json["currency"],
        prices: List<Price>.from(json["prices"].map((x) => Price.fromJson(x))),
        photos: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        auctionWinnerId: json["auction_winner_id"],
        auctionWinner: auctionWinnerValues.map[json["auction_winner"]],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "user": auctionWinnerValues.reverse[user],
        "user_photo": userPhoto,
        "email": email,
        "phone_number": phoneNumber,
        "member_date":
            "${memberDate.year.toString().padLeft(4, '0')}-${memberDate.month.toString().padLeft(2, '0')}-${memberDate.day.toString().padLeft(2, '0')}",
        "country_id": countryId,
        "country": country,
        "city_id": cityId,
        "city": city,
        "name": nameValues.reverse[name],
        "period": period,
        "details": details,
        "initial_price": initialPrice,
        "price_increase": priceIncrease,
        "currency": currency,
        "prices": List<dynamic>.from(prices.map((x) => x.toJson())),
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
        "auction_winner_id": auctionWinnerId,
        "auction_winner": auctionWinnerValues.reverse[auctionWinner],
        "created_at": createdAt.toIso8601String(),
      };
}

enum AuctionWinner { EMPTY, ABOUASY2 }

final auctionWinnerValues = EnumValues(
    {"abouasy2": AuctionWinner.ABOUASY2, "نور  الشريف": AuctionWinner.EMPTY});

enum Name { EMPTY }

final nameValues = EnumValues({"حراج الطيور": Name.EMPTY});

class Photo {
  Photo({
    this.id,
    this.auctionId,
    this.auction,
    this.photo,
    this.createdAt,
  });

  int id;
  int auctionId;
  Name auction;
  String photo;
  DateTime createdAt;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        auctionId: json["auction_id"],
        auction: nameValues.map[json["auction"]],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "auction_id": auctionId,
        "auction": nameValues.reverse[auction],
        "photo": photo,
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Price {
  Price({
    this.id,
    this.auctionId,
    this.auction,
    this.userId,
    this.user,
    this.userPhoto,
    this.price,
    this.createdAt,
  });

  int id;
  int auctionId;
  Name auction;
  int userId;
  AuctionWinner user;
  String userPhoto;
  int price;
  DateTime createdAt;

  factory Price.fromJson(Map<String, dynamic> json) => Price(
        id: json["id"],
        auctionId: json["auction_id"],
        auction: nameValues.map[json["auction"]],
        userId: json["user_id"],
        user: auctionWinnerValues.map[json["user"]],
        userPhoto: json["user_photo"],
        price: json["price"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "auction_id": auctionId,
        "auction": nameValues.reverse[auction],
        "user_id": userId,
        "user": auctionWinnerValues.reverse[user],
        "user_photo": userPhoto,
        "price": price,
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}
