// To parse this JSON data, do
//
//     final createAdModel = createAdModelFromJson(jsonString);

import 'dart:convert';

CreateAdModel createAdModelFromJson(String str) =>
    CreateAdModel.fromJson(json.decode(str));

String createAdModelToJson(CreateAdModel data) => json.encode(data.toJson());

class CreateAdModel {
  CreateAdModel({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  Data data;
  List<Error> error;

  factory CreateAdModel.fromJson(Map<String, dynamic> json) => CreateAdModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": List<dynamic>.from(error.map((x) => x.toJson())),
      };
}

class Data {
  Data({
    this.id,
    this.userId,
    this.user,
    this.userPhoto,
    this.email,
    this.phoneNumber,
    this.memberDate,
    this.cityId,
    this.city,
    this.categoryId,
    this.category,
    this.subCategoryId,
    this.subCategory,
    this.title,
    this.price,
    this.priceStatus,
    this.description,
    this.additionalDetails,
    this.photos,
    this.createdAt,
  });

  int id;
  int userId;
  String user;
  String userPhoto;
  String email;
  String phoneNumber;
  DateTime memberDate;
  int cityId;
  String city;
  int categoryId;
  String category;
  int subCategoryId;
  String subCategory;
  String title;
  int price;
  int priceStatus;
  String description;
  String additionalDetails;
  List<Photo> photos;
  DateTime createdAt;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        userId: json["user_id"],
        user: json["user"],
        userPhoto: json["user_photo"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        memberDate: DateTime.parse(json["member_date"]),
        cityId: json["city_id"],
        city: json["city"],
        categoryId: json["category_id"],
        category: json["category"],
        subCategoryId: json["sub_category_id"],
        subCategory: json["sub_category"],
        title: json["title"],
        price: json["price"],
        priceStatus: json["price_status"],
        description: json["description"],
        additionalDetails: json["additional_details"],
        photos: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "user": user,
        "user_photo": userPhoto,
        "email": email,
        "phone_number": phoneNumber,
        "member_date":
            "${memberDate.year.toString().padLeft(4, '0')}-${memberDate.month.toString().padLeft(2, '0')}-${memberDate.day.toString().padLeft(2, '0')}",
        "city_id": cityId,
        "city": city,
        "category_id": categoryId,
        "category": category,
        "sub_category_id": subCategoryId,
        "sub_category": subCategory,
        "title": title,
        "price": price,
        "price_status": priceStatus,
        "description": description,
        "additional_details": additionalDetails,
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Photo {
  Photo({
    this.id,
    this.adId,
    this.ad,
    this.photo,
    this.createdAt,
  });

  int id;
  int adId;
  String ad;
  String photo;
  DateTime createdAt;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        adId: json["ad_id"],
        ad: json["ad"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "ad_id": adId,
        "ad": ad,
        "photo": photo,
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}
