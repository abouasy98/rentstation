// To parse this JSON data, do
//
//     final deleteNotificationsModels = deleteNotificationsModelsFromJson(jsonString);

import 'dart:convert';

DeleteNotificationsModels deleteNotificationsModelsFromJson(String str) =>
    DeleteNotificationsModels.fromJson(json.decode(str));

String deleteNotificationsModelsToJson(DeleteNotificationsModels data) =>
    json.encode(data.toJson());

class DeleteNotificationsModels {
  DeleteNotificationsModels({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  List<dynamic> data;
  List<Error> error;

  factory DeleteNotificationsModels.fromJson(Map<String, dynamic> json) =>
      DeleteNotificationsModels(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null
            ? null
            : List<dynamic>.from(json["data"].map((x) => x)),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x)),
        "error": error,
      };
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}
