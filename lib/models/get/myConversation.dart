// To parse this JSON data, do
//
//     final myConversations = myConversationsFromJson(jsonString);

import 'dart:convert';

MyConversations myConversationsFromJson(String str) => MyConversations.fromJson(json.decode(str));

String myConversationsToJson(MyConversations data) => json.encode(data.toJson());

class MyConversations {
    MyConversations({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    List<Datum> data;
    List<Error> error;

    factory MyConversations.fromJson(Map<String, dynamic> json) => MyConversations(
        mainCode: json["mainCode"] == null ? null : json["mainCode"],
        code: json["code"] == null ? null : json["code"],
        data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error: json["error"] == null ? null : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode == null ? null : mainCode,
        "code": code == null ? null : code,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error == null ? null : List<dynamic>.from(error.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        this.id,
        this.seen,
        this.userId,
        this.name,
        this.photo,
        this.lastMessage,
        this.createdAt,
    });

    int id;
    int seen;
    int userId;
    String name;
    String photo;
    String lastMessage;
    DateTime createdAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        seen: json["seen"] == null ? null : json["seen"],
        userId: json["user_id"] == null ? null : json["user_id"],
        name: json["name"] == null ? null : json["name"],
        photo: json["photo"] == null ? null : json["photo"],
        lastMessage: json["last_message"] == null ? null : json["last_message"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "seen": seen == null ? null : seen,
        "user_id": userId == null ? null : userId,
        "name": name == null ? null : name,
        "photo": photo == null ? null : photo,
        "last_message": lastMessage == null ? null : lastMessage,
        "created_at": createdAt == null ? null : "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}

class Error {
    Error({
        this.key,
        this.value,
    });

    String key;
    String value;

    factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
    );

    Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
    };
}
