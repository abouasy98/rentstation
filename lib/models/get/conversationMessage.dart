// To parse this JSON data, do
//
//     final conversationsMessages = conversationsMessagesFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

ConversationsMessages conversationsMessagesFromJson(String str) => ConversationsMessages.fromJson(json.decode(str));

String conversationsMessagesToJson(ConversationsMessages data) => json.encode(data.toJson());

class ConversationsMessages {
    ConversationsMessages({
        @required this.mainCode,
        @required this.code,
        @required this.data,
        @required this.error,
    });

    final int mainCode;
    final int code;
    final List<Datum> data;
    final    List<Error> error;

    factory ConversationsMessages.fromJson(Map<String, dynamic> json) => ConversationsMessages(
        mainCode: json["mainCode"],
        code: json["code"],
        data:json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error:json["error"] == null ? null : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
    };
}

class Datum {
    Datum({
        @required this.id,
        @required this.userId,
        @required this.name,
        @required this.photo,
        @required this.createdAt,
    });

    final int id;
    final int userId;
    final String name;
    final String photo;
    final DateTime createdAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        userId: json["user_id"],
        name: json["name"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "name": name,
        "photo": photo,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}
class Error {
    Error({
        this.key,
        this.value,
    });

    String key;
    String value;

    factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
    );

    Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
    };
}