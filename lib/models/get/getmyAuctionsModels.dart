// To parse this JSON data, do
//
//     final getmyAuctionsModels = getmyAuctionsModelsFromJson(jsonString);

import 'dart:convert';

GetmyAuctionsModels getmyAuctionsModelsFromJson(String str) =>
    GetmyAuctionsModels.fromJson(json.decode(str));

String getmyAuctionsModelsToJson(GetmyAuctionsModels data) =>
    json.encode(data.toJson());

class GetmyAuctionsModels {
  GetmyAuctionsModels({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  List<MyAuction> data;
  List<Error> error;

  factory GetmyAuctionsModels.fromJson(Map<String, dynamic> json) =>
      GetmyAuctionsModels(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null
            ? null
            : List<MyAuction>.from(json["data"].map((x) => MyAuction.fromJson(x))),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
      };
}

class MyAuction {
  MyAuction({
    this.id,
    this.userId,
    this.user,
    this.userPhoto,
    this.email,
    this.phoneNumber,
    this.memberDate,
    this.countryId,
    this.country,
    this.cityId,
    this.city,
    this.name,
    this.period,
    this.details,
    this.description,
    this.initialPrice,
    this.priceIncrease,
    this.currency,
    this.prices,
    this.photos,
    this.auctionWinnerId,
    this.auctionWinner,
    this.createdAt,
    this.categoryId,
    this.category,
    this.subCategoryId,
    this.subCategory,
    this.archive,
    this.pinned,
    this.auctionWinnerPhoto,
    this.heighPrice,
  });

  int id;
  int userId;
  User user;
  String userPhoto;
  Email email;
  String phoneNumber;
  DateTime memberDate;
  int countryId;
  Country country;
  int cityId;
  String city;
  String name;
  int period;
  String details;
  dynamic description;
  int initialPrice;
  int priceIncrease;
  String currency;
  List<Price> prices;
  List<Photo> photos;
  int auctionWinnerId;
  User auctionWinner;
  DateTime createdAt;

  int categoryId;
  String category;
  int subCategoryId;
  String subCategory;

  int archive;

  int pinned;

  dynamic auctionWinnerPhoto;
  dynamic heighPrice;

  factory MyAuction.fromJson(Map<String, dynamic> json) => MyAuction(
        id: json["id"],
        userId: json["user_id"],
        user: userValues.map[json["user"]],
        userPhoto: json["user_photo"],
        email: emailValues.map[json["email"]],
        phoneNumber: json["phone_number"],
        memberDate: DateTime.parse(json["member_date"]),
        countryId: json["country_id"],
        country: countryValues.map[json["country"]],
        cityId: json["city_id"],
        city: json["city"],
        name: json["name"],
        period: json["period"],
        details: json["details"],
        description: json["description"],
        initialPrice: json["initial_price"],
        priceIncrease: json["price_increase"],
        currency: json["currency"],
        prices: List<Price>.from(json["prices"].map((x) => Price.fromJson(x))),
        photos: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        auctionWinnerId: json["auction_winner_id"] == null
            ? null
            : json["auction_winner_id"],
        auctionWinner: json["auction_winner"] == null
            ? null
            : userValues.map[json["auction_winner"]],
        createdAt: DateTime.parse(json["created_at"]),
        categoryId: json["category_id"] == null ? null : json["category_id"],
        category: json["category"] == null ? null : json["category"],
        subCategoryId:
            json["sub_category_id"] == null ? null : json["sub_category_id"],
        subCategory: json["sub_category"] == null ? null : json["sub_category"],
        archive: json["archive"] == null ? null : json["archive"],
        auctionWinnerPhoto: json["auction_winner_photo"],
        heighPrice: json["heigh_price"],
        pinned: json["pinned"] == null ? null : json["pinned"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "user": userValues.reverse[user],
        "user_photo": userPhoto,
        "email": emailValues.reverse[email],
        "phone_number": phoneNumber,
        "member_date":
            "${memberDate.year.toString().padLeft(4, '0')}-${memberDate.month.toString().padLeft(2, '0')}-${memberDate.day.toString().padLeft(2, '0')}",
        "country_id": countryId,
        "country": countryValues.reverse[country],
        "city_id": cityId,
        "city": cityValues.reverse[city],
        "name": name,
        "period": period,
        "details": details,
        "description": description,
        "initial_price": initialPrice,
        "price_increase": priceIncrease,
        "currency": currencyValues.reverse[currency],
        "prices": List<dynamic>.from(prices.map((x) => x.toJson())),
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
        "auction_winner_id": auctionWinnerId == null ? null : auctionWinnerId,
        "auction_winner":
            auctionWinner == null ? null : userValues.reverse[auctionWinner],
        "created_at": createdAt.toIso8601String(),
      };
}

enum User { ABOUASY2, EMPTY }

final userValues =
    EnumValues({"abouasy2": User.ABOUASY2, "نور  الشريف": User.EMPTY});

enum City { TALKA, MAKKAH }

final cityValues = EnumValues({"Makkah": City.MAKKAH, "talka": City.TALKA});

enum Country { EGYPT, SAUDI_ARABIA }

final countryValues =
    EnumValues({"Egypt": Country.EGYPT, "Saudi Arabia": Country.SAUDI_ARABIA});

enum Currency { EGYPTIAN_POUND, SR }

final currencyValues =
    EnumValues({"Egyptian Pound": Currency.EGYPTIAN_POUND, "SR": Currency.SR});

enum Email { MOHAMEDABOUASSI_GMAIL_COM }

final emailValues =
    EnumValues({"mohamedabouassi@gmail.com": Email.MOHAMEDABOUASSI_GMAIL_COM});

class Photo {
  Photo({
    this.id,
    this.auctionId,
    this.auction,
    this.photo,
    this.createdAt,
  });

  int id;
  int auctionId;
  String auction;
  String photo;
  DateTime createdAt;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        auctionId: json["auction_id"],
        auction: json["auction"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "auction_id": auctionId,
        "auction": auction,
        "photo": photo,
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Price {
  Price({
    this.id,
    this.auctionId,
    this.auction,
    this.userId,
    this.user,
    this.userPhoto,
    this.price,
    this.createdAt,
  });

  int id;
  int auctionId;
  Auction auction;
  int userId;
  User user;
  String userPhoto;
  int price;
  DateTime createdAt;

  factory Price.fromJson(Map<String, dynamic> json) => Price(
        id: json["id"],
        auctionId: json["auction_id"],
        auction: auctionValues.map[json["auction"]],
        userId: json["user_id"],
        user: userValues.map[json["user"]],
        userPhoto: json["user_photo"],
        price: json["price"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "auction_id": auctionId,
        "auction": auctionValues.reverse[auction],
        "user_id": userId,
        "user": userValues.reverse[user],
        "user_photo": userPhoto,
        "price": price,
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

enum Auction { EMPTY }

final auctionValues = EnumValues({"حراج الطيور": Auction.EMPTY});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}
