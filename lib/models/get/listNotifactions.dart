// To parse this JSON data, do
//
//     final listNotificationsModel = listNotificationsModelFromJson(jsonString);

import 'dart:convert';

ListNotificationsModel listNotificationsModelFromJson(String str) => ListNotificationsModel.fromJson(json.decode(str));

String listNotificationsModelToJson(ListNotificationsModel data) => json.encode(data.toJson());

class ListNotificationsModel {
    ListNotificationsModel({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    List<Datum> data;
        List<Error> error;

    factory ListNotificationsModel.fromJson(Map<String, dynamic> json) => ListNotificationsModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error: json["error"] == null ? null : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
    };
}

class Datum {
    Datum({
        this.id,
        this.type,
        this.title,
        this.message,
        this.adId,
        this.auctionId,
        this.createdAt,
    });

    int id;
    int type;
    String title;
    String message;
    int adId;
    int auctionId;
    DateTime createdAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        type: json["type"],
        title: json["title"],
        message: json["message"],
        adId: json["ad_id"] == null ? null : json["ad_id"],
        auctionId: json["auction_id"] == null ? null : json["auction_id"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "type": type,
        "title": titleValues.reverse[title],
        "message": message,
        "ad_id": adId == null ? null : adId,
        "auction_id": auctionId == null ? null : auctionId,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}

enum Title { EMPTY, TITLE }

final titleValues = EnumValues({
    "الأعلانات": Title.EMPTY,
    "المزادات": Title.TITLE
});

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
class Error {
    Error({
        this.key,
        this.value,
    });

    String key;
    String value;

    factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
    );

    Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
    };
}