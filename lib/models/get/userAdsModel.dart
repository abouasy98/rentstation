// To parse this JSON data, do
//
//     final userAdsModel = userAdsModelFromJson(jsonString);

import 'dart:convert';

UserAdsModel userAdsModelFromJson(String str) => UserAdsModel.fromJson(json.decode(str));

String userAdsModelToJson(UserAdsModel data) => json.encode(data.toJson());

class UserAdsModel {
    UserAdsModel({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    List<Datum> data;
        List<Error> error;

    factory UserAdsModel.fromJson(Map<String, dynamic> json) => UserAdsModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null ? null :List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error: json["error"] == null ? null : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
    };
}

class Datum {
    Datum({
        this.id,
        this.userId,
        this.user,
        this.userPhoto,
        this.email,
        this.phoneNumber,
        this.memberDate,
        this.countryId,
        this.country,
        this.cityId,
        this.city,
        this.categoryId,
        this.category,
        this.subCategoryId,
        this.subCategory,
        this.title,
        this.price,
        this.priceStatus,
        this.description,
        this.currency,
        this.favorite,
        this.pinned,
        this.archive,
        this.photos,
        this.createdAt,
    });

    int id;
    int userId;
    String user;
    String userPhoto;
    String email;
    String phoneNumber;
    DateTime memberDate;
    int countryId;
    String country;
    int cityId;
   String city;
    int categoryId;
    Category category;
    int subCategoryId;
    String subCategory;
    String title;
    int price;
    int priceStatus;
    String description;
    String currency;
    String favorite;
    int pinned;
    int archive;
    List<Photo> photos;
    DateTime createdAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        userId: json["user_id"],
        user: json["user"],
        userPhoto: json["user_photo"],
        email:json["email"],
        phoneNumber: json["phone_number"],
        memberDate: DateTime.parse(json["member_date"]),
        countryId: json["country_id"],
        country:json["country"],
        cityId: json["city_id"],
        city:json["city"],
        categoryId: json["category_id"],
        category: categoryValues.map[json["category"]],
        subCategoryId: json["sub_category_id"],
        subCategory: json["sub_category"],
        title: json["title"],
        price: json["price"],
        priceStatus: json["price_status"],
        description: json["description"],
        currency:json["currency"],
        favorite: json["favorite"],
        pinned: json["pinned"],
        archive: json["archive"],
        photos: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "user": userValues.reverse[user],
        "user_photo": userPhoto,
        "email": emailValues.reverse[email],
        "phone_number": phoneNumber,
        "member_date": "${memberDate.year.toString().padLeft(4, '0')}-${memberDate.month.toString().padLeft(2, '0')}-${memberDate.day.toString().padLeft(2, '0')}",
        "country_id": countryId,
        "country": countryValues.reverse[country],
        "city_id": cityId,
        "city": cityValues.reverse[city],
        "category_id": categoryId,
        "category": categoryValues.reverse[category],
        "sub_category_id": subCategoryId,
        "sub_category": subCategory,
        "title": title,
        "price": price,
        "price_status": priceStatus,
        "description": description,
        "currency": currencyValues.reverse[currency],
        "favorite": favorite,
        "pinned": pinned,
        "archive": archive,
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}

enum Category { REAL_ESTATE, MOBILE, CARS }

final categoryValues = EnumValues({
    "Cars": Category.CARS,
    "Mobile": Category.MOBILE,
    "Real Estate": Category.REAL_ESTATE
});

enum City { TALKA, EL_DAMAM, MAKKAH }

final cityValues = EnumValues({
    "El Damam": City.EL_DAMAM,
    "Makkah": City.MAKKAH,
    "talka": City.TALKA
});

enum Country { EGYPT, SAUDI_ARABIA }

final countryValues = EnumValues({
    "Egypt": Country.EGYPT,
    "Saudi Arabia": Country.SAUDI_ARABIA
});

enum Currency { EGP, SR }

final currencyValues = EnumValues({
    "EGP": Currency.EGP,
    "SR": Currency.SR
});

enum Email { TQNEE_COM_SA_GMAIL_COM }

final emailValues = EnumValues({
    "tqnee.com.sa@gmail.com": Email.TQNEE_COM_SA_GMAIL_COM
});

class Photo {
    Photo({
        this.id,
        this.adId,
        this.ad,
        this.photo,
        this.createdAt,
    });

    int id;
    int adId;
    String ad;
    String photo;
    DateTime createdAt;

    factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        adId: json["ad_id"],
        ad: json["ad"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "ad_id": adId,
        "ad": ad,
        "photo": photo,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}

enum User { EMPTY }

final userValues = EnumValues({
    "رامي محمد": User.EMPTY
});

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
class Error {
    Error({
        this.key,
        this.value,
    });

    String key;
    String value;

    factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
    );

    Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
    };
}