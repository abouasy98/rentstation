// To parse this JSON data, do
//
//     final myArchivedAds = myArchivedAdsFromJson(jsonString);

import 'dart:convert';

MyArchivedAds myArchivedAdsFromJson(String str) =>
    MyArchivedAds.fromJson(json.decode(str));

String myArchivedAdsToJson(MyArchivedAds data) => json.encode(data.toJson());

class MyArchivedAds {
  MyArchivedAds({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  List<Datum> data;
     List<Error> error;

  factory MyArchivedAds.fromJson(Map<String, dynamic> json) => MyArchivedAds(
        mainCode: json["mainCode"],
        code: json["code"],
        data:json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error: json["error"] == null ? null : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
      };
}

class Datum {
  Datum({
    this.id,
    this.userId,
    this.user,
    this.userPhoto,
    this.email,
    this.phoneNumber,
    this.memberDate,
    this.countryId,
    this.country,
    this.cityId,
    this.city,
    this.categoryId,
    this.category,
    this.subCategoryId,
    this.subCategory,
    this.title,
    this.price,
    this.priceStatus,
    this.description,
    this.currency,
    this.favorite,
    this.pinned,
    this.archive,
    this.photos,
    this.createdAt,
  });

  int id;
  int userId;
  String user;
  String userPhoto;
  String email;
  String phoneNumber;
  DateTime memberDate;
  int countryId;
  String country;
  int cityId;
  String city;
  int categoryId;
  String category;
  int subCategoryId;
  String subCategory;
  String title;
  int price;
  int priceStatus;
  String description;
  String currency;
  String favorite;
  int pinned;
  int archive;
  List<Photo> photos;
  DateTime createdAt;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        userId: json["user_id"],
        user: json["user"],
        userPhoto: json["user_photo"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        memberDate: DateTime.parse(json["member_date"]),
        countryId: json["country_id"],
        country: json["country"],
        cityId: json["city_id"],
        city: json["city"],
        categoryId: json["category_id"],
        category: json["category"],
        subCategoryId: json["sub_category_id"],
        subCategory: json["sub_category"],
        title: json["title"],
        price: json["price"],
        priceStatus: json["price_status"],
        description: json["description"],
        currency: json["currency"],
        favorite: json["favorite"],
        pinned: json["pinned"],
        archive: json["archive"],
        photos: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "user": user,
        "user_photo": userPhoto,
        "email": email,
        "phone_number": phoneNumber,
        "member_date":
            "${memberDate.year.toString().padLeft(4, '0')}-${memberDate.month.toString().padLeft(2, '0')}-${memberDate.day.toString().padLeft(2, '0')}",
        "country_id": countryId,
        "country": country,
        "city_id": cityId,
        "city": city,
        "category_id": categoryId,
        "category": category,
        "sub_category_id": subCategoryId,
        "sub_category": subCategory,
        "title": title,
        "price": price,
        "price_status": priceStatus,
        "description": description,
        "currency": currency,
        "favorite": favorite,
        "pinned": pinned,
        "archive": archive,
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Photo {
  Photo({
    this.id,
    this.adId,
    this.ad,
    this.photo,
    this.createdAt,
  });

  int id;
  int adId;
  String ad;
  String photo;
  DateTime createdAt;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        adId: json["ad_id"],
        ad: json["ad"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "ad_id": adId,
        "ad": ad,
        "photo": photo,
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}
class Error {
    Error({
        this.key,
        this.value,
    });

    String key;
    String value;

    factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
    );

    Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
    };
}