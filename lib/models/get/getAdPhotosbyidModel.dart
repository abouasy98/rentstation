// To parse this JSON data, do
//
//     final getAdPhotosModel = getAdPhotosModelFromJson(jsonString);

import 'dart:convert';

GetAdPhotosbyidModel getAdPhotosModelFromJson(String str) => GetAdPhotosbyidModel.fromJson(json.decode(str));

String getAdPhotosModelToJson(GetAdPhotosbyidModel data) => json.encode(data.toJson());

class GetAdPhotosbyidModel {
    GetAdPhotosbyidModel({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    List<Datum> data;
    List<Error> error;

    factory GetAdPhotosbyidModel.fromJson(Map<String, dynamic> json) => GetAdPhotosbyidModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data:json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error:json["error"] == null ? null : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
    };
}

class Datum {
    Datum({
        this.id,
        this.adId,
        this.ad,
        this.photo,
        this.createdAt,
    });

    int id;
    int adId;
    String ad;
    String photo;
    DateTime createdAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        adId: json["ad_id"],
        ad: json["ad"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "ad_id": adId,
        "ad": ad,
        "photo": photo,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}
class Error {
    Error({
        this.key,
        this.value,
    });

    String key;
    String value;

    factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
    );

    Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
    };
}