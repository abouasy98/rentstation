// To parse this JSON data, do
//
//     final commsion = commsionFromJson(jsonString);

import 'dart:convert';

CommsionModel commsionFromJson(String str) => CommsionModel.fromJson(json.decode(str));

String commsionToJson(CommsionModel data) => json.encode(data.toJson());

class CommsionModel {
    CommsionModel({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    List<Datum> data;
    dynamic error;

    factory CommsionModel.fromJson(Map<String, dynamic> json) => CommsionModel(
        mainCode: json["mainCode"] == null ? null : json["mainCode"],
        code: json["code"] == null ? null : json["code"],
        data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode == null ? null : mainCode,
        "code": code == null ? null : code,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
    };
}

class Datum {
    Datum({
        this.commissionText,
    });

    String commissionText;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        commissionText: json["commission_text"] == null ? null : json["commission_text"],
    );

    Map<String, dynamic> toJson() => {
        "commission_text": commissionText == null ? null : commissionText,
    };
}
