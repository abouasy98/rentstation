// To parse this JSON data, do
//
//     final adComment = adCommentFromJson(jsonString);

import 'dart:convert';

GetAdComment adCommentFromJson(String str) => GetAdComment.fromJson(json.decode(str));

String adCommentToJson(GetAdComment data) => json.encode(data.toJson());

class GetAdComment {
  GetAdComment({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  List<Comment> data;
  List<Error> error;

  factory GetAdComment.fromJson(Map<String, dynamic> json) => GetAdComment(
        mainCode: json["mainCode"] == null ? null : json["mainCode"],
        code: json["code"] == null ? null : json["code"],
        data: json["data"] == null
            ? null
            : List<Comment>.from(json["data"].map((x) => Comment.fromJson(x))),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode == null ? null : mainCode,
        "code": code == null ? null : code,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
      };
}

class Comment {
  Comment({
    this.id,
    this.userId,
    this.user,
    this.userPhoto,
    this.email,
    this.phoneNumber,
    this.adId,
    this.ad,
    this.comment,
    this.replays,
    this.createdAt,
  });

  int id;
  int userId;
  String user;
  String userPhoto;
  String email;
  String phoneNumber;
  int adId;
  String ad;
  String comment;
  List<Replay> replays;
  DateTime createdAt;

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        id: json["id"] == null ? null : json["id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        user: json["user"] == null ? null : json["user"],
        userPhoto: json["user_photo"] == null ? null : json["user_photo"],
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        adId: json["ad_id"] == null ? null : json["ad_id"],
        ad: json["ad"] == null ? null : json["ad"],
        comment: json["comment"] == null ? null : json["comment"],
        replays: json["replays"] == null
            ? null
            : List<Replay>.from(json["replays"].map((x) => Replay.fromJson(x))),
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "user": user == null ? null : user,
        "user_photo": userPhoto == null ? null : userPhoto,
        "email": email == null ? null : email,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "ad_id": adId == null ? null : adId,
        "ad": ad == null ? null : ad,
        "comment": comment == null ? null : comment,
        "replays": replays == null
            ? null
            : List<dynamic>.from(replays.map((x) => x.toJson())),
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
      };
}

class Replay {
  Replay({
    this.id,
    this.userId,
    this.user,
    this.userPhoto,
    this.email,
    this.phoneNumber,
    this.commentId,
    this.replay,
    this.createdAt,
  });

  int id;
  int userId;
  String user;
  String userPhoto;
  String email;
  String phoneNumber;
  int commentId;
  String replay;
  DateTime createdAt;

  factory Replay.fromJson(Map<String, dynamic> json) => Replay(
        id: json["id"] == null ? null : json["id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        user: json["user"] == null ? null : json["user"],
        userPhoto: json["user_photo"] == null ? null : json["user_photo"],
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        commentId: json["comment_id"] == null ? null : json["comment_id"],
        replay: json["replay"] == null ? null : json["replay"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "user": user == null ? null : user,
        "user_photo": userPhoto == null ? null : userPhoto,
        "email": email == null ? null : email,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "comment_id": commentId == null ? null : commentId,
        "replay": replay == null ? null : replay,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
      };
}

class Error {
  Error({
    this.key,
    this.message,
  });

  String key;
  String message;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        message: json["message"] == null ? null : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "message": message == null ? null : message,
      };
}
