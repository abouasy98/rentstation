// To parse this JSON data, do
//
//     final contuctUsModel = contuctUsModelFromJson(jsonString);

import 'dart:convert';

ContuctUsModel contuctUsModelFromJson(String str) => ContuctUsModel.fromJson(json.decode(str));

String contuctUsModelToJson(ContuctUsModel data) => json.encode(data.toJson());

class ContuctUsModel {
    ContuctUsModel({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    List<Datum> data;
    dynamic error;

    factory ContuctUsModel.fromJson(Map<String, dynamic> json) => ContuctUsModel(
        mainCode: json["mainCode"] == null ? null : json["mainCode"],
        code: json["code"] == null ? null : json["code"],
        data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode == null ? null : mainCode,
        "code": code == null ? null : code,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
    };
}

class Datum {
    Datum({
        this.contactUsText,
        this.contactUsNumber,
    });

    String contactUsText;
    String contactUsNumber;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        contactUsText: json["contact_us_text"] == null ? null : json["contact_us_text"],
        contactUsNumber: json["contact_us_number"] == null ? null : json["contact_us_number"],
    );

    Map<String, dynamic> toJson() => {
        "contact_us_text": contactUsText == null ? null : contactUsText,
        "contact_us_number": contactUsNumber == null ? null : contactUsNumber,
    };
}
