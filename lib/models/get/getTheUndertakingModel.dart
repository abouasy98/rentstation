// To parse this JSON data, do
//
//     final getTheUndertaking = getTheUndertakingFromJson(jsonString);

import 'dart:convert';

GetTheUndertaking getTheUndertakingFromJson(String str) => GetTheUndertaking.fromJson(json.decode(str));

String getTheUndertakingToJson(GetTheUndertaking data) => json.encode(data.toJson());

class GetTheUndertaking {
    GetTheUndertaking({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    Data data;
    dynamic error;

    factory GetTheUndertaking.fromJson(Map<String, dynamic> json) => GetTheUndertaking(
        mainCode: json["mainCode"] == null ? null : json["mainCode"],
        code: json["code"] == null ? null : json["code"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode == null ? null : mainCode,
        "code": code == null ? null : code,
        "data": data == null ? null : data.toJson(),
        "error": error,
    };
}

class Data {
    Data({
        this.key,
        this.value,
    });

    String key;
    String value;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
    );

    Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
    };
}
