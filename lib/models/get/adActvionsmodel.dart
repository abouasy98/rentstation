// To parse this JSON data, do
//
//     final adActivitionmodelDart = adActivitionmodelDartFromJson(jsonString);

import 'dart:convert';

AdActivitionmodelDart adActivitionmodelDartFromJson(String str) => AdActivitionmodelDart.fromJson(json.decode(str));

String adActivitionmodelDartToJson(AdActivitionmodelDart data) => json.encode(data.toJson());

class AdActivitionmodelDart {
    AdActivitionmodelDart({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    Data data;
     List<Error> error;

    factory AdActivitionmodelDart.fromJson(Map<String, dynamic> json) => AdActivitionmodelDart(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null ? null :Data.fromJson(json["data"]),
        error:  json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": error,
    };
}

class Data {
    Data({
        this.activation,
    });

    int activation;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        activation: json["activation"],
    );

    Map<String, dynamic> toJson() => {
        "activation": activation,
    };
}
class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}
