// To parse this JSON data, do
//
//     final myFavoritesAds = myFavoritesAdsFromJson(jsonString);

import 'dart:convert';

MyFavoritesAds myFavoritesAdsFromJson(String str) => MyFavoritesAds.fromJson(json.decode(str));

String myFavoritesAdsToJson(MyFavoritesAds data) => json.encode(data.toJson());

class MyFavoritesAds {
    MyFavoritesAds({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    List<Datum> data;
   List<Error> error;

    factory MyFavoritesAds.fromJson(Map<String, dynamic> json) => MyFavoritesAds(
        mainCode: json["mainCode"],
        code: json["code"],
        data:json["data"] == null ? null :  List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error:json["error"] == null
            ? null
            :   List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
    };
}

class Datum {
    Datum({
        this.id,
        this.userId,
        this.user,
        this.userPhoto,
        this.email,
        this.phoneNumber,
        this.memberDate,
        this.cityId,
        this.city,
        this.categoryId,
        this.category,
        this.subCategoryId,
        this.subCategory,
        this.title,
        this.price,
        this.priceStatus,
        this.description,
        this.additionalDetails,
        this.photos,
        this.createdAt,
    });

    int id;
    int userId;
    String user;
    String userPhoto;
    String email;
    String phoneNumber;
    DateTime memberDate;
    int cityId;
    String city;
    dynamic categoryId;
    String category;
    dynamic subCategoryId;
    String subCategory;
    String title;
    int price;
    String priceStatus;
    String description;
    String additionalDetails;
    List<Photo> photos;
    DateTime createdAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        userId: json["user_id"],
        user: json["user"],
        userPhoto: json["user_photo"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        memberDate: DateTime.parse(json["member_date"]),
        cityId: json["city_id"],
        city: json["city"],
        categoryId: json["category_id"],
        category: json["category"],
        subCategoryId: json["sub_category_id"],
        subCategory: json["sub_category"],
        title:json["title"],
        price: json["price"],
        priceStatus: json["price_status"],
        description: json["description"],
        additionalDetails: json["additional_details"],
        photos: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "user": user,
        "user_photo": userPhoto,
        "email": email,
        "phone_number": phoneNumber,
        "member_date": "${memberDate.year.toString().padLeft(4, '0')}-${memberDate.month.toString().padLeft(2, '0')}-${memberDate.day.toString().padLeft(2, '0')}",
        "city_id": cityId,
        "city": city,
        "category_id": categoryId,
        "category": category,
        "sub_category_id": subCategoryId,
        "sub_category": subCategory,
        "title": titleValues.reverse[title],
        "price": price,
        "price_status": priceStatus,
        "description": description,
        "additional_details": additionalDetails,
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}

class Photo {
    Photo({
        this.id,
        this.adId,
        this.ad,
        this.photo,
        this.createdAt,
    });

    int id;
    int adId;
    Title ad;
    String photo;
    DateTime createdAt;

    factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        adId: json["ad_id"],
        ad: titleValues.map[json["ad"]],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "ad_id": adId,
        "ad": titleValues.reverse[ad],
        "photo": photo,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}

enum Title { BMW, EMPTY, TITLE }

final titleValues = EnumValues({
    "BMW": Title.BMW,
    "بورتو السخنه": Title.EMPTY,
    "مهمه صعبه": Title.TITLE
});

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
class Error {
    Error({
        this.key,
        this.message,
    });

    String key;
    String message;

    factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "key": key,
        "message": message,
    };
}