// To parse this JSON data, do
//
//     final getAuctionsModel = getAuctionsModelFromJson(jsonString);

import 'dart:convert';

GetAuctionsModel getAuctionsModelFromJson(String str) => GetAuctionsModel.fromJson(json.decode(str));

String getAuctionsModelToJson(GetAuctionsModel data) => json.encode(data.toJson());

class GetAuctionsModel {
    GetAuctionsModel({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    List<Datum> data;
    List<Error> error;

    factory GetAuctionsModel.fromJson(Map<String, dynamic> json) => GetAuctionsModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data:json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error: json["error"] == null ? null : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
    };
}

class Datum {
    Datum({
        this.id,
        this.userId,
        this.user,
        this.userPhoto,
        this.email,
        this.phoneNumber,
        this.memberDate,
        this.countryId,
        this.country,
        this.cityId,
        this.city,
        this.name,
        this.period,
        this.details,
        this.description,
        this.initialPrice,
        this.priceIncrease,
        this.currency,
        this.archive,
        this.prices,
        this.photos,
        this.auctionWinnerId,
        this.auctionWinner,
        this.auctionWinnerPhoto,
        this.heighPrice,
        this.createdAt,
    });

    int id;
    int userId;
    String user;
    String userPhoto;
    String email;
    String phoneNumber;
    DateTime memberDate;
    int countryId;
    Country country;
    int cityId;
    String city;
    String name;
    int period;
    String details;
    dynamic description;
    int initialPrice;
    int priceIncrease;
   String currency;
    int archive;
    List<Price> prices;
    List<Photo> photos;
    int auctionWinnerId;
    String auctionWinner;
    String auctionWinnerPhoto;
    int heighPrice;
    DateTime createdAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        userId: json["user_id"],
        user: json["user"],
        userPhoto: json["user_photo"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        memberDate: DateTime.parse(json["member_date"]),
        countryId: json["country_id"],
        country: countryValues.map[json["country"]],
        cityId: json["city_id"],
        city: json["city"],
        name: json["name"],
        period: json["period"],
        details: json["details"],
        description: json["description"],
        initialPrice: json["initial_price"],
        priceIncrease: json["price_increase"],
        currency: json["currency"],
        archive: json["archive"],
        prices: List<Price>.from(json["prices"].map((x) => Price.fromJson(x))),
        photos: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        auctionWinnerId: json["auction_winner_id"] == null ? null : json["auction_winner_id"],
        auctionWinner: json["auction_winner"] == null ? null : json["auction_winner"],
        auctionWinnerPhoto: json["auction_winner_photo"] == null ? null : json["auction_winner_photo"],
        heighPrice: json["heigh_price"] == null ? null : json["heigh_price"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "user": user,
        "user_photo": userPhoto,
        "email": email,
        "phone_number": phoneNumber,
        "member_date": "${memberDate.year.toString().padLeft(4, '0')}-${memberDate.month.toString().padLeft(2, '0')}-${memberDate.day.toString().padLeft(2, '0')}",
        "country_id": countryId,
        "country": countryValues.reverse[country],
        "city_id": cityId,
        "city": cityValues.reverse[city],
        "name": name,
        "period": period,
        "details": details,
        "description": description,
        "initial_price": initialPrice,
        "price_increase": priceIncrease,
        "currency": currencyValues.reverse[currency],
        "archive": archive,
        "prices": List<dynamic>.from(prices.map((x) => x.toJson())),
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
        "auction_winner_id": auctionWinnerId == null ? null : auctionWinnerId,
        "auction_winner": auctionWinner == null ? null : auctionWinner,
        "auction_winner_photo": auctionWinnerPhoto == null ? null : auctionWinnerPhoto,
        "heigh_price": heighPrice == null ? null : heighPrice,
        "created_at": createdAt.toIso8601String(),
    };
}

enum City { TALKA, SHOBRA_ELKHIMA, EL_DAMAM, MAKKAH }

final cityValues = EnumValues({
    "El Damam": City.EL_DAMAM,
    "Makkah": City.MAKKAH,
    "Shobra elkhima": City.SHOBRA_ELKHIMA,
    "talka": City.TALKA
});

enum Country { EGYPT, SAUDI_ARABIA }

final countryValues = EnumValues({
    "Egypt": Country.EGYPT,
    "Saudi Arabia": Country.SAUDI_ARABIA
});

enum Currency { EGP, SR }

final currencyValues = EnumValues({
    "EGP": Currency.EGP,
    "SR": Currency.SR
});

class Photo {
    Photo({
        this.id,
        this.auctionId,
        this.auction,
        this.photo,
        this.createdAt,
    });

    int id;
    int auctionId;
    String auction;
    String photo;
    DateTime createdAt;

    factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        auctionId: json["auction_id"],
        auction: json["auction"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "auction_id": auctionId,
        "auction": auction,
        "photo": photo,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}

class Price {
    Price({
        this.id,
        this.auctionId,
        this.auction,
        this.userId,
        this.user,
        this.userPhoto,
        this.price,
        this.createdAt,
    });

    int id;
    int auctionId;
    String auction;
    int userId;
    String user;
    String userPhoto;
    int price;
    DateTime createdAt;

    factory Price.fromJson(Map<String, dynamic> json) => Price(
        id: json["id"],
        auctionId: json["auction_id"],
        auction: json["auction"],
        userId: json["user_id"],
        user: json["user"],
        userPhoto: json["user_photo"],
        price: json["price"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "auction_id": auctionId,
        "auction": auction,
        "user_id": userId,
        "user": user,
        "user_photo": userPhoto,
        "price": price,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
class Error {
    Error({
        this.key,
        this.value,
    });

    String key;
    String value;

    factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
    );

    Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
    };
}