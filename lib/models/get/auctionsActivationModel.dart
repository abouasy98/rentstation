// To parse this JSON data, do
//
//     final auctionsActivationModel = auctionsActivationModelFromJson(jsonString);

import 'dart:convert';

AuctionsActivationModel auctionsActivationModelFromJson(String str) => AuctionsActivationModel.fromJson(json.decode(str));

String auctionsActivationModelToJson(AuctionsActivationModel data) => json.encode(data.toJson());

class AuctionsActivationModel {
    AuctionsActivationModel({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    Data data;
    List<Error> error;

    factory AuctionsActivationModel.fromJson(Map<String, dynamic> json) => AuctionsActivationModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data:json["data"] == null ? null : Data.fromJson(json["data"]),
        error:  json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": error,
    };
}

class Data {
    Data({
        this.activation,
    });

    int activation;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        activation: json["activation"],
    );

    Map<String, dynamic> toJson() => {
        "activation": activation,
    };
}
class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}
