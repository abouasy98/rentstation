// To parse this JSON data, do
//
//     final getAppLogo = getAppLogoFromJson(jsonString);

import 'dart:convert';

GetAppLogo getAppLogoFromJson(String str) => GetAppLogo.fromJson(json.decode(str));

String getAppLogoToJson(GetAppLogo data) => json.encode(data.toJson());

class GetAppLogo {
    GetAppLogo({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    Data data;
    dynamic error;

    factory GetAppLogo.fromJson(Map<String, dynamic> json) => GetAppLogo(
        mainCode: json["mainCode"] == null ? null : json["mainCode"],
        code: json["code"] == null ? null : json["code"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode == null ? null : mainCode,
        "code": code == null ? null : code,
        "data": data == null ? null : data.toJson(),
        "error": error,
    };
}

class Data {
    Data({
        this.logo,
        this.appName,
    });

    String logo;
    String appName;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        logo: json["logo"] == null ? null : json["logo"],
        appName: json["app_name"] == null ? null : json["app_name"],
    );

    Map<String, dynamic> toJson() => {
        "logo": logo == null ? null : logo,
        "app_name": appName == null ? null : appName,
    };
}
