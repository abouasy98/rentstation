// To parse this JSON data, do
//
//     final getUserById = getUserByIdFromJson(jsonString);

import 'dart:convert';

GetUserById getUserByIdFromJson(String str) =>
    GetUserById.fromJson(json.decode(str));

String getUserByIdToJson(GetUserById data) => json.encode(data.toJson());

class GetUserById {
  GetUserById({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  Data data;
  List<Error> error;

  factory GetUserById.fromJson(Map<String, dynamic> json) => GetUserById(
        mainCode: json["mainCode"] == null ? null : json["mainCode"],
        code: json["code"] == null ? null : json["code"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode == null ? null : mainCode,
        "code": code == null ? null : code,
        "data": data == null ? null : data.toJson(),
        "error": error,
      };
}

class Data {
  Data({
    this.id,
    this.countryId,
    this.country,
    this.name,
    this.email,
    this.phoneNumber,
    this.currency,
    this.active,
    this.image,
    this.apiToken,
    this.createdAt,
  });

  int id;
  int countryId;
  String country;
  String name;
  String email;
  String phoneNumber;
  String currency;
  int active;
  String image;
  String apiToken;
  DateTime createdAt;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"] == null ? null : json["id"],
        countryId: json["country_id"] == null ? null : json["country_id"],
        country: json["country"] == null ? null : json["country"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        currency: json["currency"] == null ? null : json["currency"],
        active: json["active"] == null ? null : json["active"],
        image: json["image"] == null ? null : json["image"],
        apiToken: json["api_token"] == null ? null : json["api_token"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "country_id": countryId == null ? null : countryId,
        "country": country == null ? null : country,
        "name": name == null ? null : name,
        "email": email == null ? null : email,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "currency": currency == null ? null : currency,
        "active": active == null ? null : active,
        "image": image == null ? null : image,
        "api_token": apiToken == null ? null : apiToken,
        "created_at": createdAt == null
            ? null
            : "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}
