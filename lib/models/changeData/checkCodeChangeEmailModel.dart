// To parse this JSON data, do
//
//     final checkCodeChangeEmail = checkCodeChangeEmailFromJson(jsonString);

import 'dart:convert';

CheckCodeChangeEmailModel checkCodeChangeEmailFromJson(String str) =>
    CheckCodeChangeEmailModel.fromJson(json.decode(str));

String checkCodeChangeEmailToJson(CheckCodeChangeEmailModel data) =>
    json.encode(data.toJson());

class CheckCodeChangeEmailModel {
  CheckCodeChangeEmailModel({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  Data data;
  List<Error> error;

  factory CheckCodeChangeEmailModel.fromJson(Map<String, dynamic> json) =>
      CheckCodeChangeEmailModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": List<dynamic>.from(error.map((x) => x.toJson())),
      };
}

class Data {
  Data({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}
