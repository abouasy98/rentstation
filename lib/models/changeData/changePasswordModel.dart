// To parse this JSON data, do
//
//     final changePasswordModel = changePasswordModelFromJson(jsonString);

import 'dart:convert';

ChangePasswordModel changePasswordModelFromJson(String str) =>
    ChangePasswordModel.fromJson(json.decode(str));

String changePasswordModelToJson(ChangePasswordModel data) =>
    json.encode(data.toJson());

class ChangePasswordModel {
  ChangePasswordModel({
    this.code,
    this.data,
    this.error,
  });

  int code;
  Data data;
  List<Error> error;

  factory ChangePasswordModel.fromJson(Map<String, dynamic> json) =>
      ChangePasswordModel(
        code: json["code"],
         data:json["data"] == null ? null : Data.fromJson(json["data"]),
        error:json["error"] == null ? null :  List<Error>.from(json["error"].map((x) => Error.fromJson(x)))
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "data": data.toJson(),
        "error": List<dynamic>.from(error.map((x) => x.toJson())),
      };
}

class Data {
  Data({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}
