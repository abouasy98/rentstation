// To parse this JSON data, do
//
//     final changeUserDataModel = changeUserDataModelFromJson(jsonString);

import 'dart:convert';

ChangeUserDataModel changeUserDataModelFromJson(String str) => ChangeUserDataModel.fromJson(json.decode(str));

String changeUserDataModelToJson(ChangeUserDataModel data) => json.encode(data.toJson());

class ChangeUserDataModel {
    ChangeUserDataModel({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    Data data;
     List<Error> error;

    factory ChangeUserDataModel.fromJson(Map<String, dynamic> json) => ChangeUserDataModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data:json["data"] == null ? null :  Data.fromJson(json["data"]),
        error:  json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": error,
    };
}

class Data {
    Data({
        this.id,
        this.countryId,
        this.country,
        this.name,
        this.email,
        this.phoneNumber,
        this.currency,
        this.active,
        this.image,
        this.apiToken,
        this.createdAt,
    });

    int id;
    int countryId;
    String country;
    String name;
    String email;
    String phoneNumber;
    String currency;
    int active;
    dynamic image;
    String apiToken;
    DateTime createdAt;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        countryId: json["country_id"],
        country: json["country"],
        name: json["name"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        currency: json["currency"],
        active: json["active"],
        image: json["image"],
        apiToken: json["api_token"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "country_id": countryId,
        "country": country,
        "name": name,
        "email": email,
        "phone_number": phoneNumber,
        "currency": currency,
        "active": active,
        "image": image,
        "api_token": apiToken,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}
class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}
