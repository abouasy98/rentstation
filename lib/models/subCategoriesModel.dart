// To parse this JSON data, do
//
//     final subCategoriesModel = subCategoriesModelFromJson(jsonString);

import 'dart:convert';

SubCategoriesModels subCategoriesModelFromJson(String str) => SubCategoriesModels.fromJson(json.decode(str));

String subCategoriesModelToJson(SubCategoriesModels data) => json.encode(data.toJson());

class SubCategoriesModels {
    SubCategoriesModels({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    List<Datum> data;
    dynamic error;

    factory SubCategoriesModels.fromJson(Map<String, dynamic> json) => SubCategoriesModels(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null
            ? null: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error: json["error"] == null
            ? null: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
    };
}

class Datum {
    Datum({
        this.id,
        this.name,
        this.photo,
        this.categoryId,
        this.category,
        this.details,
        this.createdAt,
    });

    int id;
    String name;
    String photo;
    int categoryId;
    String category;
    String details;
    DateTime createdAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        name: json["name"],
        photo: json["photo"],
        categoryId: json["category_id"],
        category: json["category"],
        details: json["details"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "photo": photo,
        "category_id": categoryId,
        "category": category,
        "details": details,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}
