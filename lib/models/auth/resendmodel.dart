// To parse this JSON data, do
//
//     final resend = resendFromJson(jsonString);

import 'dart:convert';

Resend resendFromJson(String str) => Resend.fromJson(json.decode(str));

String resendToJson(Resend data) => json.encode(data.toJson());

class Resend {
    Resend({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    Data data;
     List<Error> error;

    factory Resend.fromJson(Map<String, dynamic> json) => Resend(
        mainCode: json["mainCode"],
        code: json["code"],
     data: json["data"] == null ? null :Data.fromJson(json["data"]),
        error: json["error"] == null ? null :  List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": error,
    };
}

class Data {
    Data({
        this.data,
        this.value,
    });

    String data;
    String value;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        data: json["data"],
        value: json["value"],
    );

    Map<String, dynamic> toJson() => {
        "data": data,
        "value": value,
    };
}
class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}
