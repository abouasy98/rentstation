// To parse this JSON data, do
//
//     final registerMobileModel = registerMobileModelFromJson(jsonString);

import 'dart:convert';

RegisterMobileorEmailModel registerMobileModelFromJson(String str) =>
    RegisterMobileorEmailModel.fromJson(json.decode(str));

String registerMobileModelToJson(RegisterMobileorEmailModel data) =>
    json.encode(data.toJson());

class RegisterMobileorEmailModel {
  RegisterMobileorEmailModel({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  Datum data;
  List<Error> error;

  factory RegisterMobileorEmailModel.fromJson(Map<String, dynamic> json) =>
      RegisterMobileorEmailModel(
          mainCode: json["mainCode"] == null ? null : json["mainCode"],
          code: json["code"] == null ? null : json["code"],
          data: json["data"] == null
              ? null
              : Datum.fromJson(json["data"]),
          error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))));

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode == null ? null : mainCode,
        "code": code == null ? null : code,
        "data": data == null
            ? null
            : data.toJson(),
        "error": error == null
            ? null
            : List<dynamic>.from(error.map((x) => x.toJson())),
      };
}
class Datum {
  Datum({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        key: json['key'] == null ? null : json['key'],
        value: json['value'] == null ? null : json['value'],
      );

  Map<String, dynamic> toJson() =>
      {'key': key == null ? null : key, 'value': value == null ? null : value};
}


class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}
