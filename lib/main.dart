import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'app.dart';
import 'Repository/appLocalization.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await localization.init();

  WidgetsFlutterBinding.ensureInitialized();

  final GlobalKey<NavigatorState> navigator = GlobalKey<NavigatorState>();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp( new MyApp(
        navigator: navigator,
      
    ));
  });
}

// class Splash extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return SplashScreen(
//       seconds: 2,
//       routeName: '/',
//       navigateAfterSeconds: new Register(),
//       imageBackground: AssetImage('images/1.jpg'),
//     );
//   }
// }
