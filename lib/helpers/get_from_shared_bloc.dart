import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rentstation/App/app_event.dart';
import 'package:rentstation/App/app_state.dart';
import 'package:rentstation/helpers/shared_preference_manger.dart';

class GetFromShared extends Bloc<AppEvent, AppState> {
  String username = "";
  String email = "";
  String image = "";
  String createdAt = "";
  String token = "";
  String countryName = "";
  String countryCode = "";
  String phone = "";
  String bankName = "";
  int bankNumber = 0;
  double commission = 0.00;
  int commissionLimit = 0;
  int countryId;
  String type = "";
  String appPhone = "";
  String appEmail = "";
  int id;
  bool notifyState = false;
  int verified = 1;
  SharedPreferenceManager _manager = SharedPreferenceManager();
GetFromShared() : super(null);
  @override
  AppState get initialState => Loading();

  @override
  Stream<AppState> mapEventToState(AppEvent event) async* {
    yield Loading();
    if (event is GetShared) {
      image = await _manager.readString(CachingKey.USER_IMAGE);
      username = await _manager.readString(CachingKey.USER_NAME);
      email = await _manager.readString(CachingKey.EMAIL);
      token = await _manager.readString(CachingKey.AUTH_TOKEN);
      type = await _manager.readString(CachingKey.USER_TYPE);
      createdAt = await _manager.readString(CachingKey.CREATED_AT);
      id = await _manager.readInteger(CachingKey.USER_ID);
      countryName = await _manager.readString(CachingKey.COUNTRY_NAME);
      phone = await _manager.readString(CachingKey.MOBILE_NUMBER);
      countryId = await _manager.readInteger(CachingKey.COUNTRY_ID);
      countryCode = await _manager.readString(CachingKey.COUNTRY_CODE);
      commission = await _manager.readDouble(CachingKey.COMMISSION);
      commissionLimit = await _manager.readInteger(CachingKey.COMMISSION_LIMIT);
      bankNumber = await _manager.readInteger(CachingKey.BANK_NUMBER);
      bankName = await _manager.readString(CachingKey.BANK_NAME);
      notifyState = await _manager.readBoolean(CachingKey.NOTIFY_STATE);
      appPhone = await _manager.readString(CachingKey.APP_PHONE);
      appEmail = await _manager.readString(CachingKey.APP_EMAIL);
      verified = await _manager.readInteger(CachingKey.VERIFIED);

      print("________________________ The End OF Shared _________________");
      yield Start();
    }
  }
}

GetFromShared getFromShared = GetFromShared();
