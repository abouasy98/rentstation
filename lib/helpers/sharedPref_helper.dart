import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPref with ChangeNotifier {
  String token;
  int id;
  String photo;
  String phone;
  String name;
  String email;
  int countryId;
  String country;
  String phoneNumber;
  String currency;
  int active;
  bool auction;
  bool ads;
  bool noadsNoAuctions;
  getSharedHelper(SharedPreferences pref) async {
    token = pref.getString('api_token');
    id = pref.getInt("id");
    photo = pref.getString("image");
    phone = pref.getString("phone");
    name = pref.getString("name");
    email = pref.getString("email");
    countryId = pref.getInt("countryId");
    country = pref.getString("country");
    currency = pref.getString("currency");
    active = pref.getInt("active");
    auction = pref.getBool('Auctions');
    ads = pref.getBool('Ads');
    noadsNoAuctions = pref.getBool('NoAdsNoAuctions');

    print(token);
    print(id);
    print(photo);
    print(phone);
    print(name);
    print(email);
    print(countryId);
    print(ads);

    print("i get shared");
    notifyListeners();
    return pref;
  }
}
