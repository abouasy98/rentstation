import 'package:shared_preferences/shared_preferences.dart';

import 'enumeration.dart';




class SharedPreferenceManager {
  SharedPreferences sharedPreferences;

  Future<bool> removeData(CachingKey key) async {
    sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.remove(key.value);
  }

  Future<bool> logout() async {
    print("___clear shared___");
    sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.clear();
  }

  Future<bool> writeData(CachingKey key, value) async {
    sharedPreferences = await SharedPreferences.getInstance();
    print("saving this value $value into local preference with key ${key.value}");
    Future returnedValue;
    if (value is String) {
      returnedValue = sharedPreferences.setString(key.value, value);
    } else if (value is int) {
      returnedValue = sharedPreferences.setInt(key.value, value);
    } else if (value is bool) {
      returnedValue = sharedPreferences.setBool(key.value, value);
    } else if (value is double) {
      returnedValue = sharedPreferences.setDouble(key.value, value);
    } else {
      return Future.error(NotValidCacheTypeException());
    }
    return returnedValue;
  }

  Future<bool> readBoolean(CachingKey key) async {
    sharedPreferences = await SharedPreferences.getInstance();
    return Future.value(sharedPreferences.getBool(key.value) ?? false);
  }

  Future<double> readDouble(CachingKey key) async {
    sharedPreferences = await SharedPreferences.getInstance();
    return Future.value(sharedPreferences.getDouble(key.value) ?? 0.0);
  }

  Future<int> readInteger(CachingKey key) async {
    sharedPreferences = await SharedPreferences.getInstance();
    return Future.value(sharedPreferences.getInt(key.value) ?? 0);
  }

  Future<String> readString(CachingKey key) async {
    sharedPreferences = await SharedPreferences.getInstance();
    return Future.value(sharedPreferences.getString(key.value) ?? "");
  }
}

final  preferenceManager = SharedPreferenceManager();



class NotValidCacheTypeException implements Exception {
  String message() => "Not a valid caching type";
}



class CachingKey extends Enum<String> {
  const CachingKey(String val) : super(val);
  static const CachingKey USER_NAME = const CachingKey('USER_NAME');
  static const CachingKey SUB_HERAGE_ID = const CachingKey('SUB_HERAGE_ID');
  static const CachingKey HERAG_TEXT = const CachingKey('HERAG_TEXT');
  static const CachingKey USER_ID = const CachingKey('USER_ID');
  static const CachingKey DEVICE_TOKEN = const CachingKey('DEVICE_TOKEN');
  static const CachingKey AUTH_TOKEN = const CachingKey('AUTH_TOKEN');
  static const CachingKey CHANGE_COUNTRY = const CachingKey('CHANGE_COUNTRY');
  static const CachingKey EMAIL = const CachingKey('EMAIL');
  static const CachingKey INTO_FIRST = const CachingKey('INTO_FIRST');
  static const CachingKey COUNTRY_NAME = const CachingKey('COUNTRY_NAME');
  static const CachingKey COUNTRY_ID = const CachingKey('COUNTRY_ID');
  static const CachingKey USER_IMAGE = const CachingKey('USER_IMAGE');
  static const CachingKey ID_IMAGE = const CachingKey('ID_IMAGE');
  static const CachingKey IS_LOGGED_IN = const CachingKey('IS_LOGGED_IN');
  static const CachingKey USER_TYPE = const CachingKey('USER_TYPE');
  static const CachingKey REFERRAL_CODE = const CachingKey('REFERRAL_CODE');
  static const CachingKey MOBILE_NUMBER = const CachingKey('MOBILE_NUMBER');
  static const CachingKey COUNTRY_CODE = const CachingKey('COUNTRY_CODE');
  static const CachingKey APP_STATE = const CachingKey('APP_STATE');
  static const CachingKey HELLO = const CachingKey('HELLO');
  static const CachingKey FIRST = const CachingKey('FIRST');
  static const CachingKey NOTIFY_STATE = const CachingKey('NOTIFY_STATE');
  static const CachingKey LONGITUDE = const CachingKey('LONGITUDE');
  static const CachingKey LATITUDE = const CachingKey('LATITUDE');
  static const CachingKey CREATED_AT = const CachingKey('CREATED_AT');
  static const CachingKey BANK_NUMBER = const CachingKey('BANK_NUMBER');
  static const CachingKey BANK_NAME = const CachingKey('BANK_NAME');
  static const CachingKey COMMISSION_LIMIT = const CachingKey('COMMISSION_LIMIT');
  static const CachingKey COMMISSION = const CachingKey('COMMISSION');
  static const CachingKey COMMISSION_HERAG = const CachingKey('COMMISSION_HERAG');
  static const CachingKey PRODUCT_ID = const CachingKey('PRODUCT_ID');
  static const CachingKey PROVIDER_ID = const CachingKey('PROVIDER_ID');
  static const CachingKey CHAT_NAME = const CachingKey('CHAT_NAME');
  static const CachingKey STATE_OF_CHAT = const CachingKey('STATE_OF_CHAT');
  static const CachingKey VERSION = const CachingKey('VERSION');
  static const CachingKey APP_PHONE = const CachingKey('APP_PHONE');
  static const CachingKey APP_EMAIL = const CachingKey('APP_EMAIL');
  static const CachingKey END_AT_MIN = const CachingKey('END_AT_MIN');
  static const CachingKey END_AT_HOUR = const CachingKey('END_AT_HOUR');
  static const CachingKey VERIFIED = const CachingKey('VERIFIED');
}

