import 'package:rentstation/provider/aboutUsProvider.dart';
import 'package:rentstation/provider/auth/confirmResetCodeProvider.dart';
import 'package:rentstation/provider/changeData/changeEmailProvider.dart';
import 'package:rentstation/provider/changeData/changeEmailCodeProvider.dart';
import 'package:rentstation/provider/commisionprovider.dart';
import 'package:rentstation/provider/contuctUsProvider.dart';
import 'package:rentstation/provider/get/GetAppLogoProvider.dart';
import 'package:rentstation/provider/get/addAdToArchiveProvider.dart';
import 'package:rentstation/provider/get/addAdToFavProvider.dart';
import 'package:rentstation/provider/get/adsActiviationProvider.dart';
import 'package:rentstation/provider/get/auctionsActivationProvider.dart';
import 'package:rentstation/provider/get/categoriesProvider.dart';
import 'package:rentstation/provider/get/getAdByIdProvider.dart';
import 'package:rentstation/provider/get/getAdByIdWithoutAuth.dart';
import 'package:rentstation/provider/get/getAddsProvider.dart';
import 'package:rentstation/provider/get/getAuctionByIdProvider.dart';
import 'package:rentstation/provider/get/getAuctionProvider.dart';
import 'package:rentstation/provider/get/getAuctionsByIdWithoutProvider.dart';
import 'package:rentstation/provider/get/getAuctionsPhotoProvider.dart';
import 'package:rentstation/provider/get/getChatsProvider.dart';
import 'package:rentstation/provider/get/getCitiesByIdProvider.dart';
import 'package:rentstation/provider/get/getCommentProvider.dart';
import 'package:rentstation/provider/get/getMyAdds.dart';
import 'package:rentstation/provider/get/getMyFavAdsProvider.dart';
import 'package:rentstation/provider/get/getTheUndertakingProvider.dart';
import 'package:rentstation/provider/get/getUserByDataProvider.dart';
import 'package:rentstation/provider/get/getadPhotosProvider.dart';
import 'package:rentstation/provider/get/getcountries.dart';
import 'package:rentstation/provider/get/getmyArchieveAds.dart';
import 'package:rentstation/provider/get/getmyAuctionsArchieveProvider.dart';
import 'package:rentstation/provider/get/getmyAuctionsProvider.dart';
import 'package:rentstation/provider/get/getregistertype.dart';
import 'package:rentstation/provider/auth/loginProvider.dart';
import 'package:rentstation/provider/auth/logoutProvider.dart';
import 'package:rentstation/provider/auth/phoneVerificationProvider.dart';
import 'package:rentstation/provider/auth/registerMobileorEmailProvider.dart';
import 'package:rentstation/provider/auth/resendCode.dart';
import 'package:rentstation/provider/auth/resetPasswordProvider.dart';
import 'package:rentstation/provider/auth/signUpProvider.dart';
import 'package:rentstation/provider/changeData/changePasswordProvider.dart';
import 'package:rentstation/provider/changeData/changePhoneCodeProvider.dart';
import 'package:rentstation/provider/changeData/changePhoneProvider.dart';
import 'package:rentstation/provider/changeData/changeUserDataProvider.dart';
import 'package:rentstation/provider/choseLangProvider.dart';
import 'package:rentstation/provider/get/myWinedAuctionProvider.dart';
import 'package:rentstation/provider/get/notificationProvider.dart';
import 'package:rentstation/provider/get/subCategoriesProvider.dart';
import 'package:rentstation/provider/get/userAdsProvider.dart';
import 'package:rentstation/provider/post/SearchstatisticsProvider.dart';
import 'package:rentstation/provider/post/adReportProvider.dart';
import 'package:rentstation/provider/post/addAuctiontoArchieveProvider.dart';
import 'package:rentstation/provider/post/addCommenProvider.dart';
import 'package:rentstation/provider/post/addReplyProvider.dart';
import 'package:rentstation/provider/post/addedAuctionWinnerProvider.dart';
import 'package:rentstation/provider/post/auctionAddPriceProvider.dart';
import 'package:rentstation/provider/post/createAdProvider.dart';
import 'package:rentstation/provider/post/createAuction.dart';
import 'package:rentstation/provider/post/createMessageProvider.dart';
import 'package:rentstation/provider/post/editAdProvider.dart';
import 'package:rentstation/provider/post/editAuction.dart';
import 'package:rentstation/provider/post/getAdsByCityFilter.dart';
import 'package:rentstation/provider/post/getAuctionsByCityFilter.dart';
import 'package:rentstation/provider/post/locationProvider.dart';
import 'package:rentstation/provider/post/reportCommentProvider.dart';
import 'package:rentstation/provider/post/sendMessageProvider.dart';
import 'package:rentstation/provider/post/userBlockConversationProvider.dart';
import 'package:rentstation/provider/termsProvider.dart';
import 'package:rentstation/screens/Intro/SplashScreen.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:rentstation/screens/confirmCode.dart';
import 'helpers/map_helper.dart';
import 'helpers/sharedPref_helper.dart';
import 'provider/auth/forgetPasswordProvider.dart';

class MyApp extends StatelessWidget {
  final GlobalKey<NavigatorState> navigator;

  const MyApp({Key key, this.navigator}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => GetMyAdsProvider()),
        ChangeNotifierProvider(create: (_) => TermsProvider()),
        ChangeNotifierProvider(create: (_) => AboutUsProvider()),
        ChangeNotifierProvider(create: (_) => ChoseLangeProvider()),
        ChangeNotifierProvider(create: (_) => SendLocationProvider()),
        ChangeNotifierProvider(create: (_) => SignUpProvider()),
        ChangeNotifierProvider(create: (_) => ResetPasswordProvider()),
        ChangeNotifierProvider(create: (_) => ResendCodeProvider()),
        ChangeNotifierProvider(create: (_) => RegisterMobileorEmailProvider()),
        ChangeNotifierProvider(create: (_) => PhoneVerificationProvider()),
        ChangeNotifierProvider(create: (_) => LoginProvider()),
        ChangeNotifierProvider(create: (_) => ForgetPasswordProvider()),
        ChangeNotifierProvider(create: (_) => ConfirmResetCodeProvider()),
        ChangeNotifierProvider(create: (_) => ChangePhoneCodeProvider()),
        ChangeNotifierProvider(create: (_) => GetRegisterType()),
        ChangeNotifierProvider(create: (_) => GetCountriesprovider()),
        ChangeNotifierProvider(create: (_) => LogoutProvider()),
        ChangeNotifierProvider(create: (_) => ChangePasswordProvider()),
        ChangeNotifierProvider(create: (_) => ChangeMobileProvider()),
        ChangeNotifierProvider(create: (_) => ChangeUserDataProvider()),
        ChangeNotifierProvider(create: (_) => ChangeEmailProvider()),
        ChangeNotifierProvider(create: (_) => MapHelper()),
        ChangeNotifierProvider(create: (_) => ChangeEmailCodeProvider()),
        ChangeNotifierProvider(create: (_) => CategoriesProvider()),
        ChangeNotifierProvider(create: (_) => SubCategoriesProvider()),
        ChangeNotifierProvider(create: (_) => GetCitiesByCountryIdProvider()),
        ChangeNotifierProvider(create: (_) => CreatAdProvider()),
        ChangeNotifierProvider(create: (_) => GetAddsByIdProvider()),
        ChangeNotifierProvider(create: (_) => GetAddsProvider()),
        ChangeNotifierProvider(create: (_) => GetMyFavAdsProvider()),
        ChangeNotifierProvider(create: (_) => AddAdToFavProvider()),
        ChangeNotifierProvider(create: (_) => GetChatsProvider()),
      //  ChangeNotifierProvider(create: (_) => GetMessagesProvider()),
        ChangeNotifierProvider(create: (_) => ChatsendProvider()),
        ChangeNotifierProvider(create: (_) => EditAdProvider()),
        ChangeNotifierProvider(create: (_) => GetphotosAuctionsbyIdProvider()),
        ChangeNotifierProvider(create: (_) => NotoficationProvider()),
        ChangeNotifierProvider(create: (_) => CreatAuctionProvider()),
        ChangeNotifierProvider(create: (_) => EditAuctionProvider()),
        ChangeNotifierProvider(create: (_) => GetAuctionsProvider()),
        ChangeNotifierProvider(create: (_) => GetAuctionsByIdProvider()),
        ChangeNotifierProvider(create: (_) => AuctionAddPrice()),
        ChangeNotifierProvider(create: (_) => GetMyAductionProvider()),
        ChangeNotifierProvider(create: (_) => GetphotosAdsbyIdProvider()),
        ChangeNotifierProvider(create: (_) => AddAdToArchProvider()),
        ChangeNotifierProvider(create: (_) => GetmyArchieveAdsProvider()),
        ChangeNotifierProvider(create: (_) => AddAuctionToArchProvider()),
        ChangeNotifierProvider(create: (_) => GetmyArchieveAuctionsProvider()),
        ChangeNotifierProvider(create: (_) => AddAuctionWinnerProvider()),
        ChangeNotifierProvider(create: (_) => GetAuctionsActivationProvider()),
        ChangeNotifierProvider(create: (_) => MyWinedAuctionProvider()),
        ChangeNotifierProvider(create: (_) => GetUserData()),
        ChangeNotifierProvider(create: (_) => SharedPref()),
        ChangeNotifierProvider(create: (_) => GetAdsActivationProvider()),
        ChangeNotifierProvider(create: (_) => CreateMessageProvider()),
        ChangeNotifierProvider(create: (_) => GetUserDataProvider()),
        //ChangeNotifierProvider(create: (_) => GetAddswithoutAuthProvider()),
        ChangeNotifierProvider(create: (_) => GetAddsByIdWithoutAuthProvider()),
     //   ChangeNotifierProvider(create: (_) => GetAuctionsWihtoutAuthProvider()),
        ChangeNotifierProvider(
            create: (_) => GetAuctionsByIdWithOutAuthProvider()),
        ChangeNotifierProvider(create: (_) => UserBlockConversationProvider()),
        ChangeNotifierProvider(create: (_) => AdReportProvider()),
        ChangeNotifierProvider(create: (_) => GetAddsByCityFilterProvider()),
        ChangeNotifierProvider(
            create: (_) => GetAuctionsByCityFilterProvider()),
        ChangeNotifierProvider(create: (_) => AddCommentProvider()),
        ChangeNotifierProvider(create: (_) => GetCommentProvider()),
        ChangeNotifierProvider(create: (_) => ReportCommentProvider()),
        ChangeNotifierProvider(create: (_) => AddReplyProvider()),
        ChangeNotifierProvider(create: (_) => GetTheUndertakingProvider()),
        ChangeNotifierProvider(create: (_) => GetAppLogoProvider()),
        ChangeNotifierProvider(create: (_) => SearchstatisticsProvider()),
        ChangeNotifierProvider(create: (_) => CommisionProvider()),
        ChangeNotifierProvider(create: (_) =>ContuctUsProvider()),
      ],
      child: MaterialApp(
        navigatorKey: navigator,
        debugShowCheckedModeBanner: false,
        home: Splash(
          navigator: navigator,
        ),
        title: 'RENT STATION',
        routes: {
        ConfirmCode.routeName: (ctx) =>ConfirmCode(),
       
        },
      ),
    );
  }
}
