import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rentstation/Components/custom_alert.dart';
import 'package:rentstation/Repository/appLocalization.dart';
import 'package:rentstation/screens/Intro/SplashScreen.dart';
import 'package:rentstation/widgets/Connection/check_connection_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NetworkUtil {
  static NetworkUtil _instance = new NetworkUtil.internal();

  NetworkUtil.internal();

  factory NetworkUtil() => _instance;

  Dio dio = Dio();

  Future<Response> get(String url, BuildContext context, {Map headers}) async {
    var response;
    try {
      dio.options.baseUrl = "https://rentstation-sa.com/api/v1/";
      response = await dio.get(url, options: Options(headers: headers));
    } on DioError catch (e) {
      if (e.response != null) {
        response = e.response;
        print("response: " + e.response.toString());
      } else {}
    }
    if (response == null) {
      final result = await Connectivity().checkConnectivity();
      if (result == ConnectivityResult.none) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (_) => CheckConnectionScreen(
                      state: false,
                    )),
            (Route<dynamic> route) => false);
      } else {
        CustomAlert().toast(
            context: context, title: "هناك خطا يرجي اعادة المحاولة لاحقا");
      }
    }
    return response == null ? null : handleResponse(response, context);
  }

  Future<Response> post(String url, BuildContext context,
      {Map headers, FormData body, encoding}) async {
    var response;
    dio.options.baseUrl = "https://rentstation-sa.com/api/v1/";
    try {
      response = await dio.post(url,
          data: body,
          options: Options(headers: headers, requestEncoder: encoding));
    } on DioError catch (e) {
      if (e.response != null) {
        response = e.response;
        print("response: " + e.response.toString());
      } else {}
    }
    if (response == null) {
      final result = await Connectivity().checkConnectivity();
      if (result == ConnectivityResult.none) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (_) => CheckConnectionScreen(
                      state: false,
                    )),
            (Route<dynamic> route) => false);
      } else {
        CustomAlert().toast(
            context: context, title: "هناك خطا يرجي اعادة المحاولة لاحقا");
      }
    } else {}
    return response == null ? null : handleResponse(response, context);
  }

  Response handleResponse(Response response, BuildContext context) {
    final int statusCode = response.statusCode;
    print("response: " + response.toString());
    Future.delayed(Duration(milliseconds: 1), () async {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      if (response.data['error'] != null) {
        print('ssdsdsdsd yup');
        if (response.data['error'][0]['key'] == 'token') {
          Fluttertoast.showToast(
              msg: localization.text('login_from_other_device'));
          pushNewScreen(
            context,
            screen: Splash(),
            withNavBar: false, // OPTIONAL VALUE. True by default.
            pageTransitionAnimation: PageTransitionAnimation.cupertino,
          );
          await _prefs.clear().then((value) => print('done'));
          return;
        }
      }
    });
    if (statusCode >= 200 && statusCode < 300) {
      return response;
    } else {
      return response;
    }
  }
}
