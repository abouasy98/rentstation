// import 'dart:async';
// import 'dart:io';
// import 'package:adhara_socket_io/manager.dart';
// import 'package:adhara_socket_io/options.dart';
// import 'package:adhara_socket_io/socket.dart';
// import 'package:audioplayers/audioplayers.dart';
// import 'package:bloc/bloc.dart';
// import 'package:flutter/material.dart';
// import 'package:herag/App/app_event.dart';
// import 'package:herag/App/app_state.dart';
// import 'package:herag/helpers/date_helper.dart';
// import 'package:herag/helpers/sharedPref_helper.dart';
// import 'package:herag/models/msg_model.dart';
// import 'package:herag/models/ols_messages_model.dart';
// import 'package:herag/models/photo_model.dart';
// import 'package:provider/provider.dart';
// import 'package:rxdart/rxdart.dart';

// import 'apiProvider.dart';

// class ChatBloc extends Bloc<AppEvent, AppState> {
//   final _api = ApiProvider();
//   final _context = BehaviorSubject<BuildContext>();
//   final _msg = BehaviorSubject<String>();
//   final _chatID = BehaviorSubject<int>();
//   final _secondUserID = BehaviorSubject<int>();
//   final _photo = BehaviorSubject<File>();
//   final _photoLink = BehaviorSubject<String>();
//   final _lat = BehaviorSubject<String>();
//   final _long = BehaviorSubject<String>();
//   final _messagesController = PublishSubject<MsgModel>();
//   final _key = BehaviorSubject<GlobalKey<ScaffoldState>>();
//   SocketIO _socket;
//   SocketIOManager _manager = SocketIOManager();
//   String _ring;
//   AudioPlayer audioPlayer = AudioPlayer();

//   Function(String) get updateMsg => _msg.sink.add;
//   Function(BuildContext) get updateContext => _context.sink.add;

//   Function(String) get updatePhotoLink => _photoLink.sink.add;

//   Function(String) get updateLat => _lat.sink.add;

//   Function(String) get updateLong => _long.sink.add;

//   Function(int) get updateChatID => _chatID.sink.add;

//   Function(int) get updateSecondUserID => _secondUserID.sink.add;

//   Function(File) get updatePhoto => _photo.sink.add;

//   Function(MsgModel) get addMessage => _messagesController.sink.add;

//   Stream<MsgModel> get messages =>
//       _messagesController.stream.asBroadcastStream();

//   Function(GlobalKey<ScaffoldState>) get updateKey => _key.sink.add;

//   dispose() {
//     _key.close();
//     _msg.close();
//     _photoLink.close();
//     _lat.close();
//     _long.close();
//     _chatID.close();
//     _secondUserID.close();
//     _msg.close();
//     _photo.close();
//     _messagesController.close();
//     _messagesController.close();
//     _context.close();
//   }

//   clear() {
//     _photo.value = null;
//     _chatID.value = null;
//     _photoLink.value = null;
//     _lat.value = null;
//     _long.value = null;
//     _msg.value = null;
//     _manager.clearInstance(_socket);
//   }

//   clearMsg() {
//     _photo.value = null;
//     _photoLink.value = null;
//     _lat.value = null;
//     _long.value = null;
//     _msg.value = null;
//   }

//   @override
//   AppState get initialState => Start();

//   @override
//   Stream<AppState> mapEventToState(AppEvent event) async* {
//     if (event is UploadPhoto) {
//       if (_photo.value != null) {
//         PhotoModel _res =
//             await _api.uploadPhoto(photo: _photo.value).catchError((e) {
//           _key.value.currentState
//               .showSnackBar(SnackBar(content: Text('تحقق من الإتصال')));
//         });
//         if (_res.code == 200) {
//           updatePhotoLink(_res.data);
//           print("photo updated");
//         }
//       }
//     }
//     if (event is Init) {
//       PhotoModel _voice = await _api.getVoiceRing().catchError((e) {
//         _key.value.currentState
//             .showSnackBar(SnackBar(content: Text('تحقق من الإتصال')));
//       });
//       if (_voice.code == 200) {
//         print("rang");
//         _ring = _voice.data;
//       }
//       OldMessagesModel _old = await _api.getMessages(
//           chatID: _chatID.value,
//           token: Provider.of<SharedPref>(_context.value, listen: false).token);
//       if (_old.code == 200) {
//         for (int i = 0; i < _old.data.length; i++) {
//           if (_old.data[i].message != null ||
//               _old.data[i].file != null ||
//               _old.data[i].latitude != null ||
//               _old.data[i].longitude != null) {
//             addMessage(_old.data[i]);
//           }
//         }
//       }
//       print('>>> Start init Chat Id ${_chatID.value}');
//       _socket = await _manager.createInstance(
//         SocketOptions("https://heraj.tqnee.com.sa:2021",
//             query: {
//               "order_id": "${_chatID.value}",
//             },
//             enableLogging: true,
//             transports: [
//               Transports.WEB_SOCKET,
//               Transports.POLLING,
//             ]),
//       );
//       _socket.emit('user-connected', [
//         {
//           "room_id": _chatID.value.toString(),
//           "api_token":
//               Provider.of<SharedPref>(_context.value, listen: false).token
//         }
//       ]);
//       _socket.emit('user-connected', [
//         {
//           "room_id": _chatID.value.toString(),
//           "api_token":
//               Provider.of<SharedPref>(_context.value, listen: false).token
//         }
//       ]);

//       _socket.onConnect((connection) {
//         print('<<< Socket connected >>> Chat Id ${_chatID.value}');

//         _socket.on('chat message', (data) async {
//           if (data["room_id"] == _chatID.value) {
//             print('<<< Receive Msg >>>');
//             print('<<< Receive Msg >>>');
//             print('<<< Receive Msg >>>');
//             print('<<< Receive Msg >>>');
//             print('<<< Receive Msg >>>');
//             print(data.toString());
//             MsgModel _msg = new MsgModel(
//               message: data["msg"],
//               // token: data["data"]["api_token"],
//               // latitude: data["data"]["latitude"],
//               // longitude: data["data"]["longitude"],
//               // file: data["data"]["file"],
//               createdAt: DateTime.now().toString(),
//               // name: data["data"]["user_name"],
//               // userPhoto: data["data"]["user_photo"],
//               // voice: data["data"]["voice"],
//               // roomID: data["data"]["room_id"]
//             );
//             await audioPlayer.play(_ring);
//             addMessage(_msg);
//           }
//         });
//       });
//       _socket.connect();
//     }
//     if (event is SendMsg) {
//       print('Chat Id ${_chatID.value}');
//       MsgModel _newMsg = new MsgModel(
//           message: _msg.value,
//           userId: Provider.of<SharedPref>(_context.value, listen: false).id,
//           token: Provider.of<SharedPref>(_context.value, listen: false).token,
//           secondUserId: _secondUserID.value,
//           latitude: _lat.value,
//           createdAt: TextHelper().formatDateTime(date: DateTime.now()),
//           longitude: _long.value,
//           file: _photoLink.value,
//           name: Provider.of<SharedPref>(_context.value, listen: false).name,
//           userPhoto:
//               Provider.of<SharedPref>(_context.value, listen: false).photo,
//           roomID: _chatID.value.toString());
//       print(
//           "token ${Provider.of<SharedPref>(_context.value, listen: false).token}");
//       print("room id ${_chatID.value.toString()}");
//       _socket.emit("send-message", [
//         {
//           "api_token":
//               Provider.of<SharedPref>(_context.value, listen: false).token,
//           "room_id": _chatID.value.toString(),
//           "file": _photoLink.value,
//           "message": _msg.value,
//           "lang": "ar",
//           "file_type": "text",
//           "countryId": "178",
//           "duration": 0
//         }
//       ]);
//       _socket.emit('chat message', [
//         {
//           "msg": _msg.value,
//           "room_id": _chatID.value.toString(),
//           "img": Provider.of<SharedPref>(_context.value, listen: false).photo
//         }
//       ]);

//       clearMsg();
//       addMessage(_newMsg);
//     }
//   }
// }

// final chatBloc = ChatBloc();
