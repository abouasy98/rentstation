import 'dart:io';

import 'package:dio/dio.dart';
import 'package:rentstation/models/my_chats_model.dart';
import 'package:rentstation/models/ols_messages_model.dart';
import 'package:rentstation/models/photo_model.dart';

import 'networkUtlis.dart';
// import 'package:herag/Repository/networkUtlis.dart';

class ApiProvider {
  NetworkUtil _utils = new NetworkUtil();



  Future<dynamic> getMessages({int chatID,String token}) async {
    return await _utils
        .get("conversations_messages/$chatID",token:token ,
            withToken: true, model: OldMessagesModel())
        .catchError((e) {
      throw e;
    });
  }

  Future<dynamic> getChats(String token) async {
    return await _utils
        .get("my_chats/", withToken: true, model: MyChatsModel())
        .catchError((e) {
      throw e;
    });
  }

  Future<dynamic> uploadPhoto({File photo}) async {
    FormData formData =
        FormData.fromMap({"image": await MultipartFile.fromFile(photo.path)});

    return await _utils.post("upload_image",
        withToken: false, body: formData, model: PhotoModel());
  }

  Future<dynamic> getVoiceRing() async {
    return await _utils.get("chat_voice",
        withToken: false, model: PhotoModel());
  }

}
